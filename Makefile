###############################################################################
# Jira CLI Build
###############################################################################
.PHONY: build clean format run install package docker docker-push docker-clean test

VERSION := 1.0.0
ITERATION ?= 1
QUALIFIER ?= a
BUILD_NUMBER ?= 0

# Remember make files use tabs to identify commands therefore these top
# sections use spaces for indentation.

#------------------------------------------------------------------------------
# Operating System & Architecture Detection
#------------------------------------------------------------------------------
# This section is responsible for detecting operating system and architecture.
#
# Be aware we do not use uname to detect the architecture. We instead check
# the ls binary to see if it is 64-bit or 32-bit. This works much better in
# builds using things like docker images which may run on a 64bit OS, but can
# be 32bit images.

DETECTED_OS := unknown
DETECTED_ARCH := unknown
ifeq "Windows_NT" "$(OS)"
  DETECTED_OS := windows
  ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
  	DETECTED_ARCH := amd64
  endif
  ifeq ($(PROCESSOR_ARCHITECTURE),x86)
  	DETECTED_ARCH := i386
  endif
else
  UNAME_S := $(shell uname -s)
  ifeq ($(UNAME_S),Linux)
    DETECTED_OS := linux
  endif
  ifeq ($(UNAME_S),Darwin)
    DETECTED_OS := darwin
  endif
  FILE_BIT := $(shell file $(shell command -v file 2> /dev/null) | awk '{print $$3}')
  ifeq ($(FILE_BIT),64-bit)
    DETECTED_ARCH := amd64
  endif
  ifeq ($(FILE_BIT),32-bit)
  	DETECTED_ARCH := i386
  endif
endif

ifeq "unknown" "$(DETECTED_ARCH)"
  DETECTED_ARCH := amd64
endif
$(info Detected Operating System:  $(DETECTED_OS))
$(info Detected Architecture:      $(DETECTED_ARCH))
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Package Manager Detection for Linux
#------------------------------------------------------------------------------
DETECTED_PACKAGE_MANAGER := unknown
ifeq ($(DETECTED_OS),linux)
  RPM := $(shell command -v rpm 2> /dev/null)
  ifdef RPM
  	DETECTED_PACKAGE_MANAGER := rpm
  endif
  DEB := $(shell command -v dpkg 2> /dev/null)
  ifdef DEB
  	DETECTED_PACKAGE_MANAGER := deb
  endif
endif
$(info Detected Package Manager:   $(DETECTED_PACKAGE_MANAGER))
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Install Prefix
#------------------------------------------------------------------------------
ifeq ($(PREFIX),)
    PREFIX := ./install
endif
#------------------------------------------------------------------------------

build:
	env CGO_ENABLED=0 GOOS=$(DETECTED_OS) GOARCH=$(DETECTED_ARCH) go build -v -work -a -o lib/go_jira.a ./...

test: build
	go test -v ./...

clean:
	rm -rf bin install *.rpm *.deb

format:
	gofmt -w .

run: build
	GRPC_GO_LOG_VERBOSITY_LEVEL=99 \
	GRPC_GO_LOG_SEVERITY_LEVEL=info \
	./bin/jirac

install: build
	install -d $(DESTDIR)$(PREFIX)/usr/lib
	install -m 755 lib/go_jira.a $(DESTDIR)$(PREFIX)/usr/lib
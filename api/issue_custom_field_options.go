/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
)

// Linger please
var (
	_ _context.Context
)

type IssueCustomFieldOptionsApi interface {

	/*
	 * CreateCustomFieldOption Create custom field options (context)
	 * Creates options and, where the custom select field is of the type Select List (cascading), cascading options for a custom select field. The options are added to a context of the field.

The maximum number of options that can be created per request is 1000 and each field can have a maximum of 10000 options.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param fieldId The ID of the custom field.
	 * @param contextId The ID of the context.
	 * @return IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest
	 */
	CreateCustomFieldOption(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest

	/*
	 * CreateCustomFieldOptionExecute executes the request
	 * @return CustomFieldCreatedContextOptionsList
	 */
	CreateCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest) (CustomFieldCreatedContextOptionsList, *_nethttp.Response, error)

	/*
	 * DeleteCustomFieldOption Delete custom field options (context)
	 * Deletes a custom field option.

Options with cascading options cannot be deleted without deleting the cascading options first.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param fieldId The ID of the custom field.
	 * @param contextId The ID of the context from which an option should be deleted.
	 * @param optionId The ID of the option to delete.
	 * @return IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest
	 */
	DeleteCustomFieldOption(ctx _context.Context, fieldId string, contextId int64, optionId int64) IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest

	/*
	 * DeleteCustomFieldOptionExecute executes the request
	 */
	DeleteCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest) (*_nethttp.Response, error)

	/*
	 * GetCustomFieldOption Get custom field option
	 * Returns a custom field option. For example, an option in a select list.

Note that this operation **only works for issue field select list options created in Jira or using operations from the [Issue custom field options](#api-group-Issue-custom-field-options) resource**, it cannot be used with issue field select list options created by Connect apps.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** The custom field option is returned as follows:

 *  if the user has the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
 *  if the user has the *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for at least one project the custom field is used in, and the field is visible in at least one layout the user has permission to view.
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param id The ID of the custom field option.
	 * @return IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest
	 */
	GetCustomFieldOption(ctx _context.Context, id string) IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest

	/*
	 * GetCustomFieldOptionExecute executes the request
	 * @return CustomFieldOption
	 */
	GetCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest) (CustomFieldOption, *_nethttp.Response, error)

	/*
	 * GetOptionsForContext Get custom field options (context)
	 * Returns a [paginated](#pagination) list of all custom field option for a context. Options are returned first then cascading options, in the order they display in Jira.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param fieldId The ID of the custom field.
	 * @param contextId The ID of the context.
	 * @return IssueCustomFieldOptionsApiApiGetOptionsForContextRequest
	 */
	GetOptionsForContext(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiGetOptionsForContextRequest

	/*
	 * GetOptionsForContextExecute executes the request
	 * @return PageBeanCustomFieldContextOption
	 */
	GetOptionsForContextExecute(r IssueCustomFieldOptionsApiApiGetOptionsForContextRequest) (PageBeanCustomFieldContextOption, *_nethttp.Response, error)

	/*
	 * ReorderCustomFieldOptions Reorder custom field options (context)
	 * Changes the order of custom field options or cascading options in a context.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param fieldId The ID of the custom field.
	 * @param contextId The ID of the context.
	 * @return IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest
	 */
	ReorderCustomFieldOptions(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest

	/*
	 * ReorderCustomFieldOptionsExecute executes the request
	 * @return interface{}
	 */
	ReorderCustomFieldOptionsExecute(r IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest) (interface{}, *_nethttp.Response, error)

	/*
	 * UpdateCustomFieldOption Update custom field options (context)
	 * Updates the options of a custom field.

If any of the options are not found, no options are updated. Options where the values in the request match the current values aren't updated and aren't reported in the response.

Note that this operation **only works for issue field select list options created in Jira or using operations from the [Issue custom field options](#api-group-Issue-custom-field-options) resource**, it cannot be used with issue field select list options created by Connect apps.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param fieldId The ID of the custom field.
	 * @param contextId The ID of the context.
	 * @return IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest
	 */
	UpdateCustomFieldOption(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest

	/*
	 * UpdateCustomFieldOptionExecute executes the request
	 * @return CustomFieldUpdatedContextOptionsList
	 */
	UpdateCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest) (CustomFieldUpdatedContextOptionsList, *_nethttp.Response, error)
}

// IssueCustomFieldOptionsApiService IssueCustomFieldOptionsApi service
type IssueCustomFieldOptionsApiService service

type IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest struct {
	ctx _context.Context
	ApiService IssueCustomFieldOptionsApi
	bulkCustomFieldOptionCreateRequest *BulkCustomFieldOptionCreateRequest
	fieldId string
	contextId int64
}

func (r IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest) BulkCustomFieldOptionCreateRequest(bulkCustomFieldOptionCreateRequest BulkCustomFieldOptionCreateRequest) IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest {
	r.bulkCustomFieldOptionCreateRequest = &bulkCustomFieldOptionCreateRequest
	return r
}

func (r IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest) Execute() (CustomFieldCreatedContextOptionsList, *_nethttp.Response, error) {
	return r.ApiService.CreateCustomFieldOptionExecute(r)
}

/*
 * CreateCustomFieldOption Create custom field options (context)
 * Creates options and, where the custom select field is of the type Select List (cascading), cascading options for a custom select field. The options are added to a context of the field.

The maximum number of options that can be created per request is 1000 and each field can have a maximum of 10000 options.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param fieldId The ID of the custom field.
 * @param contextId The ID of the context.
 * @return IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest
 */
func (a *IssueCustomFieldOptionsApiService) CreateCustomFieldOption(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest {
	return IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest{
		ApiService: a,
		ctx: ctx,
		fieldId: fieldId,
		contextId: contextId,
	}
}

/*
 * Execute executes the request
 * @return CustomFieldCreatedContextOptionsList
 */
func (a *IssueCustomFieldOptionsApiService) CreateCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiCreateCustomFieldOptionRequest) (CustomFieldCreatedContextOptionsList, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  CustomFieldCreatedContextOptionsList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueCustomFieldOptionsApiService.CreateCustomFieldOption")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/field/{fieldId}/context/{contextId}/option"
	localVarPath = strings.Replace(localVarPath, "{"+"fieldId"+"}", _neturl.PathEscape(parameterToString(r.fieldId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"contextId"+"}", _neturl.PathEscape(parameterToString(r.contextId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.bulkCustomFieldOptionCreateRequest == nil {
		return localVarReturnValue, nil, reportError("bulkCustomFieldOptionCreateRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.bulkCustomFieldOptionCreateRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest struct {
	ctx _context.Context
	ApiService IssueCustomFieldOptionsApi
	fieldId string
	contextId int64
	optionId int64
}


func (r IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest) Execute() (*_nethttp.Response, error) {
	return r.ApiService.DeleteCustomFieldOptionExecute(r)
}

/*
 * DeleteCustomFieldOption Delete custom field options (context)
 * Deletes a custom field option.

Options with cascading options cannot be deleted without deleting the cascading options first.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param fieldId The ID of the custom field.
 * @param contextId The ID of the context from which an option should be deleted.
 * @param optionId The ID of the option to delete.
 * @return IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest
 */
func (a *IssueCustomFieldOptionsApiService) DeleteCustomFieldOption(ctx _context.Context, fieldId string, contextId int64, optionId int64) IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest {
	return IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest{
		ApiService: a,
		ctx: ctx,
		fieldId: fieldId,
		contextId: contextId,
		optionId: optionId,
	}
}

/*
 * Execute executes the request
 */
func (a *IssueCustomFieldOptionsApiService) DeleteCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiDeleteCustomFieldOptionRequest) (*_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodDelete
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueCustomFieldOptionsApiService.DeleteCustomFieldOption")
	if err != nil {
		return nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/field/{fieldId}/context/{contextId}/option/{optionId}"
	localVarPath = strings.Replace(localVarPath, "{"+"fieldId"+"}", _neturl.PathEscape(parameterToString(r.fieldId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"contextId"+"}", _neturl.PathEscape(parameterToString(r.contextId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"optionId"+"}", _neturl.PathEscape(parameterToString(r.optionId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest struct {
	ctx _context.Context
	ApiService IssueCustomFieldOptionsApi
	id string
}


func (r IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest) Execute() (CustomFieldOption, *_nethttp.Response, error) {
	return r.ApiService.GetCustomFieldOptionExecute(r)
}

/*
 * GetCustomFieldOption Get custom field option
 * Returns a custom field option. For example, an option in a select list.

Note that this operation **only works for issue field select list options created in Jira or using operations from the [Issue custom field options](#api-group-Issue-custom-field-options) resource**, it cannot be used with issue field select list options created by Connect apps.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** The custom field option is returned as follows:

 *  if the user has the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
 *  if the user has the *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for at least one project the custom field is used in, and the field is visible in at least one layout the user has permission to view.
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param id The ID of the custom field option.
 * @return IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest
 */
func (a *IssueCustomFieldOptionsApiService) GetCustomFieldOption(ctx _context.Context, id string) IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest {
	return IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest{
		ApiService: a,
		ctx: ctx,
		id: id,
	}
}

/*
 * Execute executes the request
 * @return CustomFieldOption
 */
func (a *IssueCustomFieldOptionsApiService) GetCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiGetCustomFieldOptionRequest) (CustomFieldOption, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  CustomFieldOption
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueCustomFieldOptionsApiService.GetCustomFieldOption")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/customFieldOption/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", _neturl.PathEscape(parameterToString(r.id, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type IssueCustomFieldOptionsApiApiGetOptionsForContextRequest struct {
	ctx _context.Context
	ApiService IssueCustomFieldOptionsApi
	fieldId string
	contextId int64
	optionId *int64
	onlyOptions *bool
	startAt *int64
	maxResults *int32
}

func (r IssueCustomFieldOptionsApiApiGetOptionsForContextRequest) OptionId(optionId int64) IssueCustomFieldOptionsApiApiGetOptionsForContextRequest {
	r.optionId = &optionId
	return r
}
func (r IssueCustomFieldOptionsApiApiGetOptionsForContextRequest) OnlyOptions(onlyOptions bool) IssueCustomFieldOptionsApiApiGetOptionsForContextRequest {
	r.onlyOptions = &onlyOptions
	return r
}
func (r IssueCustomFieldOptionsApiApiGetOptionsForContextRequest) StartAt(startAt int64) IssueCustomFieldOptionsApiApiGetOptionsForContextRequest {
	r.startAt = &startAt
	return r
}
func (r IssueCustomFieldOptionsApiApiGetOptionsForContextRequest) MaxResults(maxResults int32) IssueCustomFieldOptionsApiApiGetOptionsForContextRequest {
	r.maxResults = &maxResults
	return r
}

func (r IssueCustomFieldOptionsApiApiGetOptionsForContextRequest) Execute() (PageBeanCustomFieldContextOption, *_nethttp.Response, error) {
	return r.ApiService.GetOptionsForContextExecute(r)
}

/*
 * GetOptionsForContext Get custom field options (context)
 * Returns a [paginated](#pagination) list of all custom field option for a context. Options are returned first then cascading options, in the order they display in Jira.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param fieldId The ID of the custom field.
 * @param contextId The ID of the context.
 * @return IssueCustomFieldOptionsApiApiGetOptionsForContextRequest
 */
func (a *IssueCustomFieldOptionsApiService) GetOptionsForContext(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiGetOptionsForContextRequest {
	return IssueCustomFieldOptionsApiApiGetOptionsForContextRequest{
		ApiService: a,
		ctx: ctx,
		fieldId: fieldId,
		contextId: contextId,
	}
}

/*
 * Execute executes the request
 * @return PageBeanCustomFieldContextOption
 */
func (a *IssueCustomFieldOptionsApiService) GetOptionsForContextExecute(r IssueCustomFieldOptionsApiApiGetOptionsForContextRequest) (PageBeanCustomFieldContextOption, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  PageBeanCustomFieldContextOption
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueCustomFieldOptionsApiService.GetOptionsForContext")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/field/{fieldId}/context/{contextId}/option"
	localVarPath = strings.Replace(localVarPath, "{"+"fieldId"+"}", _neturl.PathEscape(parameterToString(r.fieldId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"contextId"+"}", _neturl.PathEscape(parameterToString(r.contextId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.optionId != nil {
		localVarQueryParams.Add("optionId", parameterToString(*r.optionId, ""))
	}
	if r.onlyOptions != nil {
		localVarQueryParams.Add("onlyOptions", parameterToString(*r.onlyOptions, ""))
	}
	if r.startAt != nil {
		localVarQueryParams.Add("startAt", parameterToString(*r.startAt, ""))
	}
	if r.maxResults != nil {
		localVarQueryParams.Add("maxResults", parameterToString(*r.maxResults, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest struct {
	ctx _context.Context
	ApiService IssueCustomFieldOptionsApi
	orderOfCustomFieldOptions *OrderOfCustomFieldOptions
	fieldId string
	contextId int64
}

func (r IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest) OrderOfCustomFieldOptions(orderOfCustomFieldOptions OrderOfCustomFieldOptions) IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest {
	r.orderOfCustomFieldOptions = &orderOfCustomFieldOptions
	return r
}

func (r IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest) Execute() (interface{}, *_nethttp.Response, error) {
	return r.ApiService.ReorderCustomFieldOptionsExecute(r)
}

/*
 * ReorderCustomFieldOptions Reorder custom field options (context)
 * Changes the order of custom field options or cascading options in a context.

This operation works for custom field options created in Jira or the operations from this resource. **To work with issue field select list options created for Connect apps use the [Issue custom field options (apps)](#api-group-issue-custom-field-options--apps-) operations.**

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param fieldId The ID of the custom field.
 * @param contextId The ID of the context.
 * @return IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest
 */
func (a *IssueCustomFieldOptionsApiService) ReorderCustomFieldOptions(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest {
	return IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest{
		ApiService: a,
		ctx: ctx,
		fieldId: fieldId,
		contextId: contextId,
	}
}

/*
 * Execute executes the request
 * @return interface{}
 */
func (a *IssueCustomFieldOptionsApiService) ReorderCustomFieldOptionsExecute(r IssueCustomFieldOptionsApiApiReorderCustomFieldOptionsRequest) (interface{}, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  interface{}
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueCustomFieldOptionsApiService.ReorderCustomFieldOptions")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/field/{fieldId}/context/{contextId}/option/move"
	localVarPath = strings.Replace(localVarPath, "{"+"fieldId"+"}", _neturl.PathEscape(parameterToString(r.fieldId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"contextId"+"}", _neturl.PathEscape(parameterToString(r.contextId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.orderOfCustomFieldOptions == nil {
		return localVarReturnValue, nil, reportError("orderOfCustomFieldOptions is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.orderOfCustomFieldOptions
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest struct {
	ctx _context.Context
	ApiService IssueCustomFieldOptionsApi
	bulkCustomFieldOptionUpdateRequest *BulkCustomFieldOptionUpdateRequest
	fieldId string
	contextId int64
}

func (r IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest) BulkCustomFieldOptionUpdateRequest(bulkCustomFieldOptionUpdateRequest BulkCustomFieldOptionUpdateRequest) IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest {
	r.bulkCustomFieldOptionUpdateRequest = &bulkCustomFieldOptionUpdateRequest
	return r
}

func (r IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest) Execute() (CustomFieldUpdatedContextOptionsList, *_nethttp.Response, error) {
	return r.ApiService.UpdateCustomFieldOptionExecute(r)
}

/*
 * UpdateCustomFieldOption Update custom field options (context)
 * Updates the options of a custom field.

If any of the options are not found, no options are updated. Options where the values in the request match the current values aren't updated and aren't reported in the response.

Note that this operation **only works for issue field select list options created in Jira or using operations from the [Issue custom field options](#api-group-Issue-custom-field-options) resource**, it cannot be used with issue field select list options created by Connect apps.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param fieldId The ID of the custom field.
 * @param contextId The ID of the context.
 * @return IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest
 */
func (a *IssueCustomFieldOptionsApiService) UpdateCustomFieldOption(ctx _context.Context, fieldId string, contextId int64) IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest {
	return IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest{
		ApiService: a,
		ctx: ctx,
		fieldId: fieldId,
		contextId: contextId,
	}
}

/*
 * Execute executes the request
 * @return CustomFieldUpdatedContextOptionsList
 */
func (a *IssueCustomFieldOptionsApiService) UpdateCustomFieldOptionExecute(r IssueCustomFieldOptionsApiApiUpdateCustomFieldOptionRequest) (CustomFieldUpdatedContextOptionsList, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  CustomFieldUpdatedContextOptionsList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueCustomFieldOptionsApiService.UpdateCustomFieldOption")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/field/{fieldId}/context/{contextId}/option"
	localVarPath = strings.Replace(localVarPath, "{"+"fieldId"+"}", _neturl.PathEscape(parameterToString(r.fieldId, "")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"contextId"+"}", _neturl.PathEscape(parameterToString(r.contextId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.bulkCustomFieldOptionUpdateRequest == nil {
		return localVarReturnValue, nil, reportError("bulkCustomFieldOptionUpdateRequest is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.bulkCustomFieldOptionUpdateRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

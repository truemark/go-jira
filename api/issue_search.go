/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"reflect"
)

// Linger please
var (
	_ _context.Context
)

type IssueSearchApi interface {

	/*
	 * GetIssuePickerResource Get issue picker suggestions
	 * Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** None.
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @return IssueSearchApiApiGetIssuePickerResourceRequest
	 */
	GetIssuePickerResource(ctx _context.Context) IssueSearchApiApiGetIssuePickerResourceRequest

	/*
	 * GetIssuePickerResourceExecute executes the request
	 * @return IssuePickerSuggestions
	 */
	GetIssuePickerResourceExecute(r IssueSearchApiApiGetIssuePickerResourceRequest) (IssuePickerSuggestions, *_nethttp.Response, error)

	/*
	 * MatchIssues Check issues against JQL
	 * Checks whether one or more issues would be returned by one or more JQL queries.

**[Permissions](#permissions) required:** None, however, issues are only matched against JQL queries where the user has:

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If [issue-level security](https://confluence.atlassian.com/x/J4lKLg) is configured, issue-level security permission to view the issue.
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @return IssueSearchApiApiMatchIssuesRequest
	 */
	MatchIssues(ctx _context.Context) IssueSearchApiApiMatchIssuesRequest

	/*
	 * MatchIssuesExecute executes the request
	 * @return IssueMatches
	 */
	MatchIssuesExecute(r IssueSearchApiApiMatchIssuesRequest) (IssueMatches, *_nethttp.Response, error)

	/*
	 * SearchForIssuesUsingJql Search for issues using JQL (GET)
	 * Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-rest-api-3-search-post) version of this resource.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** Issues are included in the response where the user has:

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project containing the issue.
 *  If [issue-level security](https://confluence.atlassian.com/x/J4lKLg) is configured, issue-level security permission to view the issue.
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @return IssueSearchApiApiSearchForIssuesUsingJqlRequest
	 */
	SearchForIssuesUsingJql(ctx _context.Context) IssueSearchApiApiSearchForIssuesUsingJqlRequest

	/*
	 * SearchForIssuesUsingJqlExecute executes the request
	 * @return SearchResults
	 */
	SearchForIssuesUsingJqlExecute(r IssueSearchApiApiSearchForIssuesUsingJqlRequest) (SearchResults, *_nethttp.Response, error)

	/*
	 * SearchForIssuesUsingJqlPost Search for issues using JQL (POST)
	 * Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-rest-api-3-search-get) version of this resource that can be used for smaller JQL query expressions.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** Issues are included in the response where the user has:

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project containing the issue.
 *  If [issue-level security](https://confluence.atlassian.com/x/J4lKLg) is configured, issue-level security permission to view the issue.
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @return IssueSearchApiApiSearchForIssuesUsingJqlPostRequest
	 */
	SearchForIssuesUsingJqlPost(ctx _context.Context) IssueSearchApiApiSearchForIssuesUsingJqlPostRequest

	/*
	 * SearchForIssuesUsingJqlPostExecute executes the request
	 * @return SearchResults
	 */
	SearchForIssuesUsingJqlPostExecute(r IssueSearchApiApiSearchForIssuesUsingJqlPostRequest) (SearchResults, *_nethttp.Response, error)
}

// IssueSearchApiService IssueSearchApi service
type IssueSearchApiService service

type IssueSearchApiApiGetIssuePickerResourceRequest struct {
	ctx _context.Context
	ApiService IssueSearchApi
	query *string
	currentJQL *string
	currentIssueKey *string
	currentProjectId *string
	showSubTasks *bool
	showSubTaskParent *bool
}

func (r IssueSearchApiApiGetIssuePickerResourceRequest) Query(query string) IssueSearchApiApiGetIssuePickerResourceRequest {
	r.query = &query
	return r
}
func (r IssueSearchApiApiGetIssuePickerResourceRequest) CurrentJQL(currentJQL string) IssueSearchApiApiGetIssuePickerResourceRequest {
	r.currentJQL = &currentJQL
	return r
}
func (r IssueSearchApiApiGetIssuePickerResourceRequest) CurrentIssueKey(currentIssueKey string) IssueSearchApiApiGetIssuePickerResourceRequest {
	r.currentIssueKey = &currentIssueKey
	return r
}
func (r IssueSearchApiApiGetIssuePickerResourceRequest) CurrentProjectId(currentProjectId string) IssueSearchApiApiGetIssuePickerResourceRequest {
	r.currentProjectId = &currentProjectId
	return r
}
func (r IssueSearchApiApiGetIssuePickerResourceRequest) ShowSubTasks(showSubTasks bool) IssueSearchApiApiGetIssuePickerResourceRequest {
	r.showSubTasks = &showSubTasks
	return r
}
func (r IssueSearchApiApiGetIssuePickerResourceRequest) ShowSubTaskParent(showSubTaskParent bool) IssueSearchApiApiGetIssuePickerResourceRequest {
	r.showSubTaskParent = &showSubTaskParent
	return r
}

func (r IssueSearchApiApiGetIssuePickerResourceRequest) Execute() (IssuePickerSuggestions, *_nethttp.Response, error) {
	return r.ApiService.GetIssuePickerResourceExecute(r)
}

/*
 * GetIssuePickerResource Get issue picker suggestions
 * Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** None.
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @return IssueSearchApiApiGetIssuePickerResourceRequest
 */
func (a *IssueSearchApiService) GetIssuePickerResource(ctx _context.Context) IssueSearchApiApiGetIssuePickerResourceRequest {
	return IssueSearchApiApiGetIssuePickerResourceRequest{
		ApiService: a,
		ctx: ctx,
	}
}

/*
 * Execute executes the request
 * @return IssuePickerSuggestions
 */
func (a *IssueSearchApiService) GetIssuePickerResourceExecute(r IssueSearchApiApiGetIssuePickerResourceRequest) (IssuePickerSuggestions, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  IssuePickerSuggestions
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueSearchApiService.GetIssuePickerResource")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/issue/picker"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.query != nil {
		localVarQueryParams.Add("query", parameterToString(*r.query, ""))
	}
	if r.currentJQL != nil {
		localVarQueryParams.Add("currentJQL", parameterToString(*r.currentJQL, ""))
	}
	if r.currentIssueKey != nil {
		localVarQueryParams.Add("currentIssueKey", parameterToString(*r.currentIssueKey, ""))
	}
	if r.currentProjectId != nil {
		localVarQueryParams.Add("currentProjectId", parameterToString(*r.currentProjectId, ""))
	}
	if r.showSubTasks != nil {
		localVarQueryParams.Add("showSubTasks", parameterToString(*r.showSubTasks, ""))
	}
	if r.showSubTaskParent != nil {
		localVarQueryParams.Add("showSubTaskParent", parameterToString(*r.showSubTaskParent, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type IssueSearchApiApiMatchIssuesRequest struct {
	ctx _context.Context
	ApiService IssueSearchApi
	issuesAndJQLQueries *IssuesAndJQLQueries
}

func (r IssueSearchApiApiMatchIssuesRequest) IssuesAndJQLQueries(issuesAndJQLQueries IssuesAndJQLQueries) IssueSearchApiApiMatchIssuesRequest {
	r.issuesAndJQLQueries = &issuesAndJQLQueries
	return r
}

func (r IssueSearchApiApiMatchIssuesRequest) Execute() (IssueMatches, *_nethttp.Response, error) {
	return r.ApiService.MatchIssuesExecute(r)
}

/*
 * MatchIssues Check issues against JQL
 * Checks whether one or more issues would be returned by one or more JQL queries.

**[Permissions](#permissions) required:** None, however, issues are only matched against JQL queries where the user has:

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If [issue-level security](https://confluence.atlassian.com/x/J4lKLg) is configured, issue-level security permission to view the issue.
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @return IssueSearchApiApiMatchIssuesRequest
 */
func (a *IssueSearchApiService) MatchIssues(ctx _context.Context) IssueSearchApiApiMatchIssuesRequest {
	return IssueSearchApiApiMatchIssuesRequest{
		ApiService: a,
		ctx: ctx,
	}
}

/*
 * Execute executes the request
 * @return IssueMatches
 */
func (a *IssueSearchApiService) MatchIssuesExecute(r IssueSearchApiApiMatchIssuesRequest) (IssueMatches, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  IssueMatches
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueSearchApiService.MatchIssues")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/jql/match"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.issuesAndJQLQueries == nil {
		return localVarReturnValue, nil, reportError("issuesAndJQLQueries is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.issuesAndJQLQueries
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type IssueSearchApiApiSearchForIssuesUsingJqlRequest struct {
	ctx _context.Context
	ApiService IssueSearchApi
	jql *string
	startAt *int32
	maxResults *int32
	validateQuery *string
	fields *[]string
	expand *string
	properties *[]string
	fieldsByKeys *bool
}

func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) Jql(jql string) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.jql = &jql
	return r
}
func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) StartAt(startAt int32) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.startAt = &startAt
	return r
}
func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) MaxResults(maxResults int32) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.maxResults = &maxResults
	return r
}
func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) ValidateQuery(validateQuery string) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.validateQuery = &validateQuery
	return r
}
func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) Fields(fields []string) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.fields = &fields
	return r
}
func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) Expand(expand string) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.expand = &expand
	return r
}
func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) Properties(properties []string) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.properties = &properties
	return r
}
func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) FieldsByKeys(fieldsByKeys bool) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	r.fieldsByKeys = &fieldsByKeys
	return r
}

func (r IssueSearchApiApiSearchForIssuesUsingJqlRequest) Execute() (SearchResults, *_nethttp.Response, error) {
	return r.ApiService.SearchForIssuesUsingJqlExecute(r)
}

/*
 * SearchForIssuesUsingJql Search for issues using JQL (GET)
 * Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-rest-api-3-search-post) version of this resource.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** Issues are included in the response where the user has:

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project containing the issue.
 *  If [issue-level security](https://confluence.atlassian.com/x/J4lKLg) is configured, issue-level security permission to view the issue.
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @return IssueSearchApiApiSearchForIssuesUsingJqlRequest
 */
func (a *IssueSearchApiService) SearchForIssuesUsingJql(ctx _context.Context) IssueSearchApiApiSearchForIssuesUsingJqlRequest {
	return IssueSearchApiApiSearchForIssuesUsingJqlRequest{
		ApiService: a,
		ctx: ctx,
	}
}

/*
 * Execute executes the request
 * @return SearchResults
 */
func (a *IssueSearchApiService) SearchForIssuesUsingJqlExecute(r IssueSearchApiApiSearchForIssuesUsingJqlRequest) (SearchResults, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  SearchResults
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueSearchApiService.SearchForIssuesUsingJql")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/search"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.jql != nil {
		localVarQueryParams.Add("jql", parameterToString(*r.jql, ""))
	}
	if r.startAt != nil {
		localVarQueryParams.Add("startAt", parameterToString(*r.startAt, ""))
	}
	if r.maxResults != nil {
		localVarQueryParams.Add("maxResults", parameterToString(*r.maxResults, ""))
	}
	if r.validateQuery != nil {
		localVarQueryParams.Add("validateQuery", parameterToString(*r.validateQuery, ""))
	}
	if r.fields != nil {
		t := *r.fields
		if reflect.TypeOf(t).Kind() == reflect.Slice {
			s := reflect.ValueOf(t)
			for i := 0; i < s.Len(); i++ {
				localVarQueryParams.Add("fields", parameterToString(s.Index(i), "multi"))
			}
		} else {
			localVarQueryParams.Add("fields", parameterToString(t, "multi"))
		}
	}
	if r.expand != nil {
		localVarQueryParams.Add("expand", parameterToString(*r.expand, ""))
	}
	if r.properties != nil {
		t := *r.properties
		if reflect.TypeOf(t).Kind() == reflect.Slice {
			s := reflect.ValueOf(t)
			for i := 0; i < s.Len(); i++ {
				localVarQueryParams.Add("properties", parameterToString(s.Index(i), "multi"))
			}
		} else {
			localVarQueryParams.Add("properties", parameterToString(t, "multi"))
		}
	}
	if r.fieldsByKeys != nil {
		localVarQueryParams.Add("fieldsByKeys", parameterToString(*r.fieldsByKeys, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type IssueSearchApiApiSearchForIssuesUsingJqlPostRequest struct {
	ctx _context.Context
	ApiService IssueSearchApi
	searchRequestBean *SearchRequestBean
}

func (r IssueSearchApiApiSearchForIssuesUsingJqlPostRequest) SearchRequestBean(searchRequestBean SearchRequestBean) IssueSearchApiApiSearchForIssuesUsingJqlPostRequest {
	r.searchRequestBean = &searchRequestBean
	return r
}

func (r IssueSearchApiApiSearchForIssuesUsingJqlPostRequest) Execute() (SearchResults, *_nethttp.Response, error) {
	return r.ApiService.SearchForIssuesUsingJqlPostExecute(r)
}

/*
 * SearchForIssuesUsingJqlPost Search for issues using JQL (POST)
 * Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-rest-api-3-search-get) version of this resource that can be used for smaller JQL query expressions.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** Issues are included in the response where the user has:

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project containing the issue.
 *  If [issue-level security](https://confluence.atlassian.com/x/J4lKLg) is configured, issue-level security permission to view the issue.
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @return IssueSearchApiApiSearchForIssuesUsingJqlPostRequest
 */
func (a *IssueSearchApiService) SearchForIssuesUsingJqlPost(ctx _context.Context) IssueSearchApiApiSearchForIssuesUsingJqlPostRequest {
	return IssueSearchApiApiSearchForIssuesUsingJqlPostRequest{
		ApiService: a,
		ctx: ctx,
	}
}

/*
 * Execute executes the request
 * @return SearchResults
 */
func (a *IssueSearchApiService) SearchForIssuesUsingJqlPostExecute(r IssueSearchApiApiSearchForIssuesUsingJqlPostRequest) (SearchResults, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPost
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  SearchResults
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "IssueSearchApiService.SearchForIssuesUsingJqlPost")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/search"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.searchRequestBean == nil {
		return localVarReturnValue, nil, reportError("searchRequestBean is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.searchRequestBean
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

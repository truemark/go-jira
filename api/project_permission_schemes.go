/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"bytes"
	_context "context"
	_ioutil "io/ioutil"
	_nethttp "net/http"
	_neturl "net/url"
	"strings"
)

// Linger please
var (
	_ _context.Context
)

type ProjectPermissionSchemesApi interface {

	/*
	 * AssignPermissionScheme Assign permission scheme
	 * Assigns a permission scheme with a project. See [Managing project permissions](https://confluence.atlassian.com/x/yodKLg) for more information about permission schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg)
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param projectKeyOrId The project ID or project key (case sensitive).
	 * @return ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest
	 */
	AssignPermissionScheme(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest

	/*
	 * AssignPermissionSchemeExecute executes the request
	 * @return PermissionScheme
	 */
	AssignPermissionSchemeExecute(r ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest) (PermissionScheme, *_nethttp.Response, error)

	/*
	 * GetAssignedPermissionScheme Get assigned permission scheme
	 * Gets the [permission scheme](https://confluence.atlassian.com/x/yodKLg) associated with the project.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg).
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param projectKeyOrId The project ID or project key (case sensitive).
	 * @return ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest
	 */
	GetAssignedPermissionScheme(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest

	/*
	 * GetAssignedPermissionSchemeExecute executes the request
	 * @return PermissionScheme
	 */
	GetAssignedPermissionSchemeExecute(r ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest) (PermissionScheme, *_nethttp.Response, error)

	/*
	 * GetProjectIssueSecurityScheme Get project issue security scheme
	 * Returns the [issue security scheme](https://confluence.atlassian.com/x/J4lKLg) associated with the project.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or the *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param projectKeyOrId The project ID or project key (case sensitive).
	 * @return ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest
	 */
	GetProjectIssueSecurityScheme(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest

	/*
	 * GetProjectIssueSecuritySchemeExecute executes the request
	 * @return SecurityScheme
	 */
	GetProjectIssueSecuritySchemeExecute(r ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest) (SecurityScheme, *_nethttp.Response, error)

	/*
	 * GetSecurityLevelsForProject Get project issue security levels
	 * Returns all [issue security](https://confluence.atlassian.com/x/J4lKLg) levels for the project that the user has access to.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** *Browse projects* [global permission](https://confluence.atlassian.com/x/x4dKLg) for the project, however, issue security levels are only returned for authenticated user with *Set Issue Security* [global permission](https://confluence.atlassian.com/x/x4dKLg) for the project.
	 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
	 * @param projectKeyOrId The project ID or project key (case sensitive).
	 * @return ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest
	 */
	GetSecurityLevelsForProject(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest

	/*
	 * GetSecurityLevelsForProjectExecute executes the request
	 * @return ProjectIssueSecurityLevels
	 */
	GetSecurityLevelsForProjectExecute(r ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest) (ProjectIssueSecurityLevels, *_nethttp.Response, error)
}

// ProjectPermissionSchemesApiService ProjectPermissionSchemesApi service
type ProjectPermissionSchemesApiService service

type ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest struct {
	ctx _context.Context
	ApiService ProjectPermissionSchemesApi
	idBean *IdBean
	projectKeyOrId string
	expand *string
}

func (r ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest) IdBean(idBean IdBean) ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest {
	r.idBean = &idBean
	return r
}
func (r ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest) Expand(expand string) ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest {
	r.expand = &expand
	return r
}

func (r ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest) Execute() (PermissionScheme, *_nethttp.Response, error) {
	return r.ApiService.AssignPermissionSchemeExecute(r)
}

/*
 * AssignPermissionScheme Assign permission scheme
 * Assigns a permission scheme with a project. See [Managing project permissions](https://confluence.atlassian.com/x/yodKLg) for more information about permission schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg)
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param projectKeyOrId The project ID or project key (case sensitive).
 * @return ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest
 */
func (a *ProjectPermissionSchemesApiService) AssignPermissionScheme(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest {
	return ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest{
		ApiService: a,
		ctx: ctx,
		projectKeyOrId: projectKeyOrId,
	}
}

/*
 * Execute executes the request
 * @return PermissionScheme
 */
func (a *ProjectPermissionSchemesApiService) AssignPermissionSchemeExecute(r ProjectPermissionSchemesApiApiAssignPermissionSchemeRequest) (PermissionScheme, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodPut
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  PermissionScheme
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ProjectPermissionSchemesApiService.AssignPermissionScheme")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/project/{projectKeyOrId}/permissionscheme"
	localVarPath = strings.Replace(localVarPath, "{"+"projectKeyOrId"+"}", _neturl.PathEscape(parameterToString(r.projectKeyOrId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}
	if r.idBean == nil {
		return localVarReturnValue, nil, reportError("idBean is required and must be specified")
	}

	if r.expand != nil {
		localVarQueryParams.Add("expand", parameterToString(*r.expand, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.idBean
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest struct {
	ctx _context.Context
	ApiService ProjectPermissionSchemesApi
	projectKeyOrId string
	expand *string
}

func (r ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest) Expand(expand string) ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest {
	r.expand = &expand
	return r
}

func (r ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest) Execute() (PermissionScheme, *_nethttp.Response, error) {
	return r.ApiService.GetAssignedPermissionSchemeExecute(r)
}

/*
 * GetAssignedPermissionScheme Get assigned permission scheme
 * Gets the [permission scheme](https://confluence.atlassian.com/x/yodKLg) associated with the project.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg).
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param projectKeyOrId The project ID or project key (case sensitive).
 * @return ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest
 */
func (a *ProjectPermissionSchemesApiService) GetAssignedPermissionScheme(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest {
	return ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest{
		ApiService: a,
		ctx: ctx,
		projectKeyOrId: projectKeyOrId,
	}
}

/*
 * Execute executes the request
 * @return PermissionScheme
 */
func (a *ProjectPermissionSchemesApiService) GetAssignedPermissionSchemeExecute(r ProjectPermissionSchemesApiApiGetAssignedPermissionSchemeRequest) (PermissionScheme, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  PermissionScheme
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ProjectPermissionSchemesApiService.GetAssignedPermissionScheme")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/project/{projectKeyOrId}/permissionscheme"
	localVarPath = strings.Replace(localVarPath, "{"+"projectKeyOrId"+"}", _neturl.PathEscape(parameterToString(r.projectKeyOrId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	if r.expand != nil {
		localVarQueryParams.Add("expand", parameterToString(*r.expand, ""))
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest struct {
	ctx _context.Context
	ApiService ProjectPermissionSchemesApi
	projectKeyOrId string
}


func (r ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest) Execute() (SecurityScheme, *_nethttp.Response, error) {
	return r.ApiService.GetProjectIssueSecuritySchemeExecute(r)
}

/*
 * GetProjectIssueSecurityScheme Get project issue security scheme
 * Returns the [issue security scheme](https://confluence.atlassian.com/x/J4lKLg) associated with the project.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or the *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param projectKeyOrId The project ID or project key (case sensitive).
 * @return ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest
 */
func (a *ProjectPermissionSchemesApiService) GetProjectIssueSecurityScheme(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest {
	return ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest{
		ApiService: a,
		ctx: ctx,
		projectKeyOrId: projectKeyOrId,
	}
}

/*
 * Execute executes the request
 * @return SecurityScheme
 */
func (a *ProjectPermissionSchemesApiService) GetProjectIssueSecuritySchemeExecute(r ProjectPermissionSchemesApiApiGetProjectIssueSecuritySchemeRequest) (SecurityScheme, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  SecurityScheme
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ProjectPermissionSchemesApiService.GetProjectIssueSecurityScheme")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/project/{projectKeyOrId}/issuesecuritylevelscheme"
	localVarPath = strings.Replace(localVarPath, "{"+"projectKeyOrId"+"}", _neturl.PathEscape(parameterToString(r.projectKeyOrId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest struct {
	ctx _context.Context
	ApiService ProjectPermissionSchemesApi
	projectKeyOrId string
}


func (r ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest) Execute() (ProjectIssueSecurityLevels, *_nethttp.Response, error) {
	return r.ApiService.GetSecurityLevelsForProjectExecute(r)
}

/*
 * GetSecurityLevelsForProject Get project issue security levels
 * Returns all [issue security](https://confluence.atlassian.com/x/J4lKLg) levels for the project that the user has access to.

This operation can be accessed anonymously.

**[Permissions](#permissions) required:** *Browse projects* [global permission](https://confluence.atlassian.com/x/x4dKLg) for the project, however, issue security levels are only returned for authenticated user with *Set Issue Security* [global permission](https://confluence.atlassian.com/x/x4dKLg) for the project.
 * @param ctx _context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 * @param projectKeyOrId The project ID or project key (case sensitive).
 * @return ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest
 */
func (a *ProjectPermissionSchemesApiService) GetSecurityLevelsForProject(ctx _context.Context, projectKeyOrId string) ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest {
	return ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest{
		ApiService: a,
		ctx: ctx,
		projectKeyOrId: projectKeyOrId,
	}
}

/*
 * Execute executes the request
 * @return ProjectIssueSecurityLevels
 */
func (a *ProjectPermissionSchemesApiService) GetSecurityLevelsForProjectExecute(r ProjectPermissionSchemesApiApiGetSecurityLevelsForProjectRequest) (ProjectIssueSecurityLevels, *_nethttp.Response, error) {
	var (
		localVarHTTPMethod   = _nethttp.MethodGet
		localVarPostBody     interface{}
		localVarFormFileName string
		localVarFileName     string
		localVarFileBytes    []byte
		localVarReturnValue  ProjectIssueSecurityLevels
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "ProjectPermissionSchemesApiService.GetSecurityLevelsForProject")
	if err != nil {
		return localVarReturnValue, nil, GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/rest/api/3/project/{projectKeyOrId}/securitylevel"
	localVarPath = strings.Replace(localVarPath, "{"+"projectKeyOrId"+"}", _neturl.PathEscape(parameterToString(r.projectKeyOrId, "")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := _neturl.Values{}
	localVarFormParams := _neturl.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFormFileName, localVarFileName, localVarFileBytes)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := _ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = _ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

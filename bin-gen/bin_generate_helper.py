# A little... fricken py Script to Generate our correct layout... 

from os import listdir
from os.path import isfile, join
from typing import Sequence
from pathlib import Path


class PostGenerateProcessor():
    
    def __init__(self, dir: str = './') -> None:
        self.dir = dir

    def process_post_generate(self):
        self.redistribute_gen_pkgs()

    def redistribute_gen_pkgs(self):
        gend_gos = self.get_gend_go_files()
        models = list(filter(lambda f: (f.startswith("model_")), gend_gos)) 
        apis = list(filter(lambda f: (f.startswith("api_")), gend_gos)) 
        self.print_list(models)
        self.print_list(apis)
        for f in models:
            self.move_remove(f, "model", "model_")
        for f in apis:
            self.move_remove(f, "api", "api_")

    def get_gend_go_files(self) -> Sequence[str]:
        path = './'
        gend_go_files = [f for f in listdir(path) if isfile(join(path, f)) and f.endswith(".go")]
        gend_go_files.sort()
        return gend_go_files
        
    def print_list(self, l):
        for f in l:
            print(f)

    def move_remove(self, f: str, dest_folder: str, strip_text: str) -> None:
        f2 = f.replace(strip_text, "")
        Path("./" + f).rename(str("./" + dest_folder + "/" + f2))


    def process_package_rewrites(self):
        print("into: process_package_rewrites()")
        gend_api_files = [f for f in listdir('./api') if isfile(join('./api', f)) and f.endswith(".go")]
        print(gend_api_files)
        for file in gend_api_files:
            self.rewrite_package_stmt("api", file)
        gend_model_files = [f for f in listdir('./model') if isfile(join('./model', f)) and f.endswith(".go")]
        for file in gend_model_files:
            self.rewrite_package_stmt("model", file)


    def rewrite_package_stmt(self, new_pkg_name, file_path):
        print("file_path: " + file_path)
        print("new_pkg_name: " + new_pkg_name)

        with open(file_path, 'r') as file:
            file_data = file.readlines()
            for line in file_data:
                if line.startswith('package'):
                    print(line)

# # with is like your try .. finally block in this case
# with open('stats.txt', 'r') as file:
#     # read a list of lines into data
#     data = file.readlines()
#
# print data
# print "Your name: " + data[0]
#
# # now change the 2nd line, note that you have to add a newline
# data[1] = 'Mage\n'
#
# # and write everything back
# with open('stats.txt', 'w') as file:
#     file.writelines( data )


if __name__ == "__main__":
    pgp = PostGenerateProcessor()
    pgp.process_post_generate()
    # pgp.process_package_rewrites()
# ActorInputBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**User** | Pointer to **[]string** | The account IDs of the users to add as default actors. This parameter accepts a comma-separated list. For example, &#x60;\&quot;user\&quot;:[\&quot;5b10a2844c20165700ede21g\&quot;, \&quot;5b109f2e9729b51b54dc274d\&quot;]&#x60;. | [optional] 
**Group** | Pointer to **[]string** | The name of the group to add as a default actor. This parameter accepts a comma-separated list. For example, &#x60;\&quot;group\&quot;:[\&quot;project-admin\&quot;, \&quot;jira-developers\&quot;]&#x60;. | [optional] 

## Methods

### NewActorInputBean

`func NewActorInputBean() *ActorInputBean`

NewActorInputBean instantiates a new ActorInputBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActorInputBeanWithDefaults

`func NewActorInputBeanWithDefaults() *ActorInputBean`

NewActorInputBeanWithDefaults instantiates a new ActorInputBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUser

`func (o *ActorInputBean) GetUser() []string`

GetUser returns the User field if non-nil, zero value otherwise.

### GetUserOk

`func (o *ActorInputBean) GetUserOk() (*[]string, bool)`

GetUserOk returns a tuple with the User field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUser

`func (o *ActorInputBean) SetUser(v []string)`

SetUser sets User field to given value.

### HasUser

`func (o *ActorInputBean) HasUser() bool`

HasUser returns a boolean if a field has been set.

### GetGroup

`func (o *ActorInputBean) GetGroup() []string`

GetGroup returns the Group field if non-nil, zero value otherwise.

### GetGroupOk

`func (o *ActorInputBean) GetGroupOk() (*[]string, bool)`

GetGroupOk returns a tuple with the Group field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroup

`func (o *ActorInputBean) SetGroup(v []string)`

SetGroup sets Group field to given value.

### HasGroup

`func (o *ActorInputBean) HasGroup() bool`

HasGroup returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



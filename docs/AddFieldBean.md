# AddFieldBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FieldId** | **string** | The ID of the field to add. | 

## Methods

### NewAddFieldBean

`func NewAddFieldBean(fieldId string, ) *AddFieldBean`

NewAddFieldBean instantiates a new AddFieldBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddFieldBeanWithDefaults

`func NewAddFieldBeanWithDefaults() *AddFieldBean`

NewAddFieldBeanWithDefaults instantiates a new AddFieldBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFieldId

`func (o *AddFieldBean) GetFieldId() string`

GetFieldId returns the FieldId field if non-nil, zero value otherwise.

### GetFieldIdOk

`func (o *AddFieldBean) GetFieldIdOk() (*string, bool)`

GetFieldIdOk returns a tuple with the FieldId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFieldId

`func (o *AddFieldBean) SetFieldId(v string)`

SetFieldId sets FieldId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \AppPropertiesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddonPropertiesResourceDeleteAddonPropertyDelete**](AppPropertiesApi.md#AddonPropertiesResourceDeleteAddonPropertyDelete) | **Delete** /rest/atlassian-connect/1/addons/{addonKey}/properties/{propertyKey} | Delete app property
[**AddonPropertiesResourceGetAddonPropertiesGet**](AppPropertiesApi.md#AddonPropertiesResourceGetAddonPropertiesGet) | **Get** /rest/atlassian-connect/1/addons/{addonKey}/properties | Get app properties
[**AddonPropertiesResourceGetAddonPropertyGet**](AppPropertiesApi.md#AddonPropertiesResourceGetAddonPropertyGet) | **Get** /rest/atlassian-connect/1/addons/{addonKey}/properties/{propertyKey} | Get app property
[**AddonPropertiesResourcePutAddonPropertyPut**](AppPropertiesApi.md#AddonPropertiesResourcePutAddonPropertyPut) | **Put** /rest/atlassian-connect/1/addons/{addonKey}/properties/{propertyKey} | Set app property



## AddonPropertiesResourceDeleteAddonPropertyDelete

> AddonPropertiesResourceDeleteAddonPropertyDelete(ctx, addonKey, propertyKey).Execute()

Delete app property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    addonKey := "addonKey_example" // string | The key of the app, as defined in its descriptor.
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AppPropertiesApi.AddonPropertiesResourceDeleteAddonPropertyDelete(context.Background(), addonKey, propertyKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AppPropertiesApi.AddonPropertiesResourceDeleteAddonPropertyDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**addonKey** | **string** | The key of the app, as defined in its descriptor. | 
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddonPropertiesResourceDeleteAddonPropertyDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddonPropertiesResourceGetAddonPropertiesGet

> PropertyKeys AddonPropertiesResourceGetAddonPropertiesGet(ctx, addonKey).Execute()

Get app properties



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    addonKey := "addonKey_example" // string | The key of the app, as defined in its descriptor.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AppPropertiesApi.AddonPropertiesResourceGetAddonPropertiesGet(context.Background(), addonKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AppPropertiesApi.AddonPropertiesResourceGetAddonPropertiesGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddonPropertiesResourceGetAddonPropertiesGet`: PropertyKeys
    fmt.Fprintf(os.Stdout, "Response from `AppPropertiesApi.AddonPropertiesResourceGetAddonPropertiesGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**addonKey** | **string** | The key of the app, as defined in its descriptor. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddonPropertiesResourceGetAddonPropertiesGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**PropertyKeys**](PropertyKeys.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddonPropertiesResourceGetAddonPropertyGet

> EntityProperty AddonPropertiesResourceGetAddonPropertyGet(ctx, addonKey, propertyKey).Execute()

Get app property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    addonKey := "addonKey_example" // string | The key of the app, as defined in its descriptor.
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AppPropertiesApi.AddonPropertiesResourceGetAddonPropertyGet(context.Background(), addonKey, propertyKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AppPropertiesApi.AddonPropertiesResourceGetAddonPropertyGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddonPropertiesResourceGetAddonPropertyGet`: EntityProperty
    fmt.Fprintf(os.Stdout, "Response from `AppPropertiesApi.AddonPropertiesResourceGetAddonPropertyGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**addonKey** | **string** | The key of the app, as defined in its descriptor. | 
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddonPropertiesResourceGetAddonPropertyGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**EntityProperty**](EntityProperty.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddonPropertiesResourcePutAddonPropertyPut

> OperationMessage AddonPropertiesResourcePutAddonPropertyPut(ctx, addonKey, propertyKey).Body(body).Execute()

Set app property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    body := interface{}(987) // interface{} | 
    addonKey := "addonKey_example" // string | The key of the app, as defined in its descriptor.
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AppPropertiesApi.AddonPropertiesResourcePutAddonPropertyPut(context.Background(), addonKey, propertyKey).Body(body).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AppPropertiesApi.AddonPropertiesResourcePutAddonPropertyPut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddonPropertiesResourcePutAddonPropertyPut`: OperationMessage
    fmt.Fprintf(os.Stdout, "Response from `AppPropertiesApi.AddonPropertiesResourcePutAddonPropertyPut`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**addonKey** | **string** | The key of the app, as defined in its descriptor. | 
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddonPropertiesResourcePutAddonPropertyPutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **interface{}** |  | 



### Return type

[**OperationMessage**](OperationMessage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


# ApplicationProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | The ID of the application property. The ID and key are the same. | [optional] 
**Key** | Pointer to **string** | The key of the application property. The ID and key are the same. | [optional] 
**Value** | Pointer to **string** | The new value. | [optional] 
**Name** | Pointer to **string** | The name of the application property. | [optional] 
**Desc** | Pointer to **string** | The description of the application property. | [optional] 
**Type** | Pointer to **string** | The data type of the application property. | [optional] 
**DefaultValue** | Pointer to **string** | The default value of the application property. | [optional] 
**Example** | Pointer to **string** |  | [optional] 
**AllowedValues** | Pointer to **[]string** | The allowed values, if applicable. | [optional] 

## Methods

### NewApplicationProperty

`func NewApplicationProperty() *ApplicationProperty`

NewApplicationProperty instantiates a new ApplicationProperty object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewApplicationPropertyWithDefaults

`func NewApplicationPropertyWithDefaults() *ApplicationProperty`

NewApplicationPropertyWithDefaults instantiates a new ApplicationProperty object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ApplicationProperty) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ApplicationProperty) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ApplicationProperty) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ApplicationProperty) HasId() bool`

HasId returns a boolean if a field has been set.

### GetKey

`func (o *ApplicationProperty) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *ApplicationProperty) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *ApplicationProperty) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *ApplicationProperty) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetValue

`func (o *ApplicationProperty) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *ApplicationProperty) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *ApplicationProperty) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *ApplicationProperty) HasValue() bool`

HasValue returns a boolean if a field has been set.

### GetName

`func (o *ApplicationProperty) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ApplicationProperty) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ApplicationProperty) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ApplicationProperty) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDesc

`func (o *ApplicationProperty) GetDesc() string`

GetDesc returns the Desc field if non-nil, zero value otherwise.

### GetDescOk

`func (o *ApplicationProperty) GetDescOk() (*string, bool)`

GetDescOk returns a tuple with the Desc field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDesc

`func (o *ApplicationProperty) SetDesc(v string)`

SetDesc sets Desc field to given value.

### HasDesc

`func (o *ApplicationProperty) HasDesc() bool`

HasDesc returns a boolean if a field has been set.

### GetType

`func (o *ApplicationProperty) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ApplicationProperty) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ApplicationProperty) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ApplicationProperty) HasType() bool`

HasType returns a boolean if a field has been set.

### GetDefaultValue

`func (o *ApplicationProperty) GetDefaultValue() string`

GetDefaultValue returns the DefaultValue field if non-nil, zero value otherwise.

### GetDefaultValueOk

`func (o *ApplicationProperty) GetDefaultValueOk() (*string, bool)`

GetDefaultValueOk returns a tuple with the DefaultValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultValue

`func (o *ApplicationProperty) SetDefaultValue(v string)`

SetDefaultValue sets DefaultValue field to given value.

### HasDefaultValue

`func (o *ApplicationProperty) HasDefaultValue() bool`

HasDefaultValue returns a boolean if a field has been set.

### GetExample

`func (o *ApplicationProperty) GetExample() string`

GetExample returns the Example field if non-nil, zero value otherwise.

### GetExampleOk

`func (o *ApplicationProperty) GetExampleOk() (*string, bool)`

GetExampleOk returns a tuple with the Example field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExample

`func (o *ApplicationProperty) SetExample(v string)`

SetExample sets Example field to given value.

### HasExample

`func (o *ApplicationProperty) HasExample() bool`

HasExample returns a boolean if a field has been set.

### GetAllowedValues

`func (o *ApplicationProperty) GetAllowedValues() []string`

GetAllowedValues returns the AllowedValues field if non-nil, zero value otherwise.

### GetAllowedValuesOk

`func (o *ApplicationProperty) GetAllowedValuesOk() (*[]string, bool)`

GetAllowedValuesOk returns a tuple with the AllowedValues field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAllowedValues

`func (o *ApplicationProperty) SetAllowedValues(v []string)`

SetAllowedValues sets AllowedValues field to given value.

### HasAllowedValues

`func (o *ApplicationProperty) HasAllowedValues() bool`

HasAllowedValues returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



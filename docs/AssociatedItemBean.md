# AssociatedItemBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | The ID of the associated record. | [optional] [readonly] 
**Name** | Pointer to **string** | The name of the associated record. | [optional] [readonly] 
**TypeName** | Pointer to **string** | The type of the associated record. | [optional] [readonly] 
**ParentId** | Pointer to **string** | The ID of the associated parent record. | [optional] [readonly] 
**ParentName** | Pointer to **string** | The name of the associated parent record. | [optional] [readonly] 

## Methods

### NewAssociatedItemBean

`func NewAssociatedItemBean() *AssociatedItemBean`

NewAssociatedItemBean instantiates a new AssociatedItemBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssociatedItemBeanWithDefaults

`func NewAssociatedItemBeanWithDefaults() *AssociatedItemBean`

NewAssociatedItemBeanWithDefaults instantiates a new AssociatedItemBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AssociatedItemBean) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AssociatedItemBean) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AssociatedItemBean) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *AssociatedItemBean) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *AssociatedItemBean) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *AssociatedItemBean) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *AssociatedItemBean) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *AssociatedItemBean) HasName() bool`

HasName returns a boolean if a field has been set.

### GetTypeName

`func (o *AssociatedItemBean) GetTypeName() string`

GetTypeName returns the TypeName field if non-nil, zero value otherwise.

### GetTypeNameOk

`func (o *AssociatedItemBean) GetTypeNameOk() (*string, bool)`

GetTypeNameOk returns a tuple with the TypeName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTypeName

`func (o *AssociatedItemBean) SetTypeName(v string)`

SetTypeName sets TypeName field to given value.

### HasTypeName

`func (o *AssociatedItemBean) HasTypeName() bool`

HasTypeName returns a boolean if a field has been set.

### GetParentId

`func (o *AssociatedItemBean) GetParentId() string`

GetParentId returns the ParentId field if non-nil, zero value otherwise.

### GetParentIdOk

`func (o *AssociatedItemBean) GetParentIdOk() (*string, bool)`

GetParentIdOk returns a tuple with the ParentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentId

`func (o *AssociatedItemBean) SetParentId(v string)`

SetParentId sets ParentId field to given value.

### HasParentId

`func (o *AssociatedItemBean) HasParentId() bool`

HasParentId returns a boolean if a field has been set.

### GetParentName

`func (o *AssociatedItemBean) GetParentName() string`

GetParentName returns the ParentName field if non-nil, zero value otherwise.

### GetParentNameOk

`func (o *AssociatedItemBean) GetParentNameOk() (*string, bool)`

GetParentNameOk returns a tuple with the ParentName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentName

`func (o *AssociatedItemBean) SetParentName(v string)`

SetParentName sets ParentName field to given value.

### HasParentName

`func (o *AssociatedItemBean) HasParentName() bool`

HasParentName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# AttachmentArchiveEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AbbreviatedName** | Pointer to **string** |  | [optional] 
**EntryIndex** | Pointer to **int64** |  | [optional] 
**MediaType** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**Size** | Pointer to **int64** |  | [optional] 

## Methods

### NewAttachmentArchiveEntry

`func NewAttachmentArchiveEntry() *AttachmentArchiveEntry`

NewAttachmentArchiveEntry instantiates a new AttachmentArchiveEntry object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAttachmentArchiveEntryWithDefaults

`func NewAttachmentArchiveEntryWithDefaults() *AttachmentArchiveEntry`

NewAttachmentArchiveEntryWithDefaults instantiates a new AttachmentArchiveEntry object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAbbreviatedName

`func (o *AttachmentArchiveEntry) GetAbbreviatedName() string`

GetAbbreviatedName returns the AbbreviatedName field if non-nil, zero value otherwise.

### GetAbbreviatedNameOk

`func (o *AttachmentArchiveEntry) GetAbbreviatedNameOk() (*string, bool)`

GetAbbreviatedNameOk returns a tuple with the AbbreviatedName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAbbreviatedName

`func (o *AttachmentArchiveEntry) SetAbbreviatedName(v string)`

SetAbbreviatedName sets AbbreviatedName field to given value.

### HasAbbreviatedName

`func (o *AttachmentArchiveEntry) HasAbbreviatedName() bool`

HasAbbreviatedName returns a boolean if a field has been set.

### GetEntryIndex

`func (o *AttachmentArchiveEntry) GetEntryIndex() int64`

GetEntryIndex returns the EntryIndex field if non-nil, zero value otherwise.

### GetEntryIndexOk

`func (o *AttachmentArchiveEntry) GetEntryIndexOk() (*int64, bool)`

GetEntryIndexOk returns a tuple with the EntryIndex field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntryIndex

`func (o *AttachmentArchiveEntry) SetEntryIndex(v int64)`

SetEntryIndex sets EntryIndex field to given value.

### HasEntryIndex

`func (o *AttachmentArchiveEntry) HasEntryIndex() bool`

HasEntryIndex returns a boolean if a field has been set.

### GetMediaType

`func (o *AttachmentArchiveEntry) GetMediaType() string`

GetMediaType returns the MediaType field if non-nil, zero value otherwise.

### GetMediaTypeOk

`func (o *AttachmentArchiveEntry) GetMediaTypeOk() (*string, bool)`

GetMediaTypeOk returns a tuple with the MediaType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaType

`func (o *AttachmentArchiveEntry) SetMediaType(v string)`

SetMediaType sets MediaType field to given value.

### HasMediaType

`func (o *AttachmentArchiveEntry) HasMediaType() bool`

HasMediaType returns a boolean if a field has been set.

### GetName

`func (o *AttachmentArchiveEntry) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *AttachmentArchiveEntry) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *AttachmentArchiveEntry) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *AttachmentArchiveEntry) HasName() bool`

HasName returns a boolean if a field has been set.

### GetSize

`func (o *AttachmentArchiveEntry) GetSize() int64`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *AttachmentArchiveEntry) GetSizeOk() (*int64, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *AttachmentArchiveEntry) SetSize(v int64)`

SetSize sets Size field to given value.

### HasSize

`func (o *AttachmentArchiveEntry) HasSize() bool`

HasSize returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



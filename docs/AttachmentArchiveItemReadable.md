# AttachmentArchiveItemReadable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | Pointer to **string** | The path of the archive item. | [optional] [readonly] 
**Index** | Pointer to **int64** | The position of the item within the archive. | [optional] [readonly] 
**Size** | Pointer to **string** | The size of the archive item. | [optional] [readonly] 
**MediaType** | Pointer to **string** | The MIME type of the archive item. | [optional] [readonly] 
**Label** | Pointer to **string** | The label for the archive item. | [optional] [readonly] 

## Methods

### NewAttachmentArchiveItemReadable

`func NewAttachmentArchiveItemReadable() *AttachmentArchiveItemReadable`

NewAttachmentArchiveItemReadable instantiates a new AttachmentArchiveItemReadable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAttachmentArchiveItemReadableWithDefaults

`func NewAttachmentArchiveItemReadableWithDefaults() *AttachmentArchiveItemReadable`

NewAttachmentArchiveItemReadableWithDefaults instantiates a new AttachmentArchiveItemReadable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPath

`func (o *AttachmentArchiveItemReadable) GetPath() string`

GetPath returns the Path field if non-nil, zero value otherwise.

### GetPathOk

`func (o *AttachmentArchiveItemReadable) GetPathOk() (*string, bool)`

GetPathOk returns a tuple with the Path field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPath

`func (o *AttachmentArchiveItemReadable) SetPath(v string)`

SetPath sets Path field to given value.

### HasPath

`func (o *AttachmentArchiveItemReadable) HasPath() bool`

HasPath returns a boolean if a field has been set.

### GetIndex

`func (o *AttachmentArchiveItemReadable) GetIndex() int64`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *AttachmentArchiveItemReadable) GetIndexOk() (*int64, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *AttachmentArchiveItemReadable) SetIndex(v int64)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *AttachmentArchiveItemReadable) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetSize

`func (o *AttachmentArchiveItemReadable) GetSize() string`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *AttachmentArchiveItemReadable) GetSizeOk() (*string, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *AttachmentArchiveItemReadable) SetSize(v string)`

SetSize sets Size field to given value.

### HasSize

`func (o *AttachmentArchiveItemReadable) HasSize() bool`

HasSize returns a boolean if a field has been set.

### GetMediaType

`func (o *AttachmentArchiveItemReadable) GetMediaType() string`

GetMediaType returns the MediaType field if non-nil, zero value otherwise.

### GetMediaTypeOk

`func (o *AttachmentArchiveItemReadable) GetMediaTypeOk() (*string, bool)`

GetMediaTypeOk returns a tuple with the MediaType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaType

`func (o *AttachmentArchiveItemReadable) SetMediaType(v string)`

SetMediaType sets MediaType field to given value.

### HasMediaType

`func (o *AttachmentArchiveItemReadable) HasMediaType() bool`

HasMediaType returns a boolean if a field has been set.

### GetLabel

`func (o *AttachmentArchiveItemReadable) GetLabel() string`

GetLabel returns the Label field if non-nil, zero value otherwise.

### GetLabelOk

`func (o *AttachmentArchiveItemReadable) GetLabelOk() (*string, bool)`

GetLabelOk returns a tuple with the Label field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabel

`func (o *AttachmentArchiveItemReadable) SetLabel(v string)`

SetLabel sets Label field to given value.

### HasLabel

`func (o *AttachmentArchiveItemReadable) HasLabel() bool`

HasLabel returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# AvatarUrlsBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Var16x16** | Pointer to **string** | The URL of the item&#39;s 16x16 pixel avatar. | [optional] 
**Var24x24** | Pointer to **string** | The URL of the item&#39;s 24x24 pixel avatar. | [optional] 
**Var32x32** | Pointer to **string** | The URL of the item&#39;s 32x32 pixel avatar. | [optional] 
**Var48x48** | Pointer to **string** | The URL of the item&#39;s 48x48 pixel avatar. | [optional] 

## Methods

### NewAvatarUrlsBean

`func NewAvatarUrlsBean() *AvatarUrlsBean`

NewAvatarUrlsBean instantiates a new AvatarUrlsBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAvatarUrlsBeanWithDefaults

`func NewAvatarUrlsBeanWithDefaults() *AvatarUrlsBean`

NewAvatarUrlsBeanWithDefaults instantiates a new AvatarUrlsBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVar16x16

`func (o *AvatarUrlsBean) GetVar16x16() string`

GetVar16x16 returns the Var16x16 field if non-nil, zero value otherwise.

### GetVar16x16Ok

`func (o *AvatarUrlsBean) GetVar16x16Ok() (*string, bool)`

GetVar16x16Ok returns a tuple with the Var16x16 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar16x16

`func (o *AvatarUrlsBean) SetVar16x16(v string)`

SetVar16x16 sets Var16x16 field to given value.

### HasVar16x16

`func (o *AvatarUrlsBean) HasVar16x16() bool`

HasVar16x16 returns a boolean if a field has been set.

### GetVar24x24

`func (o *AvatarUrlsBean) GetVar24x24() string`

GetVar24x24 returns the Var24x24 field if non-nil, zero value otherwise.

### GetVar24x24Ok

`func (o *AvatarUrlsBean) GetVar24x24Ok() (*string, bool)`

GetVar24x24Ok returns a tuple with the Var24x24 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar24x24

`func (o *AvatarUrlsBean) SetVar24x24(v string)`

SetVar24x24 sets Var24x24 field to given value.

### HasVar24x24

`func (o *AvatarUrlsBean) HasVar24x24() bool`

HasVar24x24 returns a boolean if a field has been set.

### GetVar32x32

`func (o *AvatarUrlsBean) GetVar32x32() string`

GetVar32x32 returns the Var32x32 field if non-nil, zero value otherwise.

### GetVar32x32Ok

`func (o *AvatarUrlsBean) GetVar32x32Ok() (*string, bool)`

GetVar32x32Ok returns a tuple with the Var32x32 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar32x32

`func (o *AvatarUrlsBean) SetVar32x32(v string)`

SetVar32x32 sets Var32x32 field to given value.

### HasVar32x32

`func (o *AvatarUrlsBean) HasVar32x32() bool`

HasVar32x32 returns a boolean if a field has been set.

### GetVar48x48

`func (o *AvatarUrlsBean) GetVar48x48() string`

GetVar48x48 returns the Var48x48 field if non-nil, zero value otherwise.

### GetVar48x48Ok

`func (o *AvatarUrlsBean) GetVar48x48Ok() (*string, bool)`

GetVar48x48Ok returns a tuple with the Var48x48 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar48x48

`func (o *AvatarUrlsBean) SetVar48x48(v string)`

SetVar48x48 sets Var48x48 field to given value.

### HasVar48x48

`func (o *AvatarUrlsBean) HasVar48x48() bool`

HasVar48x48 returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



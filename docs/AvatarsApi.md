# \AvatarsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteAvatar**](AvatarsApi.md#DeleteAvatar) | **Delete** /rest/api/3/universal_avatar/type/{type}/owner/{owningObjectId}/avatar/{id} | Delete avatar
[**GetAllSystemAvatars**](AvatarsApi.md#GetAllSystemAvatars) | **Get** /rest/api/3/avatar/{type}/system | Get system avatars by type
[**GetAvatars**](AvatarsApi.md#GetAvatars) | **Get** /rest/api/3/universal_avatar/type/{type}/owner/{entityId} | Get avatars
[**StoreAvatar**](AvatarsApi.md#StoreAvatar) | **Post** /rest/api/3/universal_avatar/type/{type}/owner/{entityId} | Load avatar



## DeleteAvatar

> DeleteAvatar(ctx, type_, owningObjectId, id).Execute()

Delete avatar



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    type_ := "type__example" // string | The avatar type.
    owningObjectId := "owningObjectId_example" // string | The ID of the item the avatar is associated with.
    id := int64(789) // int64 | The ID of the avatar.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AvatarsApi.DeleteAvatar(context.Background(), type_, owningObjectId, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AvatarsApi.DeleteAvatar``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**type_** | **string** | The avatar type. | 
**owningObjectId** | **string** | The ID of the item the avatar is associated with. | 
**id** | **int64** | The ID of the avatar. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteAvatarRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllSystemAvatars

> SystemAvatars GetAllSystemAvatars(ctx, type_).Execute()

Get system avatars by type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    type_ := "project" // string | The avatar type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AvatarsApi.GetAllSystemAvatars(context.Background(), type_).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AvatarsApi.GetAllSystemAvatars``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAllSystemAvatars`: SystemAvatars
    fmt.Fprintf(os.Stdout, "Response from `AvatarsApi.GetAllSystemAvatars`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**type_** | **string** | The avatar type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAllSystemAvatarsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**SystemAvatars**](SystemAvatars.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAvatars

> Avatars GetAvatars(ctx, type_, entityId).Execute()

Get avatars



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    type_ := "type__example" // string | The avatar type.
    entityId := "entityId_example" // string | The ID of the item the avatar is associated with.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AvatarsApi.GetAvatars(context.Background(), type_, entityId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AvatarsApi.GetAvatars``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAvatars`: Avatars
    fmt.Fprintf(os.Stdout, "Response from `AvatarsApi.GetAvatars`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**type_** | **string** | The avatar type. | 
**entityId** | **string** | The ID of the item the avatar is associated with. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAvatarsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Avatars**](Avatars.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StoreAvatar

> Avatar StoreAvatar(ctx, type_, entityId).Body(body).Size(size).X(x).Y(y).Execute()

Load avatar



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    body := interface{}(987) // interface{} | 
    type_ := "type__example" // string | The avatar type.
    entityId := "entityId_example" // string | The ID of the item the avatar is associated with.
    size := int32(56) // int32 | The length of each side of the crop region.
    x := int32(56) // int32 | The X coordinate of the top-left corner of the crop region. (optional) (default to 0)
    y := int32(56) // int32 | The Y coordinate of the top-left corner of the crop region. (optional) (default to 0)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AvatarsApi.StoreAvatar(context.Background(), type_, entityId).Body(body).Size(size).X(x).Y(y).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AvatarsApi.StoreAvatar``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StoreAvatar`: Avatar
    fmt.Fprintf(os.Stdout, "Response from `AvatarsApi.StoreAvatar`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**type_** | **string** | The avatar type. | 
**entityId** | **string** | The ID of the item the avatar is associated with. | 

### Other Parameters

Other parameters are passed through a pointer to a apiStoreAvatarRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **interface{}** |  | 


 **size** | **int32** | The length of each side of the crop region. | 
 **x** | **int32** | The X coordinate of the top-left corner of the crop region. | [default to 0]
 **y** | **int32** | The Y coordinate of the top-left corner of the crop region. | [default to 0]

### Return type

[**Avatar**](Avatar.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


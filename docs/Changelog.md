# Changelog

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | The ID of the changelog. | [optional] [readonly] 
**Author** | Pointer to [**UserDetails**](UserDetails.md) | The user who made the change. | [optional] [readonly] 
**Created** | Pointer to **time.Time** | The date on which the change took place. | [optional] [readonly] 
**Items** | Pointer to [**[]ChangeDetails**](ChangeDetails.md) | The list of items changed. | [optional] [readonly] 
**HistoryMetadata** | Pointer to [**HistoryMetadata**](HistoryMetadata.md) | The history metadata associated with the changed. | [optional] [readonly] 

## Methods

### NewChangelog

`func NewChangelog() *Changelog`

NewChangelog instantiates a new Changelog object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChangelogWithDefaults

`func NewChangelogWithDefaults() *Changelog`

NewChangelogWithDefaults instantiates a new Changelog object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Changelog) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Changelog) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Changelog) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *Changelog) HasId() bool`

HasId returns a boolean if a field has been set.

### GetAuthor

`func (o *Changelog) GetAuthor() UserDetails`

GetAuthor returns the Author field if non-nil, zero value otherwise.

### GetAuthorOk

`func (o *Changelog) GetAuthorOk() (*UserDetails, bool)`

GetAuthorOk returns a tuple with the Author field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthor

`func (o *Changelog) SetAuthor(v UserDetails)`

SetAuthor sets Author field to given value.

### HasAuthor

`func (o *Changelog) HasAuthor() bool`

HasAuthor returns a boolean if a field has been set.

### GetCreated

`func (o *Changelog) GetCreated() time.Time`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *Changelog) GetCreatedOk() (*time.Time, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *Changelog) SetCreated(v time.Time)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *Changelog) HasCreated() bool`

HasCreated returns a boolean if a field has been set.

### GetItems

`func (o *Changelog) GetItems() []ChangeDetails`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *Changelog) GetItemsOk() (*[]ChangeDetails, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *Changelog) SetItems(v []ChangeDetails)`

SetItems sets Items field to given value.

### HasItems

`func (o *Changelog) HasItems() bool`

HasItems returns a boolean if a field has been set.

### GetHistoryMetadata

`func (o *Changelog) GetHistoryMetadata() HistoryMetadata`

GetHistoryMetadata returns the HistoryMetadata field if non-nil, zero value otherwise.

### GetHistoryMetadataOk

`func (o *Changelog) GetHistoryMetadataOk() (*HistoryMetadata, bool)`

GetHistoryMetadataOk returns a tuple with the HistoryMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHistoryMetadata

`func (o *Changelog) SetHistoryMetadata(v HistoryMetadata)`

SetHistoryMetadata sets HistoryMetadata field to given value.

### HasHistoryMetadata

`func (o *Changelog) HasHistoryMetadata() bool`

HasHistoryMetadata returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ContainerForRegisteredWebhooks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WebhookRegistrationResult** | Pointer to [**[]RegisteredWebhook**](RegisteredWebhook.md) | A list of registered webhooks. | [optional] 

## Methods

### NewContainerForRegisteredWebhooks

`func NewContainerForRegisteredWebhooks() *ContainerForRegisteredWebhooks`

NewContainerForRegisteredWebhooks instantiates a new ContainerForRegisteredWebhooks object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContainerForRegisteredWebhooksWithDefaults

`func NewContainerForRegisteredWebhooksWithDefaults() *ContainerForRegisteredWebhooks`

NewContainerForRegisteredWebhooksWithDefaults instantiates a new ContainerForRegisteredWebhooks object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWebhookRegistrationResult

`func (o *ContainerForRegisteredWebhooks) GetWebhookRegistrationResult() []RegisteredWebhook`

GetWebhookRegistrationResult returns the WebhookRegistrationResult field if non-nil, zero value otherwise.

### GetWebhookRegistrationResultOk

`func (o *ContainerForRegisteredWebhooks) GetWebhookRegistrationResultOk() (*[]RegisteredWebhook, bool)`

GetWebhookRegistrationResultOk returns a tuple with the WebhookRegistrationResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebhookRegistrationResult

`func (o *ContainerForRegisteredWebhooks) SetWebhookRegistrationResult(v []RegisteredWebhook)`

SetWebhookRegistrationResult sets WebhookRegistrationResult field to given value.

### HasWebhookRegistrationResult

`func (o *ContainerForRegisteredWebhooks) HasWebhookRegistrationResult() bool`

HasWebhookRegistrationResult returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



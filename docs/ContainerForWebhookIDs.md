# ContainerForWebhookIDs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WebhookIds** | **[]int64** | A list of webhook IDs. | 

## Methods

### NewContainerForWebhookIDs

`func NewContainerForWebhookIDs(webhookIds []int64, ) *ContainerForWebhookIDs`

NewContainerForWebhookIDs instantiates a new ContainerForWebhookIDs object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContainerForWebhookIDsWithDefaults

`func NewContainerForWebhookIDsWithDefaults() *ContainerForWebhookIDs`

NewContainerForWebhookIDsWithDefaults instantiates a new ContainerForWebhookIDs object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWebhookIds

`func (o *ContainerForWebhookIDs) GetWebhookIds() []int64`

GetWebhookIds returns the WebhookIds field if non-nil, zero value otherwise.

### GetWebhookIdsOk

`func (o *ContainerForWebhookIDs) GetWebhookIdsOk() (*[]int64, bool)`

GetWebhookIdsOk returns a tuple with the WebhookIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebhookIds

`func (o *ContainerForWebhookIDs) SetWebhookIds(v []int64)`

SetWebhookIds sets WebhookIds field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ContextForProjectAndIssueType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProjectId** | **string** | The ID of the project. | 
**IssueTypeId** | **string** | The ID of the issue type. | 
**ContextId** | **string** | The ID of the custom field context. | 

## Methods

### NewContextForProjectAndIssueType

`func NewContextForProjectAndIssueType(projectId string, issueTypeId string, contextId string, ) *ContextForProjectAndIssueType`

NewContextForProjectAndIssueType instantiates a new ContextForProjectAndIssueType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContextForProjectAndIssueTypeWithDefaults

`func NewContextForProjectAndIssueTypeWithDefaults() *ContextForProjectAndIssueType`

NewContextForProjectAndIssueTypeWithDefaults instantiates a new ContextForProjectAndIssueType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProjectId

`func (o *ContextForProjectAndIssueType) GetProjectId() string`

GetProjectId returns the ProjectId field if non-nil, zero value otherwise.

### GetProjectIdOk

`func (o *ContextForProjectAndIssueType) GetProjectIdOk() (*string, bool)`

GetProjectIdOk returns a tuple with the ProjectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectId

`func (o *ContextForProjectAndIssueType) SetProjectId(v string)`

SetProjectId sets ProjectId field to given value.


### GetIssueTypeId

`func (o *ContextForProjectAndIssueType) GetIssueTypeId() string`

GetIssueTypeId returns the IssueTypeId field if non-nil, zero value otherwise.

### GetIssueTypeIdOk

`func (o *ContextForProjectAndIssueType) GetIssueTypeIdOk() (*string, bool)`

GetIssueTypeIdOk returns a tuple with the IssueTypeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeId

`func (o *ContextForProjectAndIssueType) SetIssueTypeId(v string)`

SetIssueTypeId sets IssueTypeId field to given value.


### GetContextId

`func (o *ContextForProjectAndIssueType) GetContextId() string`

GetContextId returns the ContextId field if non-nil, zero value otherwise.

### GetContextIdOk

`func (o *ContextForProjectAndIssueType) GetContextIdOk() (*string, bool)`

GetContextIdOk returns a tuple with the ContextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextId

`func (o *ContextForProjectAndIssueType) SetContextId(v string)`

SetContextId sets ContextId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



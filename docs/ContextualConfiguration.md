# ContextualConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContextId** | **int64** | The ID of the context the configuration is associated with. | 
**Configuration** | Pointer to **interface{}** | The configuration associated with the context. | [optional] 

## Methods

### NewContextualConfiguration

`func NewContextualConfiguration(contextId int64, ) *ContextualConfiguration`

NewContextualConfiguration instantiates a new ContextualConfiguration object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContextualConfigurationWithDefaults

`func NewContextualConfigurationWithDefaults() *ContextualConfiguration`

NewContextualConfigurationWithDefaults instantiates a new ContextualConfiguration object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContextId

`func (o *ContextualConfiguration) GetContextId() int64`

GetContextId returns the ContextId field if non-nil, zero value otherwise.

### GetContextIdOk

`func (o *ContextualConfiguration) GetContextIdOk() (*int64, bool)`

GetContextIdOk returns a tuple with the ContextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextId

`func (o *ContextualConfiguration) SetContextId(v int64)`

SetContextId sets ContextId field to given value.


### GetConfiguration

`func (o *ContextualConfiguration) GetConfiguration() interface{}`

GetConfiguration returns the Configuration field if non-nil, zero value otherwise.

### GetConfigurationOk

`func (o *ContextualConfiguration) GetConfigurationOk() (*interface{}, bool)`

GetConfigurationOk returns a tuple with the Configuration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfiguration

`func (o *ContextualConfiguration) SetConfiguration(v interface{})`

SetConfiguration sets Configuration field to given value.

### HasConfiguration

`func (o *ContextualConfiguration) HasConfiguration() bool`

HasConfiguration returns a boolean if a field has been set.

### SetConfigurationNil

`func (o *ContextualConfiguration) SetConfigurationNil(b bool)`

 SetConfigurationNil sets the value for Configuration to be an explicit nil

### UnsetConfiguration
`func (o *ContextualConfiguration) UnsetConfiguration()`

UnsetConfiguration ensures that no value is present for Configuration, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



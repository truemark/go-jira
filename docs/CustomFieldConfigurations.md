# CustomFieldConfigurations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Configurations** | [**[]ContextualConfiguration**](ContextualConfiguration.md) | The list of custom field configuration details. | 

## Methods

### NewCustomFieldConfigurations

`func NewCustomFieldConfigurations(configurations []ContextualConfiguration, ) *CustomFieldConfigurations`

NewCustomFieldConfigurations instantiates a new CustomFieldConfigurations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldConfigurationsWithDefaults

`func NewCustomFieldConfigurationsWithDefaults() *CustomFieldConfigurations`

NewCustomFieldConfigurationsWithDefaults instantiates a new CustomFieldConfigurations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConfigurations

`func (o *CustomFieldConfigurations) GetConfigurations() []ContextualConfiguration`

GetConfigurations returns the Configurations field if non-nil, zero value otherwise.

### GetConfigurationsOk

`func (o *CustomFieldConfigurations) GetConfigurationsOk() (*[]ContextualConfiguration, bool)`

GetConfigurationsOk returns a tuple with the Configurations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfigurations

`func (o *CustomFieldConfigurations) SetConfigurations(v []ContextualConfiguration)`

SetConfigurations sets Configurations field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



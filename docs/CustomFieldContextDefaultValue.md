# CustomFieldContextDefaultValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContextId** | **string** | The ID of the context. | 
**OptionId** | **string** | The ID of the default option. | 
**CascadingOptionId** | Pointer to **string** | The ID of the default cascading option. | [optional] 
**Type** | **string** |  | 
**OptionIds** | **[]string** | The list of IDs of the default options. | 

## Methods

### NewCustomFieldContextDefaultValue

`func NewCustomFieldContextDefaultValue(contextId string, optionId string, type_ string, optionIds []string, ) *CustomFieldContextDefaultValue`

NewCustomFieldContextDefaultValue instantiates a new CustomFieldContextDefaultValue object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldContextDefaultValueWithDefaults

`func NewCustomFieldContextDefaultValueWithDefaults() *CustomFieldContextDefaultValue`

NewCustomFieldContextDefaultValueWithDefaults instantiates a new CustomFieldContextDefaultValue object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContextId

`func (o *CustomFieldContextDefaultValue) GetContextId() string`

GetContextId returns the ContextId field if non-nil, zero value otherwise.

### GetContextIdOk

`func (o *CustomFieldContextDefaultValue) GetContextIdOk() (*string, bool)`

GetContextIdOk returns a tuple with the ContextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextId

`func (o *CustomFieldContextDefaultValue) SetContextId(v string)`

SetContextId sets ContextId field to given value.


### GetOptionId

`func (o *CustomFieldContextDefaultValue) GetOptionId() string`

GetOptionId returns the OptionId field if non-nil, zero value otherwise.

### GetOptionIdOk

`func (o *CustomFieldContextDefaultValue) GetOptionIdOk() (*string, bool)`

GetOptionIdOk returns a tuple with the OptionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptionId

`func (o *CustomFieldContextDefaultValue) SetOptionId(v string)`

SetOptionId sets OptionId field to given value.


### GetCascadingOptionId

`func (o *CustomFieldContextDefaultValue) GetCascadingOptionId() string`

GetCascadingOptionId returns the CascadingOptionId field if non-nil, zero value otherwise.

### GetCascadingOptionIdOk

`func (o *CustomFieldContextDefaultValue) GetCascadingOptionIdOk() (*string, bool)`

GetCascadingOptionIdOk returns a tuple with the CascadingOptionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCascadingOptionId

`func (o *CustomFieldContextDefaultValue) SetCascadingOptionId(v string)`

SetCascadingOptionId sets CascadingOptionId field to given value.

### HasCascadingOptionId

`func (o *CustomFieldContextDefaultValue) HasCascadingOptionId() bool`

HasCascadingOptionId returns a boolean if a field has been set.

### GetType

`func (o *CustomFieldContextDefaultValue) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CustomFieldContextDefaultValue) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CustomFieldContextDefaultValue) SetType(v string)`

SetType sets Type field to given value.


### GetOptionIds

`func (o *CustomFieldContextDefaultValue) GetOptionIds() []string`

GetOptionIds returns the OptionIds field if non-nil, zero value otherwise.

### GetOptionIdsOk

`func (o *CustomFieldContextDefaultValue) GetOptionIdsOk() (*[]string, bool)`

GetOptionIdsOk returns a tuple with the OptionIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptionIds

`func (o *CustomFieldContextDefaultValue) SetOptionIds(v []string)`

SetOptionIds sets OptionIds field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



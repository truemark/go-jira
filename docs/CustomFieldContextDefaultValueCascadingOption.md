# CustomFieldContextDefaultValueCascadingOption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContextId** | **string** | The ID of the context. | 
**OptionId** | **string** | The ID of the default option. | 
**CascadingOptionId** | Pointer to **string** | The ID of the default cascading option. | [optional] 
**Type** | **string** |  | 

## Methods

### NewCustomFieldContextDefaultValueCascadingOption

`func NewCustomFieldContextDefaultValueCascadingOption(contextId string, optionId string, type_ string, ) *CustomFieldContextDefaultValueCascadingOption`

NewCustomFieldContextDefaultValueCascadingOption instantiates a new CustomFieldContextDefaultValueCascadingOption object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldContextDefaultValueCascadingOptionWithDefaults

`func NewCustomFieldContextDefaultValueCascadingOptionWithDefaults() *CustomFieldContextDefaultValueCascadingOption`

NewCustomFieldContextDefaultValueCascadingOptionWithDefaults instantiates a new CustomFieldContextDefaultValueCascadingOption object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContextId

`func (o *CustomFieldContextDefaultValueCascadingOption) GetContextId() string`

GetContextId returns the ContextId field if non-nil, zero value otherwise.

### GetContextIdOk

`func (o *CustomFieldContextDefaultValueCascadingOption) GetContextIdOk() (*string, bool)`

GetContextIdOk returns a tuple with the ContextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextId

`func (o *CustomFieldContextDefaultValueCascadingOption) SetContextId(v string)`

SetContextId sets ContextId field to given value.


### GetOptionId

`func (o *CustomFieldContextDefaultValueCascadingOption) GetOptionId() string`

GetOptionId returns the OptionId field if non-nil, zero value otherwise.

### GetOptionIdOk

`func (o *CustomFieldContextDefaultValueCascadingOption) GetOptionIdOk() (*string, bool)`

GetOptionIdOk returns a tuple with the OptionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptionId

`func (o *CustomFieldContextDefaultValueCascadingOption) SetOptionId(v string)`

SetOptionId sets OptionId field to given value.


### GetCascadingOptionId

`func (o *CustomFieldContextDefaultValueCascadingOption) GetCascadingOptionId() string`

GetCascadingOptionId returns the CascadingOptionId field if non-nil, zero value otherwise.

### GetCascadingOptionIdOk

`func (o *CustomFieldContextDefaultValueCascadingOption) GetCascadingOptionIdOk() (*string, bool)`

GetCascadingOptionIdOk returns a tuple with the CascadingOptionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCascadingOptionId

`func (o *CustomFieldContextDefaultValueCascadingOption) SetCascadingOptionId(v string)`

SetCascadingOptionId sets CascadingOptionId field to given value.

### HasCascadingOptionId

`func (o *CustomFieldContextDefaultValueCascadingOption) HasCascadingOptionId() bool`

HasCascadingOptionId returns a boolean if a field has been set.

### GetType

`func (o *CustomFieldContextDefaultValueCascadingOption) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CustomFieldContextDefaultValueCascadingOption) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CustomFieldContextDefaultValueCascadingOption) SetType(v string)`

SetType sets Type field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



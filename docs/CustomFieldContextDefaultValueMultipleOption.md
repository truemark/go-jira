# CustomFieldContextDefaultValueMultipleOption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContextId** | **string** | The ID of the context. | 
**OptionIds** | **[]string** | The list of IDs of the default options. | 
**Type** | **string** |  | 

## Methods

### NewCustomFieldContextDefaultValueMultipleOption

`func NewCustomFieldContextDefaultValueMultipleOption(contextId string, optionIds []string, type_ string, ) *CustomFieldContextDefaultValueMultipleOption`

NewCustomFieldContextDefaultValueMultipleOption instantiates a new CustomFieldContextDefaultValueMultipleOption object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldContextDefaultValueMultipleOptionWithDefaults

`func NewCustomFieldContextDefaultValueMultipleOptionWithDefaults() *CustomFieldContextDefaultValueMultipleOption`

NewCustomFieldContextDefaultValueMultipleOptionWithDefaults instantiates a new CustomFieldContextDefaultValueMultipleOption object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContextId

`func (o *CustomFieldContextDefaultValueMultipleOption) GetContextId() string`

GetContextId returns the ContextId field if non-nil, zero value otherwise.

### GetContextIdOk

`func (o *CustomFieldContextDefaultValueMultipleOption) GetContextIdOk() (*string, bool)`

GetContextIdOk returns a tuple with the ContextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextId

`func (o *CustomFieldContextDefaultValueMultipleOption) SetContextId(v string)`

SetContextId sets ContextId field to given value.


### GetOptionIds

`func (o *CustomFieldContextDefaultValueMultipleOption) GetOptionIds() []string`

GetOptionIds returns the OptionIds field if non-nil, zero value otherwise.

### GetOptionIdsOk

`func (o *CustomFieldContextDefaultValueMultipleOption) GetOptionIdsOk() (*[]string, bool)`

GetOptionIdsOk returns a tuple with the OptionIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptionIds

`func (o *CustomFieldContextDefaultValueMultipleOption) SetOptionIds(v []string)`

SetOptionIds sets OptionIds field to given value.


### GetType

`func (o *CustomFieldContextDefaultValueMultipleOption) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CustomFieldContextDefaultValueMultipleOption) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CustomFieldContextDefaultValueMultipleOption) SetType(v string)`

SetType sets Type field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



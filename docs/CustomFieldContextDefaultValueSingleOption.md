# CustomFieldContextDefaultValueSingleOption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContextId** | **string** | The ID of the context. | 
**OptionId** | **string** | The ID of the default option. | 
**Type** | **string** |  | 

## Methods

### NewCustomFieldContextDefaultValueSingleOption

`func NewCustomFieldContextDefaultValueSingleOption(contextId string, optionId string, type_ string, ) *CustomFieldContextDefaultValueSingleOption`

NewCustomFieldContextDefaultValueSingleOption instantiates a new CustomFieldContextDefaultValueSingleOption object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldContextDefaultValueSingleOptionWithDefaults

`func NewCustomFieldContextDefaultValueSingleOptionWithDefaults() *CustomFieldContextDefaultValueSingleOption`

NewCustomFieldContextDefaultValueSingleOptionWithDefaults instantiates a new CustomFieldContextDefaultValueSingleOption object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContextId

`func (o *CustomFieldContextDefaultValueSingleOption) GetContextId() string`

GetContextId returns the ContextId field if non-nil, zero value otherwise.

### GetContextIdOk

`func (o *CustomFieldContextDefaultValueSingleOption) GetContextIdOk() (*string, bool)`

GetContextIdOk returns a tuple with the ContextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextId

`func (o *CustomFieldContextDefaultValueSingleOption) SetContextId(v string)`

SetContextId sets ContextId field to given value.


### GetOptionId

`func (o *CustomFieldContextDefaultValueSingleOption) GetOptionId() string`

GetOptionId returns the OptionId field if non-nil, zero value otherwise.

### GetOptionIdOk

`func (o *CustomFieldContextDefaultValueSingleOption) GetOptionIdOk() (*string, bool)`

GetOptionIdOk returns a tuple with the OptionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptionId

`func (o *CustomFieldContextDefaultValueSingleOption) SetOptionId(v string)`

SetOptionId sets OptionId field to given value.


### GetType

`func (o *CustomFieldContextDefaultValueSingleOption) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CustomFieldContextDefaultValueSingleOption) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CustomFieldContextDefaultValueSingleOption) SetType(v string)`

SetType sets Type field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



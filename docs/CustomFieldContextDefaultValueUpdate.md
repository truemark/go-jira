# CustomFieldContextDefaultValueUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DefaultValues** | Pointer to [**[]CustomFieldContextDefaultValue**](CustomFieldContextDefaultValue.md) |  | [optional] 

## Methods

### NewCustomFieldContextDefaultValueUpdate

`func NewCustomFieldContextDefaultValueUpdate() *CustomFieldContextDefaultValueUpdate`

NewCustomFieldContextDefaultValueUpdate instantiates a new CustomFieldContextDefaultValueUpdate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldContextDefaultValueUpdateWithDefaults

`func NewCustomFieldContextDefaultValueUpdateWithDefaults() *CustomFieldContextDefaultValueUpdate`

NewCustomFieldContextDefaultValueUpdateWithDefaults instantiates a new CustomFieldContextDefaultValueUpdate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDefaultValues

`func (o *CustomFieldContextDefaultValueUpdate) GetDefaultValues() []CustomFieldContextDefaultValue`

GetDefaultValues returns the DefaultValues field if non-nil, zero value otherwise.

### GetDefaultValuesOk

`func (o *CustomFieldContextDefaultValueUpdate) GetDefaultValuesOk() (*[]CustomFieldContextDefaultValue, bool)`

GetDefaultValuesOk returns a tuple with the DefaultValues field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultValues

`func (o *CustomFieldContextDefaultValueUpdate) SetDefaultValues(v []CustomFieldContextDefaultValue)`

SetDefaultValues sets DefaultValues field to given value.

### HasDefaultValues

`func (o *CustomFieldContextDefaultValueUpdate) HasDefaultValues() bool`

HasDefaultValues returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



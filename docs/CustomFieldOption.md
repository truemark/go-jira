# CustomFieldOption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | Pointer to **string** | The URL of these custom field option details. | [optional] [readonly] 
**Value** | Pointer to **string** | The value of the custom field option. | [optional] [readonly] 

## Methods

### NewCustomFieldOption

`func NewCustomFieldOption() *CustomFieldOption`

NewCustomFieldOption instantiates a new CustomFieldOption object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldOptionWithDefaults

`func NewCustomFieldOptionWithDefaults() *CustomFieldOption`

NewCustomFieldOptionWithDefaults instantiates a new CustomFieldOption object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *CustomFieldOption) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *CustomFieldOption) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *CustomFieldOption) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *CustomFieldOption) HasSelf() bool`

HasSelf returns a boolean if a field has been set.

### GetValue

`func (o *CustomFieldOption) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *CustomFieldOption) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *CustomFieldOption) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *CustomFieldOption) HasValue() bool`

HasValue returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# CustomFieldUpdatedContextOptionsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Options** | Pointer to [**[]CustomFieldOptionUpdate**](CustomFieldOptionUpdate.md) | The updated custom field options. | [optional] 

## Methods

### NewCustomFieldUpdatedContextOptionsList

`func NewCustomFieldUpdatedContextOptionsList() *CustomFieldUpdatedContextOptionsList`

NewCustomFieldUpdatedContextOptionsList instantiates a new CustomFieldUpdatedContextOptionsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomFieldUpdatedContextOptionsListWithDefaults

`func NewCustomFieldUpdatedContextOptionsListWithDefaults() *CustomFieldUpdatedContextOptionsList`

NewCustomFieldUpdatedContextOptionsListWithDefaults instantiates a new CustomFieldUpdatedContextOptionsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOptions

`func (o *CustomFieldUpdatedContextOptionsList) GetOptions() []CustomFieldOptionUpdate`

GetOptions returns the Options field if non-nil, zero value otherwise.

### GetOptionsOk

`func (o *CustomFieldUpdatedContextOptionsList) GetOptionsOk() (*[]CustomFieldOptionUpdate, bool)`

GetOptionsOk returns a tuple with the Options field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptions

`func (o *CustomFieldUpdatedContextOptionsList) SetOptions(v []CustomFieldOptionUpdate)`

SetOptions sets Options field to given value.

### HasOptions

`func (o *CustomFieldUpdatedContextOptionsList) HasOptions() bool`

HasOptions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



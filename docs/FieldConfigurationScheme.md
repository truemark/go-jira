# FieldConfigurationScheme

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of the field configuration scheme. | 
**Name** | **string** | The name of the field configuration scheme. | 
**Description** | Pointer to **string** | The description of the field configuration scheme. | [optional] 

## Methods

### NewFieldConfigurationScheme

`func NewFieldConfigurationScheme(id string, name string, ) *FieldConfigurationScheme`

NewFieldConfigurationScheme instantiates a new FieldConfigurationScheme object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFieldConfigurationSchemeWithDefaults

`func NewFieldConfigurationSchemeWithDefaults() *FieldConfigurationScheme`

NewFieldConfigurationSchemeWithDefaults instantiates a new FieldConfigurationScheme object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *FieldConfigurationScheme) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *FieldConfigurationScheme) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *FieldConfigurationScheme) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *FieldConfigurationScheme) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *FieldConfigurationScheme) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *FieldConfigurationScheme) SetName(v string)`

SetName sets Name field to given value.


### GetDescription

`func (o *FieldConfigurationScheme) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *FieldConfigurationScheme) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *FieldConfigurationScheme) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *FieldConfigurationScheme) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



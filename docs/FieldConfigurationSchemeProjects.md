# FieldConfigurationSchemeProjects

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FieldConfigurationScheme** | Pointer to [**FieldConfigurationScheme**](FieldConfigurationScheme.md) |  | [optional] 
**ProjectIds** | **[]string** | The IDs of projects using the field configuration scheme. | 

## Methods

### NewFieldConfigurationSchemeProjects

`func NewFieldConfigurationSchemeProjects(projectIds []string, ) *FieldConfigurationSchemeProjects`

NewFieldConfigurationSchemeProjects instantiates a new FieldConfigurationSchemeProjects object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFieldConfigurationSchemeProjectsWithDefaults

`func NewFieldConfigurationSchemeProjectsWithDefaults() *FieldConfigurationSchemeProjects`

NewFieldConfigurationSchemeProjectsWithDefaults instantiates a new FieldConfigurationSchemeProjects object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFieldConfigurationScheme

`func (o *FieldConfigurationSchemeProjects) GetFieldConfigurationScheme() FieldConfigurationScheme`

GetFieldConfigurationScheme returns the FieldConfigurationScheme field if non-nil, zero value otherwise.

### GetFieldConfigurationSchemeOk

`func (o *FieldConfigurationSchemeProjects) GetFieldConfigurationSchemeOk() (*FieldConfigurationScheme, bool)`

GetFieldConfigurationSchemeOk returns a tuple with the FieldConfigurationScheme field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFieldConfigurationScheme

`func (o *FieldConfigurationSchemeProjects) SetFieldConfigurationScheme(v FieldConfigurationScheme)`

SetFieldConfigurationScheme sets FieldConfigurationScheme field to given value.

### HasFieldConfigurationScheme

`func (o *FieldConfigurationSchemeProjects) HasFieldConfigurationScheme() bool`

HasFieldConfigurationScheme returns a boolean if a field has been set.

### GetProjectIds

`func (o *FieldConfigurationSchemeProjects) GetProjectIds() []string`

GetProjectIds returns the ProjectIds field if non-nil, zero value otherwise.

### GetProjectIdsOk

`func (o *FieldConfigurationSchemeProjects) GetProjectIdsOk() (*[]string, bool)`

GetProjectIdsOk returns a tuple with the ProjectIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectIds

`func (o *FieldConfigurationSchemeProjects) SetProjectIds(v []string)`

SetProjectIds sets ProjectIds field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



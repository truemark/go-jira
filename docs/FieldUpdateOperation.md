# FieldUpdateOperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Add** | Pointer to **interface{}** | A map containing the name of a field and the value to add to it. | [optional] 
**Set** | Pointer to **interface{}** | A map containing the name of a field and the value to set in it. | [optional] 
**Remove** | Pointer to **interface{}** | A map containing the name of a field and the value to removed from it. | [optional] 
**Edit** | Pointer to **interface{}** | A map containing the name of a field and the value to edit in it. | [optional] 

## Methods

### NewFieldUpdateOperation

`func NewFieldUpdateOperation() *FieldUpdateOperation`

NewFieldUpdateOperation instantiates a new FieldUpdateOperation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFieldUpdateOperationWithDefaults

`func NewFieldUpdateOperationWithDefaults() *FieldUpdateOperation`

NewFieldUpdateOperationWithDefaults instantiates a new FieldUpdateOperation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAdd

`func (o *FieldUpdateOperation) GetAdd() interface{}`

GetAdd returns the Add field if non-nil, zero value otherwise.

### GetAddOk

`func (o *FieldUpdateOperation) GetAddOk() (*interface{}, bool)`

GetAddOk returns a tuple with the Add field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdd

`func (o *FieldUpdateOperation) SetAdd(v interface{})`

SetAdd sets Add field to given value.

### HasAdd

`func (o *FieldUpdateOperation) HasAdd() bool`

HasAdd returns a boolean if a field has been set.

### SetAddNil

`func (o *FieldUpdateOperation) SetAddNil(b bool)`

 SetAddNil sets the value for Add to be an explicit nil

### UnsetAdd
`func (o *FieldUpdateOperation) UnsetAdd()`

UnsetAdd ensures that no value is present for Add, not even an explicit nil
### GetSet

`func (o *FieldUpdateOperation) GetSet() interface{}`

GetSet returns the Set field if non-nil, zero value otherwise.

### GetSetOk

`func (o *FieldUpdateOperation) GetSetOk() (*interface{}, bool)`

GetSetOk returns a tuple with the Set field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSet

`func (o *FieldUpdateOperation) SetSet(v interface{})`

SetSet sets Set field to given value.

### HasSet

`func (o *FieldUpdateOperation) HasSet() bool`

HasSet returns a boolean if a field has been set.

### SetSetNil

`func (o *FieldUpdateOperation) SetSetNil(b bool)`

 SetSetNil sets the value for Set to be an explicit nil

### UnsetSet
`func (o *FieldUpdateOperation) UnsetSet()`

UnsetSet ensures that no value is present for Set, not even an explicit nil
### GetRemove

`func (o *FieldUpdateOperation) GetRemove() interface{}`

GetRemove returns the Remove field if non-nil, zero value otherwise.

### GetRemoveOk

`func (o *FieldUpdateOperation) GetRemoveOk() (*interface{}, bool)`

GetRemoveOk returns a tuple with the Remove field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemove

`func (o *FieldUpdateOperation) SetRemove(v interface{})`

SetRemove sets Remove field to given value.

### HasRemove

`func (o *FieldUpdateOperation) HasRemove() bool`

HasRemove returns a boolean if a field has been set.

### SetRemoveNil

`func (o *FieldUpdateOperation) SetRemoveNil(b bool)`

 SetRemoveNil sets the value for Remove to be an explicit nil

### UnsetRemove
`func (o *FieldUpdateOperation) UnsetRemove()`

UnsetRemove ensures that no value is present for Remove, not even an explicit nil
### GetEdit

`func (o *FieldUpdateOperation) GetEdit() interface{}`

GetEdit returns the Edit field if non-nil, zero value otherwise.

### GetEditOk

`func (o *FieldUpdateOperation) GetEditOk() (*interface{}, bool)`

GetEditOk returns a tuple with the Edit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEdit

`func (o *FieldUpdateOperation) SetEdit(v interface{})`

SetEdit sets Edit field to given value.

### HasEdit

`func (o *FieldUpdateOperation) HasEdit() bool`

HasEdit returns a boolean if a field has been set.

### SetEditNil

`func (o *FieldUpdateOperation) SetEditNil(b bool)`

 SetEditNil sets the value for Edit to be an explicit nil

### UnsetEdit
`func (o *FieldUpdateOperation) UnsetEdit()`

UnsetEdit ensures that no value is present for Edit, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



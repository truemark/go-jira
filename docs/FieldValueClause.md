# FieldValueClause

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Field** | [**JqlQueryField**](JqlQueryField.md) |  | 
**Operator** | **string** | The operator between the field and operand. | 
**Operand** | [**JqlQueryClauseOperand**](JqlQueryClauseOperand.md) |  | 

## Methods

### NewFieldValueClause

`func NewFieldValueClause(field JqlQueryField, operator string, operand JqlQueryClauseOperand, ) *FieldValueClause`

NewFieldValueClause instantiates a new FieldValueClause object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFieldValueClauseWithDefaults

`func NewFieldValueClauseWithDefaults() *FieldValueClause`

NewFieldValueClauseWithDefaults instantiates a new FieldValueClause object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetField

`func (o *FieldValueClause) GetField() JqlQueryField`

GetField returns the Field field if non-nil, zero value otherwise.

### GetFieldOk

`func (o *FieldValueClause) GetFieldOk() (*JqlQueryField, bool)`

GetFieldOk returns a tuple with the Field field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetField

`func (o *FieldValueClause) SetField(v JqlQueryField)`

SetField sets Field field to given value.


### GetOperator

`func (o *FieldValueClause) GetOperator() string`

GetOperator returns the Operator field if non-nil, zero value otherwise.

### GetOperatorOk

`func (o *FieldValueClause) GetOperatorOk() (*string, bool)`

GetOperatorOk returns a tuple with the Operator field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperator

`func (o *FieldValueClause) SetOperator(v string)`

SetOperator sets Operator field to given value.


### GetOperand

`func (o *FieldValueClause) GetOperand() JqlQueryClauseOperand`

GetOperand returns the Operand field if non-nil, zero value otherwise.

### GetOperandOk

`func (o *FieldValueClause) GetOperandOk() (*JqlQueryClauseOperand, bool)`

GetOperandOk returns a tuple with the Operand field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperand

`func (o *FieldValueClause) SetOperand(v JqlQueryClauseOperand)`

SetOperand sets Operand field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



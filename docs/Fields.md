# Fields

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Summary** | Pointer to **string** | The summary description of the linked issue. | [optional] [readonly] 
**Status** | Pointer to [**StatusDetails**](StatusDetails.md) | The status of the linked issue. | [optional] [readonly] 
**Priority** | Pointer to [**Priority**](Priority.md) | The priority of the linked issue. | [optional] [readonly] 
**Assignee** | Pointer to [**UserDetails**](UserDetails.md) | The assignee of the linked issue. | [optional] [readonly] 
**Timetracking** | Pointer to [**TimeTrackingDetails**](TimeTrackingDetails.md) | The time tracking of the linked issue. | [optional] [readonly] 
**Issuetype** | Pointer to [**IssueTypeDetails**](IssueTypeDetails.md) |  | [optional] 
**IssueType** | Pointer to [**IssueTypeDetails**](IssueTypeDetails.md) | The type of the linked issue. | [optional] [readonly] 

## Methods

### NewFields

`func NewFields() *Fields`

NewFields instantiates a new Fields object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFieldsWithDefaults

`func NewFieldsWithDefaults() *Fields`

NewFieldsWithDefaults instantiates a new Fields object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSummary

`func (o *Fields) GetSummary() string`

GetSummary returns the Summary field if non-nil, zero value otherwise.

### GetSummaryOk

`func (o *Fields) GetSummaryOk() (*string, bool)`

GetSummaryOk returns a tuple with the Summary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSummary

`func (o *Fields) SetSummary(v string)`

SetSummary sets Summary field to given value.

### HasSummary

`func (o *Fields) HasSummary() bool`

HasSummary returns a boolean if a field has been set.

### GetStatus

`func (o *Fields) GetStatus() StatusDetails`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Fields) GetStatusOk() (*StatusDetails, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Fields) SetStatus(v StatusDetails)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *Fields) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetPriority

`func (o *Fields) GetPriority() Priority`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *Fields) GetPriorityOk() (*Priority, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *Fields) SetPriority(v Priority)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *Fields) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetAssignee

`func (o *Fields) GetAssignee() UserDetails`

GetAssignee returns the Assignee field if non-nil, zero value otherwise.

### GetAssigneeOk

`func (o *Fields) GetAssigneeOk() (*UserDetails, bool)`

GetAssigneeOk returns a tuple with the Assignee field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssignee

`func (o *Fields) SetAssignee(v UserDetails)`

SetAssignee sets Assignee field to given value.

### HasAssignee

`func (o *Fields) HasAssignee() bool`

HasAssignee returns a boolean if a field has been set.

### GetTimetracking

`func (o *Fields) GetTimetracking() TimeTrackingDetails`

GetTimetracking returns the Timetracking field if non-nil, zero value otherwise.

### GetTimetrackingOk

`func (o *Fields) GetTimetrackingOk() (*TimeTrackingDetails, bool)`

GetTimetrackingOk returns a tuple with the Timetracking field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimetracking

`func (o *Fields) SetTimetracking(v TimeTrackingDetails)`

SetTimetracking sets Timetracking field to given value.

### HasTimetracking

`func (o *Fields) HasTimetracking() bool`

HasTimetracking returns a boolean if a field has been set.

### GetIssuetype

`func (o *Fields) GetIssuetype() IssueTypeDetails`

GetIssuetype returns the Issuetype field if non-nil, zero value otherwise.

### GetIssuetypeOk

`func (o *Fields) GetIssuetypeOk() (*IssueTypeDetails, bool)`

GetIssuetypeOk returns a tuple with the Issuetype field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssuetype

`func (o *Fields) SetIssuetype(v IssueTypeDetails)`

SetIssuetype sets Issuetype field to given value.

### HasIssuetype

`func (o *Fields) HasIssuetype() bool`

HasIssuetype returns a boolean if a field has been set.

### GetIssueType

`func (o *Fields) GetIssueType() IssueTypeDetails`

GetIssueType returns the IssueType field if non-nil, zero value otherwise.

### GetIssueTypeOk

`func (o *Fields) GetIssueTypeOk() (*IssueTypeDetails, bool)`

GetIssueTypeOk returns a tuple with the IssueType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueType

`func (o *Fields) SetIssueType(v IssueTypeDetails)`

SetIssueType sets IssueType field to given value.

### HasIssueType

`func (o *Fields) HasIssueType() bool`

HasIssueType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# FoundGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | The name of the group. | [optional] 
**Html** | Pointer to **string** | The group name with the matched query string highlighted with the HTML bold tag. | [optional] 
**Labels** | Pointer to [**[]GroupLabel**](GroupLabel.md) |  | [optional] 
**GroupId** | Pointer to **string** | The ID of the group, if available, which uniquely identifies the group across all Atlassian products. For example, *952d12c3-5b5b-4d04-bb32-44d383afc4b2*. | [optional] 

## Methods

### NewFoundGroup

`func NewFoundGroup() *FoundGroup`

NewFoundGroup instantiates a new FoundGroup object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFoundGroupWithDefaults

`func NewFoundGroupWithDefaults() *FoundGroup`

NewFoundGroupWithDefaults instantiates a new FoundGroup object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *FoundGroup) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *FoundGroup) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *FoundGroup) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *FoundGroup) HasName() bool`

HasName returns a boolean if a field has been set.

### GetHtml

`func (o *FoundGroup) GetHtml() string`

GetHtml returns the Html field if non-nil, zero value otherwise.

### GetHtmlOk

`func (o *FoundGroup) GetHtmlOk() (*string, bool)`

GetHtmlOk returns a tuple with the Html field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHtml

`func (o *FoundGroup) SetHtml(v string)`

SetHtml sets Html field to given value.

### HasHtml

`func (o *FoundGroup) HasHtml() bool`

HasHtml returns a boolean if a field has been set.

### GetLabels

`func (o *FoundGroup) GetLabels() []GroupLabel`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *FoundGroup) GetLabelsOk() (*[]GroupLabel, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *FoundGroup) SetLabels(v []GroupLabel)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *FoundGroup) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetGroupId

`func (o *FoundGroup) GetGroupId() string`

GetGroupId returns the GroupId field if non-nil, zero value otherwise.

### GetGroupIdOk

`func (o *FoundGroup) GetGroupIdOk() (*string, bool)`

GetGroupIdOk returns a tuple with the GroupId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupId

`func (o *FoundGroup) SetGroupId(v string)`

SetGroupId sets GroupId field to given value.

### HasGroupId

`func (o *FoundGroup) HasGroupId() bool`

HasGroupId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



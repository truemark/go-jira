# GroupLabel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | Pointer to **string** | The group label name. | [optional] 
**Title** | Pointer to **string** | The title of the group label. | [optional] 
**Type** | Pointer to **string** | The type of the group label. | [optional] 

## Methods

### NewGroupLabel

`func NewGroupLabel() *GroupLabel`

NewGroupLabel instantiates a new GroupLabel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGroupLabelWithDefaults

`func NewGroupLabelWithDefaults() *GroupLabel`

NewGroupLabelWithDefaults instantiates a new GroupLabel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *GroupLabel) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *GroupLabel) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *GroupLabel) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *GroupLabel) HasText() bool`

HasText returns a boolean if a field has been set.

### GetTitle

`func (o *GroupLabel) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *GroupLabel) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *GroupLabel) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *GroupLabel) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetType

`func (o *GroupLabel) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *GroupLabel) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *GroupLabel) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *GroupLabel) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



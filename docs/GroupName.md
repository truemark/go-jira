# GroupName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | The name of group. | [optional] 
**Self** | Pointer to **string** | The URL for these group details. | [optional] [readonly] 

## Methods

### NewGroupName

`func NewGroupName() *GroupName`

NewGroupName instantiates a new GroupName object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGroupNameWithDefaults

`func NewGroupNameWithDefaults() *GroupName`

NewGroupNameWithDefaults instantiates a new GroupName object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *GroupName) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *GroupName) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *GroupName) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *GroupName) HasName() bool`

HasName returns a boolean if a field has been set.

### GetSelf

`func (o *GroupName) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *GroupName) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *GroupName) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *GroupName) HasSelf() bool`

HasSelf returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# HierarchyLevel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int64** | The ID of the hierarchy level. This property is deprecated, see [Change notice: Removing hierarchy level IDs from next-gen APIs](https://developer.atlassian.com/cloud/jira/platform/change-notice-removing-hierarchy-level-ids-from-next-gen-apis/). | [optional] 
**Name** | Pointer to **string** | The name of this hierarchy level. | [optional] 
**AboveLevelId** | Pointer to **int64** | The ID of the level above this one in the hierarchy. This property is deprecated, see [Change notice: Removing hierarchy level IDs from next-gen APIs](https://developer.atlassian.com/cloud/jira/platform/change-notice-removing-hierarchy-level-ids-from-next-gen-apis/). | [optional] 
**BelowLevelId** | Pointer to **int64** | The ID of the level below this one in the hierarchy. This property is deprecated, see [Change notice: Removing hierarchy level IDs from next-gen APIs](https://developer.atlassian.com/cloud/jira/platform/change-notice-removing-hierarchy-level-ids-from-next-gen-apis/). | [optional] 
**ProjectConfigurationId** | Pointer to **int64** | The ID of the project configuration. This property is deprecated, see [Change oticen: Removing hierarchy level IDs from next-gen APIs](https://developer.atlassian.com/cloud/jira/platform/change-notice-removing-hierarchy-level-ids-from-next-gen-apis/). | [optional] 
**Level** | Pointer to **int32** | The level of this item in the hierarchy. | [optional] 
**IssueTypeIds** | Pointer to **[]int64** | The issue types available in this hierarchy level. | [optional] 
**ExternalUuid** | Pointer to **string** | The external UUID of the hierarchy level. This property is deprecated, see [Change notice: Removing hierarchy level IDs from next-gen APIs](https://developer.atlassian.com/cloud/jira/platform/change-notice-removing-hierarchy-level-ids-from-next-gen-apis/). | [optional] 
**GlobalHierarchyLevel** | Pointer to **string** |  | [optional] 

## Methods

### NewHierarchyLevel

`func NewHierarchyLevel() *HierarchyLevel`

NewHierarchyLevel instantiates a new HierarchyLevel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHierarchyLevelWithDefaults

`func NewHierarchyLevelWithDefaults() *HierarchyLevel`

NewHierarchyLevelWithDefaults instantiates a new HierarchyLevel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *HierarchyLevel) GetId() int64`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *HierarchyLevel) GetIdOk() (*int64, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *HierarchyLevel) SetId(v int64)`

SetId sets Id field to given value.

### HasId

`func (o *HierarchyLevel) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *HierarchyLevel) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *HierarchyLevel) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *HierarchyLevel) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *HierarchyLevel) HasName() bool`

HasName returns a boolean if a field has been set.

### GetAboveLevelId

`func (o *HierarchyLevel) GetAboveLevelId() int64`

GetAboveLevelId returns the AboveLevelId field if non-nil, zero value otherwise.

### GetAboveLevelIdOk

`func (o *HierarchyLevel) GetAboveLevelIdOk() (*int64, bool)`

GetAboveLevelIdOk returns a tuple with the AboveLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAboveLevelId

`func (o *HierarchyLevel) SetAboveLevelId(v int64)`

SetAboveLevelId sets AboveLevelId field to given value.

### HasAboveLevelId

`func (o *HierarchyLevel) HasAboveLevelId() bool`

HasAboveLevelId returns a boolean if a field has been set.

### GetBelowLevelId

`func (o *HierarchyLevel) GetBelowLevelId() int64`

GetBelowLevelId returns the BelowLevelId field if non-nil, zero value otherwise.

### GetBelowLevelIdOk

`func (o *HierarchyLevel) GetBelowLevelIdOk() (*int64, bool)`

GetBelowLevelIdOk returns a tuple with the BelowLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBelowLevelId

`func (o *HierarchyLevel) SetBelowLevelId(v int64)`

SetBelowLevelId sets BelowLevelId field to given value.

### HasBelowLevelId

`func (o *HierarchyLevel) HasBelowLevelId() bool`

HasBelowLevelId returns a boolean if a field has been set.

### GetProjectConfigurationId

`func (o *HierarchyLevel) GetProjectConfigurationId() int64`

GetProjectConfigurationId returns the ProjectConfigurationId field if non-nil, zero value otherwise.

### GetProjectConfigurationIdOk

`func (o *HierarchyLevel) GetProjectConfigurationIdOk() (*int64, bool)`

GetProjectConfigurationIdOk returns a tuple with the ProjectConfigurationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectConfigurationId

`func (o *HierarchyLevel) SetProjectConfigurationId(v int64)`

SetProjectConfigurationId sets ProjectConfigurationId field to given value.

### HasProjectConfigurationId

`func (o *HierarchyLevel) HasProjectConfigurationId() bool`

HasProjectConfigurationId returns a boolean if a field has been set.

### GetLevel

`func (o *HierarchyLevel) GetLevel() int32`

GetLevel returns the Level field if non-nil, zero value otherwise.

### GetLevelOk

`func (o *HierarchyLevel) GetLevelOk() (*int32, bool)`

GetLevelOk returns a tuple with the Level field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLevel

`func (o *HierarchyLevel) SetLevel(v int32)`

SetLevel sets Level field to given value.

### HasLevel

`func (o *HierarchyLevel) HasLevel() bool`

HasLevel returns a boolean if a field has been set.

### GetIssueTypeIds

`func (o *HierarchyLevel) GetIssueTypeIds() []int64`

GetIssueTypeIds returns the IssueTypeIds field if non-nil, zero value otherwise.

### GetIssueTypeIdsOk

`func (o *HierarchyLevel) GetIssueTypeIdsOk() (*[]int64, bool)`

GetIssueTypeIdsOk returns a tuple with the IssueTypeIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeIds

`func (o *HierarchyLevel) SetIssueTypeIds(v []int64)`

SetIssueTypeIds sets IssueTypeIds field to given value.

### HasIssueTypeIds

`func (o *HierarchyLevel) HasIssueTypeIds() bool`

HasIssueTypeIds returns a boolean if a field has been set.

### GetExternalUuid

`func (o *HierarchyLevel) GetExternalUuid() string`

GetExternalUuid returns the ExternalUuid field if non-nil, zero value otherwise.

### GetExternalUuidOk

`func (o *HierarchyLevel) GetExternalUuidOk() (*string, bool)`

GetExternalUuidOk returns a tuple with the ExternalUuid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalUuid

`func (o *HierarchyLevel) SetExternalUuid(v string)`

SetExternalUuid sets ExternalUuid field to given value.

### HasExternalUuid

`func (o *HierarchyLevel) HasExternalUuid() bool`

HasExternalUuid returns a boolean if a field has been set.

### GetGlobalHierarchyLevel

`func (o *HierarchyLevel) GetGlobalHierarchyLevel() string`

GetGlobalHierarchyLevel returns the GlobalHierarchyLevel field if non-nil, zero value otherwise.

### GetGlobalHierarchyLevelOk

`func (o *HierarchyLevel) GetGlobalHierarchyLevelOk() (*string, bool)`

GetGlobalHierarchyLevelOk returns a tuple with the GlobalHierarchyLevel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGlobalHierarchyLevel

`func (o *HierarchyLevel) SetGlobalHierarchyLevel(v string)`

SetGlobalHierarchyLevel sets GlobalHierarchyLevel field to given value.

### HasGlobalHierarchyLevel

`func (o *HierarchyLevel) HasGlobalHierarchyLevel() bool`

HasGlobalHierarchyLevel returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# HistoryMetadataParticipant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | The ID of the user or system associated with a history record. | [optional] 
**DisplayName** | Pointer to **string** | The display name of the user or system associated with a history record. | [optional] 
**DisplayNameKey** | Pointer to **string** | The key of the display name of the user or system associated with a history record. | [optional] 
**Type** | Pointer to **string** | The type of the user or system associated with a history record. | [optional] 
**AvatarUrl** | Pointer to **string** | The URL to an avatar for the user or system associated with a history record. | [optional] 
**Url** | Pointer to **string** | The URL of the user or system associated with a history record. | [optional] 

## Methods

### NewHistoryMetadataParticipant

`func NewHistoryMetadataParticipant() *HistoryMetadataParticipant`

NewHistoryMetadataParticipant instantiates a new HistoryMetadataParticipant object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHistoryMetadataParticipantWithDefaults

`func NewHistoryMetadataParticipantWithDefaults() *HistoryMetadataParticipant`

NewHistoryMetadataParticipantWithDefaults instantiates a new HistoryMetadataParticipant object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *HistoryMetadataParticipant) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *HistoryMetadataParticipant) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *HistoryMetadataParticipant) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *HistoryMetadataParticipant) HasId() bool`

HasId returns a boolean if a field has been set.

### GetDisplayName

`func (o *HistoryMetadataParticipant) GetDisplayName() string`

GetDisplayName returns the DisplayName field if non-nil, zero value otherwise.

### GetDisplayNameOk

`func (o *HistoryMetadataParticipant) GetDisplayNameOk() (*string, bool)`

GetDisplayNameOk returns a tuple with the DisplayName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayName

`func (o *HistoryMetadataParticipant) SetDisplayName(v string)`

SetDisplayName sets DisplayName field to given value.

### HasDisplayName

`func (o *HistoryMetadataParticipant) HasDisplayName() bool`

HasDisplayName returns a boolean if a field has been set.

### GetDisplayNameKey

`func (o *HistoryMetadataParticipant) GetDisplayNameKey() string`

GetDisplayNameKey returns the DisplayNameKey field if non-nil, zero value otherwise.

### GetDisplayNameKeyOk

`func (o *HistoryMetadataParticipant) GetDisplayNameKeyOk() (*string, bool)`

GetDisplayNameKeyOk returns a tuple with the DisplayNameKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayNameKey

`func (o *HistoryMetadataParticipant) SetDisplayNameKey(v string)`

SetDisplayNameKey sets DisplayNameKey field to given value.

### HasDisplayNameKey

`func (o *HistoryMetadataParticipant) HasDisplayNameKey() bool`

HasDisplayNameKey returns a boolean if a field has been set.

### GetType

`func (o *HistoryMetadataParticipant) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *HistoryMetadataParticipant) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *HistoryMetadataParticipant) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *HistoryMetadataParticipant) HasType() bool`

HasType returns a boolean if a field has been set.

### GetAvatarUrl

`func (o *HistoryMetadataParticipant) GetAvatarUrl() string`

GetAvatarUrl returns the AvatarUrl field if non-nil, zero value otherwise.

### GetAvatarUrlOk

`func (o *HistoryMetadataParticipant) GetAvatarUrlOk() (*string, bool)`

GetAvatarUrlOk returns a tuple with the AvatarUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvatarUrl

`func (o *HistoryMetadataParticipant) SetAvatarUrl(v string)`

SetAvatarUrl sets AvatarUrl field to given value.

### HasAvatarUrl

`func (o *HistoryMetadataParticipant) HasAvatarUrl() bool`

HasAvatarUrl returns a boolean if a field has been set.

### GetUrl

`func (o *HistoryMetadataParticipant) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *HistoryMetadataParticipant) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *HistoryMetadataParticipant) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *HistoryMetadataParticipant) HasUrl() bool`

HasUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



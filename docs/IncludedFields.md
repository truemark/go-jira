# IncludedFields

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Excluded** | Pointer to **[]string** |  | [optional] 
**Included** | Pointer to **[]string** |  | [optional] 
**ActuallyIncluded** | Pointer to **[]string** |  | [optional] 

## Methods

### NewIncludedFields

`func NewIncludedFields() *IncludedFields`

NewIncludedFields instantiates a new IncludedFields object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIncludedFieldsWithDefaults

`func NewIncludedFieldsWithDefaults() *IncludedFields`

NewIncludedFieldsWithDefaults instantiates a new IncludedFields object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetExcluded

`func (o *IncludedFields) GetExcluded() []string`

GetExcluded returns the Excluded field if non-nil, zero value otherwise.

### GetExcludedOk

`func (o *IncludedFields) GetExcludedOk() (*[]string, bool)`

GetExcludedOk returns a tuple with the Excluded field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExcluded

`func (o *IncludedFields) SetExcluded(v []string)`

SetExcluded sets Excluded field to given value.

### HasExcluded

`func (o *IncludedFields) HasExcluded() bool`

HasExcluded returns a boolean if a field has been set.

### GetIncluded

`func (o *IncludedFields) GetIncluded() []string`

GetIncluded returns the Included field if non-nil, zero value otherwise.

### GetIncludedOk

`func (o *IncludedFields) GetIncludedOk() (*[]string, bool)`

GetIncludedOk returns a tuple with the Included field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIncluded

`func (o *IncludedFields) SetIncluded(v []string)`

SetIncluded sets Included field to given value.

### HasIncluded

`func (o *IncludedFields) HasIncluded() bool`

HasIncluded returns a boolean if a field has been set.

### GetActuallyIncluded

`func (o *IncludedFields) GetActuallyIncluded() []string`

GetActuallyIncluded returns the ActuallyIncluded field if non-nil, zero value otherwise.

### GetActuallyIncludedOk

`func (o *IncludedFields) GetActuallyIncludedOk() (*[]string, bool)`

GetActuallyIncludedOk returns a tuple with the ActuallyIncluded field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActuallyIncluded

`func (o *IncludedFields) SetActuallyIncluded(v []string)`

SetActuallyIncluded sets ActuallyIncluded field to given value.

### HasActuallyIncluded

`func (o *IncludedFields) HasActuallyIncluded() bool`

HasActuallyIncluded returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \IssueCommentPropertiesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteCommentProperty**](IssueCommentPropertiesApi.md#DeleteCommentProperty) | **Delete** /rest/api/3/comment/{commentId}/properties/{propertyKey} | Delete comment property
[**GetCommentProperty**](IssueCommentPropertiesApi.md#GetCommentProperty) | **Get** /rest/api/3/comment/{commentId}/properties/{propertyKey} | Get comment property
[**GetCommentPropertyKeys**](IssueCommentPropertiesApi.md#GetCommentPropertyKeys) | **Get** /rest/api/3/comment/{commentId}/properties | Get comment property keys
[**SetCommentProperty**](IssueCommentPropertiesApi.md#SetCommentProperty) | **Put** /rest/api/3/comment/{commentId}/properties/{propertyKey} | Set comment property



## DeleteCommentProperty

> DeleteCommentProperty(ctx, commentId, propertyKey).Execute()

Delete comment property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    commentId := "commentId_example" // string | The ID of the comment.
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCommentPropertiesApi.DeleteCommentProperty(context.Background(), commentId, propertyKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCommentPropertiesApi.DeleteCommentProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**commentId** | **string** | The ID of the comment. | 
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteCommentPropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCommentProperty

> EntityProperty GetCommentProperty(ctx, commentId, propertyKey).Execute()

Get comment property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    commentId := "commentId_example" // string | The ID of the comment.
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCommentPropertiesApi.GetCommentProperty(context.Background(), commentId, propertyKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCommentPropertiesApi.GetCommentProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCommentProperty`: EntityProperty
    fmt.Fprintf(os.Stdout, "Response from `IssueCommentPropertiesApi.GetCommentProperty`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**commentId** | **string** | The ID of the comment. | 
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCommentPropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**EntityProperty**](EntityProperty.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCommentPropertyKeys

> PropertyKeys GetCommentPropertyKeys(ctx, commentId).Execute()

Get comment property keys



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    commentId := "commentId_example" // string | The ID of the comment.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCommentPropertiesApi.GetCommentPropertyKeys(context.Background(), commentId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCommentPropertiesApi.GetCommentPropertyKeys``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCommentPropertyKeys`: PropertyKeys
    fmt.Fprintf(os.Stdout, "Response from `IssueCommentPropertiesApi.GetCommentPropertyKeys`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**commentId** | **string** | The ID of the comment. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCommentPropertyKeysRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**PropertyKeys**](PropertyKeys.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetCommentProperty

> interface{} SetCommentProperty(ctx, commentId, propertyKey).Body(body).Execute()

Set comment property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    body := interface{}(987) // interface{} | 
    commentId := "commentId_example" // string | The ID of the comment.
    propertyKey := "propertyKey_example" // string | The key of the property. The maximum length is 255 characters.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCommentPropertiesApi.SetCommentProperty(context.Background(), commentId, propertyKey).Body(body).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCommentPropertiesApi.SetCommentProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetCommentProperty`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCommentPropertiesApi.SetCommentProperty`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**commentId** | **string** | The ID of the comment. | 
**propertyKey** | **string** | The key of the property. The maximum length is 255 characters. | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetCommentPropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **interface{}** |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


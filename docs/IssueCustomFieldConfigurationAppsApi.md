# \IssueCustomFieldConfigurationAppsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetCustomFieldConfiguration**](IssueCustomFieldConfigurationAppsApi.md#GetCustomFieldConfiguration) | **Get** /rest/api/3/app/field/{fieldIdOrKey}/context/configuration | Get custom field configurations
[**UpdateCustomFieldConfiguration**](IssueCustomFieldConfigurationAppsApi.md#UpdateCustomFieldConfiguration) | **Put** /rest/api/3/app/field/{fieldIdOrKey}/context/configuration | Update custom field configurations



## GetCustomFieldConfiguration

> PageBeanContextualConfiguration GetCustomFieldConfiguration(ctx, fieldIdOrKey).ContextId(contextId).IssueId(issueId).StartAt(startAt).MaxResults(maxResults).Execute()

Get custom field configurations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldIdOrKey := "fieldIdOrKey_example" // string | The ID or key of the custom field, for example `customfield_10000`.
    contextId := []int64{int64(123)} // []int64 | The list of context IDs. To include multiple contexts, separate IDs with an ampersand: `contextId=10000&contextId=10001`. Either this or `issueId` can be provided, but not both. (optional)
    issueId := int64(789) // int64 | The ID of the issue to filter results by. If the issue doesn't exist, an empty list is returned. Either this or `contextIds` can be provided, but not both. (optional)
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 100)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldConfigurationAppsApi.GetCustomFieldConfiguration(context.Background(), fieldIdOrKey).ContextId(contextId).IssueId(issueId).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldConfigurationAppsApi.GetCustomFieldConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCustomFieldConfiguration`: PageBeanContextualConfiguration
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldConfigurationAppsApi.GetCustomFieldConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldIdOrKey** | **string** | The ID or key of the custom field, for example &#x60;customfield_10000&#x60;. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCustomFieldConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contextId** | **[]int64** | The list of context IDs. To include multiple contexts, separate IDs with an ampersand: &#x60;contextId&#x3D;10000&amp;contextId&#x3D;10001&#x60;. Either this or &#x60;issueId&#x60; can be provided, but not both. | 
 **issueId** | **int64** | The ID of the issue to filter results by. If the issue doesn&#39;t exist, an empty list is returned. Either this or &#x60;contextIds&#x60; can be provided, but not both. | 
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 100]

### Return type

[**PageBeanContextualConfiguration**](PageBeanContextualConfiguration.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCustomFieldConfiguration

> interface{} UpdateCustomFieldConfiguration(ctx, fieldIdOrKey).CustomFieldConfigurations(customFieldConfigurations).Execute()

Update custom field configurations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    customFieldConfigurations := *openapiclient.NewCustomFieldConfigurations([]openapiclient.ContextualConfiguration{*openapiclient.NewContextualConfiguration(int64(123))}) // CustomFieldConfigurations | 
    fieldIdOrKey := "fieldIdOrKey_example" // string | The ID or key of the custom field, for example `customfield_10000`.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldConfigurationAppsApi.UpdateCustomFieldConfiguration(context.Background(), fieldIdOrKey).CustomFieldConfigurations(customFieldConfigurations).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldConfigurationAppsApi.UpdateCustomFieldConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateCustomFieldConfiguration`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldConfigurationAppsApi.UpdateCustomFieldConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldIdOrKey** | **string** | The ID or key of the custom field, for example &#x60;customfield_10000&#x60;. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateCustomFieldConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldConfigurations** | [**CustomFieldConfigurations**](CustomFieldConfigurations.md) |  | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


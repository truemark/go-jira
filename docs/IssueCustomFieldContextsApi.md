# \IssueCustomFieldContextsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddIssueTypesToContext**](IssueCustomFieldContextsApi.md#AddIssueTypesToContext) | **Put** /rest/api/3/field/{fieldId}/context/{contextId}/issuetype | Add issue types to context
[**AssignProjectsToCustomFieldContext**](IssueCustomFieldContextsApi.md#AssignProjectsToCustomFieldContext) | **Put** /rest/api/3/field/{fieldId}/context/{contextId}/project | Assign custom field context to projects
[**CreateCustomFieldContext**](IssueCustomFieldContextsApi.md#CreateCustomFieldContext) | **Post** /rest/api/3/field/{fieldId}/context | Create custom field context
[**DeleteCustomFieldContext**](IssueCustomFieldContextsApi.md#DeleteCustomFieldContext) | **Delete** /rest/api/3/field/{fieldId}/context/{contextId} | Delete custom field context
[**GetContextsForField**](IssueCustomFieldContextsApi.md#GetContextsForField) | **Get** /rest/api/3/field/{fieldId}/context | Get custom field contexts
[**GetCustomFieldContextsForProjectsAndIssueTypes**](IssueCustomFieldContextsApi.md#GetCustomFieldContextsForProjectsAndIssueTypes) | **Post** /rest/api/3/field/{fieldId}/context/mapping | Get custom field contexts for projects and issue types
[**GetDefaultValues**](IssueCustomFieldContextsApi.md#GetDefaultValues) | **Get** /rest/api/3/field/{fieldId}/context/defaultValue | Get custom field contexts default values
[**GetIssueTypeMappingsForContexts**](IssueCustomFieldContextsApi.md#GetIssueTypeMappingsForContexts) | **Get** /rest/api/3/field/{fieldId}/context/issuetypemapping | Get issue types for custom field context
[**GetProjectContextMapping**](IssueCustomFieldContextsApi.md#GetProjectContextMapping) | **Get** /rest/api/3/field/{fieldId}/context/projectmapping | Get project mappings for custom field context
[**RemoveCustomFieldContextFromProjects**](IssueCustomFieldContextsApi.md#RemoveCustomFieldContextFromProjects) | **Post** /rest/api/3/field/{fieldId}/context/{contextId}/project/remove | Remove custom field context from projects
[**RemoveIssueTypesFromContext**](IssueCustomFieldContextsApi.md#RemoveIssueTypesFromContext) | **Post** /rest/api/3/field/{fieldId}/context/{contextId}/issuetype/remove | Remove issue types from context
[**SetDefaultValues**](IssueCustomFieldContextsApi.md#SetDefaultValues) | **Put** /rest/api/3/field/{fieldId}/context/defaultValue | Set custom field contexts default values
[**UpdateCustomFieldContext**](IssueCustomFieldContextsApi.md#UpdateCustomFieldContext) | **Put** /rest/api/3/field/{fieldId}/context/{contextId} | Update custom field context



## AddIssueTypesToContext

> interface{} AddIssueTypesToContext(ctx, fieldId, contextId).IssueTypeIds(issueTypeIds).Execute()

Add issue types to context



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueTypeIds := *openapiclient.NewIssueTypeIds([]string{"IssueTypeIds_example"}) // IssueTypeIds | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.AddIssueTypesToContext(context.Background(), fieldId, contextId).IssueTypeIds(issueTypeIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.AddIssueTypesToContext``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddIssueTypesToContext`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.AddIssueTypesToContext`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddIssueTypesToContextRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueTypeIds** | [**IssueTypeIds**](IssueTypeIds.md) |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AssignProjectsToCustomFieldContext

> interface{} AssignProjectsToCustomFieldContext(ctx, fieldId, contextId).ProjectIds(projectIds).Execute()

Assign custom field context to projects



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIds := *openapiclient.NewProjectIds([]string{"ProjectIds_example"}) // ProjectIds | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.AssignProjectsToCustomFieldContext(context.Background(), fieldId, contextId).ProjectIds(projectIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.AssignProjectsToCustomFieldContext``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AssignProjectsToCustomFieldContext`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.AssignProjectsToCustomFieldContext`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAssignProjectsToCustomFieldContextRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectIds** | [**ProjectIds**](ProjectIds.md) |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateCustomFieldContext

> CreateCustomFieldContext CreateCustomFieldContext(ctx, fieldId).CreateCustomFieldContext(createCustomFieldContext).Execute()

Create custom field context



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    createCustomFieldContext := *openapiclient.NewCreateCustomFieldContext("Name_example") // CreateCustomFieldContext | 
    fieldId := "fieldId_example" // string | The ID of the custom field.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.CreateCustomFieldContext(context.Background(), fieldId).CreateCustomFieldContext(createCustomFieldContext).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.CreateCustomFieldContext``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateCustomFieldContext`: CreateCustomFieldContext
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.CreateCustomFieldContext`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateCustomFieldContextRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createCustomFieldContext** | [**CreateCustomFieldContext**](CreateCustomFieldContext.md) |  | 


### Return type

[**CreateCustomFieldContext**](CreateCustomFieldContext.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteCustomFieldContext

> interface{} DeleteCustomFieldContext(ctx, fieldId, contextId).Execute()

Delete custom field context



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.DeleteCustomFieldContext(context.Background(), fieldId, contextId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.DeleteCustomFieldContext``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteCustomFieldContext`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.DeleteCustomFieldContext`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteCustomFieldContextRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContextsForField

> PageBeanCustomFieldContext GetContextsForField(ctx, fieldId).IsAnyIssueType(isAnyIssueType).IsGlobalContext(isGlobalContext).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()

Get custom field contexts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the custom field.
    isAnyIssueType := true // bool | Whether to return contexts that apply to all issue types. (optional)
    isGlobalContext := true // bool | Whether to return contexts that apply to all projects. (optional)
    contextId := []int64{int64(123)} // []int64 | The list of context IDs. To include multiple contexts, separate IDs with ampersand: `contextId=10000&contextId=10001`. (optional)
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.GetContextsForField(context.Background(), fieldId).IsAnyIssueType(isAnyIssueType).IsGlobalContext(isGlobalContext).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.GetContextsForField``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContextsForField`: PageBeanCustomFieldContext
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.GetContextsForField`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContextsForFieldRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **isAnyIssueType** | **bool** | Whether to return contexts that apply to all issue types. | 
 **isGlobalContext** | **bool** | Whether to return contexts that apply to all projects. | 
 **contextId** | **[]int64** | The list of context IDs. To include multiple contexts, separate IDs with ampersand: &#x60;contextId&#x3D;10000&amp;contextId&#x3D;10001&#x60;. | 
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanCustomFieldContext**](PageBeanCustomFieldContext.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCustomFieldContextsForProjectsAndIssueTypes

> PageBeanContextForProjectAndIssueType GetCustomFieldContextsForProjectsAndIssueTypes(ctx, fieldId).ProjectIssueTypeMappings(projectIssueTypeMappings).StartAt(startAt).MaxResults(maxResults).Execute()

Get custom field contexts for projects and issue types



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIssueTypeMappings := *openapiclient.NewProjectIssueTypeMappings([]openapiclient.ProjectIssueTypeMapping{*openapiclient.NewProjectIssueTypeMapping("ProjectId_example", "IssueTypeId_example")}) // ProjectIssueTypeMappings | The list of project and issue type mappings.
    fieldId := "fieldId_example" // string | The ID of the custom field.
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.GetCustomFieldContextsForProjectsAndIssueTypes(context.Background(), fieldId).ProjectIssueTypeMappings(projectIssueTypeMappings).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.GetCustomFieldContextsForProjectsAndIssueTypes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCustomFieldContextsForProjectsAndIssueTypes`: PageBeanContextForProjectAndIssueType
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.GetCustomFieldContextsForProjectsAndIssueTypes`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCustomFieldContextsForProjectsAndIssueTypesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectIssueTypeMappings** | [**ProjectIssueTypeMappings**](ProjectIssueTypeMappings.md) | The list of project and issue type mappings. | 

 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanContextForProjectAndIssueType**](PageBeanContextForProjectAndIssueType.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDefaultValues

> PageBeanCustomFieldContextDefaultValue GetDefaultValues(ctx, fieldId).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()

Get custom field contexts default values



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the custom field, for example `customfield\\_10000`.
    contextId := []int64{int64(123)} // []int64 | The IDs of the contexts. (optional)
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.GetDefaultValues(context.Background(), fieldId).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.GetDefaultValues``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetDefaultValues`: PageBeanCustomFieldContextDefaultValue
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.GetDefaultValues`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field, for example &#x60;customfield\\_10000&#x60;. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetDefaultValuesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contextId** | **[]int64** | The IDs of the contexts. | 
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanCustomFieldContextDefaultValue**](PageBeanCustomFieldContextDefaultValue.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssueTypeMappingsForContexts

> PageBeanIssueTypeToContextMapping GetIssueTypeMappingsForContexts(ctx, fieldId).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()

Get issue types for custom field context



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := []int64{int64(123)} // []int64 | The ID of the context. To include multiple contexts, provide an ampersand-separated list. For example, `contextId=10001&contextId=10002`. (optional)
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.GetIssueTypeMappingsForContexts(context.Background(), fieldId).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.GetIssueTypeMappingsForContexts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueTypeMappingsForContexts`: PageBeanIssueTypeToContextMapping
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.GetIssueTypeMappingsForContexts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueTypeMappingsForContextsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contextId** | **[]int64** | The ID of the context. To include multiple contexts, provide an ampersand-separated list. For example, &#x60;contextId&#x3D;10001&amp;contextId&#x3D;10002&#x60;. | 
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanIssueTypeToContextMapping**](PageBeanIssueTypeToContextMapping.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProjectContextMapping

> PageBeanCustomFieldContextProjectMapping GetProjectContextMapping(ctx, fieldId).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()

Get project mappings for custom field context



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the custom field, for example `customfield\\_10000`.
    contextId := []int64{int64(123)} // []int64 | The list of context IDs. To include multiple context, separate IDs with ampersand: `contextId=10000&contextId=10001`. (optional)
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.GetProjectContextMapping(context.Background(), fieldId).ContextId(contextId).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.GetProjectContextMapping``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProjectContextMapping`: PageBeanCustomFieldContextProjectMapping
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.GetProjectContextMapping`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field, for example &#x60;customfield\\_10000&#x60;. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProjectContextMappingRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contextId** | **[]int64** | The list of context IDs. To include multiple context, separate IDs with ampersand: &#x60;contextId&#x3D;10000&amp;contextId&#x3D;10001&#x60;. | 
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanCustomFieldContextProjectMapping**](PageBeanCustomFieldContextProjectMapping.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveCustomFieldContextFromProjects

> interface{} RemoveCustomFieldContextFromProjects(ctx, fieldId, contextId).ProjectIds(projectIds).Execute()

Remove custom field context from projects



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIds := *openapiclient.NewProjectIds([]string{"ProjectIds_example"}) // ProjectIds | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.RemoveCustomFieldContextFromProjects(context.Background(), fieldId, contextId).ProjectIds(projectIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.RemoveCustomFieldContextFromProjects``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RemoveCustomFieldContextFromProjects`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.RemoveCustomFieldContextFromProjects`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiRemoveCustomFieldContextFromProjectsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectIds** | [**ProjectIds**](ProjectIds.md) |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveIssueTypesFromContext

> interface{} RemoveIssueTypesFromContext(ctx, fieldId, contextId).IssueTypeIds(issueTypeIds).Execute()

Remove issue types from context



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueTypeIds := *openapiclient.NewIssueTypeIds([]string{"IssueTypeIds_example"}) // IssueTypeIds | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.RemoveIssueTypesFromContext(context.Background(), fieldId, contextId).IssueTypeIds(issueTypeIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.RemoveIssueTypesFromContext``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RemoveIssueTypesFromContext`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.RemoveIssueTypesFromContext`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiRemoveIssueTypesFromContextRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueTypeIds** | [**IssueTypeIds**](IssueTypeIds.md) |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetDefaultValues

> interface{} SetDefaultValues(ctx, fieldId).CustomFieldContextDefaultValueUpdate(customFieldContextDefaultValueUpdate).Execute()

Set custom field contexts default values



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    customFieldContextDefaultValueUpdate := *openapiclient.NewCustomFieldContextDefaultValueUpdate() // CustomFieldContextDefaultValueUpdate | 
    fieldId := "fieldId_example" // string | The ID of the custom field.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.SetDefaultValues(context.Background(), fieldId).CustomFieldContextDefaultValueUpdate(customFieldContextDefaultValueUpdate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.SetDefaultValues``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetDefaultValues`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.SetDefaultValues`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetDefaultValuesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldContextDefaultValueUpdate** | [**CustomFieldContextDefaultValueUpdate**](CustomFieldContextDefaultValueUpdate.md) |  | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCustomFieldContext

> interface{} UpdateCustomFieldContext(ctx, fieldId, contextId).CustomFieldContextUpdateDetails(customFieldContextUpdateDetails).Execute()

Update custom field context



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    customFieldContextUpdateDetails := *openapiclient.NewCustomFieldContextUpdateDetails() // CustomFieldContextUpdateDetails | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldContextsApi.UpdateCustomFieldContext(context.Background(), fieldId, contextId).CustomFieldContextUpdateDetails(customFieldContextUpdateDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldContextsApi.UpdateCustomFieldContext``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateCustomFieldContext`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldContextsApi.UpdateCustomFieldContext`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateCustomFieldContextRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldContextUpdateDetails** | [**CustomFieldContextUpdateDetails**](CustomFieldContextUpdateDetails.md) |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


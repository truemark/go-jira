# \IssueCustomFieldOptionsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCustomFieldOption**](IssueCustomFieldOptionsApi.md#CreateCustomFieldOption) | **Post** /rest/api/3/field/{fieldId}/context/{contextId}/option | Create custom field options (context)
[**DeleteCustomFieldOption**](IssueCustomFieldOptionsApi.md#DeleteCustomFieldOption) | **Delete** /rest/api/3/field/{fieldId}/context/{contextId}/option/{optionId} | Delete custom field options (context)
[**GetCustomFieldOption**](IssueCustomFieldOptionsApi.md#GetCustomFieldOption) | **Get** /rest/api/3/customFieldOption/{id} | Get custom field option
[**GetOptionsForContext**](IssueCustomFieldOptionsApi.md#GetOptionsForContext) | **Get** /rest/api/3/field/{fieldId}/context/{contextId}/option | Get custom field options (context)
[**ReorderCustomFieldOptions**](IssueCustomFieldOptionsApi.md#ReorderCustomFieldOptions) | **Put** /rest/api/3/field/{fieldId}/context/{contextId}/option/move | Reorder custom field options (context)
[**UpdateCustomFieldOption**](IssueCustomFieldOptionsApi.md#UpdateCustomFieldOption) | **Put** /rest/api/3/field/{fieldId}/context/{contextId}/option | Update custom field options (context)



## CreateCustomFieldOption

> CustomFieldCreatedContextOptionsList CreateCustomFieldOption(ctx, fieldId, contextId).BulkCustomFieldOptionCreateRequest(bulkCustomFieldOptionCreateRequest).Execute()

Create custom field options (context)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bulkCustomFieldOptionCreateRequest := *openapiclient.NewBulkCustomFieldOptionCreateRequest() // BulkCustomFieldOptionCreateRequest | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldOptionsApi.CreateCustomFieldOption(context.Background(), fieldId, contextId).BulkCustomFieldOptionCreateRequest(bulkCustomFieldOptionCreateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldOptionsApi.CreateCustomFieldOption``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateCustomFieldOption`: CustomFieldCreatedContextOptionsList
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldOptionsApi.CreateCustomFieldOption`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateCustomFieldOptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bulkCustomFieldOptionCreateRequest** | [**BulkCustomFieldOptionCreateRequest**](BulkCustomFieldOptionCreateRequest.md) |  | 



### Return type

[**CustomFieldCreatedContextOptionsList**](CustomFieldCreatedContextOptionsList.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteCustomFieldOption

> DeleteCustomFieldOption(ctx, fieldId, contextId, optionId).Execute()

Delete custom field options (context)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context from which an option should be deleted.
    optionId := int64(789) // int64 | The ID of the option to delete.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldOptionsApi.DeleteCustomFieldOption(context.Background(), fieldId, contextId, optionId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldOptionsApi.DeleteCustomFieldOption``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context from which an option should be deleted. | 
**optionId** | **int64** | The ID of the option to delete. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteCustomFieldOptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCustomFieldOption

> CustomFieldOption GetCustomFieldOption(ctx, id).Execute()

Get custom field option



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | The ID of the custom field option.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldOptionsApi.GetCustomFieldOption(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldOptionsApi.GetCustomFieldOption``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCustomFieldOption`: CustomFieldOption
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldOptionsApi.GetCustomFieldOption`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The ID of the custom field option. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCustomFieldOptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**CustomFieldOption**](CustomFieldOption.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOptionsForContext

> PageBeanCustomFieldContextOption GetOptionsForContext(ctx, fieldId, contextId).OptionId(optionId).OnlyOptions(onlyOptions).StartAt(startAt).MaxResults(maxResults).Execute()

Get custom field options (context)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.
    optionId := int64(789) // int64 | The ID of the option. (optional)
    onlyOptions := true // bool | Whether only options are returned. (optional) (default to false)
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 100)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldOptionsApi.GetOptionsForContext(context.Background(), fieldId, contextId).OptionId(optionId).OnlyOptions(onlyOptions).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldOptionsApi.GetOptionsForContext``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOptionsForContext`: PageBeanCustomFieldContextOption
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldOptionsApi.GetOptionsForContext`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOptionsForContextRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **optionId** | **int64** | The ID of the option. | 
 **onlyOptions** | **bool** | Whether only options are returned. | [default to false]
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 100]

### Return type

[**PageBeanCustomFieldContextOption**](PageBeanCustomFieldContextOption.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ReorderCustomFieldOptions

> interface{} ReorderCustomFieldOptions(ctx, fieldId, contextId).OrderOfCustomFieldOptions(orderOfCustomFieldOptions).Execute()

Reorder custom field options (context)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    orderOfCustomFieldOptions := *openapiclient.NewOrderOfCustomFieldOptions([]string{"CustomFieldOptionIds_example"}) // OrderOfCustomFieldOptions | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldOptionsApi.ReorderCustomFieldOptions(context.Background(), fieldId, contextId).OrderOfCustomFieldOptions(orderOfCustomFieldOptions).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldOptionsApi.ReorderCustomFieldOptions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ReorderCustomFieldOptions`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldOptionsApi.ReorderCustomFieldOptions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiReorderCustomFieldOptionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderOfCustomFieldOptions** | [**OrderOfCustomFieldOptions**](OrderOfCustomFieldOptions.md) |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCustomFieldOption

> CustomFieldUpdatedContextOptionsList UpdateCustomFieldOption(ctx, fieldId, contextId).BulkCustomFieldOptionUpdateRequest(bulkCustomFieldOptionUpdateRequest).Execute()

Update custom field options (context)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bulkCustomFieldOptionUpdateRequest := *openapiclient.NewBulkCustomFieldOptionUpdateRequest() // BulkCustomFieldOptionUpdateRequest | 
    fieldId := "fieldId_example" // string | The ID of the custom field.
    contextId := int64(789) // int64 | The ID of the context.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldOptionsApi.UpdateCustomFieldOption(context.Background(), fieldId, contextId).BulkCustomFieldOptionUpdateRequest(bulkCustomFieldOptionUpdateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldOptionsApi.UpdateCustomFieldOption``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateCustomFieldOption`: CustomFieldUpdatedContextOptionsList
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldOptionsApi.UpdateCustomFieldOption`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 
**contextId** | **int64** | The ID of the context. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateCustomFieldOptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bulkCustomFieldOptionUpdateRequest** | [**BulkCustomFieldOptionUpdateRequest**](BulkCustomFieldOptionUpdateRequest.md) |  | 



### Return type

[**CustomFieldUpdatedContextOptionsList**](CustomFieldUpdatedContextOptionsList.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


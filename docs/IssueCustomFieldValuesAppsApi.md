# \IssueCustomFieldValuesAppsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UpdateCustomFieldValue**](IssueCustomFieldValuesAppsApi.md#UpdateCustomFieldValue) | **Put** /rest/api/3/app/field/{fieldIdOrKey}/value | Update custom field value



## UpdateCustomFieldValue

> interface{} UpdateCustomFieldValue(ctx, fieldIdOrKey).CustomFieldValueUpdateRequest(customFieldValueUpdateRequest).Execute()

Update custom field value



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    customFieldValueUpdateRequest := *openapiclient.NewCustomFieldValueUpdateRequest() // CustomFieldValueUpdateRequest | 
    fieldIdOrKey := "fieldIdOrKey_example" // string | The ID or key of the custom field. For example, `customfield_10010`.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueCustomFieldValuesAppsApi.UpdateCustomFieldValue(context.Background(), fieldIdOrKey).CustomFieldValueUpdateRequest(customFieldValueUpdateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueCustomFieldValuesAppsApi.UpdateCustomFieldValue``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateCustomFieldValue`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueCustomFieldValuesAppsApi.UpdateCustomFieldValue`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldIdOrKey** | **string** | The ID or key of the custom field. For example, &#x60;customfield_10010&#x60;. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateCustomFieldValueRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldValueUpdateRequest** | [**CustomFieldValueUpdateRequest**](CustomFieldValueUpdateRequest.md) |  | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


# \IssueFieldConfigurationsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AssignFieldConfigurationSchemeToProject**](IssueFieldConfigurationsApi.md#AssignFieldConfigurationSchemeToProject) | **Put** /rest/api/3/fieldconfigurationscheme/project | Assign field configuration scheme to project
[**GetAllFieldConfigurationSchemes**](IssueFieldConfigurationsApi.md#GetAllFieldConfigurationSchemes) | **Get** /rest/api/3/fieldconfigurationscheme | Get all field configuration schemes
[**GetAllFieldConfigurations**](IssueFieldConfigurationsApi.md#GetAllFieldConfigurations) | **Get** /rest/api/3/fieldconfiguration | Get all field configurations
[**GetFieldConfigurationItems**](IssueFieldConfigurationsApi.md#GetFieldConfigurationItems) | **Get** /rest/api/3/fieldconfiguration/{id}/fields | Get field configuration items
[**GetFieldConfigurationSchemeMappings**](IssueFieldConfigurationsApi.md#GetFieldConfigurationSchemeMappings) | **Get** /rest/api/3/fieldconfigurationscheme/mapping | Get field configuration issue type items
[**GetFieldConfigurationSchemeProjectMapping**](IssueFieldConfigurationsApi.md#GetFieldConfigurationSchemeProjectMapping) | **Get** /rest/api/3/fieldconfigurationscheme/project | Get field configuration schemes for projects



## AssignFieldConfigurationSchemeToProject

> interface{} AssignFieldConfigurationSchemeToProject(ctx).FieldConfigurationSchemeProjectAssociation(fieldConfigurationSchemeProjectAssociation).Execute()

Assign field configuration scheme to project



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldConfigurationSchemeProjectAssociation := *openapiclient.NewFieldConfigurationSchemeProjectAssociation("ProjectId_example") // FieldConfigurationSchemeProjectAssociation | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldConfigurationsApi.AssignFieldConfigurationSchemeToProject(context.Background()).FieldConfigurationSchemeProjectAssociation(fieldConfigurationSchemeProjectAssociation).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldConfigurationsApi.AssignFieldConfigurationSchemeToProject``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AssignFieldConfigurationSchemeToProject`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldConfigurationsApi.AssignFieldConfigurationSchemeToProject`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAssignFieldConfigurationSchemeToProjectRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fieldConfigurationSchemeProjectAssociation** | [**FieldConfigurationSchemeProjectAssociation**](FieldConfigurationSchemeProjectAssociation.md) |  | 

### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllFieldConfigurationSchemes

> PageBeanFieldConfigurationScheme GetAllFieldConfigurationSchemes(ctx).StartAt(startAt).MaxResults(maxResults).Id(id).Execute()

Get all field configuration schemes



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)
    id := []int64{int64(123)} // []int64 | The list of field configuration scheme IDs. To include multiple IDs, provide an ampersand-separated list. For example, `id=10000&id=10001`. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldConfigurationsApi.GetAllFieldConfigurationSchemes(context.Background()).StartAt(startAt).MaxResults(maxResults).Id(id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldConfigurationsApi.GetAllFieldConfigurationSchemes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAllFieldConfigurationSchemes`: PageBeanFieldConfigurationScheme
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldConfigurationsApi.GetAllFieldConfigurationSchemes`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAllFieldConfigurationSchemesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]
 **id** | **[]int64** | The list of field configuration scheme IDs. To include multiple IDs, provide an ampersand-separated list. For example, &#x60;id&#x3D;10000&amp;id&#x3D;10001&#x60;. | 

### Return type

[**PageBeanFieldConfigurationScheme**](PageBeanFieldConfigurationScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllFieldConfigurations

> PageBeanFieldConfiguration GetAllFieldConfigurations(ctx).StartAt(startAt).MaxResults(maxResults).Id(id).IsDefault(isDefault).Query(query).Execute()

Get all field configurations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)
    id := []int64{int64(123)} // []int64 | The list of field configuration IDs. To include multiple IDs, provide an ampersand-separated list. For example, `id=10000&id=10001`. (optional)
    isDefault := true // bool | If *true* returns default field configurations only. (optional) (default to false)
    query := "query_example" // string | The query string used to match against field configuration names and descriptions. (optional) (default to "")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldConfigurationsApi.GetAllFieldConfigurations(context.Background()).StartAt(startAt).MaxResults(maxResults).Id(id).IsDefault(isDefault).Query(query).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldConfigurationsApi.GetAllFieldConfigurations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAllFieldConfigurations`: PageBeanFieldConfiguration
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldConfigurationsApi.GetAllFieldConfigurations`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAllFieldConfigurationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]
 **id** | **[]int64** | The list of field configuration IDs. To include multiple IDs, provide an ampersand-separated list. For example, &#x60;id&#x3D;10000&amp;id&#x3D;10001&#x60;. | 
 **isDefault** | **bool** | If *true* returns default field configurations only. | [default to false]
 **query** | **string** | The query string used to match against field configuration names and descriptions. | [default to &quot;&quot;]

### Return type

[**PageBeanFieldConfiguration**](PageBeanFieldConfiguration.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFieldConfigurationItems

> PageBeanFieldConfigurationItem GetFieldConfigurationItems(ctx, id).StartAt(startAt).MaxResults(maxResults).Execute()

Get field configuration items



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the field configuration.
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldConfigurationsApi.GetFieldConfigurationItems(context.Background(), id).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldConfigurationsApi.GetFieldConfigurationItems``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFieldConfigurationItems`: PageBeanFieldConfigurationItem
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldConfigurationsApi.GetFieldConfigurationItems`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the field configuration. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetFieldConfigurationItemsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanFieldConfigurationItem**](PageBeanFieldConfigurationItem.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFieldConfigurationSchemeMappings

> PageBeanFieldConfigurationIssueTypeItem GetFieldConfigurationSchemeMappings(ctx).StartAt(startAt).MaxResults(maxResults).FieldConfigurationSchemeId(fieldConfigurationSchemeId).Execute()

Get field configuration issue type items



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)
    fieldConfigurationSchemeId := []int64{int64(10020)} // []int64 | The list of field configuration scheme IDs. To include multiple field configuration schemes separate IDs with ampersand: `fieldConfigurationSchemeId=10000&fieldConfigurationSchemeId=10001`. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldConfigurationsApi.GetFieldConfigurationSchemeMappings(context.Background()).StartAt(startAt).MaxResults(maxResults).FieldConfigurationSchemeId(fieldConfigurationSchemeId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldConfigurationsApi.GetFieldConfigurationSchemeMappings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFieldConfigurationSchemeMappings`: PageBeanFieldConfigurationIssueTypeItem
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldConfigurationsApi.GetFieldConfigurationSchemeMappings`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetFieldConfigurationSchemeMappingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]
 **fieldConfigurationSchemeId** | **[]int64** | The list of field configuration scheme IDs. To include multiple field configuration schemes separate IDs with ampersand: &#x60;fieldConfigurationSchemeId&#x3D;10000&amp;fieldConfigurationSchemeId&#x3D;10001&#x60;. | 

### Return type

[**PageBeanFieldConfigurationIssueTypeItem**](PageBeanFieldConfigurationIssueTypeItem.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFieldConfigurationSchemeProjectMapping

> PageBeanFieldConfigurationSchemeProjects GetFieldConfigurationSchemeProjectMapping(ctx).ProjectId(projectId).StartAt(startAt).MaxResults(maxResults).Execute()

Get field configuration schemes for projects



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectId := []int64{int64(123)} // []int64 | The list of project IDs. To include multiple projects, separate IDs with ampersand: `projectId=10000&projectId=10001`.
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldConfigurationsApi.GetFieldConfigurationSchemeProjectMapping(context.Background()).ProjectId(projectId).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldConfigurationsApi.GetFieldConfigurationSchemeProjectMapping``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFieldConfigurationSchemeProjectMapping`: PageBeanFieldConfigurationSchemeProjects
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldConfigurationsApi.GetFieldConfigurationSchemeProjectMapping`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetFieldConfigurationSchemeProjectMappingRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **[]int64** | The list of project IDs. To include multiple projects, separate IDs with ampersand: &#x60;projectId&#x3D;10000&amp;projectId&#x3D;10001&#x60;. | 
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanFieldConfigurationSchemeProjects**](PageBeanFieldConfigurationSchemeProjects.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


# \IssueFieldsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCustomField**](IssueFieldsApi.md#CreateCustomField) | **Post** /rest/api/3/field | Create custom field
[**GetContextsForFieldDeprecated**](IssueFieldsApi.md#GetContextsForFieldDeprecated) | **Get** /rest/api/3/field/{fieldId}/contexts | Get contexts for a field
[**GetFields**](IssueFieldsApi.md#GetFields) | **Get** /rest/api/3/field | Get fields
[**GetFieldsPaginated**](IssueFieldsApi.md#GetFieldsPaginated) | **Get** /rest/api/3/field/search | Get fields paginated
[**UpdateCustomField**](IssueFieldsApi.md#UpdateCustomField) | **Put** /rest/api/3/field/{fieldId} | Update custom field



## CreateCustomField

> FieldDetails CreateCustomField(ctx).CustomFieldDefinitionJsonBean(customFieldDefinitionJsonBean).Execute()

Create custom field



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    customFieldDefinitionJsonBean := *openapiclient.NewCustomFieldDefinitionJsonBean("Name_example", "Type_example") // CustomFieldDefinitionJsonBean | Definition of the custom field to be created

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldsApi.CreateCustomField(context.Background()).CustomFieldDefinitionJsonBean(customFieldDefinitionJsonBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldsApi.CreateCustomField``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateCustomField`: FieldDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldsApi.CreateCustomField`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateCustomFieldRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionJsonBean** | [**CustomFieldDefinitionJsonBean**](CustomFieldDefinitionJsonBean.md) | Definition of the custom field to be created | 

### Return type

[**FieldDetails**](FieldDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContextsForFieldDeprecated

> PageBeanContext GetContextsForFieldDeprecated(ctx, fieldId).StartAt(startAt).MaxResults(maxResults).Execute()

Get contexts for a field



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldId := "fieldId_example" // string | The ID of the field to return contexts for.
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 20)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldsApi.GetContextsForFieldDeprecated(context.Background(), fieldId).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldsApi.GetContextsForFieldDeprecated``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContextsForFieldDeprecated`: PageBeanContext
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldsApi.GetContextsForFieldDeprecated`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the field to return contexts for. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContextsForFieldDeprecatedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 20]

### Return type

[**PageBeanContext**](PageBeanContext.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFields

> []FieldDetails GetFields(ctx).Execute()

Get fields



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldsApi.GetFields(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldsApi.GetFields``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFields`: []FieldDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldsApi.GetFields`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetFieldsRequest struct via the builder pattern


### Return type

[**[]FieldDetails**](FieldDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFieldsPaginated

> PageBeanField GetFieldsPaginated(ctx).StartAt(startAt).MaxResults(maxResults).Type_(type_).Id(id).Query(query).OrderBy(orderBy).Expand(expand).Execute()

Get fields paginated



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)
    type_ := []string{"Type_example"} // []string | The type of fields to search. (optional)
    id := []string{"Inner_example"} // []string | The IDs of the custom fields to return or, where `query` is specified, filter. (optional)
    query := "query_example" // string | String used to perform a case-insensitive partial match with field names or descriptions. (optional)
    orderBy := "orderBy_example" // string | [Order](#ordering) the results by a field:   *  `contextsCount` Sorts by the number of contexts related to a field.  *  `lastUsed` Sorts by the date when the value of the field last changed.  *  `name` Sorts by the field name.  *  `screensCount` Sorts by the number of screens related to a field. (optional)
    expand := "expand_example" // string | Use [expand](#expansion) to include additional information in the response. This parameter accepts a comma-separated list. Expand options include:   *  `key` Returns the key for each field.  *  `lastUsed` Returns the date when the value of the field last changed.  *  `screensCount` Returns the number of screens related to a field.  *  `contextsCount` Returns the number of contexts related to a field.  *  `isLocked` Returns information about whether the field is [locked](https://confluence.atlassian.com/x/ZSN7Og).  *  `searcherKey` Returns the searcher key for each custom field. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldsApi.GetFieldsPaginated(context.Background()).StartAt(startAt).MaxResults(maxResults).Type_(type_).Id(id).Query(query).OrderBy(orderBy).Expand(expand).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldsApi.GetFieldsPaginated``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFieldsPaginated`: PageBeanField
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldsApi.GetFieldsPaginated`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetFieldsPaginatedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]
 **type_** | **[]string** | The type of fields to search. | 
 **id** | **[]string** | The IDs of the custom fields to return or, where &#x60;query&#x60; is specified, filter. | 
 **query** | **string** | String used to perform a case-insensitive partial match with field names or descriptions. | 
 **orderBy** | **string** | [Order](#ordering) the results by a field:   *  &#x60;contextsCount&#x60; Sorts by the number of contexts related to a field.  *  &#x60;lastUsed&#x60; Sorts by the date when the value of the field last changed.  *  &#x60;name&#x60; Sorts by the field name.  *  &#x60;screensCount&#x60; Sorts by the number of screens related to a field. | 
 **expand** | **string** | Use [expand](#expansion) to include additional information in the response. This parameter accepts a comma-separated list. Expand options include:   *  &#x60;key&#x60; Returns the key for each field.  *  &#x60;lastUsed&#x60; Returns the date when the value of the field last changed.  *  &#x60;screensCount&#x60; Returns the number of screens related to a field.  *  &#x60;contextsCount&#x60; Returns the number of contexts related to a field.  *  &#x60;isLocked&#x60; Returns information about whether the field is [locked](https://confluence.atlassian.com/x/ZSN7Og).  *  &#x60;searcherKey&#x60; Returns the searcher key for each custom field. | 

### Return type

[**PageBeanField**](PageBeanField.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCustomField

> interface{} UpdateCustomField(ctx, fieldId).UpdateCustomFieldDetails(updateCustomFieldDetails).Execute()

Update custom field



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    updateCustomFieldDetails := *openapiclient.NewUpdateCustomFieldDetails() // UpdateCustomFieldDetails | The custom field update details.
    fieldId := "fieldId_example" // string | The ID of the custom field.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueFieldsApi.UpdateCustomField(context.Background(), fieldId).UpdateCustomFieldDetails(updateCustomFieldDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueFieldsApi.UpdateCustomField``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateCustomField`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueFieldsApi.UpdateCustomField`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**fieldId** | **string** | The ID of the custom field. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateCustomFieldRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateCustomFieldDetails** | [**UpdateCustomFieldDetails**](UpdateCustomFieldDetails.md) | The custom field update details. | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


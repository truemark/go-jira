# IssueLinkTypes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueLinkTypes** | Pointer to [**[]IssueLinkType**](IssueLinkType.md) | The issue link type bean. | [optional] [readonly] 

## Methods

### NewIssueLinkTypes

`func NewIssueLinkTypes() *IssueLinkTypes`

NewIssueLinkTypes instantiates a new IssueLinkTypes object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueLinkTypesWithDefaults

`func NewIssueLinkTypesWithDefaults() *IssueLinkTypes`

NewIssueLinkTypesWithDefaults instantiates a new IssueLinkTypes object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueLinkTypes

`func (o *IssueLinkTypes) GetIssueLinkTypes() []IssueLinkType`

GetIssueLinkTypes returns the IssueLinkTypes field if non-nil, zero value otherwise.

### GetIssueLinkTypesOk

`func (o *IssueLinkTypes) GetIssueLinkTypesOk() (*[]IssueLinkType, bool)`

GetIssueLinkTypesOk returns a tuple with the IssueLinkTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueLinkTypes

`func (o *IssueLinkTypes) SetIssueLinkTypes(v []IssueLinkType)`

SetIssueLinkTypes sets IssueLinkTypes field to given value.

### HasIssueLinkTypes

`func (o *IssueLinkTypes) HasIssueLinkTypes() bool`

HasIssueLinkTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



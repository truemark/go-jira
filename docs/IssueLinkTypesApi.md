# \IssueLinkTypesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateIssueLinkType**](IssueLinkTypesApi.md#CreateIssueLinkType) | **Post** /rest/api/3/issueLinkType | Create issue link type
[**DeleteIssueLinkType**](IssueLinkTypesApi.md#DeleteIssueLinkType) | **Delete** /rest/api/3/issueLinkType/{issueLinkTypeId} | Delete issue link type
[**GetIssueLinkType**](IssueLinkTypesApi.md#GetIssueLinkType) | **Get** /rest/api/3/issueLinkType/{issueLinkTypeId} | Get issue link type
[**GetIssueLinkTypes**](IssueLinkTypesApi.md#GetIssueLinkTypes) | **Get** /rest/api/3/issueLinkType | Get issue link types
[**UpdateIssueLinkType**](IssueLinkTypesApi.md#UpdateIssueLinkType) | **Put** /rest/api/3/issueLinkType/{issueLinkTypeId} | Update issue link type



## CreateIssueLinkType

> IssueLinkType CreateIssueLinkType(ctx).IssueLinkType(issueLinkType).Execute()

Create issue link type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueLinkType := *openapiclient.NewIssueLinkType() // IssueLinkType | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueLinkTypesApi.CreateIssueLinkType(context.Background()).IssueLinkType(issueLinkType).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueLinkTypesApi.CreateIssueLinkType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateIssueLinkType`: IssueLinkType
    fmt.Fprintf(os.Stdout, "Response from `IssueLinkTypesApi.CreateIssueLinkType`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateIssueLinkTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueLinkType** | [**IssueLinkType**](IssueLinkType.md) |  | 

### Return type

[**IssueLinkType**](IssueLinkType.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteIssueLinkType

> DeleteIssueLinkType(ctx, issueLinkTypeId).Execute()

Delete issue link type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueLinkTypeId := "issueLinkTypeId_example" // string | The ID of the issue link type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueLinkTypesApi.DeleteIssueLinkType(context.Background(), issueLinkTypeId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueLinkTypesApi.DeleteIssueLinkType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueLinkTypeId** | **string** | The ID of the issue link type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteIssueLinkTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssueLinkType

> IssueLinkType GetIssueLinkType(ctx, issueLinkTypeId).Execute()

Get issue link type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueLinkTypeId := "issueLinkTypeId_example" // string | The ID of the issue link type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueLinkTypesApi.GetIssueLinkType(context.Background(), issueLinkTypeId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueLinkTypesApi.GetIssueLinkType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueLinkType`: IssueLinkType
    fmt.Fprintf(os.Stdout, "Response from `IssueLinkTypesApi.GetIssueLinkType`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueLinkTypeId** | **string** | The ID of the issue link type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueLinkTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**IssueLinkType**](IssueLinkType.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssueLinkTypes

> IssueLinkTypes GetIssueLinkTypes(ctx).Execute()

Get issue link types



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueLinkTypesApi.GetIssueLinkTypes(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueLinkTypesApi.GetIssueLinkTypes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueLinkTypes`: IssueLinkTypes
    fmt.Fprintf(os.Stdout, "Response from `IssueLinkTypesApi.GetIssueLinkTypes`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueLinkTypesRequest struct via the builder pattern


### Return type

[**IssueLinkTypes**](IssueLinkTypes.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateIssueLinkType

> IssueLinkType UpdateIssueLinkType(ctx, issueLinkTypeId).IssueLinkType(issueLinkType).Execute()

Update issue link type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueLinkType := *openapiclient.NewIssueLinkType() // IssueLinkType | 
    issueLinkTypeId := "issueLinkTypeId_example" // string | The ID of the issue link type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueLinkTypesApi.UpdateIssueLinkType(context.Background(), issueLinkTypeId).IssueLinkType(issueLinkType).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueLinkTypesApi.UpdateIssueLinkType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateIssueLinkType`: IssueLinkType
    fmt.Fprintf(os.Stdout, "Response from `IssueLinkTypesApi.UpdateIssueLinkType`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueLinkTypeId** | **string** | The ID of the issue link type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateIssueLinkTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueLinkType** | [**IssueLinkType**](IssueLinkType.md) |  | 


### Return type

[**IssueLinkType**](IssueLinkType.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


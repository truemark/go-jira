# IssueMatches

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Matches** | [**[]IssueMatchesForJQL**](IssueMatchesForJQL.md) |  | 

## Methods

### NewIssueMatches

`func NewIssueMatches(matches []IssueMatchesForJQL, ) *IssueMatches`

NewIssueMatches instantiates a new IssueMatches object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueMatchesWithDefaults

`func NewIssueMatchesWithDefaults() *IssueMatches`

NewIssueMatchesWithDefaults instantiates a new IssueMatches object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMatches

`func (o *IssueMatches) GetMatches() []IssueMatchesForJQL`

GetMatches returns the Matches field if non-nil, zero value otherwise.

### GetMatchesOk

`func (o *IssueMatches) GetMatchesOk() (*[]IssueMatchesForJQL, bool)`

GetMatchesOk returns a tuple with the Matches field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMatches

`func (o *IssueMatches) SetMatches(v []IssueMatchesForJQL)`

SetMatches sets Matches field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \IssueNavigatorSettingsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetIssueNavigatorDefaultColumns**](IssueNavigatorSettingsApi.md#GetIssueNavigatorDefaultColumns) | **Get** /rest/api/3/settings/columns | Get issue navigator default columns
[**SetIssueNavigatorDefaultColumns**](IssueNavigatorSettingsApi.md#SetIssueNavigatorDefaultColumns) | **Put** /rest/api/3/settings/columns | Set issue navigator default columns



## GetIssueNavigatorDefaultColumns

> []ColumnItem GetIssueNavigatorDefaultColumns(ctx).Execute()

Get issue navigator default columns



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueNavigatorSettingsApi.GetIssueNavigatorDefaultColumns(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueNavigatorSettingsApi.GetIssueNavigatorDefaultColumns``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueNavigatorDefaultColumns`: []ColumnItem
    fmt.Fprintf(os.Stdout, "Response from `IssueNavigatorSettingsApi.GetIssueNavigatorDefaultColumns`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueNavigatorDefaultColumnsRequest struct via the builder pattern


### Return type

[**[]ColumnItem**](ColumnItem.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetIssueNavigatorDefaultColumns

> interface{} SetIssueNavigatorDefaultColumns(ctx).RequestBody(requestBody).Execute()

Set issue navigator default columns



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := []string{"Property_example"} // []string | A navigable field value. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueNavigatorSettingsApi.SetIssueNavigatorDefaultColumns(context.Background()).RequestBody(requestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueNavigatorSettingsApi.SetIssueNavigatorDefaultColumns``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetIssueNavigatorDefaultColumns`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueNavigatorSettingsApi.SetIssueNavigatorDefaultColumns`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSetIssueNavigatorDefaultColumnsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **[]string** | A navigable field value. | 

### Return type

**interface{}**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


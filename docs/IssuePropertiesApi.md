# \IssuePropertiesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**BulkDeleteIssueProperty**](IssuePropertiesApi.md#BulkDeleteIssueProperty) | **Delete** /rest/api/3/issue/properties/{propertyKey} | Bulk delete issue property
[**BulkSetIssueProperty**](IssuePropertiesApi.md#BulkSetIssueProperty) | **Put** /rest/api/3/issue/properties/{propertyKey} | Bulk set issue property
[**BulkSetIssuesProperties**](IssuePropertiesApi.md#BulkSetIssuesProperties) | **Post** /rest/api/3/issue/properties | Bulk set issues properties
[**DeleteIssueProperty**](IssuePropertiesApi.md#DeleteIssueProperty) | **Delete** /rest/api/3/issue/{issueIdOrKey}/properties/{propertyKey} | Delete issue property
[**GetIssueProperty**](IssuePropertiesApi.md#GetIssueProperty) | **Get** /rest/api/3/issue/{issueIdOrKey}/properties/{propertyKey} | Get issue property
[**GetIssuePropertyKeys**](IssuePropertiesApi.md#GetIssuePropertyKeys) | **Get** /rest/api/3/issue/{issueIdOrKey}/properties | Get issue property keys
[**SetIssueProperty**](IssuePropertiesApi.md#SetIssueProperty) | **Put** /rest/api/3/issue/{issueIdOrKey}/properties/{propertyKey} | Set issue property



## BulkDeleteIssueProperty

> BulkDeleteIssueProperty(ctx, propertyKey).IssueFilterForBulkPropertyDelete(issueFilterForBulkPropertyDelete).Execute()

Bulk delete issue property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueFilterForBulkPropertyDelete := *openapiclient.NewIssueFilterForBulkPropertyDelete() // IssueFilterForBulkPropertyDelete | 
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuePropertiesApi.BulkDeleteIssueProperty(context.Background(), propertyKey).IssueFilterForBulkPropertyDelete(issueFilterForBulkPropertyDelete).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuePropertiesApi.BulkDeleteIssueProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiBulkDeleteIssuePropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueFilterForBulkPropertyDelete** | [**IssueFilterForBulkPropertyDelete**](IssueFilterForBulkPropertyDelete.md) |  | 


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## BulkSetIssueProperty

> BulkSetIssueProperty(ctx, propertyKey).BulkIssuePropertyUpdateRequest(bulkIssuePropertyUpdateRequest).Execute()

Bulk set issue property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    bulkIssuePropertyUpdateRequest := *openapiclient.NewBulkIssuePropertyUpdateRequest() // BulkIssuePropertyUpdateRequest | 
    propertyKey := "propertyKey_example" // string | The key of the property. The maximum length is 255 characters.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuePropertiesApi.BulkSetIssueProperty(context.Background(), propertyKey).BulkIssuePropertyUpdateRequest(bulkIssuePropertyUpdateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuePropertiesApi.BulkSetIssueProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**propertyKey** | **string** | The key of the property. The maximum length is 255 characters. | 

### Other Parameters

Other parameters are passed through a pointer to a apiBulkSetIssuePropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bulkIssuePropertyUpdateRequest** | [**BulkIssuePropertyUpdateRequest**](BulkIssuePropertyUpdateRequest.md) |  | 


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## BulkSetIssuesProperties

> BulkSetIssuesProperties(ctx).IssueEntityProperties(issueEntityProperties).Execute()

Bulk set issues properties



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueEntityProperties := *openapiclient.NewIssueEntityProperties() // IssueEntityProperties | Issue properties to be set or updated with values.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuePropertiesApi.BulkSetIssuesProperties(context.Background()).IssueEntityProperties(issueEntityProperties).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuePropertiesApi.BulkSetIssuesProperties``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiBulkSetIssuesPropertiesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueEntityProperties** | [**IssueEntityProperties**](IssueEntityProperties.md) | Issue properties to be set or updated with values. | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteIssueProperty

> DeleteIssueProperty(ctx, issueIdOrKey, propertyKey).Execute()

Delete issue property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The key or ID of the issue.
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuePropertiesApi.DeleteIssueProperty(context.Background(), issueIdOrKey, propertyKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuePropertiesApi.DeleteIssueProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The key or ID of the issue. | 
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteIssuePropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssueProperty

> EntityProperty GetIssueProperty(ctx, issueIdOrKey, propertyKey).Execute()

Get issue property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The key or ID of the issue.
    propertyKey := "propertyKey_example" // string | The key of the property.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuePropertiesApi.GetIssueProperty(context.Background(), issueIdOrKey, propertyKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuePropertiesApi.GetIssueProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueProperty`: EntityProperty
    fmt.Fprintf(os.Stdout, "Response from `IssuePropertiesApi.GetIssueProperty`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The key or ID of the issue. | 
**propertyKey** | **string** | The key of the property. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssuePropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**EntityProperty**](EntityProperty.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssuePropertyKeys

> PropertyKeys GetIssuePropertyKeys(ctx, issueIdOrKey).Execute()

Get issue property keys



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The key or ID of the issue.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuePropertiesApi.GetIssuePropertyKeys(context.Background(), issueIdOrKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuePropertiesApi.GetIssuePropertyKeys``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssuePropertyKeys`: PropertyKeys
    fmt.Fprintf(os.Stdout, "Response from `IssuePropertiesApi.GetIssuePropertyKeys`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The key or ID of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssuePropertyKeysRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**PropertyKeys**](PropertyKeys.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetIssueProperty

> interface{} SetIssueProperty(ctx, issueIdOrKey, propertyKey).Body(body).Execute()

Set issue property



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    body := interface{}(987) // interface{} | 
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    propertyKey := "propertyKey_example" // string | The key of the issue property. The maximum length is 255 characters.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuePropertiesApi.SetIssueProperty(context.Background(), issueIdOrKey, propertyKey).Body(body).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuePropertiesApi.SetIssueProperty``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetIssueProperty`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssuePropertiesApi.SetIssueProperty`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 
**propertyKey** | **string** | The key of the issue property. The maximum length is 255 characters. | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetIssuePropertyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **interface{}** |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


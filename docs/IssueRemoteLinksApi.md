# \IssueRemoteLinksApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateOrUpdateRemoteIssueLink**](IssueRemoteLinksApi.md#CreateOrUpdateRemoteIssueLink) | **Post** /rest/api/3/issue/{issueIdOrKey}/remotelink | Create or update remote issue link
[**DeleteRemoteIssueLinkByGlobalId**](IssueRemoteLinksApi.md#DeleteRemoteIssueLinkByGlobalId) | **Delete** /rest/api/3/issue/{issueIdOrKey}/remotelink | Delete remote issue link by global ID
[**DeleteRemoteIssueLinkById**](IssueRemoteLinksApi.md#DeleteRemoteIssueLinkById) | **Delete** /rest/api/3/issue/{issueIdOrKey}/remotelink/{linkId} | Delete remote issue link by ID
[**GetRemoteIssueLinkById**](IssueRemoteLinksApi.md#GetRemoteIssueLinkById) | **Get** /rest/api/3/issue/{issueIdOrKey}/remotelink/{linkId} | Get remote issue link by ID
[**GetRemoteIssueLinks**](IssueRemoteLinksApi.md#GetRemoteIssueLinks) | **Get** /rest/api/3/issue/{issueIdOrKey}/remotelink | Get remote issue links
[**UpdateRemoteIssueLink**](IssueRemoteLinksApi.md#UpdateRemoteIssueLink) | **Put** /rest/api/3/issue/{issueIdOrKey}/remotelink/{linkId} | Update remote issue link by ID



## CreateOrUpdateRemoteIssueLink

> RemoteIssueLinkIdentifies CreateOrUpdateRemoteIssueLink(ctx, issueIdOrKey).RequestBody(requestBody).Execute()

Create or update remote issue link



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := map[string]map[string]interface{}{"key": map[string]interface{}(123)} // map[string]map[string]interface{} | 
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueRemoteLinksApi.CreateOrUpdateRemoteIssueLink(context.Background(), issueIdOrKey).RequestBody(requestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueRemoteLinksApi.CreateOrUpdateRemoteIssueLink``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateOrUpdateRemoteIssueLink`: RemoteIssueLinkIdentifies
    fmt.Fprintf(os.Stdout, "Response from `IssueRemoteLinksApi.CreateOrUpdateRemoteIssueLink`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateOrUpdateRemoteIssueLinkRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **map[string]map[string]interface{}** |  | 


### Return type

[**RemoteIssueLinkIdentifies**](RemoteIssueLinkIdentifies.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteRemoteIssueLinkByGlobalId

> DeleteRemoteIssueLinkByGlobalId(ctx, issueIdOrKey).GlobalId(globalId).Execute()

Delete remote issue link by global ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "10000" // string | The ID or key of the issue.
    globalId := "system=http://www.mycompany.com/support&id=1" // string | The global ID of a remote issue link.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueRemoteLinksApi.DeleteRemoteIssueLinkByGlobalId(context.Background(), issueIdOrKey).GlobalId(globalId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueRemoteLinksApi.DeleteRemoteIssueLinkByGlobalId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteRemoteIssueLinkByGlobalIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **globalId** | **string** | The global ID of a remote issue link. | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteRemoteIssueLinkById

> DeleteRemoteIssueLinkById(ctx, issueIdOrKey, linkId).Execute()

Delete remote issue link by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "10000" // string | The ID or key of the issue.
    linkId := "10000" // string | The ID of a remote issue link.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueRemoteLinksApi.DeleteRemoteIssueLinkById(context.Background(), issueIdOrKey, linkId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueRemoteLinksApi.DeleteRemoteIssueLinkById``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 
**linkId** | **string** | The ID of a remote issue link. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteRemoteIssueLinkByIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetRemoteIssueLinkById

> RemoteIssueLink GetRemoteIssueLinkById(ctx, issueIdOrKey, linkId).Execute()

Get remote issue link by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    linkId := "linkId_example" // string | The ID of the remote issue link.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueRemoteLinksApi.GetRemoteIssueLinkById(context.Background(), issueIdOrKey, linkId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueRemoteLinksApi.GetRemoteIssueLinkById``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetRemoteIssueLinkById`: RemoteIssueLink
    fmt.Fprintf(os.Stdout, "Response from `IssueRemoteLinksApi.GetRemoteIssueLinkById`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 
**linkId** | **string** | The ID of the remote issue link. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetRemoteIssueLinkByIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**RemoteIssueLink**](RemoteIssueLink.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetRemoteIssueLinks

> RemoteIssueLink GetRemoteIssueLinks(ctx, issueIdOrKey).GlobalId(globalId).Execute()

Get remote issue links



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "10000" // string | The ID or key of the issue.
    globalId := "globalId_example" // string | The global ID of the remote issue link. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueRemoteLinksApi.GetRemoteIssueLinks(context.Background(), issueIdOrKey).GlobalId(globalId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueRemoteLinksApi.GetRemoteIssueLinks``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetRemoteIssueLinks`: RemoteIssueLink
    fmt.Fprintf(os.Stdout, "Response from `IssueRemoteLinksApi.GetRemoteIssueLinks`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetRemoteIssueLinksRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **globalId** | **string** | The global ID of the remote issue link. | 

### Return type

[**RemoteIssueLink**](RemoteIssueLink.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateRemoteIssueLink

> interface{} UpdateRemoteIssueLink(ctx, issueIdOrKey, linkId).RequestBody(requestBody).Execute()

Update remote issue link by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := map[string]map[string]interface{}{"key": map[string]interface{}(123)} // map[string]map[string]interface{} | 
    issueIdOrKey := "10000" // string | The ID or key of the issue.
    linkId := "10000" // string | The ID of the remote issue link.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueRemoteLinksApi.UpdateRemoteIssueLink(context.Background(), issueIdOrKey, linkId).RequestBody(requestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueRemoteLinksApi.UpdateRemoteIssueLink``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateRemoteIssueLink`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssueRemoteLinksApi.UpdateRemoteIssueLink`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 
**linkId** | **string** | The ID of the remote issue link. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateRemoteIssueLinkRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **map[string]map[string]interface{}** |  | 



### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


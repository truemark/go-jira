# \IssueSearchApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetIssuePickerResource**](IssueSearchApi.md#GetIssuePickerResource) | **Get** /rest/api/3/issue/picker | Get issue picker suggestions
[**MatchIssues**](IssueSearchApi.md#MatchIssues) | **Post** /rest/api/3/jql/match | Check issues against JQL
[**SearchForIssuesUsingJql**](IssueSearchApi.md#SearchForIssuesUsingJql) | **Get** /rest/api/3/search | Search for issues using JQL (GET)
[**SearchForIssuesUsingJqlPost**](IssueSearchApi.md#SearchForIssuesUsingJqlPost) | **Post** /rest/api/3/search | Search for issues using JQL (POST)



## GetIssuePickerResource

> IssuePickerSuggestions GetIssuePickerResource(ctx).Query(query).CurrentJQL(currentJQL).CurrentIssueKey(currentIssueKey).CurrentProjectId(currentProjectId).ShowSubTasks(showSubTasks).ShowSubTaskParent(showSubTaskParent).Execute()

Get issue picker suggestions



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    query := "query" // string | A string to match against text fields in the issue such as title, description, or comments. (optional)
    currentJQL := "currentJQL_example" // string | A JQL query defining a list of issues to search for the query term. Note that `username` and `userkey` cannot be used as search terms for this parameter, due to privacy reasons. Use `accountId` instead. (optional)
    currentIssueKey := "currentIssueKey_example" // string | The key of an issue to exclude from search results. For example, the issue the user is viewing when they perform this query. (optional)
    currentProjectId := "currentProjectId_example" // string | The ID of a project that suggested issues must belong to. (optional)
    showSubTasks := true // bool | Indicate whether to include subtasks in the suggestions list. (optional)
    showSubTaskParent := true // bool | When `currentIssueKey` is a subtask, whether to include the parent issue in the suggestions if it matches the query. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueSearchApi.GetIssuePickerResource(context.Background()).Query(query).CurrentJQL(currentJQL).CurrentIssueKey(currentIssueKey).CurrentProjectId(currentProjectId).ShowSubTasks(showSubTasks).ShowSubTaskParent(showSubTaskParent).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueSearchApi.GetIssuePickerResource``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssuePickerResource`: IssuePickerSuggestions
    fmt.Fprintf(os.Stdout, "Response from `IssueSearchApi.GetIssuePickerResource`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetIssuePickerResourceRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **string** | A string to match against text fields in the issue such as title, description, or comments. | 
 **currentJQL** | **string** | A JQL query defining a list of issues to search for the query term. Note that &#x60;username&#x60; and &#x60;userkey&#x60; cannot be used as search terms for this parameter, due to privacy reasons. Use &#x60;accountId&#x60; instead. | 
 **currentIssueKey** | **string** | The key of an issue to exclude from search results. For example, the issue the user is viewing when they perform this query. | 
 **currentProjectId** | **string** | The ID of a project that suggested issues must belong to. | 
 **showSubTasks** | **bool** | Indicate whether to include subtasks in the suggestions list. | 
 **showSubTaskParent** | **bool** | When &#x60;currentIssueKey&#x60; is a subtask, whether to include the parent issue in the suggestions if it matches the query. | 

### Return type

[**IssuePickerSuggestions**](IssuePickerSuggestions.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MatchIssues

> IssueMatches MatchIssues(ctx).IssuesAndJQLQueries(issuesAndJQLQueries).Execute()

Check issues against JQL



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issuesAndJQLQueries := *openapiclient.NewIssuesAndJQLQueries([]string{"Jqls_example"}, []int64{int64(123)}) // IssuesAndJQLQueries | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueSearchApi.MatchIssues(context.Background()).IssuesAndJQLQueries(issuesAndJQLQueries).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueSearchApi.MatchIssues``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `MatchIssues`: IssueMatches
    fmt.Fprintf(os.Stdout, "Response from `IssueSearchApi.MatchIssues`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiMatchIssuesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issuesAndJQLQueries** | [**IssuesAndJQLQueries**](IssuesAndJQLQueries.md) |  | 

### Return type

[**IssueMatches**](IssueMatches.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SearchForIssuesUsingJql

> SearchResults SearchForIssuesUsingJql(ctx).Jql(jql).StartAt(startAt).MaxResults(maxResults).ValidateQuery(validateQuery).Fields(fields).Expand(expand).Properties(properties).FieldsByKeys(fieldsByKeys).Execute()

Search for issues using JQL (GET)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    jql := "project = HSP" // string | The [JQL](https://confluence.atlassian.com/x/egORLQ) that defines the search. Note:   *  If no JQL expression is provided, all issues are returned.  *  `username` and `userkey` cannot be used as search terms due to privacy reasons. Use `accountId` instead.  *  If a user has hidden their email address in their user profile, partial matches of the email address will not find the user. An exact match is required. (optional)
    startAt := int32(56) // int32 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. To manage page size, Jira may return fewer items per page where a large number of fields are requested. The greatest number of items returned per page is achieved when requesting `id` or `key` only. (optional) (default to 50)
    validateQuery := "validateQuery_example" // string | Determines how to validate the JQL query and treat the validation results. Supported values are:   *  `strict` Returns a 400 response code if any errors are found, along with a list of all errors (and warnings).  *  `warn` Returns all errors as warnings.  *  `none` No validation is performed.  *  `true` *Deprecated* A legacy synonym for `strict`.  *  `false` *Deprecated* A legacy synonym for `warn`.  Note: If the JQL is not correctly formed a 400 response code is returned, regardless of the `validateQuery` value. (optional) (default to "strict")
    fields := []string{"Inner_example"} // []string | A list of fields to return for each issue, use it to retrieve a subset of fields. This parameter accepts a comma-separated list. Expand options include:   *  `*all` Returns all fields.  *  `*navigable` Returns navigable fields.  *  Any issue field, prefixed with a minus to exclude.  Examples:   *  `summary,comment` Returns only the summary and comments fields.  *  `-description` Returns all navigable (default) fields except description.  *  `*all,-comment` Returns all fields except comments.  This parameter may be specified multiple times. For example, `fields=field1,field2&fields=field3`.  Note: All navigable fields are returned by default. This differs from [GET issue](#api-rest-api-3-issue-issueIdOrKey-get) where the default is all fields. (optional)
    expand := "expand_example" // string | Use [expand](#expansion) to include additional information about issues in the response. This parameter accepts a comma-separated list. Expand options include:   *  `renderedFields` Returns field values rendered in HTML format.  *  `names` Returns the display name of each field.  *  `schema` Returns the schema describing a field type.  *  `transitions` Returns all possible transitions for the issue.  *  `operations` Returns all possible operations for the issue.  *  `editmeta` Returns information about how each field can be edited.  *  `changelog` Returns a list of recent updates to an issue, sorted by date, starting from the most recent.  *  `versionedRepresentations` Instead of `fields`, returns `versionedRepresentations` a JSON array containing each version of a field's value, with the highest numbered item representing the most recent version. (optional)
    properties := []string{"Inner_example"} // []string | A list of issue property keys for issue properties to include in the results. This parameter accepts a comma-separated list. Multiple properties can also be provided using an ampersand separated list. For example, `properties=prop1,prop2&properties=prop3`. A maximum of 5 issue property keys can be specified. (optional)
    fieldsByKeys := true // bool | Reference fields by their key (rather than ID). (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueSearchApi.SearchForIssuesUsingJql(context.Background()).Jql(jql).StartAt(startAt).MaxResults(maxResults).ValidateQuery(validateQuery).Fields(fields).Expand(expand).Properties(properties).FieldsByKeys(fieldsByKeys).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueSearchApi.SearchForIssuesUsingJql``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SearchForIssuesUsingJql`: SearchResults
    fmt.Fprintf(os.Stdout, "Response from `IssueSearchApi.SearchForIssuesUsingJql`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSearchForIssuesUsingJqlRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jql** | **string** | The [JQL](https://confluence.atlassian.com/x/egORLQ) that defines the search. Note:   *  If no JQL expression is provided, all issues are returned.  *  &#x60;username&#x60; and &#x60;userkey&#x60; cannot be used as search terms due to privacy reasons. Use &#x60;accountId&#x60; instead.  *  If a user has hidden their email address in their user profile, partial matches of the email address will not find the user. An exact match is required. | 
 **startAt** | **int32** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. To manage page size, Jira may return fewer items per page where a large number of fields are requested. The greatest number of items returned per page is achieved when requesting &#x60;id&#x60; or &#x60;key&#x60; only. | [default to 50]
 **validateQuery** | **string** | Determines how to validate the JQL query and treat the validation results. Supported values are:   *  &#x60;strict&#x60; Returns a 400 response code if any errors are found, along with a list of all errors (and warnings).  *  &#x60;warn&#x60; Returns all errors as warnings.  *  &#x60;none&#x60; No validation is performed.  *  &#x60;true&#x60; *Deprecated* A legacy synonym for &#x60;strict&#x60;.  *  &#x60;false&#x60; *Deprecated* A legacy synonym for &#x60;warn&#x60;.  Note: If the JQL is not correctly formed a 400 response code is returned, regardless of the &#x60;validateQuery&#x60; value. | [default to &quot;strict&quot;]
 **fields** | **[]string** | A list of fields to return for each issue, use it to retrieve a subset of fields. This parameter accepts a comma-separated list. Expand options include:   *  &#x60;*all&#x60; Returns all fields.  *  &#x60;*navigable&#x60; Returns navigable fields.  *  Any issue field, prefixed with a minus to exclude.  Examples:   *  &#x60;summary,comment&#x60; Returns only the summary and comments fields.  *  &#x60;-description&#x60; Returns all navigable (default) fields except description.  *  &#x60;*all,-comment&#x60; Returns all fields except comments.  This parameter may be specified multiple times. For example, &#x60;fields&#x3D;field1,field2&amp;fields&#x3D;field3&#x60;.  Note: All navigable fields are returned by default. This differs from [GET issue](#api-rest-api-3-issue-issueIdOrKey-get) where the default is all fields. | 
 **expand** | **string** | Use [expand](#expansion) to include additional information about issues in the response. This parameter accepts a comma-separated list. Expand options include:   *  &#x60;renderedFields&#x60; Returns field values rendered in HTML format.  *  &#x60;names&#x60; Returns the display name of each field.  *  &#x60;schema&#x60; Returns the schema describing a field type.  *  &#x60;transitions&#x60; Returns all possible transitions for the issue.  *  &#x60;operations&#x60; Returns all possible operations for the issue.  *  &#x60;editmeta&#x60; Returns information about how each field can be edited.  *  &#x60;changelog&#x60; Returns a list of recent updates to an issue, sorted by date, starting from the most recent.  *  &#x60;versionedRepresentations&#x60; Instead of &#x60;fields&#x60;, returns &#x60;versionedRepresentations&#x60; a JSON array containing each version of a field&#39;s value, with the highest numbered item representing the most recent version. | 
 **properties** | **[]string** | A list of issue property keys for issue properties to include in the results. This parameter accepts a comma-separated list. Multiple properties can also be provided using an ampersand separated list. For example, &#x60;properties&#x3D;prop1,prop2&amp;properties&#x3D;prop3&#x60;. A maximum of 5 issue property keys can be specified. | 
 **fieldsByKeys** | **bool** | Reference fields by their key (rather than ID). | [default to false]

### Return type

[**SearchResults**](SearchResults.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SearchForIssuesUsingJqlPost

> SearchResults SearchForIssuesUsingJqlPost(ctx).SearchRequestBean(searchRequestBean).Execute()

Search for issues using JQL (POST)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    searchRequestBean := *openapiclient.NewSearchRequestBean() // SearchRequestBean | A JSON object containing the search request.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueSearchApi.SearchForIssuesUsingJqlPost(context.Background()).SearchRequestBean(searchRequestBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueSearchApi.SearchForIssuesUsingJqlPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SearchForIssuesUsingJqlPost`: SearchResults
    fmt.Fprintf(os.Stdout, "Response from `IssueSearchApi.SearchForIssuesUsingJqlPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSearchForIssuesUsingJqlPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchRequestBean** | [**SearchRequestBean**](SearchRequestBean.md) | A JSON object containing the search request. | 

### Return type

[**SearchResults**](SearchResults.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


# IssueTypeCreateBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The unique name for the issue type. The maximum length is 60 characters. | 
**Description** | Pointer to **string** | The description of the issue type. | [optional] 
**Type** | Pointer to **string** | Whether the issue type is &#x60;subtype&#x60; or &#x60;standard&#x60;. Defaults to &#x60;standard&#x60;. | [optional] 

## Methods

### NewIssueTypeCreateBean

`func NewIssueTypeCreateBean(name string, ) *IssueTypeCreateBean`

NewIssueTypeCreateBean instantiates a new IssueTypeCreateBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeCreateBeanWithDefaults

`func NewIssueTypeCreateBeanWithDefaults() *IssueTypeCreateBean`

NewIssueTypeCreateBeanWithDefaults instantiates a new IssueTypeCreateBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *IssueTypeCreateBean) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *IssueTypeCreateBean) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *IssueTypeCreateBean) SetName(v string)`

SetName sets Name field to given value.


### GetDescription

`func (o *IssueTypeCreateBean) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *IssueTypeCreateBean) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *IssueTypeCreateBean) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *IssueTypeCreateBean) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetType

`func (o *IssueTypeCreateBean) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *IssueTypeCreateBean) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *IssueTypeCreateBean) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *IssueTypeCreateBean) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



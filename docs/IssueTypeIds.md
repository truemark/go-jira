# IssueTypeIds

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeIds** | **[]string** | The list of issue type IDs. | 

## Methods

### NewIssueTypeIds

`func NewIssueTypeIds(issueTypeIds []string, ) *IssueTypeIds`

NewIssueTypeIds instantiates a new IssueTypeIds object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeIdsWithDefaults

`func NewIssueTypeIdsWithDefaults() *IssueTypeIds`

NewIssueTypeIdsWithDefaults instantiates a new IssueTypeIds object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeIds

`func (o *IssueTypeIds) GetIssueTypeIds() []string`

GetIssueTypeIds returns the IssueTypeIds field if non-nil, zero value otherwise.

### GetIssueTypeIdsOk

`func (o *IssueTypeIds) GetIssueTypeIdsOk() (*[]string, bool)`

GetIssueTypeIdsOk returns a tuple with the IssueTypeIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeIds

`func (o *IssueTypeIds) SetIssueTypeIds(v []string)`

SetIssueTypeIds sets IssueTypeIds field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



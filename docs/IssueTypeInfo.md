# IssueTypeInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int64** | The ID of the issue type. | [optional] [readonly] 
**Name** | Pointer to **string** | The name of the issue type. | [optional] [readonly] 
**AvatarId** | Pointer to **int64** | The avatar of the issue type. | [optional] [readonly] 

## Methods

### NewIssueTypeInfo

`func NewIssueTypeInfo() *IssueTypeInfo`

NewIssueTypeInfo instantiates a new IssueTypeInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeInfoWithDefaults

`func NewIssueTypeInfoWithDefaults() *IssueTypeInfo`

NewIssueTypeInfoWithDefaults instantiates a new IssueTypeInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *IssueTypeInfo) GetId() int64`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *IssueTypeInfo) GetIdOk() (*int64, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *IssueTypeInfo) SetId(v int64)`

SetId sets Id field to given value.

### HasId

`func (o *IssueTypeInfo) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *IssueTypeInfo) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *IssueTypeInfo) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *IssueTypeInfo) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *IssueTypeInfo) HasName() bool`

HasName returns a boolean if a field has been set.

### GetAvatarId

`func (o *IssueTypeInfo) GetAvatarId() int64`

GetAvatarId returns the AvatarId field if non-nil, zero value otherwise.

### GetAvatarIdOk

`func (o *IssueTypeInfo) GetAvatarIdOk() (*int64, bool)`

GetAvatarIdOk returns a tuple with the AvatarId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvatarId

`func (o *IssueTypeInfo) SetAvatarId(v int64)`

SetAvatarId sets AvatarId field to given value.

### HasAvatarId

`func (o *IssueTypeInfo) HasAvatarId() bool`

HasAvatarId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



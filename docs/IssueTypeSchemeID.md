# IssueTypeSchemeID

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeSchemeId** | **string** | The ID of the issue type scheme. | [readonly] 

## Methods

### NewIssueTypeSchemeID

`func NewIssueTypeSchemeID(issueTypeSchemeId string, ) *IssueTypeSchemeID`

NewIssueTypeSchemeID instantiates a new IssueTypeSchemeID object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeSchemeIDWithDefaults

`func NewIssueTypeSchemeIDWithDefaults() *IssueTypeSchemeID`

NewIssueTypeSchemeIDWithDefaults instantiates a new IssueTypeSchemeID object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeSchemeId

`func (o *IssueTypeSchemeID) GetIssueTypeSchemeId() string`

GetIssueTypeSchemeId returns the IssueTypeSchemeId field if non-nil, zero value otherwise.

### GetIssueTypeSchemeIdOk

`func (o *IssueTypeSchemeID) GetIssueTypeSchemeIdOk() (*string, bool)`

GetIssueTypeSchemeIdOk returns a tuple with the IssueTypeSchemeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeSchemeId

`func (o *IssueTypeSchemeID) SetIssueTypeSchemeId(v string)`

SetIssueTypeSchemeId sets IssueTypeSchemeId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



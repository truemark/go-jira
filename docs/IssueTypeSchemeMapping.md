# IssueTypeSchemeMapping

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeSchemeId** | **string** | The ID of the issue type scheme. | 
**IssueTypeId** | **string** | The ID of the issue type. | 

## Methods

### NewIssueTypeSchemeMapping

`func NewIssueTypeSchemeMapping(issueTypeSchemeId string, issueTypeId string, ) *IssueTypeSchemeMapping`

NewIssueTypeSchemeMapping instantiates a new IssueTypeSchemeMapping object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeSchemeMappingWithDefaults

`func NewIssueTypeSchemeMappingWithDefaults() *IssueTypeSchemeMapping`

NewIssueTypeSchemeMappingWithDefaults instantiates a new IssueTypeSchemeMapping object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeSchemeId

`func (o *IssueTypeSchemeMapping) GetIssueTypeSchemeId() string`

GetIssueTypeSchemeId returns the IssueTypeSchemeId field if non-nil, zero value otherwise.

### GetIssueTypeSchemeIdOk

`func (o *IssueTypeSchemeMapping) GetIssueTypeSchemeIdOk() (*string, bool)`

GetIssueTypeSchemeIdOk returns a tuple with the IssueTypeSchemeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeSchemeId

`func (o *IssueTypeSchemeMapping) SetIssueTypeSchemeId(v string)`

SetIssueTypeSchemeId sets IssueTypeSchemeId field to given value.


### GetIssueTypeId

`func (o *IssueTypeSchemeMapping) GetIssueTypeId() string`

GetIssueTypeId returns the IssueTypeId field if non-nil, zero value otherwise.

### GetIssueTypeIdOk

`func (o *IssueTypeSchemeMapping) GetIssueTypeIdOk() (*string, bool)`

GetIssueTypeIdOk returns a tuple with the IssueTypeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeId

`func (o *IssueTypeSchemeMapping) SetIssueTypeId(v string)`

SetIssueTypeId sets IssueTypeId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



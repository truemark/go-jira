# IssueTypeSchemeProjectAssociation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeSchemeId** | **string** | The ID of the issue type scheme. | 
**ProjectId** | **string** | The ID of the project. | 

## Methods

### NewIssueTypeSchemeProjectAssociation

`func NewIssueTypeSchemeProjectAssociation(issueTypeSchemeId string, projectId string, ) *IssueTypeSchemeProjectAssociation`

NewIssueTypeSchemeProjectAssociation instantiates a new IssueTypeSchemeProjectAssociation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeSchemeProjectAssociationWithDefaults

`func NewIssueTypeSchemeProjectAssociationWithDefaults() *IssueTypeSchemeProjectAssociation`

NewIssueTypeSchemeProjectAssociationWithDefaults instantiates a new IssueTypeSchemeProjectAssociation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeSchemeId

`func (o *IssueTypeSchemeProjectAssociation) GetIssueTypeSchemeId() string`

GetIssueTypeSchemeId returns the IssueTypeSchemeId field if non-nil, zero value otherwise.

### GetIssueTypeSchemeIdOk

`func (o *IssueTypeSchemeProjectAssociation) GetIssueTypeSchemeIdOk() (*string, bool)`

GetIssueTypeSchemeIdOk returns a tuple with the IssueTypeSchemeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeSchemeId

`func (o *IssueTypeSchemeProjectAssociation) SetIssueTypeSchemeId(v string)`

SetIssueTypeSchemeId sets IssueTypeSchemeId field to given value.


### GetProjectId

`func (o *IssueTypeSchemeProjectAssociation) GetProjectId() string`

GetProjectId returns the ProjectId field if non-nil, zero value otherwise.

### GetProjectIdOk

`func (o *IssueTypeSchemeProjectAssociation) GetProjectIdOk() (*string, bool)`

GetProjectIdOk returns a tuple with the ProjectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectId

`func (o *IssueTypeSchemeProjectAssociation) SetProjectId(v string)`

SetProjectId sets ProjectId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# IssueTypeSchemeProjects

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeScheme** | [**IssueTypeScheme**](IssueTypeScheme.md) | Details of an issue type scheme. | 
**ProjectIds** | **[]string** | The IDs of the projects using the issue type scheme. | 

## Methods

### NewIssueTypeSchemeProjects

`func NewIssueTypeSchemeProjects(issueTypeScheme IssueTypeScheme, projectIds []string, ) *IssueTypeSchemeProjects`

NewIssueTypeSchemeProjects instantiates a new IssueTypeSchemeProjects object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeSchemeProjectsWithDefaults

`func NewIssueTypeSchemeProjectsWithDefaults() *IssueTypeSchemeProjects`

NewIssueTypeSchemeProjectsWithDefaults instantiates a new IssueTypeSchemeProjects object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeScheme

`func (o *IssueTypeSchemeProjects) GetIssueTypeScheme() IssueTypeScheme`

GetIssueTypeScheme returns the IssueTypeScheme field if non-nil, zero value otherwise.

### GetIssueTypeSchemeOk

`func (o *IssueTypeSchemeProjects) GetIssueTypeSchemeOk() (*IssueTypeScheme, bool)`

GetIssueTypeSchemeOk returns a tuple with the IssueTypeScheme field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeScheme

`func (o *IssueTypeSchemeProjects) SetIssueTypeScheme(v IssueTypeScheme)`

SetIssueTypeScheme sets IssueTypeScheme field to given value.


### GetProjectIds

`func (o *IssueTypeSchemeProjects) GetProjectIds() []string`

GetProjectIds returns the ProjectIds field if non-nil, zero value otherwise.

### GetProjectIdsOk

`func (o *IssueTypeSchemeProjects) GetProjectIdsOk() (*[]string, bool)`

GetProjectIdsOk returns a tuple with the ProjectIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectIds

`func (o *IssueTypeSchemeProjects) SetProjectIds(v []string)`

SetProjectIds sets ProjectIds field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# IssueTypeScreenScheme

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | The ID of the issue type screen scheme. | 
**Name** | **string** | The name of the issue type screen scheme. | 
**Description** | Pointer to **string** | The description of the issue type screen scheme. | [optional] 

## Methods

### NewIssueTypeScreenScheme

`func NewIssueTypeScreenScheme(id string, name string, ) *IssueTypeScreenScheme`

NewIssueTypeScreenScheme instantiates a new IssueTypeScreenScheme object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeScreenSchemeWithDefaults

`func NewIssueTypeScreenSchemeWithDefaults() *IssueTypeScreenScheme`

NewIssueTypeScreenSchemeWithDefaults instantiates a new IssueTypeScreenScheme object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *IssueTypeScreenScheme) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *IssueTypeScreenScheme) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *IssueTypeScreenScheme) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *IssueTypeScreenScheme) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *IssueTypeScreenScheme) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *IssueTypeScreenScheme) SetName(v string)`

SetName sets Name field to given value.


### GetDescription

`func (o *IssueTypeScreenScheme) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *IssueTypeScreenScheme) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *IssueTypeScreenScheme) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *IssueTypeScreenScheme) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# IssueTypeScreenSchemeMappingDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeMappings** | [**[]IssueTypeScreenSchemeMapping**](IssueTypeScreenSchemeMapping.md) | The list of issue type to screen scheme mappings. A *default* entry cannot be specified because a default entry is added when an issue type screen scheme is created. | 

## Methods

### NewIssueTypeScreenSchemeMappingDetails

`func NewIssueTypeScreenSchemeMappingDetails(issueTypeMappings []IssueTypeScreenSchemeMapping, ) *IssueTypeScreenSchemeMappingDetails`

NewIssueTypeScreenSchemeMappingDetails instantiates a new IssueTypeScreenSchemeMappingDetails object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeScreenSchemeMappingDetailsWithDefaults

`func NewIssueTypeScreenSchemeMappingDetailsWithDefaults() *IssueTypeScreenSchemeMappingDetails`

NewIssueTypeScreenSchemeMappingDetailsWithDefaults instantiates a new IssueTypeScreenSchemeMappingDetails object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeMappings

`func (o *IssueTypeScreenSchemeMappingDetails) GetIssueTypeMappings() []IssueTypeScreenSchemeMapping`

GetIssueTypeMappings returns the IssueTypeMappings field if non-nil, zero value otherwise.

### GetIssueTypeMappingsOk

`func (o *IssueTypeScreenSchemeMappingDetails) GetIssueTypeMappingsOk() (*[]IssueTypeScreenSchemeMapping, bool)`

GetIssueTypeMappingsOk returns a tuple with the IssueTypeMappings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeMappings

`func (o *IssueTypeScreenSchemeMappingDetails) SetIssueTypeMappings(v []IssueTypeScreenSchemeMapping)`

SetIssueTypeMappings sets IssueTypeMappings field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



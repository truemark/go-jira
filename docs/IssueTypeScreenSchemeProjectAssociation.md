# IssueTypeScreenSchemeProjectAssociation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeScreenSchemeId** | Pointer to **string** | The ID of the issue type screen scheme. | [optional] 
**ProjectId** | Pointer to **string** | The ID of the project. | [optional] 

## Methods

### NewIssueTypeScreenSchemeProjectAssociation

`func NewIssueTypeScreenSchemeProjectAssociation() *IssueTypeScreenSchemeProjectAssociation`

NewIssueTypeScreenSchemeProjectAssociation instantiates a new IssueTypeScreenSchemeProjectAssociation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeScreenSchemeProjectAssociationWithDefaults

`func NewIssueTypeScreenSchemeProjectAssociationWithDefaults() *IssueTypeScreenSchemeProjectAssociation`

NewIssueTypeScreenSchemeProjectAssociationWithDefaults instantiates a new IssueTypeScreenSchemeProjectAssociation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeScreenSchemeId

`func (o *IssueTypeScreenSchemeProjectAssociation) GetIssueTypeScreenSchemeId() string`

GetIssueTypeScreenSchemeId returns the IssueTypeScreenSchemeId field if non-nil, zero value otherwise.

### GetIssueTypeScreenSchemeIdOk

`func (o *IssueTypeScreenSchemeProjectAssociation) GetIssueTypeScreenSchemeIdOk() (*string, bool)`

GetIssueTypeScreenSchemeIdOk returns a tuple with the IssueTypeScreenSchemeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeScreenSchemeId

`func (o *IssueTypeScreenSchemeProjectAssociation) SetIssueTypeScreenSchemeId(v string)`

SetIssueTypeScreenSchemeId sets IssueTypeScreenSchemeId field to given value.

### HasIssueTypeScreenSchemeId

`func (o *IssueTypeScreenSchemeProjectAssociation) HasIssueTypeScreenSchemeId() bool`

HasIssueTypeScreenSchemeId returns a boolean if a field has been set.

### GetProjectId

`func (o *IssueTypeScreenSchemeProjectAssociation) GetProjectId() string`

GetProjectId returns the ProjectId field if non-nil, zero value otherwise.

### GetProjectIdOk

`func (o *IssueTypeScreenSchemeProjectAssociation) GetProjectIdOk() (*string, bool)`

GetProjectIdOk returns a tuple with the ProjectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectId

`func (o *IssueTypeScreenSchemeProjectAssociation) SetProjectId(v string)`

SetProjectId sets ProjectId field to given value.

### HasProjectId

`func (o *IssueTypeScreenSchemeProjectAssociation) HasProjectId() bool`

HasProjectId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



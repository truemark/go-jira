# IssueTypeScreenSchemeUpdateDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | The name of the issue type screen scheme. The name must be unique. The maximum length is 255 characters. | [optional] 
**Description** | Pointer to **string** | The description of the issue type screen scheme. The maximum length is 255 characters. | [optional] 

## Methods

### NewIssueTypeScreenSchemeUpdateDetails

`func NewIssueTypeScreenSchemeUpdateDetails() *IssueTypeScreenSchemeUpdateDetails`

NewIssueTypeScreenSchemeUpdateDetails instantiates a new IssueTypeScreenSchemeUpdateDetails object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeScreenSchemeUpdateDetailsWithDefaults

`func NewIssueTypeScreenSchemeUpdateDetailsWithDefaults() *IssueTypeScreenSchemeUpdateDetails`

NewIssueTypeScreenSchemeUpdateDetailsWithDefaults instantiates a new IssueTypeScreenSchemeUpdateDetails object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *IssueTypeScreenSchemeUpdateDetails) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *IssueTypeScreenSchemeUpdateDetails) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *IssueTypeScreenSchemeUpdateDetails) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *IssueTypeScreenSchemeUpdateDetails) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDescription

`func (o *IssueTypeScreenSchemeUpdateDetails) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *IssueTypeScreenSchemeUpdateDetails) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *IssueTypeScreenSchemeUpdateDetails) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *IssueTypeScreenSchemeUpdateDetails) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



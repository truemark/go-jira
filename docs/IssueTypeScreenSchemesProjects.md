# IssueTypeScreenSchemesProjects

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueTypeScreenScheme** | [**IssueTypeScreenScheme**](IssueTypeScreenScheme.md) | Details of an issue type screen scheme. | 
**ProjectIds** | **[]string** | The IDs of the projects using the issue type screen scheme. | 

## Methods

### NewIssueTypeScreenSchemesProjects

`func NewIssueTypeScreenSchemesProjects(issueTypeScreenScheme IssueTypeScreenScheme, projectIds []string, ) *IssueTypeScreenSchemesProjects`

NewIssueTypeScreenSchemesProjects instantiates a new IssueTypeScreenSchemesProjects object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssueTypeScreenSchemesProjectsWithDefaults

`func NewIssueTypeScreenSchemesProjectsWithDefaults() *IssueTypeScreenSchemesProjects`

NewIssueTypeScreenSchemesProjectsWithDefaults instantiates a new IssueTypeScreenSchemesProjects object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueTypeScreenScheme

`func (o *IssueTypeScreenSchemesProjects) GetIssueTypeScreenScheme() IssueTypeScreenScheme`

GetIssueTypeScreenScheme returns the IssueTypeScreenScheme field if non-nil, zero value otherwise.

### GetIssueTypeScreenSchemeOk

`func (o *IssueTypeScreenSchemesProjects) GetIssueTypeScreenSchemeOk() (*IssueTypeScreenScheme, bool)`

GetIssueTypeScreenSchemeOk returns a tuple with the IssueTypeScreenScheme field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeScreenScheme

`func (o *IssueTypeScreenSchemesProjects) SetIssueTypeScreenScheme(v IssueTypeScreenScheme)`

SetIssueTypeScreenScheme sets IssueTypeScreenScheme field to given value.


### GetProjectIds

`func (o *IssueTypeScreenSchemesProjects) GetProjectIds() []string`

GetProjectIds returns the ProjectIds field if non-nil, zero value otherwise.

### GetProjectIdsOk

`func (o *IssueTypeScreenSchemesProjects) GetProjectIdsOk() (*[]string, bool)`

GetProjectIdsOk returns a tuple with the ProjectIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectIds

`func (o *IssueTypeScreenSchemesProjects) SetProjectIds(v []string)`

SetProjectIds sets ProjectIds field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \IssueTypesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateIssueType**](IssueTypesApi.md#CreateIssueType) | **Post** /rest/api/3/issuetype | Create issue type
[**CreateIssueTypeAvatar**](IssueTypesApi.md#CreateIssueTypeAvatar) | **Post** /rest/api/3/issuetype/{id}/avatar2 | Load issue type avatar
[**DeleteIssueType**](IssueTypesApi.md#DeleteIssueType) | **Delete** /rest/api/3/issuetype/{id} | Delete issue type
[**GetAlternativeIssueTypes**](IssueTypesApi.md#GetAlternativeIssueTypes) | **Get** /rest/api/3/issuetype/{id}/alternatives | Get alternative issue types
[**GetIssueAllTypes**](IssueTypesApi.md#GetIssueAllTypes) | **Get** /rest/api/3/issuetype | Get all issue types for user
[**GetIssueType**](IssueTypesApi.md#GetIssueType) | **Get** /rest/api/3/issuetype/{id} | Get issue type
[**GetIssueTypesForProject**](IssueTypesApi.md#GetIssueTypesForProject) | **Get** /rest/api/3/issuetype/project | Get issue types for project
[**UpdateIssueType**](IssueTypesApi.md#UpdateIssueType) | **Put** /rest/api/3/issuetype/{id} | Update issue type



## CreateIssueType

> IssueTypeDetails CreateIssueType(ctx).IssueTypeCreateBean(issueTypeCreateBean).Execute()

Create issue type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueTypeCreateBean := *openapiclient.NewIssueTypeCreateBean("Name_example") // IssueTypeCreateBean | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.CreateIssueType(context.Background()).IssueTypeCreateBean(issueTypeCreateBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.CreateIssueType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateIssueType`: IssueTypeDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueTypesApi.CreateIssueType`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateIssueTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueTypeCreateBean** | [**IssueTypeCreateBean**](IssueTypeCreateBean.md) |  | 

### Return type

[**IssueTypeDetails**](IssueTypeDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateIssueTypeAvatar

> Avatar CreateIssueTypeAvatar(ctx, id).Body(body).Size(size).X(x).Y(y).Execute()

Load issue type avatar



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    body := interface{}(987) // interface{} | 
    id := "id_example" // string | The ID of the issue type.
    size := int32(56) // int32 | The length of each side of the crop region.
    x := int32(56) // int32 | The X coordinate of the top-left corner of the crop region. (optional) (default to 0)
    y := int32(56) // int32 | The Y coordinate of the top-left corner of the crop region. (optional) (default to 0)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.CreateIssueTypeAvatar(context.Background(), id).Body(body).Size(size).X(x).Y(y).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.CreateIssueTypeAvatar``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateIssueTypeAvatar`: Avatar
    fmt.Fprintf(os.Stdout, "Response from `IssueTypesApi.CreateIssueTypeAvatar`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateIssueTypeAvatarRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **interface{}** |  | 

 **size** | **int32** | The length of each side of the crop region. | 
 **x** | **int32** | The X coordinate of the top-left corner of the crop region. | [default to 0]
 **y** | **int32** | The Y coordinate of the top-left corner of the crop region. | [default to 0]

### Return type

[**Avatar**](Avatar.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteIssueType

> DeleteIssueType(ctx, id).AlternativeIssueTypeId(alternativeIssueTypeId).Execute()

Delete issue type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | The ID of the issue type.
    alternativeIssueTypeId := "alternativeIssueTypeId_example" // string | The ID of the replacement issue type. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.DeleteIssueType(context.Background(), id).AlternativeIssueTypeId(alternativeIssueTypeId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.DeleteIssueType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteIssueTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **alternativeIssueTypeId** | **string** | The ID of the replacement issue type. | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAlternativeIssueTypes

> []IssueTypeDetails GetAlternativeIssueTypes(ctx, id).Execute()

Get alternative issue types



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | The ID of the issue type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.GetAlternativeIssueTypes(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.GetAlternativeIssueTypes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAlternativeIssueTypes`: []IssueTypeDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueTypesApi.GetAlternativeIssueTypes`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAlternativeIssueTypesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]IssueTypeDetails**](IssueTypeDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssueAllTypes

> []IssueTypeDetails GetIssueAllTypes(ctx).Execute()

Get all issue types for user



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.GetIssueAllTypes(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.GetIssueAllTypes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueAllTypes`: []IssueTypeDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueTypesApi.GetIssueAllTypes`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueAllTypesRequest struct via the builder pattern


### Return type

[**[]IssueTypeDetails**](IssueTypeDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssueType

> IssueTypeDetails GetIssueType(ctx, id).Execute()

Get issue type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := "id_example" // string | The ID of the issue type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.GetIssueType(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.GetIssueType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueType`: IssueTypeDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueTypesApi.GetIssueType`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**IssueTypeDetails**](IssueTypeDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssueTypesForProject

> []IssueTypeDetails GetIssueTypesForProject(ctx).ProjectId(projectId).Level(level).Execute()

Get issue types for project



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectId := int64(789) // int64 | The ID of the project.
    level := int32(56) // int32 | The level of the issue type to filter by. Use:   *  `-1` for Subtask.  *  `0` for Base.  *  `1` for Epic. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.GetIssueTypesForProject(context.Background()).ProjectId(projectId).Level(level).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.GetIssueTypesForProject``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssueTypesForProject`: []IssueTypeDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueTypesApi.GetIssueTypesForProject`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueTypesForProjectRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **int64** | The ID of the project. | 
 **level** | **int32** | The level of the issue type to filter by. Use:   *  &#x60;-1&#x60; for Subtask.  *  &#x60;0&#x60; for Base.  *  &#x60;1&#x60; for Epic. | 

### Return type

[**[]IssueTypeDetails**](IssueTypeDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateIssueType

> IssueTypeDetails UpdateIssueType(ctx, id).IssueTypeUpdateBean(issueTypeUpdateBean).Execute()

Update issue type



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueTypeUpdateBean := *openapiclient.NewIssueTypeUpdateBean() // IssueTypeUpdateBean | 
    id := "id_example" // string | The ID of the issue type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssueTypesApi.UpdateIssueType(context.Background(), id).IssueTypeUpdateBean(issueTypeUpdateBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssueTypesApi.UpdateIssueType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateIssueType`: IssueTypeDetails
    fmt.Fprintf(os.Stdout, "Response from `IssueTypesApi.UpdateIssueType`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateIssueTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueTypeUpdateBean** | [**IssueTypeUpdateBean**](IssueTypeUpdateBean.md) |  | 


### Return type

[**IssueTypeDetails**](IssueTypeDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


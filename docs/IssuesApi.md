# \IssuesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AssignIssue**](IssuesApi.md#AssignIssue) | **Put** /rest/api/3/issue/{issueIdOrKey}/assignee | Assign issue
[**CreateIssue**](IssuesApi.md#CreateIssue) | **Post** /rest/api/3/issue | Create issue
[**CreateIssues**](IssuesApi.md#CreateIssues) | **Post** /rest/api/3/issue/bulk | Bulk create issue
[**DeleteIssue**](IssuesApi.md#DeleteIssue) | **Delete** /rest/api/3/issue/{issueIdOrKey} | Delete issue
[**DoTransition**](IssuesApi.md#DoTransition) | **Post** /rest/api/3/issue/{issueIdOrKey}/transitions | Transition issue
[**EditIssue**](IssuesApi.md#EditIssue) | **Put** /rest/api/3/issue/{issueIdOrKey} | Edit issue
[**GetChangeLogs**](IssuesApi.md#GetChangeLogs) | **Get** /rest/api/3/issue/{issueIdOrKey}/changelog | Get change logs
[**GetCreateIssueMeta**](IssuesApi.md#GetCreateIssueMeta) | **Get** /rest/api/3/issue/createmeta | Get create issue metadata
[**GetEditIssueMeta**](IssuesApi.md#GetEditIssueMeta) | **Get** /rest/api/3/issue/{issueIdOrKey}/editmeta | Get edit issue metadata
[**GetIssue**](IssuesApi.md#GetIssue) | **Get** /rest/api/3/issue/{issueIdOrKey} | Get issue
[**GetTransitions**](IssuesApi.md#GetTransitions) | **Get** /rest/api/3/issue/{issueIdOrKey}/transitions | Get transitions
[**Notify**](IssuesApi.md#Notify) | **Post** /rest/api/3/issue/{issueIdOrKey}/notify | Send notification for issue



## AssignIssue

> interface{} AssignIssue(ctx, issueIdOrKey).User(user).Execute()

Assign issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    user := *openapiclient.NewUser() // User | The request object with the user that the issue is assigned to.
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue to be assigned.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.AssignIssue(context.Background(), issueIdOrKey).User(user).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.AssignIssue``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AssignIssue`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.AssignIssue`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue to be assigned. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAssignIssueRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md) | The request object with the user that the issue is assigned to. | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateIssue

> CreatedIssue CreateIssue(ctx).RequestBody(requestBody).UpdateHistory(updateHistory).Execute()

Create issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := map[string]map[string]interface{}{"key": map[string]interface{}(123)} // map[string]map[string]interface{} | 
    updateHistory := true // bool | Whether the project in which the issue is created is added to the user's **Recently viewed** project list, as shown under **Projects** in Jira. When provided, the issue type and request type are added to the user's history for a project. These values are then used to provide defaults on the issue create screen. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.CreateIssue(context.Background()).RequestBody(requestBody).UpdateHistory(updateHistory).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.CreateIssue``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateIssue`: CreatedIssue
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.CreateIssue`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateIssueRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **map[string]map[string]interface{}** |  | 
 **updateHistory** | **bool** | Whether the project in which the issue is created is added to the user&#39;s **Recently viewed** project list, as shown under **Projects** in Jira. When provided, the issue type and request type are added to the user&#39;s history for a project. These values are then used to provide defaults on the issue create screen. | [default to false]

### Return type

[**CreatedIssue**](CreatedIssue.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateIssues

> CreatedIssues CreateIssues(ctx).RequestBody(requestBody).Execute()

Bulk create issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := map[string]map[string]interface{}{"key": map[string]interface{}(123)} // map[string]map[string]interface{} | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.CreateIssues(context.Background()).RequestBody(requestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.CreateIssues``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateIssues`: CreatedIssues
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.CreateIssues`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateIssuesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **map[string]map[string]interface{}** |  | 

### Return type

[**CreatedIssues**](CreatedIssues.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteIssue

> DeleteIssue(ctx, issueIdOrKey).DeleteSubtasks(deleteSubtasks).Execute()

Delete issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    deleteSubtasks := "deleteSubtasks_example" // string | Whether the issue's subtasks are deleted when the issue is deleted. (optional) (default to "false")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.DeleteIssue(context.Background(), issueIdOrKey).DeleteSubtasks(deleteSubtasks).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.DeleteIssue``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteIssueRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **deleteSubtasks** | **string** | Whether the issue&#39;s subtasks are deleted when the issue is deleted. | [default to &quot;false&quot;]

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DoTransition

> interface{} DoTransition(ctx, issueIdOrKey).RequestBody(requestBody).Execute()

Transition issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := map[string]map[string]interface{}{"key": map[string]interface{}(123)} // map[string]map[string]interface{} | 
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.DoTransition(context.Background(), issueIdOrKey).RequestBody(requestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.DoTransition``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DoTransition`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.DoTransition`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDoTransitionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **map[string]map[string]interface{}** |  | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## EditIssue

> interface{} EditIssue(ctx, issueIdOrKey).RequestBody(requestBody).NotifyUsers(notifyUsers).OverrideScreenSecurity(overrideScreenSecurity).OverrideEditableFlag(overrideEditableFlag).Execute()

Edit issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := map[string]map[string]interface{}{"key": map[string]interface{}(123)} // map[string]map[string]interface{} | 
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    notifyUsers := true // bool | Whether a notification email about the issue update is sent to all watchers. To disable the notification, administer Jira or administer project permissions are required. If the user doesn't have the necessary permission the request is ignored. (optional) (default to true)
    overrideScreenSecurity := true // bool | Whether screen security should be overridden to enable hidden fields to be edited. Available to Connect app users with admin permissions. (optional) (default to false)
    overrideEditableFlag := true // bool | Whether screen security should be overridden to enable uneditable fields to be edited. Available to Connect app users with admin permissions. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.EditIssue(context.Background(), issueIdOrKey).RequestBody(requestBody).NotifyUsers(notifyUsers).OverrideScreenSecurity(overrideScreenSecurity).OverrideEditableFlag(overrideEditableFlag).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.EditIssue``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `EditIssue`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.EditIssue`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiEditIssueRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **map[string]map[string]interface{}** |  | 

 **notifyUsers** | **bool** | Whether a notification email about the issue update is sent to all watchers. To disable the notification, administer Jira or administer project permissions are required. If the user doesn&#39;t have the necessary permission the request is ignored. | [default to true]
 **overrideScreenSecurity** | **bool** | Whether screen security should be overridden to enable hidden fields to be edited. Available to Connect app users with admin permissions. | [default to false]
 **overrideEditableFlag** | **bool** | Whether screen security should be overridden to enable uneditable fields to be edited. Available to Connect app users with admin permissions. | [default to false]

### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChangeLogs

> PageBeanChangelog GetChangeLogs(ctx, issueIdOrKey).StartAt(startAt).MaxResults(maxResults).Execute()

Get change logs



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    startAt := int32(56) // int32 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 100)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.GetChangeLogs(context.Background(), issueIdOrKey).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.GetChangeLogs``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChangeLogs`: PageBeanChangelog
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.GetChangeLogs`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChangeLogsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **startAt** | **int32** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 100]

### Return type

[**PageBeanChangelog**](PageBeanChangelog.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCreateIssueMeta

> IssueCreateMetadata GetCreateIssueMeta(ctx).ProjectIds(projectIds).ProjectKeys(projectKeys).IssuetypeIds(issuetypeIds).IssuetypeNames(issuetypeNames).Expand(expand).Execute()

Get create issue metadata



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIds := []string{"Inner_example"} // []string | List of project IDs. This parameter accepts a comma-separated list. Multiple project IDs can also be provided using an ampersand-separated list. For example, `projectIds=10000,10001&projectIds=10020,10021`. This parameter may be provided with `projectKeys`. (optional)
    projectKeys := []string{"Inner_example"} // []string | List of project keys. This parameter accepts a comma-separated list. Multiple project keys can also be provided using an ampersand-separated list. For example, `projectKeys=proj1,proj2&projectKeys=proj3`. This parameter may be provided with `projectIds`. (optional)
    issuetypeIds := []string{"Inner_example"} // []string | List of issue type IDs. This parameter accepts a comma-separated list. Multiple issue type IDs can also be provided using an ampersand-separated list. For example, `issuetypeIds=10000,10001&issuetypeIds=10020,10021`. This parameter may be provided with `issuetypeNames`. (optional)
    issuetypeNames := []string{"Inner_example"} // []string | List of issue type names. This parameter accepts a comma-separated list. Multiple issue type names can also be provided using an ampersand-separated list. For example, `issuetypeNames=name1,name2&issuetypeNames=name3`. This parameter may be provided with `issuetypeIds`. (optional)
    expand := "expand_example" // string | Use [expand](#expansion) to include additional information about issue metadata in the response. This parameter accepts `projects.issuetypes.fields`, which returns information about the fields in the issue creation screen for each issue type. Fields hidden from the screen are not returned. Use the information to populate the `fields` and `update` fields in [Create issue](#api-rest-api-3-issue-post) and [Create issues](#api-rest-api-3-issue-bulk-post). (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.GetCreateIssueMeta(context.Background()).ProjectIds(projectIds).ProjectKeys(projectKeys).IssuetypeIds(issuetypeIds).IssuetypeNames(issuetypeNames).Expand(expand).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.GetCreateIssueMeta``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCreateIssueMeta`: IssueCreateMetadata
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.GetCreateIssueMeta`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetCreateIssueMetaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectIds** | **[]string** | List of project IDs. This parameter accepts a comma-separated list. Multiple project IDs can also be provided using an ampersand-separated list. For example, &#x60;projectIds&#x3D;10000,10001&amp;projectIds&#x3D;10020,10021&#x60;. This parameter may be provided with &#x60;projectKeys&#x60;. | 
 **projectKeys** | **[]string** | List of project keys. This parameter accepts a comma-separated list. Multiple project keys can also be provided using an ampersand-separated list. For example, &#x60;projectKeys&#x3D;proj1,proj2&amp;projectKeys&#x3D;proj3&#x60;. This parameter may be provided with &#x60;projectIds&#x60;. | 
 **issuetypeIds** | **[]string** | List of issue type IDs. This parameter accepts a comma-separated list. Multiple issue type IDs can also be provided using an ampersand-separated list. For example, &#x60;issuetypeIds&#x3D;10000,10001&amp;issuetypeIds&#x3D;10020,10021&#x60;. This parameter may be provided with &#x60;issuetypeNames&#x60;. | 
 **issuetypeNames** | **[]string** | List of issue type names. This parameter accepts a comma-separated list. Multiple issue type names can also be provided using an ampersand-separated list. For example, &#x60;issuetypeNames&#x3D;name1,name2&amp;issuetypeNames&#x3D;name3&#x60;. This parameter may be provided with &#x60;issuetypeIds&#x60;. | 
 **expand** | **string** | Use [expand](#expansion) to include additional information about issue metadata in the response. This parameter accepts &#x60;projects.issuetypes.fields&#x60;, which returns information about the fields in the issue creation screen for each issue type. Fields hidden from the screen are not returned. Use the information to populate the &#x60;fields&#x60; and &#x60;update&#x60; fields in [Create issue](#api-rest-api-3-issue-post) and [Create issues](#api-rest-api-3-issue-bulk-post). | 

### Return type

[**IssueCreateMetadata**](IssueCreateMetadata.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetEditIssueMeta

> IssueUpdateMetadata GetEditIssueMeta(ctx, issueIdOrKey).OverrideScreenSecurity(overrideScreenSecurity).OverrideEditableFlag(overrideEditableFlag).Execute()

Get edit issue metadata



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    overrideScreenSecurity := true // bool | Whether hidden fields should be returned. Available to connect app users with admin permissions. (optional) (default to false)
    overrideEditableFlag := true // bool | Whether non-editable fields should be returned. Available to connect app users with admin permissions. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.GetEditIssueMeta(context.Background(), issueIdOrKey).OverrideScreenSecurity(overrideScreenSecurity).OverrideEditableFlag(overrideEditableFlag).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.GetEditIssueMeta``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetEditIssueMeta`: IssueUpdateMetadata
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.GetEditIssueMeta`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetEditIssueMetaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **overrideScreenSecurity** | **bool** | Whether hidden fields should be returned. Available to connect app users with admin permissions. | [default to false]
 **overrideEditableFlag** | **bool** | Whether non-editable fields should be returned. Available to connect app users with admin permissions. | [default to false]

### Return type

[**IssueUpdateMetadata**](IssueUpdateMetadata.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetIssue

> IssueBean GetIssue(ctx, issueIdOrKey).Fields(fields).FieldsByKeys(fieldsByKeys).Expand(expand).Properties(properties).UpdateHistory(updateHistory).Execute()

Get issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    fields := []string{"Inner_example"} // []string | A list of fields to return for the issue. This parameter accepts a comma-separated list. Use it to retrieve a subset of fields. Allowed values:   *  `*all` Returns all fields.  *  `*navigable` Returns navigable fields.  *  Any issue field, prefixed with a minus to exclude.  Examples:   *  `summary,comment` Returns only the summary and comments fields.  *  `-description` Returns all (default) fields except description.  *  `*navigable,-comment` Returns all navigable fields except comment.  This parameter may be specified multiple times. For example, `fields=field1,field2& fields=field3`.  Note: All fields are returned by default. This differs from [Search for issues using JQL (GET)](#api-rest-api-3-search-get) and [Search for issues using JQL (POST)](#api-rest-api-3-search-post) where the default is all navigable fields. (optional)
    fieldsByKeys := true // bool | Whether fields in `fields` are referenced by keys rather than IDs. This parameter is useful where fields have been added by a connect app and a field's key may differ from its ID. (optional) (default to false)
    expand := "expand_example" // string | Use [expand](#expansion) to include additional information about the issues in the response. This parameter accepts a comma-separated list. Expand options include:   *  `renderedFields` Returns field values rendered in HTML format.  *  `names` Returns the display name of each field.  *  `schema` Returns the schema describing a field type.  *  `transitions` Returns all possible transitions for the issue.  *  `editmeta` Returns information about how each field can be edited.  *  `changelog` Returns a list of recent updates to an issue, sorted by date, starting from the most recent.  *  `versionedRepresentations` Returns a JSON array for each version of a field's value, with the highest number representing the most recent version. Note: When included in the request, the `fields` parameter is ignored. (optional)
    properties := []string{"Inner_example"} // []string | A list of issue properties to return for the issue. This parameter accepts a comma-separated list. Allowed values:   *  `*all` Returns all issue properties.  *  Any issue property key, prefixed with a minus to exclude.  Examples:   *  `*all` Returns all properties.  *  `*all,-prop1` Returns all properties except `prop1`.  *  `prop1,prop2` Returns `prop1` and `prop2` properties.  This parameter may be specified multiple times. For example, `properties=prop1,prop2& properties=prop3`. (optional)
    updateHistory := true // bool | Whether the project in which the issue is created is added to the user's **Recently viewed** project list, as shown under **Projects** in Jira. This also populates the [JQL issues search](#api-rest-api-3-search-get) `lastViewed` field. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.GetIssue(context.Background(), issueIdOrKey).Fields(fields).FieldsByKeys(fieldsByKeys).Expand(expand).Properties(properties).UpdateHistory(updateHistory).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.GetIssue``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetIssue`: IssueBean
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.GetIssue`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetIssueRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **fields** | **[]string** | A list of fields to return for the issue. This parameter accepts a comma-separated list. Use it to retrieve a subset of fields. Allowed values:   *  &#x60;*all&#x60; Returns all fields.  *  &#x60;*navigable&#x60; Returns navigable fields.  *  Any issue field, prefixed with a minus to exclude.  Examples:   *  &#x60;summary,comment&#x60; Returns only the summary and comments fields.  *  &#x60;-description&#x60; Returns all (default) fields except description.  *  &#x60;*navigable,-comment&#x60; Returns all navigable fields except comment.  This parameter may be specified multiple times. For example, &#x60;fields&#x3D;field1,field2&amp; fields&#x3D;field3&#x60;.  Note: All fields are returned by default. This differs from [Search for issues using JQL (GET)](#api-rest-api-3-search-get) and [Search for issues using JQL (POST)](#api-rest-api-3-search-post) where the default is all navigable fields. | 
 **fieldsByKeys** | **bool** | Whether fields in &#x60;fields&#x60; are referenced by keys rather than IDs. This parameter is useful where fields have been added by a connect app and a field&#39;s key may differ from its ID. | [default to false]
 **expand** | **string** | Use [expand](#expansion) to include additional information about the issues in the response. This parameter accepts a comma-separated list. Expand options include:   *  &#x60;renderedFields&#x60; Returns field values rendered in HTML format.  *  &#x60;names&#x60; Returns the display name of each field.  *  &#x60;schema&#x60; Returns the schema describing a field type.  *  &#x60;transitions&#x60; Returns all possible transitions for the issue.  *  &#x60;editmeta&#x60; Returns information about how each field can be edited.  *  &#x60;changelog&#x60; Returns a list of recent updates to an issue, sorted by date, starting from the most recent.  *  &#x60;versionedRepresentations&#x60; Returns a JSON array for each version of a field&#39;s value, with the highest number representing the most recent version. Note: When included in the request, the &#x60;fields&#x60; parameter is ignored. | 
 **properties** | **[]string** | A list of issue properties to return for the issue. This parameter accepts a comma-separated list. Allowed values:   *  &#x60;*all&#x60; Returns all issue properties.  *  Any issue property key, prefixed with a minus to exclude.  Examples:   *  &#x60;*all&#x60; Returns all properties.  *  &#x60;*all,-prop1&#x60; Returns all properties except &#x60;prop1&#x60;.  *  &#x60;prop1,prop2&#x60; Returns &#x60;prop1&#x60; and &#x60;prop2&#x60; properties.  This parameter may be specified multiple times. For example, &#x60;properties&#x3D;prop1,prop2&amp; properties&#x3D;prop3&#x60;. | 
 **updateHistory** | **bool** | Whether the project in which the issue is created is added to the user&#39;s **Recently viewed** project list, as shown under **Projects** in Jira. This also populates the [JQL issues search](#api-rest-api-3-search-get) &#x60;lastViewed&#x60; field. | [default to false]

### Return type

[**IssueBean**](IssueBean.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTransitions

> Transitions GetTransitions(ctx, issueIdOrKey).Expand(expand).TransitionId(transitionId).SkipRemoteOnlyCondition(skipRemoteOnlyCondition).IncludeUnavailableTransitions(includeUnavailableTransitions).SortByOpsBarAndStatus(sortByOpsBarAndStatus).Execute()

Get transitions



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueIdOrKey := "issueIdOrKey_example" // string | The ID or key of the issue.
    expand := "expand_example" // string | Use [expand](#expansion) to include additional information about transitions in the response. This parameter accepts `transitions.fields`, which returns information about the fields in the transition screen for each transition. Fields hidden from the screen are not returned. Use this information to populate the `fields` and `update` fields in [Transition issue](#api-rest-api-3-issue-issueIdOrKey-transitions-post). (optional)
    transitionId := "transitionId_example" // string | The ID of the transition. (optional)
    skipRemoteOnlyCondition := true // bool | Whether transitions with the condition *Hide From User Condition* are included in the response. (optional) (default to false)
    includeUnavailableTransitions := true // bool | Whether details of transitions that fail a condition are included in the response (optional) (default to false)
    sortByOpsBarAndStatus := true // bool | Whether the transitions are sorted by ops-bar sequence value first then category order (Todo, In Progress, Done) or only by ops-bar sequence value. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.GetTransitions(context.Background(), issueIdOrKey).Expand(expand).TransitionId(transitionId).SkipRemoteOnlyCondition(skipRemoteOnlyCondition).IncludeUnavailableTransitions(includeUnavailableTransitions).SortByOpsBarAndStatus(sortByOpsBarAndStatus).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.GetTransitions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTransitions`: Transitions
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.GetTransitions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | The ID or key of the issue. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTransitionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **expand** | **string** | Use [expand](#expansion) to include additional information about transitions in the response. This parameter accepts &#x60;transitions.fields&#x60;, which returns information about the fields in the transition screen for each transition. Fields hidden from the screen are not returned. Use this information to populate the &#x60;fields&#x60; and &#x60;update&#x60; fields in [Transition issue](#api-rest-api-3-issue-issueIdOrKey-transitions-post). | 
 **transitionId** | **string** | The ID of the transition. | 
 **skipRemoteOnlyCondition** | **bool** | Whether transitions with the condition *Hide From User Condition* are included in the response. | [default to false]
 **includeUnavailableTransitions** | **bool** | Whether details of transitions that fail a condition are included in the response | [default to false]
 **sortByOpsBarAndStatus** | **bool** | Whether the transitions are sorted by ops-bar sequence value first then category order (Todo, In Progress, Done) or only by ops-bar sequence value. | [default to false]

### Return type

[**Transitions**](Transitions.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Notify

> interface{} Notify(ctx, issueIdOrKey).RequestBody(requestBody).Execute()

Send notification for issue



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    requestBody := map[string]map[string]interface{}{"key": map[string]interface{}(123)} // map[string]map[string]interface{} | The request object for the notification and recipients.
    issueIdOrKey := "issueIdOrKey_example" // string | ID or key of the issue that the notification is sent for.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.IssuesApi.Notify(context.Background(), issueIdOrKey).RequestBody(requestBody).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `IssuesApi.Notify``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `Notify`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `IssuesApi.Notify`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**issueIdOrKey** | **string** | ID or key of the issue that the notification is sent for. | 

### Other Parameters

Other parameters are passed through a pointer to a apiNotifyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestBody** | **map[string]map[string]interface{}** | The request object for the notification and recipients. | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


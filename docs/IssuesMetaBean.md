# IssuesMetaBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Jql** | Pointer to [**IssuesJqlMetaDataBean**](IssuesJqlMetaDataBean.md) |  | [optional] 

## Methods

### NewIssuesMetaBean

`func NewIssuesMetaBean() *IssuesMetaBean`

NewIssuesMetaBean instantiates a new IssuesMetaBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssuesMetaBeanWithDefaults

`func NewIssuesMetaBeanWithDefaults() *IssuesMetaBean`

NewIssuesMetaBeanWithDefaults instantiates a new IssuesMetaBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetJql

`func (o *IssuesMetaBean) GetJql() IssuesJqlMetaDataBean`

GetJql returns the Jql field if non-nil, zero value otherwise.

### GetJqlOk

`func (o *IssuesMetaBean) GetJqlOk() (*IssuesJqlMetaDataBean, bool)`

GetJqlOk returns a tuple with the Jql field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJql

`func (o *IssuesMetaBean) SetJql(v IssuesJqlMetaDataBean)`

SetJql sets Jql field to given value.

### HasJql

`func (o *IssuesMetaBean) HasJql() bool`

HasJql returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# IssuesUpdateBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueUpdates** | Pointer to [**[]IssueUpdateDetails**](IssueUpdateDetails.md) |  | [optional] 

## Methods

### NewIssuesUpdateBean

`func NewIssuesUpdateBean() *IssuesUpdateBean`

NewIssuesUpdateBean instantiates a new IssuesUpdateBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIssuesUpdateBeanWithDefaults

`func NewIssuesUpdateBeanWithDefaults() *IssuesUpdateBean`

NewIssuesUpdateBeanWithDefaults instantiates a new IssuesUpdateBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueUpdates

`func (o *IssuesUpdateBean) GetIssueUpdates() []IssueUpdateDetails`

GetIssueUpdates returns the IssueUpdates field if non-nil, zero value otherwise.

### GetIssueUpdatesOk

`func (o *IssuesUpdateBean) GetIssueUpdatesOk() (*[]IssueUpdateDetails, bool)`

GetIssueUpdatesOk returns a tuple with the IssueUpdates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueUpdates

`func (o *IssuesUpdateBean) SetIssueUpdates(v []IssueUpdateDetails)`

SetIssueUpdates sets IssueUpdates field to given value.

### HasIssueUpdates

`func (o *IssuesUpdateBean) HasIssueUpdates() bool`

HasIssueUpdates returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



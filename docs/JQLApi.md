# \JQLApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAutoComplete**](JQLApi.md#GetAutoComplete) | **Get** /rest/api/3/jql/autocompletedata | Get field reference data (GET)
[**GetAutoCompletePost**](JQLApi.md#GetAutoCompletePost) | **Post** /rest/api/3/jql/autocompletedata | Get field reference data (POST)
[**GetFieldAutoCompleteForQueryString**](JQLApi.md#GetFieldAutoCompleteForQueryString) | **Get** /rest/api/3/jql/autocompletedata/suggestions | Get field auto complete suggestions
[**MigrateQueries**](JQLApi.md#MigrateQueries) | **Post** /rest/api/3/jql/pdcleaner | Convert user identifiers to account IDs in JQL queries
[**ParseJqlQueries**](JQLApi.md#ParseJqlQueries) | **Post** /rest/api/3/jql/parse | Parse JQL query



## GetAutoComplete

> JQLReferenceData GetAutoComplete(ctx).Execute()

Get field reference data (GET)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.JQLApi.GetAutoComplete(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `JQLApi.GetAutoComplete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAutoComplete`: JQLReferenceData
    fmt.Fprintf(os.Stdout, "Response from `JQLApi.GetAutoComplete`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetAutoCompleteRequest struct via the builder pattern


### Return type

[**JQLReferenceData**](JQLReferenceData.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAutoCompletePost

> JQLReferenceData GetAutoCompletePost(ctx).SearchAutoCompleteFilter(searchAutoCompleteFilter).Execute()

Get field reference data (POST)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    searchAutoCompleteFilter := *openapiclient.NewSearchAutoCompleteFilter() // SearchAutoCompleteFilter | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.JQLApi.GetAutoCompletePost(context.Background()).SearchAutoCompleteFilter(searchAutoCompleteFilter).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `JQLApi.GetAutoCompletePost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAutoCompletePost`: JQLReferenceData
    fmt.Fprintf(os.Stdout, "Response from `JQLApi.GetAutoCompletePost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAutoCompletePostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchAutoCompleteFilter** | [**SearchAutoCompleteFilter**](SearchAutoCompleteFilter.md) |  | 

### Return type

[**JQLReferenceData**](JQLReferenceData.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFieldAutoCompleteForQueryString

> AutoCompleteSuggestions GetFieldAutoCompleteForQueryString(ctx).FieldName(fieldName).FieldValue(fieldValue).PredicateName(predicateName).PredicateValue(predicateValue).Execute()

Get field auto complete suggestions



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    fieldName := "reporter" // string | The name of the field. (optional)
    fieldValue := "fieldValue_example" // string | The partial field item name entered by the user. (optional)
    predicateName := "predicateName_example" // string | The name of the [ CHANGED operator predicate](https://confluence.atlassian.com/x/hQORLQ#Advancedsearching-operatorsreference-CHANGEDCHANGED) for which the suggestions are generated. The valid predicate operators are *by*, *from*, and *to*. (optional)
    predicateValue := "predicateValue_example" // string | The partial predicate item name entered by the user. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.JQLApi.GetFieldAutoCompleteForQueryString(context.Background()).FieldName(fieldName).FieldValue(fieldValue).PredicateName(predicateName).PredicateValue(predicateValue).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `JQLApi.GetFieldAutoCompleteForQueryString``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFieldAutoCompleteForQueryString`: AutoCompleteSuggestions
    fmt.Fprintf(os.Stdout, "Response from `JQLApi.GetFieldAutoCompleteForQueryString`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetFieldAutoCompleteForQueryStringRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fieldName** | **string** | The name of the field. | 
 **fieldValue** | **string** | The partial field item name entered by the user. | 
 **predicateName** | **string** | The name of the [ CHANGED operator predicate](https://confluence.atlassian.com/x/hQORLQ#Advancedsearching-operatorsreference-CHANGEDCHANGED) for which the suggestions are generated. The valid predicate operators are *by*, *from*, and *to*. | 
 **predicateValue** | **string** | The partial predicate item name entered by the user. | 

### Return type

[**AutoCompleteSuggestions**](AutoCompleteSuggestions.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MigrateQueries

> ConvertedJQLQueries MigrateQueries(ctx).JQLPersonalDataMigrationRequest(jQLPersonalDataMigrationRequest).Execute()

Convert user identifiers to account IDs in JQL queries



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    jQLPersonalDataMigrationRequest := *openapiclient.NewJQLPersonalDataMigrationRequest() // JQLPersonalDataMigrationRequest | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.JQLApi.MigrateQueries(context.Background()).JQLPersonalDataMigrationRequest(jQLPersonalDataMigrationRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `JQLApi.MigrateQueries``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `MigrateQueries`: ConvertedJQLQueries
    fmt.Fprintf(os.Stdout, "Response from `JQLApi.MigrateQueries`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiMigrateQueriesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jQLPersonalDataMigrationRequest** | [**JQLPersonalDataMigrationRequest**](JQLPersonalDataMigrationRequest.md) |  | 

### Return type

[**ConvertedJQLQueries**](ConvertedJQLQueries.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ParseJqlQueries

> ParsedJqlQueries ParseJqlQueries(ctx).JqlQueriesToParse(jqlQueriesToParse).Validation(validation).Execute()

Parse JQL query



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    jqlQueriesToParse := *openapiclient.NewJqlQueriesToParse([]string{"Queries_example"}) // JqlQueriesToParse | 
    validation := "validation_example" // string | How to validate the JQL query and treat the validation results. Validation options include:   *  `strict` Returns all errors. If validation fails, the query structure is not returned.  *  `warn` Returns all errors. If validation fails but the JQL query is correctly formed, the query structure is returned.  *  `none` No validation is performed. If JQL query is correctly formed, the query structure is returned. (optional) (default to "strict")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.JQLApi.ParseJqlQueries(context.Background()).JqlQueriesToParse(jqlQueriesToParse).Validation(validation).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `JQLApi.ParseJqlQueries``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ParseJqlQueries`: ParsedJqlQueries
    fmt.Fprintf(os.Stdout, "Response from `JQLApi.ParseJqlQueries`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiParseJqlQueriesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jqlQueriesToParse** | [**JqlQueriesToParse**](JqlQueriesToParse.md) |  | 
 **validation** | **string** | How to validate the JQL query and treat the validation results. Validation options include:   *  &#x60;strict&#x60; Returns all errors. If validation fails, the query structure is not returned.  *  &#x60;warn&#x60; Returns all errors. If validation fails but the JQL query is correctly formed, the query structure is returned.  *  &#x60;none&#x60; No validation is performed. If JQL query is correctly formed, the query structure is returned. | [default to &quot;strict&quot;]

### Return type

[**ParsedJqlQueries**](ParsedJqlQueries.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


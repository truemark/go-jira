# JQLPersonalDataMigrationRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**QueryStrings** | Pointer to **[]string** | A list of queries with user identifiers. Maximum of 100 queries. | [optional] 

## Methods

### NewJQLPersonalDataMigrationRequest

`func NewJQLPersonalDataMigrationRequest() *JQLPersonalDataMigrationRequest`

NewJQLPersonalDataMigrationRequest instantiates a new JQLPersonalDataMigrationRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewJQLPersonalDataMigrationRequestWithDefaults

`func NewJQLPersonalDataMigrationRequestWithDefaults() *JQLPersonalDataMigrationRequest`

NewJQLPersonalDataMigrationRequestWithDefaults instantiates a new JQLPersonalDataMigrationRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetQueryStrings

`func (o *JQLPersonalDataMigrationRequest) GetQueryStrings() []string`

GetQueryStrings returns the QueryStrings field if non-nil, zero value otherwise.

### GetQueryStringsOk

`func (o *JQLPersonalDataMigrationRequest) GetQueryStringsOk() (*[]string, bool)`

GetQueryStringsOk returns a tuple with the QueryStrings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQueryStrings

`func (o *JQLPersonalDataMigrationRequest) SetQueryStrings(v []string)`

SetQueryStrings sets QueryStrings field to given value.

### HasQueryStrings

`func (o *JQLPersonalDataMigrationRequest) HasQueryStrings() bool`

HasQueryStrings returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



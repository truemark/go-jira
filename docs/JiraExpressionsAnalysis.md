# JiraExpressionsAnalysis

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]JiraExpressionAnalysis**](JiraExpressionAnalysis.md) | The results of Jira expressions analysis. | 

## Methods

### NewJiraExpressionsAnalysis

`func NewJiraExpressionsAnalysis(results []JiraExpressionAnalysis, ) *JiraExpressionsAnalysis`

NewJiraExpressionsAnalysis instantiates a new JiraExpressionsAnalysis object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewJiraExpressionsAnalysisWithDefaults

`func NewJiraExpressionsAnalysisWithDefaults() *JiraExpressionsAnalysis`

NewJiraExpressionsAnalysisWithDefaults instantiates a new JiraExpressionsAnalysis object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *JiraExpressionsAnalysis) GetResults() []JiraExpressionAnalysis`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *JiraExpressionsAnalysis) GetResultsOk() (*[]JiraExpressionAnalysis, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *JiraExpressionsAnalysis) SetResults(v []JiraExpressionAnalysis)`

SetResults sets Results field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



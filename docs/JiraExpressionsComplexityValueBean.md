# JiraExpressionsComplexityValueBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **int32** | The complexity value of the current expression. | 
**Limit** | **int32** | The maximum allowed complexity. The evaluation will fail if this value is exceeded. | 

## Methods

### NewJiraExpressionsComplexityValueBean

`func NewJiraExpressionsComplexityValueBean(value int32, limit int32, ) *JiraExpressionsComplexityValueBean`

NewJiraExpressionsComplexityValueBean instantiates a new JiraExpressionsComplexityValueBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewJiraExpressionsComplexityValueBeanWithDefaults

`func NewJiraExpressionsComplexityValueBeanWithDefaults() *JiraExpressionsComplexityValueBean`

NewJiraExpressionsComplexityValueBeanWithDefaults instantiates a new JiraExpressionsComplexityValueBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetValue

`func (o *JiraExpressionsComplexityValueBean) GetValue() int32`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *JiraExpressionsComplexityValueBean) GetValueOk() (*int32, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *JiraExpressionsComplexityValueBean) SetValue(v int32)`

SetValue sets Value field to given value.


### GetLimit

`func (o *JiraExpressionsComplexityValueBean) GetLimit() int32`

GetLimit returns the Limit field if non-nil, zero value otherwise.

### GetLimitOk

`func (o *JiraExpressionsComplexityValueBean) GetLimitOk() (*int32, bool)`

GetLimitOk returns a tuple with the Limit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLimit

`func (o *JiraExpressionsComplexityValueBean) SetLimit(v int32)`

SetLimit sets Limit field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# JqlQueryOrderByClause

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fields** | [**[]JqlQueryOrderByClauseElement**](JqlQueryOrderByClauseElement.md) | The list of order-by clause fields and their ordering directives. | 

## Methods

### NewJqlQueryOrderByClause

`func NewJqlQueryOrderByClause(fields []JqlQueryOrderByClauseElement, ) *JqlQueryOrderByClause`

NewJqlQueryOrderByClause instantiates a new JqlQueryOrderByClause object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewJqlQueryOrderByClauseWithDefaults

`func NewJqlQueryOrderByClauseWithDefaults() *JqlQueryOrderByClause`

NewJqlQueryOrderByClauseWithDefaults instantiates a new JqlQueryOrderByClause object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFields

`func (o *JqlQueryOrderByClause) GetFields() []JqlQueryOrderByClauseElement`

GetFields returns the Fields field if non-nil, zero value otherwise.

### GetFieldsOk

`func (o *JqlQueryOrderByClause) GetFieldsOk() (*[]JqlQueryOrderByClauseElement, bool)`

GetFieldsOk returns a tuple with the Fields field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFields

`func (o *JqlQueryOrderByClause) SetFields(v []JqlQueryOrderByClauseElement)`

SetFields sets Fields field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



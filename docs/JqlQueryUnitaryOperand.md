# JqlQueryUnitaryOperand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **string** | The operand value. | 
**Function** | **string** | The name of the function. | 
**Arguments** | **[]string** | The list of function arguments. | 
**Keyword** | **string** | The keyword that is the operand value. | 

## Methods

### NewJqlQueryUnitaryOperand

`func NewJqlQueryUnitaryOperand(value string, function string, arguments []string, keyword string, ) *JqlQueryUnitaryOperand`

NewJqlQueryUnitaryOperand instantiates a new JqlQueryUnitaryOperand object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewJqlQueryUnitaryOperandWithDefaults

`func NewJqlQueryUnitaryOperandWithDefaults() *JqlQueryUnitaryOperand`

NewJqlQueryUnitaryOperandWithDefaults instantiates a new JqlQueryUnitaryOperand object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetValue

`func (o *JqlQueryUnitaryOperand) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *JqlQueryUnitaryOperand) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *JqlQueryUnitaryOperand) SetValue(v string)`

SetValue sets Value field to given value.


### GetFunction

`func (o *JqlQueryUnitaryOperand) GetFunction() string`

GetFunction returns the Function field if non-nil, zero value otherwise.

### GetFunctionOk

`func (o *JqlQueryUnitaryOperand) GetFunctionOk() (*string, bool)`

GetFunctionOk returns a tuple with the Function field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFunction

`func (o *JqlQueryUnitaryOperand) SetFunction(v string)`

SetFunction sets Function field to given value.


### GetArguments

`func (o *JqlQueryUnitaryOperand) GetArguments() []string`

GetArguments returns the Arguments field if non-nil, zero value otherwise.

### GetArgumentsOk

`func (o *JqlQueryUnitaryOperand) GetArgumentsOk() (*[]string, bool)`

GetArgumentsOk returns a tuple with the Arguments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetArguments

`func (o *JqlQueryUnitaryOperand) SetArguments(v []string)`

SetArguments sets Arguments field to given value.


### GetKeyword

`func (o *JqlQueryUnitaryOperand) GetKeyword() string`

GetKeyword returns the Keyword field if non-nil, zero value otherwise.

### GetKeywordOk

`func (o *JqlQueryUnitaryOperand) GetKeywordOk() (*string, bool)`

GetKeywordOk returns a tuple with the Keyword field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKeyword

`func (o *JqlQueryUnitaryOperand) SetKeyword(v string)`

SetKeyword sets Keyword field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# KeywordOperand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Keyword** | **string** | The keyword that is the operand value. | 

## Methods

### NewKeywordOperand

`func NewKeywordOperand(keyword string, ) *KeywordOperand`

NewKeywordOperand instantiates a new KeywordOperand object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewKeywordOperandWithDefaults

`func NewKeywordOperandWithDefaults() *KeywordOperand`

NewKeywordOperandWithDefaults instantiates a new KeywordOperand object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKeyword

`func (o *KeywordOperand) GetKeyword() string`

GetKeyword returns the Keyword field if non-nil, zero value otherwise.

### GetKeywordOk

`func (o *KeywordOperand) GetKeywordOk() (*string, bool)`

GetKeywordOk returns a tuple with the Keyword field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKeyword

`func (o *KeywordOperand) SetKeyword(v string)`

SetKeyword sets Keyword field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# PageBeanIssueTypeScreenScheme

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | Pointer to **string** | The URL of the page. | [optional] [readonly] 
**NextPage** | Pointer to **string** | If there is another page of results, the URL of the next page. | [optional] [readonly] 
**MaxResults** | Pointer to **int32** | The maximum number of items that could be returned. | [optional] [readonly] 
**StartAt** | Pointer to **int64** | The index of the first item returned. | [optional] [readonly] 
**Total** | Pointer to **int64** | The number of items returned. | [optional] [readonly] 
**IsLast** | Pointer to **bool** | Whether this is the last page. | [optional] [readonly] 
**Values** | Pointer to [**[]IssueTypeScreenScheme**](IssueTypeScreenScheme.md) | The list of items. | [optional] [readonly] 

## Methods

### NewPageBeanIssueTypeScreenScheme

`func NewPageBeanIssueTypeScreenScheme() *PageBeanIssueTypeScreenScheme`

NewPageBeanIssueTypeScreenScheme instantiates a new PageBeanIssueTypeScreenScheme object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPageBeanIssueTypeScreenSchemeWithDefaults

`func NewPageBeanIssueTypeScreenSchemeWithDefaults() *PageBeanIssueTypeScreenScheme`

NewPageBeanIssueTypeScreenSchemeWithDefaults instantiates a new PageBeanIssueTypeScreenScheme object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *PageBeanIssueTypeScreenScheme) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *PageBeanIssueTypeScreenScheme) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *PageBeanIssueTypeScreenScheme) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *PageBeanIssueTypeScreenScheme) HasSelf() bool`

HasSelf returns a boolean if a field has been set.

### GetNextPage

`func (o *PageBeanIssueTypeScreenScheme) GetNextPage() string`

GetNextPage returns the NextPage field if non-nil, zero value otherwise.

### GetNextPageOk

`func (o *PageBeanIssueTypeScreenScheme) GetNextPageOk() (*string, bool)`

GetNextPageOk returns a tuple with the NextPage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNextPage

`func (o *PageBeanIssueTypeScreenScheme) SetNextPage(v string)`

SetNextPage sets NextPage field to given value.

### HasNextPage

`func (o *PageBeanIssueTypeScreenScheme) HasNextPage() bool`

HasNextPage returns a boolean if a field has been set.

### GetMaxResults

`func (o *PageBeanIssueTypeScreenScheme) GetMaxResults() int32`

GetMaxResults returns the MaxResults field if non-nil, zero value otherwise.

### GetMaxResultsOk

`func (o *PageBeanIssueTypeScreenScheme) GetMaxResultsOk() (*int32, bool)`

GetMaxResultsOk returns a tuple with the MaxResults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxResults

`func (o *PageBeanIssueTypeScreenScheme) SetMaxResults(v int32)`

SetMaxResults sets MaxResults field to given value.

### HasMaxResults

`func (o *PageBeanIssueTypeScreenScheme) HasMaxResults() bool`

HasMaxResults returns a boolean if a field has been set.

### GetStartAt

`func (o *PageBeanIssueTypeScreenScheme) GetStartAt() int64`

GetStartAt returns the StartAt field if non-nil, zero value otherwise.

### GetStartAtOk

`func (o *PageBeanIssueTypeScreenScheme) GetStartAtOk() (*int64, bool)`

GetStartAtOk returns a tuple with the StartAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartAt

`func (o *PageBeanIssueTypeScreenScheme) SetStartAt(v int64)`

SetStartAt sets StartAt field to given value.

### HasStartAt

`func (o *PageBeanIssueTypeScreenScheme) HasStartAt() bool`

HasStartAt returns a boolean if a field has been set.

### GetTotal

`func (o *PageBeanIssueTypeScreenScheme) GetTotal() int64`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *PageBeanIssueTypeScreenScheme) GetTotalOk() (*int64, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *PageBeanIssueTypeScreenScheme) SetTotal(v int64)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *PageBeanIssueTypeScreenScheme) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetIsLast

`func (o *PageBeanIssueTypeScreenScheme) GetIsLast() bool`

GetIsLast returns the IsLast field if non-nil, zero value otherwise.

### GetIsLastOk

`func (o *PageBeanIssueTypeScreenScheme) GetIsLastOk() (*bool, bool)`

GetIsLastOk returns a tuple with the IsLast field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsLast

`func (o *PageBeanIssueTypeScreenScheme) SetIsLast(v bool)`

SetIsLast sets IsLast field to given value.

### HasIsLast

`func (o *PageBeanIssueTypeScreenScheme) HasIsLast() bool`

HasIsLast returns a boolean if a field has been set.

### GetValues

`func (o *PageBeanIssueTypeScreenScheme) GetValues() []IssueTypeScreenScheme`

GetValues returns the Values field if non-nil, zero value otherwise.

### GetValuesOk

`func (o *PageBeanIssueTypeScreenScheme) GetValuesOk() (*[]IssueTypeScreenScheme, bool)`

GetValuesOk returns a tuple with the Values field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValues

`func (o *PageBeanIssueTypeScreenScheme) SetValues(v []IssueTypeScreenScheme)`

SetValues sets Values field to given value.

### HasValues

`func (o *PageBeanIssueTypeScreenScheme) HasValues() bool`

HasValues returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



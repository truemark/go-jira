# PageOfComments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartAt** | Pointer to **int64** | The index of the first item returned. | [optional] [readonly] 
**MaxResults** | Pointer to **int32** | The maximum number of items that could be returned. | [optional] [readonly] 
**Total** | Pointer to **int64** | The number of items returned. | [optional] [readonly] 
**Comments** | Pointer to [**[]Comment**](Comment.md) | The list of comments. | [optional] [readonly] 

## Methods

### NewPageOfComments

`func NewPageOfComments() *PageOfComments`

NewPageOfComments instantiates a new PageOfComments object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPageOfCommentsWithDefaults

`func NewPageOfCommentsWithDefaults() *PageOfComments`

NewPageOfCommentsWithDefaults instantiates a new PageOfComments object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStartAt

`func (o *PageOfComments) GetStartAt() int64`

GetStartAt returns the StartAt field if non-nil, zero value otherwise.

### GetStartAtOk

`func (o *PageOfComments) GetStartAtOk() (*int64, bool)`

GetStartAtOk returns a tuple with the StartAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartAt

`func (o *PageOfComments) SetStartAt(v int64)`

SetStartAt sets StartAt field to given value.

### HasStartAt

`func (o *PageOfComments) HasStartAt() bool`

HasStartAt returns a boolean if a field has been set.

### GetMaxResults

`func (o *PageOfComments) GetMaxResults() int32`

GetMaxResults returns the MaxResults field if non-nil, zero value otherwise.

### GetMaxResultsOk

`func (o *PageOfComments) GetMaxResultsOk() (*int32, bool)`

GetMaxResultsOk returns a tuple with the MaxResults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxResults

`func (o *PageOfComments) SetMaxResults(v int32)`

SetMaxResults sets MaxResults field to given value.

### HasMaxResults

`func (o *PageOfComments) HasMaxResults() bool`

HasMaxResults returns a boolean if a field has been set.

### GetTotal

`func (o *PageOfComments) GetTotal() int64`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *PageOfComments) GetTotalOk() (*int64, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *PageOfComments) SetTotal(v int64)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *PageOfComments) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetComments

`func (o *PageOfComments) GetComments() []Comment`

GetComments returns the Comments field if non-nil, zero value otherwise.

### GetCommentsOk

`func (o *PageOfComments) GetCommentsOk() (*[]Comment, bool)`

GetCommentsOk returns a tuple with the Comments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComments

`func (o *PageOfComments) SetComments(v []Comment)`

SetComments sets Comments field to given value.

### HasComments

`func (o *PageOfComments) HasComments() bool`

HasComments returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ParsedJqlQueries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Queries** | [**[]ParsedJqlQuery**](ParsedJqlQuery.md) | A list of parsed JQL queries. | 

## Methods

### NewParsedJqlQueries

`func NewParsedJqlQueries(queries []ParsedJqlQuery, ) *ParsedJqlQueries`

NewParsedJqlQueries instantiates a new ParsedJqlQueries object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewParsedJqlQueriesWithDefaults

`func NewParsedJqlQueriesWithDefaults() *ParsedJqlQueries`

NewParsedJqlQueriesWithDefaults instantiates a new ParsedJqlQueries object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetQueries

`func (o *ParsedJqlQueries) GetQueries() []ParsedJqlQuery`

GetQueries returns the Queries field if non-nil, zero value otherwise.

### GetQueriesOk

`func (o *ParsedJqlQueries) GetQueriesOk() (*[]ParsedJqlQuery, bool)`

GetQueriesOk returns a tuple with the Queries field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQueries

`func (o *ParsedJqlQueries) SetQueries(v []ParsedJqlQuery)`

SetQueries sets Queries field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# PermissionHolder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | The type of permission holder. | 
**Parameter** | Pointer to **string** | The identifier of permission holder. | [optional] 
**Expand** | Pointer to **string** | Expand options that include additional permission holder details in the response. | [optional] [readonly] 

## Methods

### NewPermissionHolder

`func NewPermissionHolder(type_ string, ) *PermissionHolder`

NewPermissionHolder instantiates a new PermissionHolder object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPermissionHolderWithDefaults

`func NewPermissionHolderWithDefaults() *PermissionHolder`

NewPermissionHolderWithDefaults instantiates a new PermissionHolder object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *PermissionHolder) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *PermissionHolder) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *PermissionHolder) SetType(v string)`

SetType sets Type field to given value.


### GetParameter

`func (o *PermissionHolder) GetParameter() string`

GetParameter returns the Parameter field if non-nil, zero value otherwise.

### GetParameterOk

`func (o *PermissionHolder) GetParameterOk() (*string, bool)`

GetParameterOk returns a tuple with the Parameter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParameter

`func (o *PermissionHolder) SetParameter(v string)`

SetParameter sets Parameter field to given value.

### HasParameter

`func (o *PermissionHolder) HasParameter() bool`

HasParameter returns a boolean if a field has been set.

### GetExpand

`func (o *PermissionHolder) GetExpand() string`

GetExpand returns the Expand field if non-nil, zero value otherwise.

### GetExpandOk

`func (o *PermissionHolder) GetExpandOk() (*string, bool)`

GetExpandOk returns a tuple with the Expand field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpand

`func (o *PermissionHolder) SetExpand(v string)`

SetExpand sets Expand field to given value.

### HasExpand

`func (o *PermissionHolder) HasExpand() bool`

HasExpand returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



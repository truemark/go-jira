# PermissionSchemes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PermissionSchemes** | Pointer to [**[]PermissionScheme**](PermissionScheme.md) | Permission schemes list. | [optional] [readonly] 

## Methods

### NewPermissionSchemes

`func NewPermissionSchemes() *PermissionSchemes`

NewPermissionSchemes instantiates a new PermissionSchemes object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPermissionSchemesWithDefaults

`func NewPermissionSchemesWithDefaults() *PermissionSchemes`

NewPermissionSchemesWithDefaults instantiates a new PermissionSchemes object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPermissionSchemes

`func (o *PermissionSchemes) GetPermissionSchemes() []PermissionScheme`

GetPermissionSchemes returns the PermissionSchemes field if non-nil, zero value otherwise.

### GetPermissionSchemesOk

`func (o *PermissionSchemes) GetPermissionSchemesOk() (*[]PermissionScheme, bool)`

GetPermissionSchemesOk returns a tuple with the PermissionSchemes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermissionSchemes

`func (o *PermissionSchemes) SetPermissionSchemes(v []PermissionScheme)`

SetPermissionSchemes sets PermissionSchemes field to given value.

### HasPermissionSchemes

`func (o *PermissionSchemes) HasPermissionSchemes() bool`

HasPermissionSchemes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



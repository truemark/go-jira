# ProjectForScope

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | Pointer to **string** | The URL of the project details. | [optional] [readonly] 
**Id** | Pointer to **string** | The ID of the project. | [optional] 
**Key** | Pointer to **string** | The key of the project. | [optional] [readonly] 
**Name** | Pointer to **string** | The name of the project. | [optional] [readonly] 
**ProjectTypeKey** | Pointer to **string** | The [project type](https://confluence.atlassian.com/x/GwiiLQ#Jiraapplicationsoverview-Productfeaturesandprojecttypes) of the project. | [optional] [readonly] 
**Simplified** | Pointer to **bool** | Whether or not the project is simplified. | [optional] [readonly] 
**AvatarUrls** | Pointer to [**AvatarUrlsBean**](AvatarUrlsBean.md) | The URLs of the project&#39;s avatars. | [optional] [readonly] 
**ProjectCategory** | Pointer to [**UpdatedProjectCategory**](UpdatedProjectCategory.md) | The category the project belongs to. | [optional] [readonly] 

## Methods

### NewProjectForScope

`func NewProjectForScope() *ProjectForScope`

NewProjectForScope instantiates a new ProjectForScope object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProjectForScopeWithDefaults

`func NewProjectForScopeWithDefaults() *ProjectForScope`

NewProjectForScopeWithDefaults instantiates a new ProjectForScope object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *ProjectForScope) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *ProjectForScope) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *ProjectForScope) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *ProjectForScope) HasSelf() bool`

HasSelf returns a boolean if a field has been set.

### GetId

`func (o *ProjectForScope) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ProjectForScope) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ProjectForScope) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ProjectForScope) HasId() bool`

HasId returns a boolean if a field has been set.

### GetKey

`func (o *ProjectForScope) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *ProjectForScope) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *ProjectForScope) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *ProjectForScope) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetName

`func (o *ProjectForScope) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ProjectForScope) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ProjectForScope) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ProjectForScope) HasName() bool`

HasName returns a boolean if a field has been set.

### GetProjectTypeKey

`func (o *ProjectForScope) GetProjectTypeKey() string`

GetProjectTypeKey returns the ProjectTypeKey field if non-nil, zero value otherwise.

### GetProjectTypeKeyOk

`func (o *ProjectForScope) GetProjectTypeKeyOk() (*string, bool)`

GetProjectTypeKeyOk returns a tuple with the ProjectTypeKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectTypeKey

`func (o *ProjectForScope) SetProjectTypeKey(v string)`

SetProjectTypeKey sets ProjectTypeKey field to given value.

### HasProjectTypeKey

`func (o *ProjectForScope) HasProjectTypeKey() bool`

HasProjectTypeKey returns a boolean if a field has been set.

### GetSimplified

`func (o *ProjectForScope) GetSimplified() bool`

GetSimplified returns the Simplified field if non-nil, zero value otherwise.

### GetSimplifiedOk

`func (o *ProjectForScope) GetSimplifiedOk() (*bool, bool)`

GetSimplifiedOk returns a tuple with the Simplified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSimplified

`func (o *ProjectForScope) SetSimplified(v bool)`

SetSimplified sets Simplified field to given value.

### HasSimplified

`func (o *ProjectForScope) HasSimplified() bool`

HasSimplified returns a boolean if a field has been set.

### GetAvatarUrls

`func (o *ProjectForScope) GetAvatarUrls() AvatarUrlsBean`

GetAvatarUrls returns the AvatarUrls field if non-nil, zero value otherwise.

### GetAvatarUrlsOk

`func (o *ProjectForScope) GetAvatarUrlsOk() (*AvatarUrlsBean, bool)`

GetAvatarUrlsOk returns a tuple with the AvatarUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvatarUrls

`func (o *ProjectForScope) SetAvatarUrls(v AvatarUrlsBean)`

SetAvatarUrls sets AvatarUrls field to given value.

### HasAvatarUrls

`func (o *ProjectForScope) HasAvatarUrls() bool`

HasAvatarUrls returns a boolean if a field has been set.

### GetProjectCategory

`func (o *ProjectForScope) GetProjectCategory() UpdatedProjectCategory`

GetProjectCategory returns the ProjectCategory field if non-nil, zero value otherwise.

### GetProjectCategoryOk

`func (o *ProjectForScope) GetProjectCategoryOk() (*UpdatedProjectCategory, bool)`

GetProjectCategoryOk returns a tuple with the ProjectCategory field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectCategory

`func (o *ProjectForScope) SetProjectCategory(v UpdatedProjectCategory)`

SetProjectCategory sets ProjectCategory field to given value.

### HasProjectCategory

`func (o *ProjectForScope) HasProjectCategory() bool`

HasProjectCategory returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ProjectIssueSecurityLevels

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Levels** | [**[]SecurityLevel**](SecurityLevel.md) | Issue level security items list. | [readonly] 

## Methods

### NewProjectIssueSecurityLevels

`func NewProjectIssueSecurityLevels(levels []SecurityLevel, ) *ProjectIssueSecurityLevels`

NewProjectIssueSecurityLevels instantiates a new ProjectIssueSecurityLevels object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProjectIssueSecurityLevelsWithDefaults

`func NewProjectIssueSecurityLevelsWithDefaults() *ProjectIssueSecurityLevels`

NewProjectIssueSecurityLevelsWithDefaults instantiates a new ProjectIssueSecurityLevels object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLevels

`func (o *ProjectIssueSecurityLevels) GetLevels() []SecurityLevel`

GetLevels returns the Levels field if non-nil, zero value otherwise.

### GetLevelsOk

`func (o *ProjectIssueSecurityLevels) GetLevelsOk() (*[]SecurityLevel, bool)`

GetLevelsOk returns a tuple with the Levels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLevels

`func (o *ProjectIssueSecurityLevels) SetLevels(v []SecurityLevel)`

SetLevels sets Levels field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



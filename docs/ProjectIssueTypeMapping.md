# ProjectIssueTypeMapping

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProjectId** | **string** | The ID of the project. | 
**IssueTypeId** | **string** | The ID of the issue type. | 

## Methods

### NewProjectIssueTypeMapping

`func NewProjectIssueTypeMapping(projectId string, issueTypeId string, ) *ProjectIssueTypeMapping`

NewProjectIssueTypeMapping instantiates a new ProjectIssueTypeMapping object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProjectIssueTypeMappingWithDefaults

`func NewProjectIssueTypeMappingWithDefaults() *ProjectIssueTypeMapping`

NewProjectIssueTypeMappingWithDefaults instantiates a new ProjectIssueTypeMapping object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProjectId

`func (o *ProjectIssueTypeMapping) GetProjectId() string`

GetProjectId returns the ProjectId field if non-nil, zero value otherwise.

### GetProjectIdOk

`func (o *ProjectIssueTypeMapping) GetProjectIdOk() (*string, bool)`

GetProjectIdOk returns a tuple with the ProjectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectId

`func (o *ProjectIssueTypeMapping) SetProjectId(v string)`

SetProjectId sets ProjectId field to given value.


### GetIssueTypeId

`func (o *ProjectIssueTypeMapping) GetIssueTypeId() string`

GetIssueTypeId returns the IssueTypeId field if non-nil, zero value otherwise.

### GetIssueTypeIdOk

`func (o *ProjectIssueTypeMapping) GetIssueTypeIdOk() (*string, bool)`

GetIssueTypeIdOk returns a tuple with the IssueTypeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueTypeId

`func (o *ProjectIssueTypeMapping) SetIssueTypeId(v string)`

SetIssueTypeId sets IssueTypeId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



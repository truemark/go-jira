# ProjectIssueTypeMappings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Mappings** | [**[]ProjectIssueTypeMapping**](ProjectIssueTypeMapping.md) | The project and issue type mappings. | 

## Methods

### NewProjectIssueTypeMappings

`func NewProjectIssueTypeMappings(mappings []ProjectIssueTypeMapping, ) *ProjectIssueTypeMappings`

NewProjectIssueTypeMappings instantiates a new ProjectIssueTypeMappings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProjectIssueTypeMappingsWithDefaults

`func NewProjectIssueTypeMappingsWithDefaults() *ProjectIssueTypeMappings`

NewProjectIssueTypeMappingsWithDefaults instantiates a new ProjectIssueTypeMappings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMappings

`func (o *ProjectIssueTypeMappings) GetMappings() []ProjectIssueTypeMapping`

GetMappings returns the Mappings field if non-nil, zero value otherwise.

### GetMappingsOk

`func (o *ProjectIssueTypeMappings) GetMappingsOk() (*[]ProjectIssueTypeMapping, bool)`

GetMappingsOk returns a tuple with the Mappings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMappings

`func (o *ProjectIssueTypeMappings) SetMappings(v []ProjectIssueTypeMapping)`

SetMappings sets Mappings field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



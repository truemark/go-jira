# ProjectLandingPageInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | Pointer to **string** |  | [optional] 
**ProjectKey** | Pointer to **string** |  | [optional] 
**ProjectType** | Pointer to **string** |  | [optional] 
**BoardId** | Pointer to **int64** |  | [optional] 
**Simplified** | Pointer to **bool** |  | [optional] 

## Methods

### NewProjectLandingPageInfo

`func NewProjectLandingPageInfo() *ProjectLandingPageInfo`

NewProjectLandingPageInfo instantiates a new ProjectLandingPageInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProjectLandingPageInfoWithDefaults

`func NewProjectLandingPageInfoWithDefaults() *ProjectLandingPageInfo`

NewProjectLandingPageInfoWithDefaults instantiates a new ProjectLandingPageInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *ProjectLandingPageInfo) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *ProjectLandingPageInfo) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *ProjectLandingPageInfo) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *ProjectLandingPageInfo) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetProjectKey

`func (o *ProjectLandingPageInfo) GetProjectKey() string`

GetProjectKey returns the ProjectKey field if non-nil, zero value otherwise.

### GetProjectKeyOk

`func (o *ProjectLandingPageInfo) GetProjectKeyOk() (*string, bool)`

GetProjectKeyOk returns a tuple with the ProjectKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectKey

`func (o *ProjectLandingPageInfo) SetProjectKey(v string)`

SetProjectKey sets ProjectKey field to given value.

### HasProjectKey

`func (o *ProjectLandingPageInfo) HasProjectKey() bool`

HasProjectKey returns a boolean if a field has been set.

### GetProjectType

`func (o *ProjectLandingPageInfo) GetProjectType() string`

GetProjectType returns the ProjectType field if non-nil, zero value otherwise.

### GetProjectTypeOk

`func (o *ProjectLandingPageInfo) GetProjectTypeOk() (*string, bool)`

GetProjectTypeOk returns a tuple with the ProjectType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectType

`func (o *ProjectLandingPageInfo) SetProjectType(v string)`

SetProjectType sets ProjectType field to given value.

### HasProjectType

`func (o *ProjectLandingPageInfo) HasProjectType() bool`

HasProjectType returns a boolean if a field has been set.

### GetBoardId

`func (o *ProjectLandingPageInfo) GetBoardId() int64`

GetBoardId returns the BoardId field if non-nil, zero value otherwise.

### GetBoardIdOk

`func (o *ProjectLandingPageInfo) GetBoardIdOk() (*int64, bool)`

GetBoardIdOk returns a tuple with the BoardId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBoardId

`func (o *ProjectLandingPageInfo) SetBoardId(v int64)`

SetBoardId sets BoardId field to given value.

### HasBoardId

`func (o *ProjectLandingPageInfo) HasBoardId() bool`

HasBoardId returns a boolean if a field has been set.

### GetSimplified

`func (o *ProjectLandingPageInfo) GetSimplified() bool`

GetSimplified returns the Simplified field if non-nil, zero value otherwise.

### GetSimplifiedOk

`func (o *ProjectLandingPageInfo) GetSimplifiedOk() (*bool, bool)`

GetSimplifiedOk returns a tuple with the Simplified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSimplified

`func (o *ProjectLandingPageInfo) SetSimplified(v bool)`

SetSimplified sets Simplified field to given value.

### HasSimplified

`func (o *ProjectLandingPageInfo) HasSimplified() bool`

HasSimplified returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



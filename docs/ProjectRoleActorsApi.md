# \ProjectRoleActorsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddActorUsers**](ProjectRoleActorsApi.md#AddActorUsers) | **Post** /rest/api/3/project/{projectIdOrKey}/role/{id} | Add actors to project role
[**AddProjectRoleActorsToRole**](ProjectRoleActorsApi.md#AddProjectRoleActorsToRole) | **Post** /rest/api/3/role/{id}/actors | Add default actors to project role
[**DeleteActor**](ProjectRoleActorsApi.md#DeleteActor) | **Delete** /rest/api/3/project/{projectIdOrKey}/role/{id} | Delete actors from project role
[**DeleteProjectRoleActorsFromRole**](ProjectRoleActorsApi.md#DeleteProjectRoleActorsFromRole) | **Delete** /rest/api/3/role/{id}/actors | Delete default actors from project role
[**GetProjectRoleActorsForRole**](ProjectRoleActorsApi.md#GetProjectRoleActorsForRole) | **Get** /rest/api/3/role/{id}/actors | Get default actors for project role
[**SetActors**](ProjectRoleActorsApi.md#SetActors) | **Put** /rest/api/3/project/{projectIdOrKey}/role/{id} | Set actors for project role



## AddActorUsers

> ProjectRole AddActorUsers(ctx, projectIdOrKey, id).ActorsMap(actorsMap).Execute()

Add actors to project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    actorsMap := *openapiclient.NewActorsMap() // ActorsMap | The groups or users to associate with the project role for this project. Provide the user account ID or group name.
    projectIdOrKey := "projectIdOrKey_example" // string | The project ID or project key (case sensitive).
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRoleActorsApi.AddActorUsers(context.Background(), projectIdOrKey, id).ActorsMap(actorsMap).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRoleActorsApi.AddActorUsers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddActorUsers`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRoleActorsApi.AddActorUsers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**projectIdOrKey** | **string** | The project ID or project key (case sensitive). | 
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddActorUsersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **actorsMap** | [**ActorsMap**](ActorsMap.md) | The groups or users to associate with the project role for this project. Provide the user account ID or group name. | 



### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddProjectRoleActorsToRole

> ProjectRole AddProjectRoleActorsToRole(ctx, id).ActorInputBean(actorInputBean).Execute()

Add default actors to project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    actorInputBean := *openapiclient.NewActorInputBean() // ActorInputBean | 
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRoleActorsApi.AddProjectRoleActorsToRole(context.Background(), id).ActorInputBean(actorInputBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRoleActorsApi.AddProjectRoleActorsToRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddProjectRoleActorsToRole`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRoleActorsApi.AddProjectRoleActorsToRole`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddProjectRoleActorsToRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **actorInputBean** | [**ActorInputBean**](ActorInputBean.md) |  | 


### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteActor

> DeleteActor(ctx, projectIdOrKey, id).User(user).Group(group).Execute()

Delete actors from project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIdOrKey := "projectIdOrKey_example" // string | The project ID or project key (case sensitive).
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.
    user := "5b10ac8d82e05b22cc7d4ef5" // string | The user account ID of the user to remove from the project role. (optional)
    group := "group_example" // string | The name of the group to remove from the project role. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRoleActorsApi.DeleteActor(context.Background(), projectIdOrKey, id).User(user).Group(group).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRoleActorsApi.DeleteActor``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**projectIdOrKey** | **string** | The project ID or project key (case sensitive). | 
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteActorRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **user** | **string** | The user account ID of the user to remove from the project role. | 
 **group** | **string** | The name of the group to remove from the project role. | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteProjectRoleActorsFromRole

> ProjectRole DeleteProjectRoleActorsFromRole(ctx, id).User(user).Group(group).Execute()

Delete default actors from project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.
    user := "5b10ac8d82e05b22cc7d4ef5" // string | The user account ID of the user to remove as a default actor. (optional)
    group := "group_example" // string | The group name of the group to be removed as a default actor. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRoleActorsApi.DeleteProjectRoleActorsFromRole(context.Background(), id).User(user).Group(group).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRoleActorsApi.DeleteProjectRoleActorsFromRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteProjectRoleActorsFromRole`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRoleActorsApi.DeleteProjectRoleActorsFromRole`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteProjectRoleActorsFromRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **user** | **string** | The user account ID of the user to remove as a default actor. | 
 **group** | **string** | The group name of the group to be removed as a default actor. | 

### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProjectRoleActorsForRole

> ProjectRole GetProjectRoleActorsForRole(ctx, id).Execute()

Get default actors for project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRoleActorsApi.GetProjectRoleActorsForRole(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRoleActorsApi.GetProjectRoleActorsForRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProjectRoleActorsForRole`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRoleActorsApi.GetProjectRoleActorsForRole`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProjectRoleActorsForRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetActors

> ProjectRole SetActors(ctx, projectIdOrKey, id).ProjectRoleActorsUpdateBean(projectRoleActorsUpdateBean).Execute()

Set actors for project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectRoleActorsUpdateBean := *openapiclient.NewProjectRoleActorsUpdateBean() // ProjectRoleActorsUpdateBean | The groups or users to associate with the project role for this project. Provide the user account ID or group name.
    projectIdOrKey := "projectIdOrKey_example" // string | The project ID or project key (case sensitive).
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRoleActorsApi.SetActors(context.Background(), projectIdOrKey, id).ProjectRoleActorsUpdateBean(projectRoleActorsUpdateBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRoleActorsApi.SetActors``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetActors`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRoleActorsApi.SetActors`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**projectIdOrKey** | **string** | The project ID or project key (case sensitive). | 
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetActorsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectRoleActorsUpdateBean** | [**ProjectRoleActorsUpdateBean**](ProjectRoleActorsUpdateBean.md) | The groups or users to associate with the project role for this project. Provide the user account ID or group name. | 



### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


# \ProjectRolesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateProjectRole**](ProjectRolesApi.md#CreateProjectRole) | **Post** /rest/api/3/role | Create project role
[**DeleteProjectRole**](ProjectRolesApi.md#DeleteProjectRole) | **Delete** /rest/api/3/role/{id} | Delete project role
[**FullyUpdateProjectRole**](ProjectRolesApi.md#FullyUpdateProjectRole) | **Put** /rest/api/3/role/{id} | Fully update project role
[**GetAllProjectRoles**](ProjectRolesApi.md#GetAllProjectRoles) | **Get** /rest/api/3/role | Get all project roles
[**GetProjectRole**](ProjectRolesApi.md#GetProjectRole) | **Get** /rest/api/3/project/{projectIdOrKey}/role/{id} | Get project role for project
[**GetProjectRoleById**](ProjectRolesApi.md#GetProjectRoleById) | **Get** /rest/api/3/role/{id} | Get project role by ID
[**GetProjectRoleDetails**](ProjectRolesApi.md#GetProjectRoleDetails) | **Get** /rest/api/3/project/{projectIdOrKey}/roledetails | Get project role details
[**GetProjectRoles**](ProjectRolesApi.md#GetProjectRoles) | **Get** /rest/api/3/project/{projectIdOrKey}/role | Get project roles for project
[**PartialUpdateProjectRole**](ProjectRolesApi.md#PartialUpdateProjectRole) | **Post** /rest/api/3/role/{id} | Partial update project role



## CreateProjectRole

> ProjectRole CreateProjectRole(ctx).CreateUpdateRoleRequestBean(createUpdateRoleRequestBean).Execute()

Create project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    createUpdateRoleRequestBean := *openapiclient.NewCreateUpdateRoleRequestBean() // CreateUpdateRoleRequestBean | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.CreateProjectRole(context.Background()).CreateUpdateRoleRequestBean(createUpdateRoleRequestBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.CreateProjectRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateProjectRole`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.CreateProjectRole`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateProjectRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUpdateRoleRequestBean** | [**CreateUpdateRoleRequestBean**](CreateUpdateRoleRequestBean.md) |  | 

### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteProjectRole

> DeleteProjectRole(ctx, id).Swap(swap).Execute()

Delete project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the project role to delete. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.
    swap := int64(789) // int64 | The ID of the project role that will replace the one being deleted. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.DeleteProjectRole(context.Background(), id).Swap(swap).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.DeleteProjectRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the project role to delete. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteProjectRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **swap** | **int64** | The ID of the project role that will replace the one being deleted. | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FullyUpdateProjectRole

> ProjectRole FullyUpdateProjectRole(ctx, id).CreateUpdateRoleRequestBean(createUpdateRoleRequestBean).Execute()

Fully update project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    createUpdateRoleRequestBean := *openapiclient.NewCreateUpdateRoleRequestBean() // CreateUpdateRoleRequestBean | 
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.FullyUpdateProjectRole(context.Background(), id).CreateUpdateRoleRequestBean(createUpdateRoleRequestBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.FullyUpdateProjectRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `FullyUpdateProjectRole`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.FullyUpdateProjectRole`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiFullyUpdateProjectRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUpdateRoleRequestBean** | [**CreateUpdateRoleRequestBean**](CreateUpdateRoleRequestBean.md) |  | 


### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllProjectRoles

> []ProjectRole GetAllProjectRoles(ctx).Execute()

Get all project roles



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.GetAllProjectRoles(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.GetAllProjectRoles``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAllProjectRoles`: []ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.GetAllProjectRoles`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetAllProjectRolesRequest struct via the builder pattern


### Return type

[**[]ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProjectRole

> ProjectRole GetProjectRole(ctx, projectIdOrKey, id).Execute()

Get project role for project



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIdOrKey := "projectIdOrKey_example" // string | The project ID or project key (case sensitive).
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.GetProjectRole(context.Background(), projectIdOrKey, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.GetProjectRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProjectRole`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.GetProjectRole`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**projectIdOrKey** | **string** | The project ID or project key (case sensitive). | 
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProjectRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProjectRoleById

> ProjectRole GetProjectRoleById(ctx, id).Execute()

Get project role by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.GetProjectRoleById(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.GetProjectRoleById``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProjectRoleById`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.GetProjectRoleById`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProjectRoleByIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProjectRoleDetails

> []ProjectRoleDetails GetProjectRoleDetails(ctx, projectIdOrKey).CurrentMember(currentMember).ExcludeConnectAddons(excludeConnectAddons).Execute()

Get project role details



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIdOrKey := "projectIdOrKey_example" // string | The project ID or project key (case sensitive).
    currentMember := true // bool | Whether the roles should be filtered to include only those the user is assigned to. (optional) (default to false)
    excludeConnectAddons := true // bool |  (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.GetProjectRoleDetails(context.Background(), projectIdOrKey).CurrentMember(currentMember).ExcludeConnectAddons(excludeConnectAddons).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.GetProjectRoleDetails``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProjectRoleDetails`: []ProjectRoleDetails
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.GetProjectRoleDetails`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**projectIdOrKey** | **string** | The project ID or project key (case sensitive). | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProjectRoleDetailsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **currentMember** | **bool** | Whether the roles should be filtered to include only those the user is assigned to. | [default to false]
 **excludeConnectAddons** | **bool** |  | [default to false]

### Return type

[**[]ProjectRoleDetails**](ProjectRoleDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProjectRoles

> map[string]string GetProjectRoles(ctx, projectIdOrKey).Execute()

Get project roles for project



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    projectIdOrKey := "projectIdOrKey_example" // string | The project ID or project key (case sensitive).

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.GetProjectRoles(context.Background(), projectIdOrKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.GetProjectRoles``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProjectRoles`: map[string]string
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.GetProjectRoles`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**projectIdOrKey** | **string** | The project ID or project key (case sensitive). | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProjectRolesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

**map[string]string**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PartialUpdateProjectRole

> ProjectRole PartialUpdateProjectRole(ctx, id).CreateUpdateRoleRequestBean(createUpdateRoleRequestBean).Execute()

Partial update project role



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    createUpdateRoleRequestBean := *openapiclient.NewCreateUpdateRoleRequestBean() // CreateUpdateRoleRequestBean | 
    id := int64(789) // int64 | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProjectRolesApi.PartialUpdateProjectRole(context.Background(), id).CreateUpdateRoleRequestBean(createUpdateRoleRequestBean).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProjectRolesApi.PartialUpdateProjectRole``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PartialUpdateProjectRole`: ProjectRole
    fmt.Fprintf(os.Stdout, "Response from `ProjectRolesApi.PartialUpdateProjectRole`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs. | 

### Other Parameters

Other parameters are passed through a pointer to a apiPartialUpdateProjectRoleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUpdateRoleRequestBean** | [**CreateUpdateRoleRequestBean**](CreateUpdateRoleRequestBean.md) |  | 


### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


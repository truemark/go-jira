# ProjectType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | Pointer to **string** | The key of the project type. | [optional] [readonly] 
**FormattedKey** | Pointer to **string** | The formatted key of the project type. | [optional] [readonly] 
**DescriptionI18nKey** | Pointer to **string** | The key of the project type&#39;s description. | [optional] [readonly] 
**Icon** | Pointer to **string** | The icon of the project type. | [optional] [readonly] 
**Color** | Pointer to **string** | The color of the project type. | [optional] [readonly] 

## Methods

### NewProjectType

`func NewProjectType() *ProjectType`

NewProjectType instantiates a new ProjectType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProjectTypeWithDefaults

`func NewProjectTypeWithDefaults() *ProjectType`

NewProjectTypeWithDefaults instantiates a new ProjectType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKey

`func (o *ProjectType) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *ProjectType) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *ProjectType) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *ProjectType) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetFormattedKey

`func (o *ProjectType) GetFormattedKey() string`

GetFormattedKey returns the FormattedKey field if non-nil, zero value otherwise.

### GetFormattedKeyOk

`func (o *ProjectType) GetFormattedKeyOk() (*string, bool)`

GetFormattedKeyOk returns a tuple with the FormattedKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormattedKey

`func (o *ProjectType) SetFormattedKey(v string)`

SetFormattedKey sets FormattedKey field to given value.

### HasFormattedKey

`func (o *ProjectType) HasFormattedKey() bool`

HasFormattedKey returns a boolean if a field has been set.

### GetDescriptionI18nKey

`func (o *ProjectType) GetDescriptionI18nKey() string`

GetDescriptionI18nKey returns the DescriptionI18nKey field if non-nil, zero value otherwise.

### GetDescriptionI18nKeyOk

`func (o *ProjectType) GetDescriptionI18nKeyOk() (*string, bool)`

GetDescriptionI18nKeyOk returns a tuple with the DescriptionI18nKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptionI18nKey

`func (o *ProjectType) SetDescriptionI18nKey(v string)`

SetDescriptionI18nKey sets DescriptionI18nKey field to given value.

### HasDescriptionI18nKey

`func (o *ProjectType) HasDescriptionI18nKey() bool`

HasDescriptionI18nKey returns a boolean if a field has been set.

### GetIcon

`func (o *ProjectType) GetIcon() string`

GetIcon returns the Icon field if non-nil, zero value otherwise.

### GetIconOk

`func (o *ProjectType) GetIconOk() (*string, bool)`

GetIconOk returns a tuple with the Icon field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIcon

`func (o *ProjectType) SetIcon(v string)`

SetIcon sets Icon field to given value.

### HasIcon

`func (o *ProjectType) HasIcon() bool`

HasIcon returns a boolean if a field has been set.

### GetColor

`func (o *ProjectType) GetColor() string`

GetColor returns the Color field if non-nil, zero value otherwise.

### GetColorOk

`func (o *ProjectType) GetColorOk() (*string, bool)`

GetColorOk returns a tuple with the Color field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetColor

`func (o *ProjectType) SetColor(v string)`

SetColor sets Color field to given value.

### HasColor

`func (o *ProjectType) HasColor() bool`

HasColor returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# PublishDraftWorkflowScheme

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StatusMappings** | Pointer to [**[]StatusMapping**](StatusMapping.md) | Mappings of statuses to new statuses for issue types. | [optional] 

## Methods

### NewPublishDraftWorkflowScheme

`func NewPublishDraftWorkflowScheme() *PublishDraftWorkflowScheme`

NewPublishDraftWorkflowScheme instantiates a new PublishDraftWorkflowScheme object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPublishDraftWorkflowSchemeWithDefaults

`func NewPublishDraftWorkflowSchemeWithDefaults() *PublishDraftWorkflowScheme`

NewPublishDraftWorkflowSchemeWithDefaults instantiates a new PublishDraftWorkflowScheme object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatusMappings

`func (o *PublishDraftWorkflowScheme) GetStatusMappings() []StatusMapping`

GetStatusMappings returns the StatusMappings field if non-nil, zero value otherwise.

### GetStatusMappingsOk

`func (o *PublishDraftWorkflowScheme) GetStatusMappingsOk() (*[]StatusMapping, bool)`

GetStatusMappingsOk returns a tuple with the StatusMappings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatusMappings

`func (o *PublishDraftWorkflowScheme) SetStatusMappings(v []StatusMapping)`

SetStatusMappings sets StatusMappings field to given value.

### HasStatusMappings

`func (o *PublishDraftWorkflowScheme) HasStatusMappings() bool`

HasStatusMappings returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



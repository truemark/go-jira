# RichText

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Finalised** | Pointer to **bool** |  | [optional] 
**EmptyAdf** | Pointer to **bool** |  | [optional] 
**ValueSet** | Pointer to **bool** |  | [optional] 

## Methods

### NewRichText

`func NewRichText() *RichText`

NewRichText instantiates a new RichText object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRichTextWithDefaults

`func NewRichTextWithDefaults() *RichText`

NewRichTextWithDefaults instantiates a new RichText object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFinalised

`func (o *RichText) GetFinalised() bool`

GetFinalised returns the Finalised field if non-nil, zero value otherwise.

### GetFinalisedOk

`func (o *RichText) GetFinalisedOk() (*bool, bool)`

GetFinalisedOk returns a tuple with the Finalised field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFinalised

`func (o *RichText) SetFinalised(v bool)`

SetFinalised sets Finalised field to given value.

### HasFinalised

`func (o *RichText) HasFinalised() bool`

HasFinalised returns a boolean if a field has been set.

### GetEmptyAdf

`func (o *RichText) GetEmptyAdf() bool`

GetEmptyAdf returns the EmptyAdf field if non-nil, zero value otherwise.

### GetEmptyAdfOk

`func (o *RichText) GetEmptyAdfOk() (*bool, bool)`

GetEmptyAdfOk returns a tuple with the EmptyAdf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmptyAdf

`func (o *RichText) SetEmptyAdf(v bool)`

SetEmptyAdf sets EmptyAdf field to given value.

### HasEmptyAdf

`func (o *RichText) HasEmptyAdf() bool`

HasEmptyAdf returns a boolean if a field has been set.

### GetValueSet

`func (o *RichText) GetValueSet() bool`

GetValueSet returns the ValueSet field if non-nil, zero value otherwise.

### GetValueSetOk

`func (o *RichText) GetValueSetOk() (*bool, bool)`

GetValueSetOk returns a tuple with the ValueSet field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValueSet

`func (o *RichText) SetValueSet(v bool)`

SetValueSet sets ValueSet field to given value.

### HasValueSet

`func (o *RichText) HasValueSet() bool`

HasValueSet returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



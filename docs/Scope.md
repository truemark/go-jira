# Scope

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** | The type of scope. | [optional] [readonly] 
**Project** | Pointer to [**ProjectForScope**](ProjectForScope.md) | The project the item has scope in. | [optional] [readonly] 

## Methods

### NewScope

`func NewScope() *Scope`

NewScope instantiates a new Scope object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScopeWithDefaults

`func NewScopeWithDefaults() *Scope`

NewScopeWithDefaults instantiates a new Scope object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *Scope) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Scope) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Scope) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *Scope) HasType() bool`

HasType returns a boolean if a field has been set.

### GetProject

`func (o *Scope) GetProject() ProjectForScope`

GetProject returns the Project field if non-nil, zero value otherwise.

### GetProjectOk

`func (o *Scope) GetProjectOk() (*ProjectForScope, bool)`

GetProjectOk returns a tuple with the Project field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProject

`func (o *Scope) SetProject(v ProjectForScope)`

SetProject sets Project field to given value.

### HasProject

`func (o *Scope) HasProject() bool`

HasProject returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



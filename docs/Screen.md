# Screen

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int64** | The ID of the screen. | [optional] [readonly] 
**Name** | Pointer to **string** | The name of the screen. | [optional] [readonly] 
**Description** | Pointer to **string** | The description of the screen. | [optional] [readonly] 
**Scope** | Pointer to [**Scope**](Scope.md) | The scope of the screen. | [optional] 

## Methods

### NewScreen

`func NewScreen() *Screen`

NewScreen instantiates a new Screen object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScreenWithDefaults

`func NewScreenWithDefaults() *Screen`

NewScreenWithDefaults instantiates a new Screen object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Screen) GetId() int64`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Screen) GetIdOk() (*int64, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Screen) SetId(v int64)`

SetId sets Id field to given value.

### HasId

`func (o *Screen) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *Screen) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Screen) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Screen) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *Screen) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDescription

`func (o *Screen) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *Screen) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *Screen) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *Screen) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetScope

`func (o *Screen) GetScope() Scope`

GetScope returns the Scope field if non-nil, zero value otherwise.

### GetScopeOk

`func (o *Screen) GetScopeOk() (*Scope, bool)`

GetScopeOk returns a tuple with the Scope field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScope

`func (o *Screen) SetScope(v Scope)`

SetScope sets Scope field to given value.

### HasScope

`func (o *Screen) HasScope() bool`

HasScope returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



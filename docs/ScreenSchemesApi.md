# \ScreenSchemesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateScreenScheme**](ScreenSchemesApi.md#CreateScreenScheme) | **Post** /rest/api/3/screenscheme | Create screen scheme
[**DeleteScreenScheme**](ScreenSchemesApi.md#DeleteScreenScheme) | **Delete** /rest/api/3/screenscheme/{screenSchemeId} | Delete screen scheme
[**GetScreenSchemes**](ScreenSchemesApi.md#GetScreenSchemes) | **Get** /rest/api/3/screenscheme | Get screen schemes
[**UpdateScreenScheme**](ScreenSchemesApi.md#UpdateScreenScheme) | **Put** /rest/api/3/screenscheme/{screenSchemeId} | Update screen scheme



## CreateScreenScheme

> ScreenSchemeId CreateScreenScheme(ctx).ScreenSchemeDetails(screenSchemeDetails).Execute()

Create screen scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    screenSchemeDetails := *openapiclient.NewScreenSchemeDetails("Name_example", *openapiclient.NewScreenTypes()) // ScreenSchemeDetails | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenSchemesApi.CreateScreenScheme(context.Background()).ScreenSchemeDetails(screenSchemeDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenSchemesApi.CreateScreenScheme``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateScreenScheme`: ScreenSchemeId
    fmt.Fprintf(os.Stdout, "Response from `ScreenSchemesApi.CreateScreenScheme`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateScreenSchemeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **screenSchemeDetails** | [**ScreenSchemeDetails**](ScreenSchemeDetails.md) |  | 

### Return type

[**ScreenSchemeId**](ScreenSchemeId.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteScreenScheme

> DeleteScreenScheme(ctx, screenSchemeId).Execute()

Delete screen scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    screenSchemeId := "screenSchemeId_example" // string | The ID of the screen scheme.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenSchemesApi.DeleteScreenScheme(context.Background(), screenSchemeId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenSchemesApi.DeleteScreenScheme``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**screenSchemeId** | **string** | The ID of the screen scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteScreenSchemeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetScreenSchemes

> PageBeanScreenScheme GetScreenSchemes(ctx).StartAt(startAt).MaxResults(maxResults).Id(id).Execute()

Get screen schemes



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 25)
    id := []int64{int64(123)} // []int64 | The list of screen scheme IDs. To include multiple IDs, provide an ampersand-separated list. For example, `id=10000&id=10001`. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenSchemesApi.GetScreenSchemes(context.Background()).StartAt(startAt).MaxResults(maxResults).Id(id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenSchemesApi.GetScreenSchemes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetScreenSchemes`: PageBeanScreenScheme
    fmt.Fprintf(os.Stdout, "Response from `ScreenSchemesApi.GetScreenSchemes`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetScreenSchemesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 25]
 **id** | **[]int64** | The list of screen scheme IDs. To include multiple IDs, provide an ampersand-separated list. For example, &#x60;id&#x3D;10000&amp;id&#x3D;10001&#x60;. | 

### Return type

[**PageBeanScreenScheme**](PageBeanScreenScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateScreenScheme

> interface{} UpdateScreenScheme(ctx, screenSchemeId).UpdateScreenSchemeDetails(updateScreenSchemeDetails).Execute()

Update screen scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    updateScreenSchemeDetails := *openapiclient.NewUpdateScreenSchemeDetails() // UpdateScreenSchemeDetails | The screen scheme update details.
    screenSchemeId := "screenSchemeId_example" // string | The ID of the screen scheme.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenSchemesApi.UpdateScreenScheme(context.Background(), screenSchemeId).UpdateScreenSchemeDetails(updateScreenSchemeDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenSchemesApi.UpdateScreenScheme``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateScreenScheme`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `ScreenSchemesApi.UpdateScreenScheme`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**screenSchemeId** | **string** | The ID of the screen scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateScreenSchemeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateScreenSchemeDetails** | [**UpdateScreenSchemeDetails**](UpdateScreenSchemeDetails.md) | The screen scheme update details. | 


### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


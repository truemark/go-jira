# \ScreenTabsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddScreenTab**](ScreenTabsApi.md#AddScreenTab) | **Post** /rest/api/3/screens/{screenId}/tabs | Create screen tab
[**DeleteScreenTab**](ScreenTabsApi.md#DeleteScreenTab) | **Delete** /rest/api/3/screens/{screenId}/tabs/{tabId} | Delete screen tab
[**GetAllScreenTabs**](ScreenTabsApi.md#GetAllScreenTabs) | **Get** /rest/api/3/screens/{screenId}/tabs | Get all screen tabs
[**MoveScreenTab**](ScreenTabsApi.md#MoveScreenTab) | **Post** /rest/api/3/screens/{screenId}/tabs/{tabId}/move/{pos} | Move screen tab
[**RenameScreenTab**](ScreenTabsApi.md#RenameScreenTab) | **Put** /rest/api/3/screens/{screenId}/tabs/{tabId} | Update screen tab



## AddScreenTab

> ScreenableTab AddScreenTab(ctx, screenId).ScreenableTab(screenableTab).Execute()

Create screen tab



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    screenableTab := *openapiclient.NewScreenableTab("Name_example") // ScreenableTab | 
    screenId := int64(789) // int64 | The ID of the screen.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenTabsApi.AddScreenTab(context.Background(), screenId).ScreenableTab(screenableTab).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenTabsApi.AddScreenTab``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddScreenTab`: ScreenableTab
    fmt.Fprintf(os.Stdout, "Response from `ScreenTabsApi.AddScreenTab`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**screenId** | **int64** | The ID of the screen. | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddScreenTabRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **screenableTab** | [**ScreenableTab**](ScreenableTab.md) |  | 


### Return type

[**ScreenableTab**](ScreenableTab.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteScreenTab

> DeleteScreenTab(ctx, screenId, tabId).Execute()

Delete screen tab



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    screenId := int64(789) // int64 | The ID of the screen.
    tabId := int64(789) // int64 | The ID of the screen tab.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenTabsApi.DeleteScreenTab(context.Background(), screenId, tabId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenTabsApi.DeleteScreenTab``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**screenId** | **int64** | The ID of the screen. | 
**tabId** | **int64** | The ID of the screen tab. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteScreenTabRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllScreenTabs

> []ScreenableTab GetAllScreenTabs(ctx, screenId).ProjectKey(projectKey).Execute()

Get all screen tabs



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    screenId := int64(789) // int64 | The ID of the screen.
    projectKey := "projectKey_example" // string | The key of the project. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenTabsApi.GetAllScreenTabs(context.Background(), screenId).ProjectKey(projectKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenTabsApi.GetAllScreenTabs``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAllScreenTabs`: []ScreenableTab
    fmt.Fprintf(os.Stdout, "Response from `ScreenTabsApi.GetAllScreenTabs`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**screenId** | **int64** | The ID of the screen. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAllScreenTabsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **projectKey** | **string** | The key of the project. | 

### Return type

[**[]ScreenableTab**](ScreenableTab.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MoveScreenTab

> interface{} MoveScreenTab(ctx, screenId, tabId, pos).Execute()

Move screen tab



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    screenId := int64(789) // int64 | The ID of the screen.
    tabId := int64(789) // int64 | The ID of the screen tab.
    pos := int32(56) // int32 | The position of tab. The base index is 0.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenTabsApi.MoveScreenTab(context.Background(), screenId, tabId, pos).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenTabsApi.MoveScreenTab``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `MoveScreenTab`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `ScreenTabsApi.MoveScreenTab`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**screenId** | **int64** | The ID of the screen. | 
**tabId** | **int64** | The ID of the screen tab. | 
**pos** | **int32** | The position of tab. The base index is 0. | 

### Other Parameters

Other parameters are passed through a pointer to a apiMoveScreenTabRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RenameScreenTab

> ScreenableTab RenameScreenTab(ctx, screenId, tabId).ScreenableTab(screenableTab).Execute()

Update screen tab



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    screenableTab := *openapiclient.NewScreenableTab("Name_example") // ScreenableTab | 
    screenId := int64(789) // int64 | The ID of the screen.
    tabId := int64(789) // int64 | The ID of the screen tab.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ScreenTabsApi.RenameScreenTab(context.Background(), screenId, tabId).ScreenableTab(screenableTab).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScreenTabsApi.RenameScreenTab``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RenameScreenTab`: ScreenableTab
    fmt.Fprintf(os.Stdout, "Response from `ScreenTabsApi.RenameScreenTab`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**screenId** | **int64** | The ID of the screen. | 
**tabId** | **int64** | The ID of the screen tab. | 

### Other Parameters

Other parameters are passed through a pointer to a apiRenameScreenTabRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **screenableTab** | [**ScreenableTab**](ScreenableTab.md) |  | 



### Return type

[**ScreenableTab**](ScreenableTab.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


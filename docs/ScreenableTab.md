# ScreenableTab

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **int64** | The ID of the screen tab. | [optional] [readonly] 
**Name** | **string** | The name of the screen tab. The maximum length is 255 characters. | 

## Methods

### NewScreenableTab

`func NewScreenableTab(name string, ) *ScreenableTab`

NewScreenableTab instantiates a new ScreenableTab object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScreenableTabWithDefaults

`func NewScreenableTabWithDefaults() *ScreenableTab`

NewScreenableTabWithDefaults instantiates a new ScreenableTab object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ScreenableTab) GetId() int64`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ScreenableTab) GetIdOk() (*int64, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ScreenableTab) SetId(v int64)`

SetId sets Id field to given value.

### HasId

`func (o *ScreenableTab) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *ScreenableTab) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ScreenableTab) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ScreenableTab) SetName(v string)`

SetName sets Name field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



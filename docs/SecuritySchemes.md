# SecuritySchemes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueSecuritySchemes** | Pointer to [**[]SecurityScheme**](SecurityScheme.md) | List of security schemes. | [optional] [readonly] 

## Methods

### NewSecuritySchemes

`func NewSecuritySchemes() *SecuritySchemes`

NewSecuritySchemes instantiates a new SecuritySchemes object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSecuritySchemesWithDefaults

`func NewSecuritySchemesWithDefaults() *SecuritySchemes`

NewSecuritySchemesWithDefaults instantiates a new SecuritySchemes object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueSecuritySchemes

`func (o *SecuritySchemes) GetIssueSecuritySchemes() []SecurityScheme`

GetIssueSecuritySchemes returns the IssueSecuritySchemes field if non-nil, zero value otherwise.

### GetIssueSecuritySchemesOk

`func (o *SecuritySchemes) GetIssueSecuritySchemesOk() (*[]SecurityScheme, bool)`

GetIssueSecuritySchemesOk returns a tuple with the IssueSecuritySchemes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueSecuritySchemes

`func (o *SecuritySchemes) SetIssueSecuritySchemes(v []SecurityScheme)`

SetIssueSecuritySchemes sets IssueSecuritySchemes field to given value.

### HasIssueSecuritySchemes

`func (o *SecuritySchemes) HasIssueSecuritySchemes() bool`

HasIssueSecuritySchemes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



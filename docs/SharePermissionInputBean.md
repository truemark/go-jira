# SharePermissionInputBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | The type of the share permission.Specify the type as follows:   *  &#x60;group&#x60; Share with a group. Specify &#x60;groupname&#x60; as well.  *  &#x60;project&#x60; Share with a project. Specify &#x60;projectId&#x60; as well.  *  &#x60;projectRole&#x60; Share with a project role in a project. Specify &#x60;projectId&#x60; and &#x60;projectRoleId&#x60; as well.  *  &#x60;global&#x60; Share globally, including anonymous users. If set, this type overrides all existing share permissions and must be deleted before any non-global share permissions is set.  *  &#x60;authenticated&#x60; Share with all logged-in users. This shows as &#x60;loggedin&#x60; in the response. If set, this type overrides all existing share permissions and must be deleted before any non-global share permissions is set. | 
**ProjectId** | Pointer to **string** | The ID of the project to share the filter with. Set &#x60;type&#x60; to &#x60;project&#x60;. | [optional] 
**Groupname** | Pointer to **string** | The name of the group to share the filter with. Set &#x60;type&#x60; to &#x60;group&#x60;. | [optional] 
**ProjectRoleId** | Pointer to **string** | The ID of the project role to share the filter with. Set &#x60;type&#x60; to &#x60;projectRole&#x60; and the &#x60;projectId&#x60; for the project that the role is in. | [optional] 

## Methods

### NewSharePermissionInputBean

`func NewSharePermissionInputBean(type_ string, ) *SharePermissionInputBean`

NewSharePermissionInputBean instantiates a new SharePermissionInputBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSharePermissionInputBeanWithDefaults

`func NewSharePermissionInputBeanWithDefaults() *SharePermissionInputBean`

NewSharePermissionInputBeanWithDefaults instantiates a new SharePermissionInputBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *SharePermissionInputBean) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *SharePermissionInputBean) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *SharePermissionInputBean) SetType(v string)`

SetType sets Type field to given value.


### GetProjectId

`func (o *SharePermissionInputBean) GetProjectId() string`

GetProjectId returns the ProjectId field if non-nil, zero value otherwise.

### GetProjectIdOk

`func (o *SharePermissionInputBean) GetProjectIdOk() (*string, bool)`

GetProjectIdOk returns a tuple with the ProjectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectId

`func (o *SharePermissionInputBean) SetProjectId(v string)`

SetProjectId sets ProjectId field to given value.

### HasProjectId

`func (o *SharePermissionInputBean) HasProjectId() bool`

HasProjectId returns a boolean if a field has been set.

### GetGroupname

`func (o *SharePermissionInputBean) GetGroupname() string`

GetGroupname returns the Groupname field if non-nil, zero value otherwise.

### GetGroupnameOk

`func (o *SharePermissionInputBean) GetGroupnameOk() (*string, bool)`

GetGroupnameOk returns a tuple with the Groupname field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupname

`func (o *SharePermissionInputBean) SetGroupname(v string)`

SetGroupname sets Groupname field to given value.

### HasGroupname

`func (o *SharePermissionInputBean) HasGroupname() bool`

HasGroupname returns a boolean if a field has been set.

### GetProjectRoleId

`func (o *SharePermissionInputBean) GetProjectRoleId() string`

GetProjectRoleId returns the ProjectRoleId field if non-nil, zero value otherwise.

### GetProjectRoleIdOk

`func (o *SharePermissionInputBean) GetProjectRoleIdOk() (*string, bool)`

GetProjectRoleIdOk returns a tuple with the ProjectRoleId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProjectRoleId

`func (o *SharePermissionInputBean) SetProjectRoleId(v string)`

SetProjectRoleId sets ProjectRoleId field to given value.

### HasProjectRoleId

`func (o *SharePermissionInputBean) HasProjectRoleId() bool`

HasProjectRoleId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# SimpleLink

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**StyleClass** | Pointer to **string** |  | [optional] 
**IconClass** | Pointer to **string** |  | [optional] 
**Label** | Pointer to **string** |  | [optional] 
**Title** | Pointer to **string** |  | [optional] 
**Href** | Pointer to **string** |  | [optional] 
**Weight** | Pointer to **int32** |  | [optional] 

## Methods

### NewSimpleLink

`func NewSimpleLink() *SimpleLink`

NewSimpleLink instantiates a new SimpleLink object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSimpleLinkWithDefaults

`func NewSimpleLinkWithDefaults() *SimpleLink`

NewSimpleLinkWithDefaults instantiates a new SimpleLink object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *SimpleLink) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *SimpleLink) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *SimpleLink) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *SimpleLink) HasId() bool`

HasId returns a boolean if a field has been set.

### GetStyleClass

`func (o *SimpleLink) GetStyleClass() string`

GetStyleClass returns the StyleClass field if non-nil, zero value otherwise.

### GetStyleClassOk

`func (o *SimpleLink) GetStyleClassOk() (*string, bool)`

GetStyleClassOk returns a tuple with the StyleClass field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStyleClass

`func (o *SimpleLink) SetStyleClass(v string)`

SetStyleClass sets StyleClass field to given value.

### HasStyleClass

`func (o *SimpleLink) HasStyleClass() bool`

HasStyleClass returns a boolean if a field has been set.

### GetIconClass

`func (o *SimpleLink) GetIconClass() string`

GetIconClass returns the IconClass field if non-nil, zero value otherwise.

### GetIconClassOk

`func (o *SimpleLink) GetIconClassOk() (*string, bool)`

GetIconClassOk returns a tuple with the IconClass field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIconClass

`func (o *SimpleLink) SetIconClass(v string)`

SetIconClass sets IconClass field to given value.

### HasIconClass

`func (o *SimpleLink) HasIconClass() bool`

HasIconClass returns a boolean if a field has been set.

### GetLabel

`func (o *SimpleLink) GetLabel() string`

GetLabel returns the Label field if non-nil, zero value otherwise.

### GetLabelOk

`func (o *SimpleLink) GetLabelOk() (*string, bool)`

GetLabelOk returns a tuple with the Label field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabel

`func (o *SimpleLink) SetLabel(v string)`

SetLabel sets Label field to given value.

### HasLabel

`func (o *SimpleLink) HasLabel() bool`

HasLabel returns a boolean if a field has been set.

### GetTitle

`func (o *SimpleLink) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *SimpleLink) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *SimpleLink) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *SimpleLink) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetHref

`func (o *SimpleLink) GetHref() string`

GetHref returns the Href field if non-nil, zero value otherwise.

### GetHrefOk

`func (o *SimpleLink) GetHrefOk() (*string, bool)`

GetHrefOk returns a tuple with the Href field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHref

`func (o *SimpleLink) SetHref(v string)`

SetHref sets Href field to given value.

### HasHref

`func (o *SimpleLink) HasHref() bool`

HasHref returns a boolean if a field has been set.

### GetWeight

`func (o *SimpleLink) GetWeight() int32`

GetWeight returns the Weight field if non-nil, zero value otherwise.

### GetWeightOk

`func (o *SimpleLink) GetWeightOk() (*int32, bool)`

GetWeightOk returns a tuple with the Weight field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWeight

`func (o *SimpleLink) SetWeight(v int32)`

SetWeight sets Weight field to given value.

### HasWeight

`func (o *SimpleLink) HasWeight() bool`

HasWeight returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



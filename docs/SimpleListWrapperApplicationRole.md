# SimpleListWrapperApplicationRole

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Size** | Pointer to **int32** |  | [optional] 
**Items** | Pointer to [**[]ApplicationRole**](ApplicationRole.md) |  | [optional] 
**PagingCallback** | Pointer to **map[string]interface{}** |  | [optional] 
**Callback** | Pointer to **map[string]interface{}** |  | [optional] 
**MaxResults** | Pointer to **int32** |  | [optional] 

## Methods

### NewSimpleListWrapperApplicationRole

`func NewSimpleListWrapperApplicationRole() *SimpleListWrapperApplicationRole`

NewSimpleListWrapperApplicationRole instantiates a new SimpleListWrapperApplicationRole object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSimpleListWrapperApplicationRoleWithDefaults

`func NewSimpleListWrapperApplicationRoleWithDefaults() *SimpleListWrapperApplicationRole`

NewSimpleListWrapperApplicationRoleWithDefaults instantiates a new SimpleListWrapperApplicationRole object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSize

`func (o *SimpleListWrapperApplicationRole) GetSize() int32`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *SimpleListWrapperApplicationRole) GetSizeOk() (*int32, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *SimpleListWrapperApplicationRole) SetSize(v int32)`

SetSize sets Size field to given value.

### HasSize

`func (o *SimpleListWrapperApplicationRole) HasSize() bool`

HasSize returns a boolean if a field has been set.

### GetItems

`func (o *SimpleListWrapperApplicationRole) GetItems() []ApplicationRole`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *SimpleListWrapperApplicationRole) GetItemsOk() (*[]ApplicationRole, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *SimpleListWrapperApplicationRole) SetItems(v []ApplicationRole)`

SetItems sets Items field to given value.

### HasItems

`func (o *SimpleListWrapperApplicationRole) HasItems() bool`

HasItems returns a boolean if a field has been set.

### GetPagingCallback

`func (o *SimpleListWrapperApplicationRole) GetPagingCallback() map[string]interface{}`

GetPagingCallback returns the PagingCallback field if non-nil, zero value otherwise.

### GetPagingCallbackOk

`func (o *SimpleListWrapperApplicationRole) GetPagingCallbackOk() (*map[string]interface{}, bool)`

GetPagingCallbackOk returns a tuple with the PagingCallback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPagingCallback

`func (o *SimpleListWrapperApplicationRole) SetPagingCallback(v map[string]interface{})`

SetPagingCallback sets PagingCallback field to given value.

### HasPagingCallback

`func (o *SimpleListWrapperApplicationRole) HasPagingCallback() bool`

HasPagingCallback returns a boolean if a field has been set.

### GetCallback

`func (o *SimpleListWrapperApplicationRole) GetCallback() map[string]interface{}`

GetCallback returns the Callback field if non-nil, zero value otherwise.

### GetCallbackOk

`func (o *SimpleListWrapperApplicationRole) GetCallbackOk() (*map[string]interface{}, bool)`

GetCallbackOk returns a tuple with the Callback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallback

`func (o *SimpleListWrapperApplicationRole) SetCallback(v map[string]interface{})`

SetCallback sets Callback field to given value.

### HasCallback

`func (o *SimpleListWrapperApplicationRole) HasCallback() bool`

HasCallback returns a boolean if a field has been set.

### GetMaxResults

`func (o *SimpleListWrapperApplicationRole) GetMaxResults() int32`

GetMaxResults returns the MaxResults field if non-nil, zero value otherwise.

### GetMaxResultsOk

`func (o *SimpleListWrapperApplicationRole) GetMaxResultsOk() (*int32, bool)`

GetMaxResultsOk returns a tuple with the MaxResults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxResults

`func (o *SimpleListWrapperApplicationRole) SetMaxResults(v int32)`

SetMaxResults sets MaxResults field to given value.

### HasMaxResults

`func (o *SimpleListWrapperApplicationRole) HasMaxResults() bool`

HasMaxResults returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# SimpleListWrapperGroupName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Size** | Pointer to **int32** |  | [optional] 
**Items** | Pointer to [**[]GroupName**](GroupName.md) |  | [optional] 
**PagingCallback** | Pointer to **map[string]interface{}** |  | [optional] 
**Callback** | Pointer to **map[string]interface{}** |  | [optional] 
**MaxResults** | Pointer to **int32** |  | [optional] 

## Methods

### NewSimpleListWrapperGroupName

`func NewSimpleListWrapperGroupName() *SimpleListWrapperGroupName`

NewSimpleListWrapperGroupName instantiates a new SimpleListWrapperGroupName object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSimpleListWrapperGroupNameWithDefaults

`func NewSimpleListWrapperGroupNameWithDefaults() *SimpleListWrapperGroupName`

NewSimpleListWrapperGroupNameWithDefaults instantiates a new SimpleListWrapperGroupName object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSize

`func (o *SimpleListWrapperGroupName) GetSize() int32`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *SimpleListWrapperGroupName) GetSizeOk() (*int32, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *SimpleListWrapperGroupName) SetSize(v int32)`

SetSize sets Size field to given value.

### HasSize

`func (o *SimpleListWrapperGroupName) HasSize() bool`

HasSize returns a boolean if a field has been set.

### GetItems

`func (o *SimpleListWrapperGroupName) GetItems() []GroupName`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *SimpleListWrapperGroupName) GetItemsOk() (*[]GroupName, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *SimpleListWrapperGroupName) SetItems(v []GroupName)`

SetItems sets Items field to given value.

### HasItems

`func (o *SimpleListWrapperGroupName) HasItems() bool`

HasItems returns a boolean if a field has been set.

### GetPagingCallback

`func (o *SimpleListWrapperGroupName) GetPagingCallback() map[string]interface{}`

GetPagingCallback returns the PagingCallback field if non-nil, zero value otherwise.

### GetPagingCallbackOk

`func (o *SimpleListWrapperGroupName) GetPagingCallbackOk() (*map[string]interface{}, bool)`

GetPagingCallbackOk returns a tuple with the PagingCallback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPagingCallback

`func (o *SimpleListWrapperGroupName) SetPagingCallback(v map[string]interface{})`

SetPagingCallback sets PagingCallback field to given value.

### HasPagingCallback

`func (o *SimpleListWrapperGroupName) HasPagingCallback() bool`

HasPagingCallback returns a boolean if a field has been set.

### GetCallback

`func (o *SimpleListWrapperGroupName) GetCallback() map[string]interface{}`

GetCallback returns the Callback field if non-nil, zero value otherwise.

### GetCallbackOk

`func (o *SimpleListWrapperGroupName) GetCallbackOk() (*map[string]interface{}, bool)`

GetCallbackOk returns a tuple with the Callback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallback

`func (o *SimpleListWrapperGroupName) SetCallback(v map[string]interface{})`

SetCallback sets Callback field to given value.

### HasCallback

`func (o *SimpleListWrapperGroupName) HasCallback() bool`

HasCallback returns a boolean if a field has been set.

### GetMaxResults

`func (o *SimpleListWrapperGroupName) GetMaxResults() int32`

GetMaxResults returns the MaxResults field if non-nil, zero value otherwise.

### GetMaxResultsOk

`func (o *SimpleListWrapperGroupName) GetMaxResultsOk() (*int32, bool)`

GetMaxResultsOk returns a tuple with the MaxResults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxResults

`func (o *SimpleListWrapperGroupName) SetMaxResults(v int32)`

SetMaxResults sets MaxResults field to given value.

### HasMaxResults

`func (o *SimpleListWrapperGroupName) HasMaxResults() bool`

HasMaxResults returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



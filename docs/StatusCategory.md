# StatusCategory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | Pointer to **string** | The URL of the status category. | [optional] [readonly] 
**Id** | Pointer to **int64** | The ID of the status category. | [optional] [readonly] 
**Key** | Pointer to **string** | The key of the status category. | [optional] [readonly] 
**ColorName** | Pointer to **string** | The name of the color used to represent the status category. | [optional] [readonly] 
**Name** | Pointer to **string** | The name of the status category. | [optional] [readonly] 

## Methods

### NewStatusCategory

`func NewStatusCategory() *StatusCategory`

NewStatusCategory instantiates a new StatusCategory object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStatusCategoryWithDefaults

`func NewStatusCategoryWithDefaults() *StatusCategory`

NewStatusCategoryWithDefaults instantiates a new StatusCategory object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *StatusCategory) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *StatusCategory) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *StatusCategory) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *StatusCategory) HasSelf() bool`

HasSelf returns a boolean if a field has been set.

### GetId

`func (o *StatusCategory) GetId() int64`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *StatusCategory) GetIdOk() (*int64, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *StatusCategory) SetId(v int64)`

SetId sets Id field to given value.

### HasId

`func (o *StatusCategory) HasId() bool`

HasId returns a boolean if a field has been set.

### GetKey

`func (o *StatusCategory) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *StatusCategory) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *StatusCategory) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *StatusCategory) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetColorName

`func (o *StatusCategory) GetColorName() string`

GetColorName returns the ColorName field if non-nil, zero value otherwise.

### GetColorNameOk

`func (o *StatusCategory) GetColorNameOk() (*string, bool)`

GetColorNameOk returns a tuple with the ColorName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetColorName

`func (o *StatusCategory) SetColorName(v string)`

SetColorName sets ColorName field to given value.

### HasColorName

`func (o *StatusCategory) HasColorName() bool`

HasColorName returns a boolean if a field has been set.

### GetName

`func (o *StatusCategory) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *StatusCategory) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *StatusCategory) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *StatusCategory) HasName() bool`

HasName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



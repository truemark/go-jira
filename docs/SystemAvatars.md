# SystemAvatars

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**System** | Pointer to [**[]Avatar**](Avatar.md) | A list of avatar details. | [optional] [readonly] 

## Methods

### NewSystemAvatars

`func NewSystemAvatars() *SystemAvatars`

NewSystemAvatars instantiates a new SystemAvatars object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSystemAvatarsWithDefaults

`func NewSystemAvatarsWithDefaults() *SystemAvatars`

NewSystemAvatarsWithDefaults instantiates a new SystemAvatars object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSystem

`func (o *SystemAvatars) GetSystem() []Avatar`

GetSystem returns the System field if non-nil, zero value otherwise.

### GetSystemOk

`func (o *SystemAvatars) GetSystemOk() (*[]Avatar, bool)`

GetSystemOk returns a tuple with the System field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSystem

`func (o *SystemAvatars) SetSystem(v []Avatar)`

SetSystem sets System field to given value.

### HasSystem

`func (o *SystemAvatars) HasSystem() bool`

HasSystem returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# \TimeTrackingApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAvailableTimeTrackingImplementations**](TimeTrackingApi.md#GetAvailableTimeTrackingImplementations) | **Get** /rest/api/3/configuration/timetracking/list | Get all time tracking providers
[**GetSelectedTimeTrackingImplementation**](TimeTrackingApi.md#GetSelectedTimeTrackingImplementation) | **Get** /rest/api/3/configuration/timetracking | Get selected time tracking provider
[**GetSharedTimeTrackingConfiguration**](TimeTrackingApi.md#GetSharedTimeTrackingConfiguration) | **Get** /rest/api/3/configuration/timetracking/options | Get time tracking settings
[**SelectTimeTrackingImplementation**](TimeTrackingApi.md#SelectTimeTrackingImplementation) | **Put** /rest/api/3/configuration/timetracking | Select time tracking provider
[**SetSharedTimeTrackingConfiguration**](TimeTrackingApi.md#SetSharedTimeTrackingConfiguration) | **Put** /rest/api/3/configuration/timetracking/options | Set time tracking settings



## GetAvailableTimeTrackingImplementations

> []TimeTrackingProvider GetAvailableTimeTrackingImplementations(ctx).Execute()

Get all time tracking providers



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TimeTrackingApi.GetAvailableTimeTrackingImplementations(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TimeTrackingApi.GetAvailableTimeTrackingImplementations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAvailableTimeTrackingImplementations`: []TimeTrackingProvider
    fmt.Fprintf(os.Stdout, "Response from `TimeTrackingApi.GetAvailableTimeTrackingImplementations`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetAvailableTimeTrackingImplementationsRequest struct via the builder pattern


### Return type

[**[]TimeTrackingProvider**](TimeTrackingProvider.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSelectedTimeTrackingImplementation

> TimeTrackingProvider GetSelectedTimeTrackingImplementation(ctx).Execute()

Get selected time tracking provider



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TimeTrackingApi.GetSelectedTimeTrackingImplementation(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TimeTrackingApi.GetSelectedTimeTrackingImplementation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSelectedTimeTrackingImplementation`: TimeTrackingProvider
    fmt.Fprintf(os.Stdout, "Response from `TimeTrackingApi.GetSelectedTimeTrackingImplementation`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetSelectedTimeTrackingImplementationRequest struct via the builder pattern


### Return type

[**TimeTrackingProvider**](TimeTrackingProvider.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSharedTimeTrackingConfiguration

> TimeTrackingConfiguration GetSharedTimeTrackingConfiguration(ctx).Execute()

Get time tracking settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TimeTrackingApi.GetSharedTimeTrackingConfiguration(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TimeTrackingApi.GetSharedTimeTrackingConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSharedTimeTrackingConfiguration`: TimeTrackingConfiguration
    fmt.Fprintf(os.Stdout, "Response from `TimeTrackingApi.GetSharedTimeTrackingConfiguration`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetSharedTimeTrackingConfigurationRequest struct via the builder pattern


### Return type

[**TimeTrackingConfiguration**](TimeTrackingConfiguration.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SelectTimeTrackingImplementation

> interface{} SelectTimeTrackingImplementation(ctx).TimeTrackingProvider(timeTrackingProvider).Execute()

Select time tracking provider



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    timeTrackingProvider := *openapiclient.NewTimeTrackingProvider("Key_example") // TimeTrackingProvider | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TimeTrackingApi.SelectTimeTrackingImplementation(context.Background()).TimeTrackingProvider(timeTrackingProvider).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TimeTrackingApi.SelectTimeTrackingImplementation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SelectTimeTrackingImplementation`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `TimeTrackingApi.SelectTimeTrackingImplementation`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSelectTimeTrackingImplementationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeTrackingProvider** | [**TimeTrackingProvider**](TimeTrackingProvider.md) |  | 

### Return type

**interface{}**

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetSharedTimeTrackingConfiguration

> TimeTrackingConfiguration SetSharedTimeTrackingConfiguration(ctx).TimeTrackingConfiguration(timeTrackingConfiguration).Execute()

Set time tracking settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    timeTrackingConfiguration := *openapiclient.NewTimeTrackingConfiguration(float64(123), float64(123), "TimeFormat_example", "DefaultUnit_example") // TimeTrackingConfiguration | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TimeTrackingApi.SetSharedTimeTrackingConfiguration(context.Background()).TimeTrackingConfiguration(timeTrackingConfiguration).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TimeTrackingApi.SetSharedTimeTrackingConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetSharedTimeTrackingConfiguration`: TimeTrackingConfiguration
    fmt.Fprintf(os.Stdout, "Response from `TimeTrackingApi.SetSharedTimeTrackingConfiguration`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSetSharedTimeTrackingConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeTrackingConfiguration** | [**TimeTrackingConfiguration**](TimeTrackingConfiguration.md) |  | 

### Return type

[**TimeTrackingConfiguration**](TimeTrackingConfiguration.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


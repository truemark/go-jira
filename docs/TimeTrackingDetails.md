# TimeTrackingDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OriginalEstimate** | Pointer to **string** | The original estimate of time needed for this issue in readable format. | [optional] [readonly] 
**RemainingEstimate** | Pointer to **string** | The remaining estimate of time needed for this issue in readable format. | [optional] [readonly] 
**TimeSpent** | Pointer to **string** | Time worked on this issue in readable format. | [optional] [readonly] 
**OriginalEstimateSeconds** | Pointer to **int64** | The original estimate of time needed for this issue in seconds. | [optional] [readonly] 
**RemainingEstimateSeconds** | Pointer to **int64** | The remaining estimate of time needed for this issue in seconds. | [optional] [readonly] 
**TimeSpentSeconds** | Pointer to **int64** | Time worked on this issue in seconds. | [optional] [readonly] 

## Methods

### NewTimeTrackingDetails

`func NewTimeTrackingDetails() *TimeTrackingDetails`

NewTimeTrackingDetails instantiates a new TimeTrackingDetails object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTimeTrackingDetailsWithDefaults

`func NewTimeTrackingDetailsWithDefaults() *TimeTrackingDetails`

NewTimeTrackingDetailsWithDefaults instantiates a new TimeTrackingDetails object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOriginalEstimate

`func (o *TimeTrackingDetails) GetOriginalEstimate() string`

GetOriginalEstimate returns the OriginalEstimate field if non-nil, zero value otherwise.

### GetOriginalEstimateOk

`func (o *TimeTrackingDetails) GetOriginalEstimateOk() (*string, bool)`

GetOriginalEstimateOk returns a tuple with the OriginalEstimate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOriginalEstimate

`func (o *TimeTrackingDetails) SetOriginalEstimate(v string)`

SetOriginalEstimate sets OriginalEstimate field to given value.

### HasOriginalEstimate

`func (o *TimeTrackingDetails) HasOriginalEstimate() bool`

HasOriginalEstimate returns a boolean if a field has been set.

### GetRemainingEstimate

`func (o *TimeTrackingDetails) GetRemainingEstimate() string`

GetRemainingEstimate returns the RemainingEstimate field if non-nil, zero value otherwise.

### GetRemainingEstimateOk

`func (o *TimeTrackingDetails) GetRemainingEstimateOk() (*string, bool)`

GetRemainingEstimateOk returns a tuple with the RemainingEstimate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemainingEstimate

`func (o *TimeTrackingDetails) SetRemainingEstimate(v string)`

SetRemainingEstimate sets RemainingEstimate field to given value.

### HasRemainingEstimate

`func (o *TimeTrackingDetails) HasRemainingEstimate() bool`

HasRemainingEstimate returns a boolean if a field has been set.

### GetTimeSpent

`func (o *TimeTrackingDetails) GetTimeSpent() string`

GetTimeSpent returns the TimeSpent field if non-nil, zero value otherwise.

### GetTimeSpentOk

`func (o *TimeTrackingDetails) GetTimeSpentOk() (*string, bool)`

GetTimeSpentOk returns a tuple with the TimeSpent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeSpent

`func (o *TimeTrackingDetails) SetTimeSpent(v string)`

SetTimeSpent sets TimeSpent field to given value.

### HasTimeSpent

`func (o *TimeTrackingDetails) HasTimeSpent() bool`

HasTimeSpent returns a boolean if a field has been set.

### GetOriginalEstimateSeconds

`func (o *TimeTrackingDetails) GetOriginalEstimateSeconds() int64`

GetOriginalEstimateSeconds returns the OriginalEstimateSeconds field if non-nil, zero value otherwise.

### GetOriginalEstimateSecondsOk

`func (o *TimeTrackingDetails) GetOriginalEstimateSecondsOk() (*int64, bool)`

GetOriginalEstimateSecondsOk returns a tuple with the OriginalEstimateSeconds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOriginalEstimateSeconds

`func (o *TimeTrackingDetails) SetOriginalEstimateSeconds(v int64)`

SetOriginalEstimateSeconds sets OriginalEstimateSeconds field to given value.

### HasOriginalEstimateSeconds

`func (o *TimeTrackingDetails) HasOriginalEstimateSeconds() bool`

HasOriginalEstimateSeconds returns a boolean if a field has been set.

### GetRemainingEstimateSeconds

`func (o *TimeTrackingDetails) GetRemainingEstimateSeconds() int64`

GetRemainingEstimateSeconds returns the RemainingEstimateSeconds field if non-nil, zero value otherwise.

### GetRemainingEstimateSecondsOk

`func (o *TimeTrackingDetails) GetRemainingEstimateSecondsOk() (*int64, bool)`

GetRemainingEstimateSecondsOk returns a tuple with the RemainingEstimateSeconds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemainingEstimateSeconds

`func (o *TimeTrackingDetails) SetRemainingEstimateSeconds(v int64)`

SetRemainingEstimateSeconds sets RemainingEstimateSeconds field to given value.

### HasRemainingEstimateSeconds

`func (o *TimeTrackingDetails) HasRemainingEstimateSeconds() bool`

HasRemainingEstimateSeconds returns a boolean if a field has been set.

### GetTimeSpentSeconds

`func (o *TimeTrackingDetails) GetTimeSpentSeconds() int64`

GetTimeSpentSeconds returns the TimeSpentSeconds field if non-nil, zero value otherwise.

### GetTimeSpentSecondsOk

`func (o *TimeTrackingDetails) GetTimeSpentSecondsOk() (*int64, bool)`

GetTimeSpentSecondsOk returns a tuple with the TimeSpentSeconds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeSpentSeconds

`func (o *TimeTrackingDetails) SetTimeSpentSeconds(v int64)`

SetTimeSpentSeconds sets TimeSpentSeconds field to given value.

### HasTimeSpentSeconds

`func (o *TimeTrackingDetails) HasTimeSpentSeconds() bool`

HasTimeSpentSeconds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



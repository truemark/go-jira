# UnrestrictedUserEmail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountId** | Pointer to **string** | The accountId of the user | [optional] 
**Email** | Pointer to **string** | The email of the user | [optional] 

## Methods

### NewUnrestrictedUserEmail

`func NewUnrestrictedUserEmail() *UnrestrictedUserEmail`

NewUnrestrictedUserEmail instantiates a new UnrestrictedUserEmail object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUnrestrictedUserEmailWithDefaults

`func NewUnrestrictedUserEmailWithDefaults() *UnrestrictedUserEmail`

NewUnrestrictedUserEmailWithDefaults instantiates a new UnrestrictedUserEmail object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAccountId

`func (o *UnrestrictedUserEmail) GetAccountId() string`

GetAccountId returns the AccountId field if non-nil, zero value otherwise.

### GetAccountIdOk

`func (o *UnrestrictedUserEmail) GetAccountIdOk() (*string, bool)`

GetAccountIdOk returns a tuple with the AccountId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccountId

`func (o *UnrestrictedUserEmail) SetAccountId(v string)`

SetAccountId sets AccountId field to given value.

### HasAccountId

`func (o *UnrestrictedUserEmail) HasAccountId() bool`

HasAccountId returns a boolean if a field has been set.

### GetEmail

`func (o *UnrestrictedUserEmail) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *UnrestrictedUserEmail) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *UnrestrictedUserEmail) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *UnrestrictedUserEmail) HasEmail() bool`

HasEmail returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# UpdateDefaultScreenScheme

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ScreenSchemeId** | **string** | The ID of the screen scheme. | 

## Methods

### NewUpdateDefaultScreenScheme

`func NewUpdateDefaultScreenScheme(screenSchemeId string, ) *UpdateDefaultScreenScheme`

NewUpdateDefaultScreenScheme instantiates a new UpdateDefaultScreenScheme object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateDefaultScreenSchemeWithDefaults

`func NewUpdateDefaultScreenSchemeWithDefaults() *UpdateDefaultScreenScheme`

NewUpdateDefaultScreenSchemeWithDefaults instantiates a new UpdateDefaultScreenScheme object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetScreenSchemeId

`func (o *UpdateDefaultScreenScheme) GetScreenSchemeId() string`

GetScreenSchemeId returns the ScreenSchemeId field if non-nil, zero value otherwise.

### GetScreenSchemeIdOk

`func (o *UpdateDefaultScreenScheme) GetScreenSchemeIdOk() (*string, bool)`

GetScreenSchemeIdOk returns a tuple with the ScreenSchemeId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScreenSchemeId

`func (o *UpdateDefaultScreenScheme) SetScreenSchemeId(v string)`

SetScreenSchemeId sets ScreenSchemeId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# UpdatedProjectCategory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | Pointer to **string** | The URL of the project category. | [optional] [readonly] 
**Id** | Pointer to **string** | The ID of the project category. | [optional] [readonly] 
**Description** | Pointer to **string** | The name of the project category. | [optional] [readonly] 
**Name** | Pointer to **string** | The description of the project category. | [optional] [readonly] 

## Methods

### NewUpdatedProjectCategory

`func NewUpdatedProjectCategory() *UpdatedProjectCategory`

NewUpdatedProjectCategory instantiates a new UpdatedProjectCategory object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdatedProjectCategoryWithDefaults

`func NewUpdatedProjectCategoryWithDefaults() *UpdatedProjectCategory`

NewUpdatedProjectCategoryWithDefaults instantiates a new UpdatedProjectCategory object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *UpdatedProjectCategory) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *UpdatedProjectCategory) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *UpdatedProjectCategory) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *UpdatedProjectCategory) HasSelf() bool`

HasSelf returns a boolean if a field has been set.

### GetId

`func (o *UpdatedProjectCategory) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *UpdatedProjectCategory) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *UpdatedProjectCategory) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *UpdatedProjectCategory) HasId() bool`

HasId returns a boolean if a field has been set.

### GetDescription

`func (o *UpdatedProjectCategory) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *UpdatedProjectCategory) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *UpdatedProjectCategory) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *UpdatedProjectCategory) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetName

`func (o *UpdatedProjectCategory) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *UpdatedProjectCategory) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *UpdatedProjectCategory) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *UpdatedProjectCategory) HasName() bool`

HasName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



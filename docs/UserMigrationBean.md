# UserMigrationBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | Pointer to **string** |  | [optional] 
**Username** | Pointer to **string** |  | [optional] 
**AccountId** | Pointer to **string** |  | [optional] 

## Methods

### NewUserMigrationBean

`func NewUserMigrationBean() *UserMigrationBean`

NewUserMigrationBean instantiates a new UserMigrationBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserMigrationBeanWithDefaults

`func NewUserMigrationBeanWithDefaults() *UserMigrationBean`

NewUserMigrationBeanWithDefaults instantiates a new UserMigrationBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKey

`func (o *UserMigrationBean) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *UserMigrationBean) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *UserMigrationBean) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *UserMigrationBean) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetUsername

`func (o *UserMigrationBean) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *UserMigrationBean) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *UserMigrationBean) SetUsername(v string)`

SetUsername sets Username field to given value.

### HasUsername

`func (o *UserMigrationBean) HasUsername() bool`

HasUsername returns a boolean if a field has been set.

### GetAccountId

`func (o *UserMigrationBean) GetAccountId() string`

GetAccountId returns the AccountId field if non-nil, zero value otherwise.

### GetAccountIdOk

`func (o *UserMigrationBean) GetAccountIdOk() (*string, bool)`

GetAccountIdOk returns a tuple with the AccountId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccountId

`func (o *UserMigrationBean) SetAccountId(v string)`

SetAccountId sets AccountId field to given value.

### HasAccountId

`func (o *UserMigrationBean) HasAccountId() bool`

HasAccountId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



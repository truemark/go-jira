# UserWriteBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | Pointer to **string** | The URL of the user. | [optional] [readonly] 
**Key** | Pointer to **string** | This parameter is no longer available. See the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details. | [optional] 
**Name** | Pointer to **string** | This parameter is no longer available. See the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details. | [optional] 
**Password** | Pointer to **string** | This parameter is no longer available. If the user has an Atlassian account, their password is not changed. If the user does not have an Atlassian account, they are sent an email asking them set up an account. | [optional] 
**EmailAddress** | **string** | The email address for the user. | 
**DisplayName** | **string** | A suggested display name for the user. If the user has an Atlassian account, their display name is not changed. If the user does not have an Atlassian account, this display name is used as a suggestion for creating an account. The user is sent an email asking them to set their display name and privacy preferences. | 
**ApplicationKeys** | Pointer to **[]string** | Deprecated, do not use. | [optional] 

## Methods

### NewUserWriteBean

`func NewUserWriteBean(emailAddress string, displayName string, ) *UserWriteBean`

NewUserWriteBean instantiates a new UserWriteBean object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserWriteBeanWithDefaults

`func NewUserWriteBeanWithDefaults() *UserWriteBean`

NewUserWriteBeanWithDefaults instantiates a new UserWriteBean object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *UserWriteBean) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *UserWriteBean) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *UserWriteBean) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *UserWriteBean) HasSelf() bool`

HasSelf returns a boolean if a field has been set.

### GetKey

`func (o *UserWriteBean) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *UserWriteBean) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *UserWriteBean) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *UserWriteBean) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetName

`func (o *UserWriteBean) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *UserWriteBean) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *UserWriteBean) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *UserWriteBean) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPassword

`func (o *UserWriteBean) GetPassword() string`

GetPassword returns the Password field if non-nil, zero value otherwise.

### GetPasswordOk

`func (o *UserWriteBean) GetPasswordOk() (*string, bool)`

GetPasswordOk returns a tuple with the Password field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPassword

`func (o *UserWriteBean) SetPassword(v string)`

SetPassword sets Password field to given value.

### HasPassword

`func (o *UserWriteBean) HasPassword() bool`

HasPassword returns a boolean if a field has been set.

### GetEmailAddress

`func (o *UserWriteBean) GetEmailAddress() string`

GetEmailAddress returns the EmailAddress field if non-nil, zero value otherwise.

### GetEmailAddressOk

`func (o *UserWriteBean) GetEmailAddressOk() (*string, bool)`

GetEmailAddressOk returns a tuple with the EmailAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmailAddress

`func (o *UserWriteBean) SetEmailAddress(v string)`

SetEmailAddress sets EmailAddress field to given value.


### GetDisplayName

`func (o *UserWriteBean) GetDisplayName() string`

GetDisplayName returns the DisplayName field if non-nil, zero value otherwise.

### GetDisplayNameOk

`func (o *UserWriteBean) GetDisplayNameOk() (*string, bool)`

GetDisplayNameOk returns a tuple with the DisplayName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayName

`func (o *UserWriteBean) SetDisplayName(v string)`

SetDisplayName sets DisplayName field to given value.


### GetApplicationKeys

`func (o *UserWriteBean) GetApplicationKeys() []string`

GetApplicationKeys returns the ApplicationKeys field if non-nil, zero value otherwise.

### GetApplicationKeysOk

`func (o *UserWriteBean) GetApplicationKeysOk() (*[]string, bool)`

GetApplicationKeysOk returns a tuple with the ApplicationKeys field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApplicationKeys

`func (o *UserWriteBean) SetApplicationKeys(v []string)`

SetApplicationKeys sets ApplicationKeys field to given value.

### HasApplicationKeys

`func (o *UserWriteBean) HasApplicationKeys() bool`

HasApplicationKeys returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



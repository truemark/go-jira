# VersionUnresolvedIssuesCount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | Pointer to **string** | The URL of these count details. | [optional] [readonly] 
**IssuesUnresolvedCount** | Pointer to **int64** | Count of unresolved issues. | [optional] [readonly] 
**IssuesCount** | Pointer to **int64** | Count of issues. | [optional] [readonly] 

## Methods

### NewVersionUnresolvedIssuesCount

`func NewVersionUnresolvedIssuesCount() *VersionUnresolvedIssuesCount`

NewVersionUnresolvedIssuesCount instantiates a new VersionUnresolvedIssuesCount object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVersionUnresolvedIssuesCountWithDefaults

`func NewVersionUnresolvedIssuesCountWithDefaults() *VersionUnresolvedIssuesCount`

NewVersionUnresolvedIssuesCountWithDefaults instantiates a new VersionUnresolvedIssuesCount object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *VersionUnresolvedIssuesCount) GetSelf() string`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *VersionUnresolvedIssuesCount) GetSelfOk() (*string, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *VersionUnresolvedIssuesCount) SetSelf(v string)`

SetSelf sets Self field to given value.

### HasSelf

`func (o *VersionUnresolvedIssuesCount) HasSelf() bool`

HasSelf returns a boolean if a field has been set.

### GetIssuesUnresolvedCount

`func (o *VersionUnresolvedIssuesCount) GetIssuesUnresolvedCount() int64`

GetIssuesUnresolvedCount returns the IssuesUnresolvedCount field if non-nil, zero value otherwise.

### GetIssuesUnresolvedCountOk

`func (o *VersionUnresolvedIssuesCount) GetIssuesUnresolvedCountOk() (*int64, bool)`

GetIssuesUnresolvedCountOk returns a tuple with the IssuesUnresolvedCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssuesUnresolvedCount

`func (o *VersionUnresolvedIssuesCount) SetIssuesUnresolvedCount(v int64)`

SetIssuesUnresolvedCount sets IssuesUnresolvedCount field to given value.

### HasIssuesUnresolvedCount

`func (o *VersionUnresolvedIssuesCount) HasIssuesUnresolvedCount() bool`

HasIssuesUnresolvedCount returns a boolean if a field has been set.

### GetIssuesCount

`func (o *VersionUnresolvedIssuesCount) GetIssuesCount() int64`

GetIssuesCount returns the IssuesCount field if non-nil, zero value otherwise.

### GetIssuesCountOk

`func (o *VersionUnresolvedIssuesCount) GetIssuesCountOk() (*int64, bool)`

GetIssuesCountOk returns a tuple with the IssuesCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssuesCount

`func (o *VersionUnresolvedIssuesCount) SetIssuesCount(v int64)`

SetIssuesCount sets IssuesCount field to given value.

### HasIssuesCount

`func (o *VersionUnresolvedIssuesCount) HasIssuesCount() bool`

HasIssuesCount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



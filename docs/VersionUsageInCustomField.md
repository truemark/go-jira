# VersionUsageInCustomField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FieldName** | Pointer to **string** | The name of the custom field. | [optional] [readonly] 
**CustomFieldId** | Pointer to **int64** | The ID of the custom field. | [optional] [readonly] 
**IssueCountWithVersionInCustomField** | Pointer to **int64** | Count of the issues where the custom field contains the version. | [optional] [readonly] 

## Methods

### NewVersionUsageInCustomField

`func NewVersionUsageInCustomField() *VersionUsageInCustomField`

NewVersionUsageInCustomField instantiates a new VersionUsageInCustomField object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVersionUsageInCustomFieldWithDefaults

`func NewVersionUsageInCustomFieldWithDefaults() *VersionUsageInCustomField`

NewVersionUsageInCustomFieldWithDefaults instantiates a new VersionUsageInCustomField object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFieldName

`func (o *VersionUsageInCustomField) GetFieldName() string`

GetFieldName returns the FieldName field if non-nil, zero value otherwise.

### GetFieldNameOk

`func (o *VersionUsageInCustomField) GetFieldNameOk() (*string, bool)`

GetFieldNameOk returns a tuple with the FieldName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFieldName

`func (o *VersionUsageInCustomField) SetFieldName(v string)`

SetFieldName sets FieldName field to given value.

### HasFieldName

`func (o *VersionUsageInCustomField) HasFieldName() bool`

HasFieldName returns a boolean if a field has been set.

### GetCustomFieldId

`func (o *VersionUsageInCustomField) GetCustomFieldId() int64`

GetCustomFieldId returns the CustomFieldId field if non-nil, zero value otherwise.

### GetCustomFieldIdOk

`func (o *VersionUsageInCustomField) GetCustomFieldIdOk() (*int64, bool)`

GetCustomFieldIdOk returns a tuple with the CustomFieldId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomFieldId

`func (o *VersionUsageInCustomField) SetCustomFieldId(v int64)`

SetCustomFieldId sets CustomFieldId field to given value.

### HasCustomFieldId

`func (o *VersionUsageInCustomField) HasCustomFieldId() bool`

HasCustomFieldId returns a boolean if a field has been set.

### GetIssueCountWithVersionInCustomField

`func (o *VersionUsageInCustomField) GetIssueCountWithVersionInCustomField() int64`

GetIssueCountWithVersionInCustomField returns the IssueCountWithVersionInCustomField field if non-nil, zero value otherwise.

### GetIssueCountWithVersionInCustomFieldOk

`func (o *VersionUsageInCustomField) GetIssueCountWithVersionInCustomFieldOk() (*int64, bool)`

GetIssueCountWithVersionInCustomFieldOk returns a tuple with the IssueCountWithVersionInCustomField field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueCountWithVersionInCustomField

`func (o *VersionUsageInCustomField) SetIssueCountWithVersionInCustomField(v int64)`

SetIssueCountWithVersionInCustomField sets IssueCountWithVersionInCustomField field to given value.

### HasIssueCountWithVersionInCustomField

`func (o *VersionUsageInCustomField) HasIssueCountWithVersionInCustomField() bool`

HasIssueCountWithVersionInCustomField returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Webhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int64** | The ID of the webhook. | 
**JqlFilter** | **string** | The JQL filter that specifies which issues the webhook is sent for. | 
**Events** | **[]string** | The Jira events that trigger the webhook. | 
**ExpirationDate** | **int64** |  | [readonly] 

## Methods

### NewWebhook

`func NewWebhook(id int64, jqlFilter string, events []string, expirationDate int64, ) *Webhook`

NewWebhook instantiates a new Webhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookWithDefaults

`func NewWebhookWithDefaults() *Webhook`

NewWebhookWithDefaults instantiates a new Webhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Webhook) GetId() int64`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Webhook) GetIdOk() (*int64, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Webhook) SetId(v int64)`

SetId sets Id field to given value.


### GetJqlFilter

`func (o *Webhook) GetJqlFilter() string`

GetJqlFilter returns the JqlFilter field if non-nil, zero value otherwise.

### GetJqlFilterOk

`func (o *Webhook) GetJqlFilterOk() (*string, bool)`

GetJqlFilterOk returns a tuple with the JqlFilter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJqlFilter

`func (o *Webhook) SetJqlFilter(v string)`

SetJqlFilter sets JqlFilter field to given value.


### GetEvents

`func (o *Webhook) GetEvents() []string`

GetEvents returns the Events field if non-nil, zero value otherwise.

### GetEventsOk

`func (o *Webhook) GetEventsOk() (*[]string, bool)`

GetEventsOk returns a tuple with the Events field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvents

`func (o *Webhook) SetEvents(v []string)`

SetEvents sets Events field to given value.


### GetExpirationDate

`func (o *Webhook) GetExpirationDate() int64`

GetExpirationDate returns the ExpirationDate field if non-nil, zero value otherwise.

### GetExpirationDateOk

`func (o *Webhook) GetExpirationDateOk() (*int64, bool)`

GetExpirationDateOk returns a tuple with the ExpirationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpirationDate

`func (o *Webhook) SetExpirationDate(v int64)`

SetExpirationDate sets ExpirationDate field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



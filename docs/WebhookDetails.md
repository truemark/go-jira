# WebhookDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**JqlFilter** | **string** | The JQL filter that specifies which issues the webhook is sent for. Only a subset of JQL can be used. The supported elements are:   *  Fields: &#x60;issueKey&#x60;, &#x60;project&#x60;, &#x60;issuetype&#x60;, &#x60;status&#x60;, &#x60;assignee&#x60;, &#x60;reporter&#x60;, &#x60;issue.property&#x60;, and &#x60;cf[id]&#x60; (for custom fields—only the epic label custom field is supported).  *  Operators: &#x60;&#x3D;&#x60;, &#x60;!&#x3D;&#x60;, &#x60;IN&#x60;, and &#x60;NOT IN&#x60;. | 
**Events** | **[]string** | The Jira events that trigger the webhook. | 

## Methods

### NewWebhookDetails

`func NewWebhookDetails(jqlFilter string, events []string, ) *WebhookDetails`

NewWebhookDetails instantiates a new WebhookDetails object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookDetailsWithDefaults

`func NewWebhookDetailsWithDefaults() *WebhookDetails`

NewWebhookDetailsWithDefaults instantiates a new WebhookDetails object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetJqlFilter

`func (o *WebhookDetails) GetJqlFilter() string`

GetJqlFilter returns the JqlFilter field if non-nil, zero value otherwise.

### GetJqlFilterOk

`func (o *WebhookDetails) GetJqlFilterOk() (*string, bool)`

GetJqlFilterOk returns a tuple with the JqlFilter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJqlFilter

`func (o *WebhookDetails) SetJqlFilter(v string)`

SetJqlFilter sets JqlFilter field to given value.


### GetEvents

`func (o *WebhookDetails) GetEvents() []string`

GetEvents returns the Events field if non-nil, zero value otherwise.

### GetEventsOk

`func (o *WebhookDetails) GetEventsOk() (*[]string, bool)`

GetEventsOk returns a tuple with the Events field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvents

`func (o *WebhookDetails) SetEvents(v []string)`

SetEvents sets Events field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



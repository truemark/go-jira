# WebhooksExpirationDate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExpirationDate** | **int64** |  | [readonly] 

## Methods

### NewWebhooksExpirationDate

`func NewWebhooksExpirationDate(expirationDate int64, ) *WebhooksExpirationDate`

NewWebhooksExpirationDate instantiates a new WebhooksExpirationDate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhooksExpirationDateWithDefaults

`func NewWebhooksExpirationDateWithDefaults() *WebhooksExpirationDate`

NewWebhooksExpirationDateWithDefaults instantiates a new WebhooksExpirationDate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetExpirationDate

`func (o *WebhooksExpirationDate) GetExpirationDate() int64`

GetExpirationDate returns the ExpirationDate field if non-nil, zero value otherwise.

### GetExpirationDateOk

`func (o *WebhooksExpirationDate) GetExpirationDateOk() (*int64, bool)`

GetExpirationDateOk returns a tuple with the ExpirationDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpirationDate

`func (o *WebhooksExpirationDate) SetExpirationDate(v int64)`

SetExpirationDate sets ExpirationDate field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



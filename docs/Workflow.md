# Workflow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | [**PublishedWorkflowId**](PublishedWorkflowId.md) |  | 
**Description** | **string** | The description of the workflow. | 
**Transitions** | Pointer to [**[]Transition**](Transition.md) | The transitions of the workflow. | [optional] 
**Statuses** | Pointer to [**[]WorkflowStatus**](WorkflowStatus.md) | The statuses of the workflow. | [optional] 
**IsDefault** | Pointer to **bool** | Whether this is the default workflow. | [optional] 

## Methods

### NewWorkflow

`func NewWorkflow(id PublishedWorkflowId, description string, ) *Workflow`

NewWorkflow instantiates a new Workflow object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkflowWithDefaults

`func NewWorkflowWithDefaults() *Workflow`

NewWorkflowWithDefaults instantiates a new Workflow object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Workflow) GetId() PublishedWorkflowId`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Workflow) GetIdOk() (*PublishedWorkflowId, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Workflow) SetId(v PublishedWorkflowId)`

SetId sets Id field to given value.


### GetDescription

`func (o *Workflow) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *Workflow) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *Workflow) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetTransitions

`func (o *Workflow) GetTransitions() []Transition`

GetTransitions returns the Transitions field if non-nil, zero value otherwise.

### GetTransitionsOk

`func (o *Workflow) GetTransitionsOk() (*[]Transition, bool)`

GetTransitionsOk returns a tuple with the Transitions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransitions

`func (o *Workflow) SetTransitions(v []Transition)`

SetTransitions sets Transitions field to given value.

### HasTransitions

`func (o *Workflow) HasTransitions() bool`

HasTransitions returns a boolean if a field has been set.

### GetStatuses

`func (o *Workflow) GetStatuses() []WorkflowStatus`

GetStatuses returns the Statuses field if non-nil, zero value otherwise.

### GetStatusesOk

`func (o *Workflow) GetStatusesOk() (*[]WorkflowStatus, bool)`

GetStatusesOk returns a tuple with the Statuses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatuses

`func (o *Workflow) SetStatuses(v []WorkflowStatus)`

SetStatuses sets Statuses field to given value.

### HasStatuses

`func (o *Workflow) HasStatuses() bool`

HasStatuses returns a boolean if a field has been set.

### GetIsDefault

`func (o *Workflow) GetIsDefault() bool`

GetIsDefault returns the IsDefault field if non-nil, zero value otherwise.

### GetIsDefaultOk

`func (o *Workflow) GetIsDefaultOk() (*bool, bool)`

GetIsDefaultOk returns a tuple with the IsDefault field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsDefault

`func (o *Workflow) SetIsDefault(v bool)`

SetIsDefault sets IsDefault field to given value.

### HasIsDefault

`func (o *Workflow) HasIsDefault() bool`

HasIsDefault returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# WorkflowIDs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of the workflow. | 
**EntityId** | Pointer to **string** | The entity ID of the workflow. | [optional] 

## Methods

### NewWorkflowIDs

`func NewWorkflowIDs(name string, ) *WorkflowIDs`

NewWorkflowIDs instantiates a new WorkflowIDs object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkflowIDsWithDefaults

`func NewWorkflowIDsWithDefaults() *WorkflowIDs`

NewWorkflowIDsWithDefaults instantiates a new WorkflowIDs object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *WorkflowIDs) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WorkflowIDs) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WorkflowIDs) SetName(v string)`

SetName sets Name field to given value.


### GetEntityId

`func (o *WorkflowIDs) GetEntityId() string`

GetEntityId returns the EntityId field if non-nil, zero value otherwise.

### GetEntityIdOk

`func (o *WorkflowIDs) GetEntityIdOk() (*string, bool)`

GetEntityIdOk returns a tuple with the EntityId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntityId

`func (o *WorkflowIDs) SetEntityId(v string)`

SetEntityId sets EntityId field to given value.

### HasEntityId

`func (o *WorkflowIDs) HasEntityId() bool`

HasEntityId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



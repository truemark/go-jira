# \WorkflowSchemesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateWorkflowScheme**](WorkflowSchemesApi.md#CreateWorkflowScheme) | **Post** /rest/api/3/workflowscheme | Create workflow scheme
[**DeleteDefaultWorkflow**](WorkflowSchemesApi.md#DeleteDefaultWorkflow) | **Delete** /rest/api/3/workflowscheme/{id}/default | Delete default workflow
[**DeleteWorkflowMapping**](WorkflowSchemesApi.md#DeleteWorkflowMapping) | **Delete** /rest/api/3/workflowscheme/{id}/workflow | Delete issue types for workflow in workflow scheme
[**DeleteWorkflowScheme**](WorkflowSchemesApi.md#DeleteWorkflowScheme) | **Delete** /rest/api/3/workflowscheme/{id} | Delete workflow scheme
[**DeleteWorkflowSchemeIssueType**](WorkflowSchemesApi.md#DeleteWorkflowSchemeIssueType) | **Delete** /rest/api/3/workflowscheme/{id}/issuetype/{issueType} | Delete workflow for issue type in workflow scheme
[**GetAllWorkflowSchemes**](WorkflowSchemesApi.md#GetAllWorkflowSchemes) | **Get** /rest/api/3/workflowscheme | Get all workflow schemes
[**GetDefaultWorkflow**](WorkflowSchemesApi.md#GetDefaultWorkflow) | **Get** /rest/api/3/workflowscheme/{id}/default | Get default workflow
[**GetWorkflow**](WorkflowSchemesApi.md#GetWorkflow) | **Get** /rest/api/3/workflowscheme/{id}/workflow | Get issue types for workflows in workflow scheme
[**GetWorkflowScheme**](WorkflowSchemesApi.md#GetWorkflowScheme) | **Get** /rest/api/3/workflowscheme/{id} | Get workflow scheme
[**GetWorkflowSchemeIssueType**](WorkflowSchemesApi.md#GetWorkflowSchemeIssueType) | **Get** /rest/api/3/workflowscheme/{id}/issuetype/{issueType} | Get workflow for issue type in workflow scheme
[**SetWorkflowSchemeIssueType**](WorkflowSchemesApi.md#SetWorkflowSchemeIssueType) | **Put** /rest/api/3/workflowscheme/{id}/issuetype/{issueType} | Set workflow for issue type in workflow scheme
[**UpdateDefaultWorkflow**](WorkflowSchemesApi.md#UpdateDefaultWorkflow) | **Put** /rest/api/3/workflowscheme/{id}/default | Update default workflow
[**UpdateWorkflowMapping**](WorkflowSchemesApi.md#UpdateWorkflowMapping) | **Put** /rest/api/3/workflowscheme/{id}/workflow | Set issue types for workflow in workflow scheme
[**UpdateWorkflowScheme**](WorkflowSchemesApi.md#UpdateWorkflowScheme) | **Put** /rest/api/3/workflowscheme/{id} | Update workflow scheme



## CreateWorkflowScheme

> WorkflowScheme CreateWorkflowScheme(ctx).WorkflowScheme(workflowScheme).Execute()

Create workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    workflowScheme := *openapiclient.NewWorkflowScheme() // WorkflowScheme | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.CreateWorkflowScheme(context.Background()).WorkflowScheme(workflowScheme).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.CreateWorkflowScheme``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateWorkflowScheme`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.CreateWorkflowScheme`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateWorkflowSchemeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowScheme** | [**WorkflowScheme**](WorkflowScheme.md) |  | 

### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDefaultWorkflow

> WorkflowScheme DeleteDefaultWorkflow(ctx, id).UpdateDraftIfNeeded(updateDraftIfNeeded).Execute()

Delete default workflow



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme.
    updateDraftIfNeeded := true // bool | Set to true to create or update the draft of a workflow scheme and delete the mapping from the draft, when the workflow scheme cannot be edited. Defaults to `false`. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.DeleteDefaultWorkflow(context.Background(), id).UpdateDraftIfNeeded(updateDraftIfNeeded).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.DeleteDefaultWorkflow``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteDefaultWorkflow`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.DeleteDefaultWorkflow`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteDefaultWorkflowRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **updateDraftIfNeeded** | **bool** | Set to true to create or update the draft of a workflow scheme and delete the mapping from the draft, when the workflow scheme cannot be edited. Defaults to &#x60;false&#x60;. | 

### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteWorkflowMapping

> DeleteWorkflowMapping(ctx, id).WorkflowName(workflowName).UpdateDraftIfNeeded(updateDraftIfNeeded).Execute()

Delete issue types for workflow in workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme.
    workflowName := "workflowName_example" // string | The name of the workflow.
    updateDraftIfNeeded := true // bool | Set to true to create or update the draft of a workflow scheme and delete the mapping from the draft, when the workflow scheme cannot be edited. Defaults to `false`. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.DeleteWorkflowMapping(context.Background(), id).WorkflowName(workflowName).UpdateDraftIfNeeded(updateDraftIfNeeded).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.DeleteWorkflowMapping``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteWorkflowMappingRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **workflowName** | **string** | The name of the workflow. | 
 **updateDraftIfNeeded** | **bool** | Set to true to create or update the draft of a workflow scheme and delete the mapping from the draft, when the workflow scheme cannot be edited. Defaults to &#x60;false&#x60;. | 

### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteWorkflowScheme

> DeleteWorkflowScheme(ctx, id).Execute()

Delete workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme. Find this ID by editing the desired workflow scheme in Jira. The ID is shown in the URL as `schemeId`. For example, *schemeId=10301*.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.DeleteWorkflowScheme(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.DeleteWorkflowScheme``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. Find this ID by editing the desired workflow scheme in Jira. The ID is shown in the URL as &#x60;schemeId&#x60;. For example, *schemeId&#x3D;10301*. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteWorkflowSchemeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteWorkflowSchemeIssueType

> WorkflowScheme DeleteWorkflowSchemeIssueType(ctx, id, issueType).UpdateDraftIfNeeded(updateDraftIfNeeded).Execute()

Delete workflow for issue type in workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme.
    issueType := "issueType_example" // string | The ID of the issue type.
    updateDraftIfNeeded := true // bool | Set to true to create or update the draft of a workflow scheme and update the mapping in the draft, when the workflow scheme cannot be edited. Defaults to `false`. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.DeleteWorkflowSchemeIssueType(context.Background(), id, issueType).UpdateDraftIfNeeded(updateDraftIfNeeded).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.DeleteWorkflowSchemeIssueType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteWorkflowSchemeIssueType`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.DeleteWorkflowSchemeIssueType`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 
**issueType** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteWorkflowSchemeIssueTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **updateDraftIfNeeded** | **bool** | Set to true to create or update the draft of a workflow scheme and update the mapping in the draft, when the workflow scheme cannot be edited. Defaults to &#x60;false&#x60;. | 

### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllWorkflowSchemes

> PageBeanWorkflowScheme GetAllWorkflowSchemes(ctx).StartAt(startAt).MaxResults(maxResults).Execute()

Get all workflow schemes



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.GetAllWorkflowSchemes(context.Background()).StartAt(startAt).MaxResults(maxResults).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.GetAllWorkflowSchemes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAllWorkflowSchemes`: PageBeanWorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.GetAllWorkflowSchemes`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAllWorkflowSchemesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]

### Return type

[**PageBeanWorkflowScheme**](PageBeanWorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDefaultWorkflow

> DefaultWorkflow GetDefaultWorkflow(ctx, id).ReturnDraftIfExists(returnDraftIfExists).Execute()

Get default workflow



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme.
    returnDraftIfExists := true // bool | Set to `true` to return the default workflow for the workflow scheme's draft rather than scheme itself. If the workflow scheme does not have a draft, then the default workflow for the workflow scheme is returned. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.GetDefaultWorkflow(context.Background(), id).ReturnDraftIfExists(returnDraftIfExists).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.GetDefaultWorkflow``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetDefaultWorkflow`: DefaultWorkflow
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.GetDefaultWorkflow`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetDefaultWorkflowRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **returnDraftIfExists** | **bool** | Set to &#x60;true&#x60; to return the default workflow for the workflow scheme&#39;s draft rather than scheme itself. If the workflow scheme does not have a draft, then the default workflow for the workflow scheme is returned. | [default to false]

### Return type

[**DefaultWorkflow**](DefaultWorkflow.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWorkflow

> IssueTypesWorkflowMapping GetWorkflow(ctx, id).WorkflowName(workflowName).ReturnDraftIfExists(returnDraftIfExists).Execute()

Get issue types for workflows in workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme.
    workflowName := "workflowName_example" // string | The name of a workflow in the scheme. Limits the results to the workflow-issue type mapping for the specified workflow. (optional)
    returnDraftIfExists := true // bool | Returns the mapping from the workflow scheme's draft rather than the workflow scheme, if set to true. If no draft exists, the mapping from the workflow scheme is returned. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.GetWorkflow(context.Background(), id).WorkflowName(workflowName).ReturnDraftIfExists(returnDraftIfExists).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.GetWorkflow``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetWorkflow`: IssueTypesWorkflowMapping
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.GetWorkflow`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetWorkflowRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **workflowName** | **string** | The name of a workflow in the scheme. Limits the results to the workflow-issue type mapping for the specified workflow. | 
 **returnDraftIfExists** | **bool** | Returns the mapping from the workflow scheme&#39;s draft rather than the workflow scheme, if set to true. If no draft exists, the mapping from the workflow scheme is returned. | [default to false]

### Return type

[**IssueTypesWorkflowMapping**](IssueTypesWorkflowMapping.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWorkflowScheme

> WorkflowScheme GetWorkflowScheme(ctx, id).ReturnDraftIfExists(returnDraftIfExists).Execute()

Get workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme. Find this ID by editing the desired workflow scheme in Jira. The ID is shown in the URL as `schemeId`. For example, *schemeId=10301*.
    returnDraftIfExists := true // bool | Returns the workflow scheme's draft rather than scheme itself, if set to true. If the workflow scheme does not have a draft, then the workflow scheme is returned. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.GetWorkflowScheme(context.Background(), id).ReturnDraftIfExists(returnDraftIfExists).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.GetWorkflowScheme``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetWorkflowScheme`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.GetWorkflowScheme`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. Find this ID by editing the desired workflow scheme in Jira. The ID is shown in the URL as &#x60;schemeId&#x60;. For example, *schemeId&#x3D;10301*. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetWorkflowSchemeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **returnDraftIfExists** | **bool** | Returns the workflow scheme&#39;s draft rather than scheme itself, if set to true. If the workflow scheme does not have a draft, then the workflow scheme is returned. | [default to false]

### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWorkflowSchemeIssueType

> IssueTypeWorkflowMapping GetWorkflowSchemeIssueType(ctx, id, issueType).ReturnDraftIfExists(returnDraftIfExists).Execute()

Get workflow for issue type in workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    id := int64(789) // int64 | The ID of the workflow scheme.
    issueType := "issueType_example" // string | The ID of the issue type.
    returnDraftIfExists := true // bool | Returns the mapping from the workflow scheme's draft rather than the workflow scheme, if set to true. If no draft exists, the mapping from the workflow scheme is returned. (optional) (default to false)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.GetWorkflowSchemeIssueType(context.Background(), id, issueType).ReturnDraftIfExists(returnDraftIfExists).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.GetWorkflowSchemeIssueType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetWorkflowSchemeIssueType`: IssueTypeWorkflowMapping
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.GetWorkflowSchemeIssueType`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 
**issueType** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetWorkflowSchemeIssueTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **returnDraftIfExists** | **bool** | Returns the mapping from the workflow scheme&#39;s draft rather than the workflow scheme, if set to true. If no draft exists, the mapping from the workflow scheme is returned. | [default to false]

### Return type

[**IssueTypeWorkflowMapping**](IssueTypeWorkflowMapping.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetWorkflowSchemeIssueType

> WorkflowScheme SetWorkflowSchemeIssueType(ctx, id, issueType).IssueTypeWorkflowMapping(issueTypeWorkflowMapping).Execute()

Set workflow for issue type in workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueTypeWorkflowMapping := *openapiclient.NewIssueTypeWorkflowMapping() // IssueTypeWorkflowMapping | The issue type-project mapping.
    id := int64(789) // int64 | The ID of the workflow scheme.
    issueType := "issueType_example" // string | The ID of the issue type.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.SetWorkflowSchemeIssueType(context.Background(), id, issueType).IssueTypeWorkflowMapping(issueTypeWorkflowMapping).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.SetWorkflowSchemeIssueType``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SetWorkflowSchemeIssueType`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.SetWorkflowSchemeIssueType`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 
**issueType** | **string** | The ID of the issue type. | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetWorkflowSchemeIssueTypeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueTypeWorkflowMapping** | [**IssueTypeWorkflowMapping**](IssueTypeWorkflowMapping.md) | The issue type-project mapping. | 



### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateDefaultWorkflow

> WorkflowScheme UpdateDefaultWorkflow(ctx, id).DefaultWorkflow(defaultWorkflow).Execute()

Update default workflow



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    defaultWorkflow := *openapiclient.NewDefaultWorkflow("Workflow_example") // DefaultWorkflow | The new default workflow.
    id := int64(789) // int64 | The ID of the workflow scheme.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.UpdateDefaultWorkflow(context.Background(), id).DefaultWorkflow(defaultWorkflow).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.UpdateDefaultWorkflow``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateDefaultWorkflow`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.UpdateDefaultWorkflow`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateDefaultWorkflowRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **defaultWorkflow** | [**DefaultWorkflow**](DefaultWorkflow.md) | The new default workflow. | 


### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateWorkflowMapping

> WorkflowScheme UpdateWorkflowMapping(ctx, id).IssueTypesWorkflowMapping(issueTypesWorkflowMapping).WorkflowName(workflowName).Execute()

Set issue types for workflow in workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    issueTypesWorkflowMapping := *openapiclient.NewIssueTypesWorkflowMapping() // IssueTypesWorkflowMapping | 
    id := int64(789) // int64 | The ID of the workflow scheme.
    workflowName := "workflowName_example" // string | The name of the workflow.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.UpdateWorkflowMapping(context.Background(), id).IssueTypesWorkflowMapping(issueTypesWorkflowMapping).WorkflowName(workflowName).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.UpdateWorkflowMapping``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateWorkflowMapping`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.UpdateWorkflowMapping`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateWorkflowMappingRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issueTypesWorkflowMapping** | [**IssueTypesWorkflowMapping**](IssueTypesWorkflowMapping.md) |  | 

 **workflowName** | **string** | The name of the workflow. | 

### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateWorkflowScheme

> WorkflowScheme UpdateWorkflowScheme(ctx, id).WorkflowScheme(workflowScheme).Execute()

Update workflow scheme



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    workflowScheme := *openapiclient.NewWorkflowScheme() // WorkflowScheme | 
    id := int64(789) // int64 | The ID of the workflow scheme. Find this ID by editing the desired workflow scheme in Jira. The ID is shown in the URL as `schemeId`. For example, *schemeId=10301*.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowSchemesApi.UpdateWorkflowScheme(context.Background(), id).WorkflowScheme(workflowScheme).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowSchemesApi.UpdateWorkflowScheme``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateWorkflowScheme`: WorkflowScheme
    fmt.Fprintf(os.Stdout, "Response from `WorkflowSchemesApi.UpdateWorkflowScheme`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **int64** | The ID of the workflow scheme. Find this ID by editing the desired workflow scheme in Jira. The ID is shown in the URL as &#x60;schemeId&#x60;. For example, *schemeId&#x3D;10301*. | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateWorkflowSchemeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowScheme** | [**WorkflowScheme**](WorkflowScheme.md) |  | 


### Return type

[**WorkflowScheme**](WorkflowScheme.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


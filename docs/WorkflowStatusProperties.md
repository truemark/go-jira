# WorkflowStatusProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IssueEditable** | **bool** | Whether issues are editable in this status. | 

## Methods

### NewWorkflowStatusProperties

`func NewWorkflowStatusProperties(issueEditable bool, ) *WorkflowStatusProperties`

NewWorkflowStatusProperties instantiates a new WorkflowStatusProperties object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkflowStatusPropertiesWithDefaults

`func NewWorkflowStatusPropertiesWithDefaults() *WorkflowStatusProperties`

NewWorkflowStatusPropertiesWithDefaults instantiates a new WorkflowStatusProperties object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIssueEditable

`func (o *WorkflowStatusProperties) GetIssueEditable() bool`

GetIssueEditable returns the IssueEditable field if non-nil, zero value otherwise.

### GetIssueEditableOk

`func (o *WorkflowStatusProperties) GetIssueEditableOk() (*bool, bool)`

GetIssueEditableOk returns a tuple with the IssueEditable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssueEditable

`func (o *WorkflowStatusProperties) SetIssueEditable(v bool)`

SetIssueEditable sets IssueEditable field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



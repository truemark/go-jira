# WorkflowTransitionRules

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WorkflowId** | [**WorkflowId**](WorkflowId.md) |  | 
**PostFunctions** | [**[]ConnectWorkflowTransitionRule**](ConnectWorkflowTransitionRule.md) | The list of post functions within the workflow. | 
**Conditions** | [**[]ConnectWorkflowTransitionRule**](ConnectWorkflowTransitionRule.md) | The list of conditions within the workflow. | 
**Validators** | [**[]ConnectWorkflowTransitionRule**](ConnectWorkflowTransitionRule.md) | The list of validators within the workflow. | 

## Methods

### NewWorkflowTransitionRules

`func NewWorkflowTransitionRules(workflowId WorkflowId, postFunctions []ConnectWorkflowTransitionRule, conditions []ConnectWorkflowTransitionRule, validators []ConnectWorkflowTransitionRule, ) *WorkflowTransitionRules`

NewWorkflowTransitionRules instantiates a new WorkflowTransitionRules object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkflowTransitionRulesWithDefaults

`func NewWorkflowTransitionRulesWithDefaults() *WorkflowTransitionRules`

NewWorkflowTransitionRulesWithDefaults instantiates a new WorkflowTransitionRules object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWorkflowId

`func (o *WorkflowTransitionRules) GetWorkflowId() WorkflowId`

GetWorkflowId returns the WorkflowId field if non-nil, zero value otherwise.

### GetWorkflowIdOk

`func (o *WorkflowTransitionRules) GetWorkflowIdOk() (*WorkflowId, bool)`

GetWorkflowIdOk returns a tuple with the WorkflowId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWorkflowId

`func (o *WorkflowTransitionRules) SetWorkflowId(v WorkflowId)`

SetWorkflowId sets WorkflowId field to given value.


### GetPostFunctions

`func (o *WorkflowTransitionRules) GetPostFunctions() []ConnectWorkflowTransitionRule`

GetPostFunctions returns the PostFunctions field if non-nil, zero value otherwise.

### GetPostFunctionsOk

`func (o *WorkflowTransitionRules) GetPostFunctionsOk() (*[]ConnectWorkflowTransitionRule, bool)`

GetPostFunctionsOk returns a tuple with the PostFunctions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPostFunctions

`func (o *WorkflowTransitionRules) SetPostFunctions(v []ConnectWorkflowTransitionRule)`

SetPostFunctions sets PostFunctions field to given value.


### GetConditions

`func (o *WorkflowTransitionRules) GetConditions() []ConnectWorkflowTransitionRule`

GetConditions returns the Conditions field if non-nil, zero value otherwise.

### GetConditionsOk

`func (o *WorkflowTransitionRules) GetConditionsOk() (*[]ConnectWorkflowTransitionRule, bool)`

GetConditionsOk returns a tuple with the Conditions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConditions

`func (o *WorkflowTransitionRules) SetConditions(v []ConnectWorkflowTransitionRule)`

SetConditions sets Conditions field to given value.


### GetValidators

`func (o *WorkflowTransitionRules) GetValidators() []ConnectWorkflowTransitionRule`

GetValidators returns the Validators field if non-nil, zero value otherwise.

### GetValidatorsOk

`func (o *WorkflowTransitionRules) GetValidatorsOk() (*[]ConnectWorkflowTransitionRule, bool)`

GetValidatorsOk returns a tuple with the Validators field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidators

`func (o *WorkflowTransitionRules) SetValidators(v []ConnectWorkflowTransitionRule)`

SetValidators sets Validators field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



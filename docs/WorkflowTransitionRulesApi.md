# \WorkflowTransitionRulesApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteWorkflowTransitionRuleConfigurations**](WorkflowTransitionRulesApi.md#DeleteWorkflowTransitionRuleConfigurations) | **Put** /rest/api/3/workflow/rule/config/delete | Delete workflow transition rule configurations
[**GetWorkflowTransitionRuleConfigurations**](WorkflowTransitionRulesApi.md#GetWorkflowTransitionRuleConfigurations) | **Get** /rest/api/3/workflow/rule/config | Get workflow transition rule configurations
[**UpdateWorkflowTransitionRuleConfigurations**](WorkflowTransitionRulesApi.md#UpdateWorkflowTransitionRuleConfigurations) | **Put** /rest/api/3/workflow/rule/config | Update workflow transition rule configurations



## DeleteWorkflowTransitionRuleConfigurations

> WorkflowTransitionRulesUpdateErrors DeleteWorkflowTransitionRuleConfigurations(ctx).WorkflowsWithTransitionRulesDetails(workflowsWithTransitionRulesDetails).Execute()

Delete workflow transition rule configurations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    workflowsWithTransitionRulesDetails := *openapiclient.NewWorkflowsWithTransitionRulesDetails([]openapiclient.WorkflowTransitionRulesDetails{*openapiclient.NewWorkflowTransitionRulesDetails(*openapiclient.NewWorkflowId("Name_example", false), []string{"WorkflowRuleIds_example"})}) // WorkflowsWithTransitionRulesDetails | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowTransitionRulesApi.DeleteWorkflowTransitionRuleConfigurations(context.Background()).WorkflowsWithTransitionRulesDetails(workflowsWithTransitionRulesDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowTransitionRulesApi.DeleteWorkflowTransitionRuleConfigurations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteWorkflowTransitionRuleConfigurations`: WorkflowTransitionRulesUpdateErrors
    fmt.Fprintf(os.Stdout, "Response from `WorkflowTransitionRulesApi.DeleteWorkflowTransitionRuleConfigurations`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiDeleteWorkflowTransitionRuleConfigurationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowsWithTransitionRulesDetails** | [**WorkflowsWithTransitionRulesDetails**](WorkflowsWithTransitionRulesDetails.md) |  | 

### Return type

[**WorkflowTransitionRulesUpdateErrors**](WorkflowTransitionRulesUpdateErrors.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWorkflowTransitionRuleConfigurations

> PageBeanWorkflowTransitionRules GetWorkflowTransitionRuleConfigurations(ctx).Types(types).StartAt(startAt).MaxResults(maxResults).Keys(keys).Expand(expand).Execute()

Get workflow transition rule configurations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    types := []string{"Types_example"} // []string | The types of the transition rules to return.
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 10)
    keys := []string{"Inner_example"} // []string | The transition rule class keys, as defined in the Connect app descriptor, of the transition rules to return. (optional)
    expand := "expand_example" // string | Use [expand](#expansion) to include additional information in the response. This parameter accepts `transition`, which, for each rule, returns information about the transition the rule is assigned to. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowTransitionRulesApi.GetWorkflowTransitionRuleConfigurations(context.Background()).Types(types).StartAt(startAt).MaxResults(maxResults).Keys(keys).Expand(expand).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowTransitionRulesApi.GetWorkflowTransitionRuleConfigurations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetWorkflowTransitionRuleConfigurations`: PageBeanWorkflowTransitionRules
    fmt.Fprintf(os.Stdout, "Response from `WorkflowTransitionRulesApi.GetWorkflowTransitionRuleConfigurations`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetWorkflowTransitionRuleConfigurationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **types** | **[]string** | The types of the transition rules to return. | 
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 10]
 **keys** | **[]string** | The transition rule class keys, as defined in the Connect app descriptor, of the transition rules to return. | 
 **expand** | **string** | Use [expand](#expansion) to include additional information in the response. This parameter accepts &#x60;transition&#x60;, which, for each rule, returns information about the transition the rule is assigned to. | 

### Return type

[**PageBeanWorkflowTransitionRules**](PageBeanWorkflowTransitionRules.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateWorkflowTransitionRuleConfigurations

> WorkflowTransitionRulesUpdateErrors UpdateWorkflowTransitionRuleConfigurations(ctx).WorkflowTransitionRulesUpdate(workflowTransitionRulesUpdate).Execute()

Update workflow transition rule configurations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    workflowTransitionRulesUpdate := *openapiclient.NewWorkflowTransitionRulesUpdate([]openapiclient.WorkflowTransitionRules{*openapiclient.NewWorkflowTransitionRules(*openapiclient.NewWorkflowId("Name_example", false), []openapiclient.ConnectWorkflowTransitionRule{*openapiclient.NewConnectWorkflowTransitionRule("Id_example", "Key_example", *openapiclient.NewRuleConfiguration("Value_example"))}, []openapiclient.ConnectWorkflowTransitionRule{*openapiclient.NewConnectWorkflowTransitionRule("Id_example", "Key_example", *openapiclient.NewRuleConfiguration("Value_example"))}, []openapiclient.ConnectWorkflowTransitionRule{})}) // WorkflowTransitionRulesUpdate | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowTransitionRulesApi.UpdateWorkflowTransitionRuleConfigurations(context.Background()).WorkflowTransitionRulesUpdate(workflowTransitionRulesUpdate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowTransitionRulesApi.UpdateWorkflowTransitionRuleConfigurations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateWorkflowTransitionRuleConfigurations`: WorkflowTransitionRulesUpdateErrors
    fmt.Fprintf(os.Stdout, "Response from `WorkflowTransitionRulesApi.UpdateWorkflowTransitionRuleConfigurations`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUpdateWorkflowTransitionRuleConfigurationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowTransitionRulesUpdate** | [**WorkflowTransitionRulesUpdate**](WorkflowTransitionRulesUpdate.md) |  | 

### Return type

[**WorkflowTransitionRulesUpdateErrors**](WorkflowTransitionRulesUpdateErrors.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


# WorkflowTransitionRulesUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Workflows** | [**[]WorkflowTransitionRules**](WorkflowTransitionRules.md) | The list of workflows with transition rules to update. | 

## Methods

### NewWorkflowTransitionRulesUpdate

`func NewWorkflowTransitionRulesUpdate(workflows []WorkflowTransitionRules, ) *WorkflowTransitionRulesUpdate`

NewWorkflowTransitionRulesUpdate instantiates a new WorkflowTransitionRulesUpdate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkflowTransitionRulesUpdateWithDefaults

`func NewWorkflowTransitionRulesUpdateWithDefaults() *WorkflowTransitionRulesUpdate`

NewWorkflowTransitionRulesUpdateWithDefaults instantiates a new WorkflowTransitionRulesUpdate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWorkflows

`func (o *WorkflowTransitionRulesUpdate) GetWorkflows() []WorkflowTransitionRules`

GetWorkflows returns the Workflows field if non-nil, zero value otherwise.

### GetWorkflowsOk

`func (o *WorkflowTransitionRulesUpdate) GetWorkflowsOk() (*[]WorkflowTransitionRules, bool)`

GetWorkflowsOk returns a tuple with the Workflows field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWorkflows

`func (o *WorkflowTransitionRulesUpdate) SetWorkflows(v []WorkflowTransitionRules)`

SetWorkflows sets Workflows field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



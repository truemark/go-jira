# WorkflowTransitionRulesUpdateErrors

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UpdateResults** | [**[]WorkflowTransitionRulesUpdateErrorDetails**](WorkflowTransitionRulesUpdateErrorDetails.md) | A list of workflows. | 

## Methods

### NewWorkflowTransitionRulesUpdateErrors

`func NewWorkflowTransitionRulesUpdateErrors(updateResults []WorkflowTransitionRulesUpdateErrorDetails, ) *WorkflowTransitionRulesUpdateErrors`

NewWorkflowTransitionRulesUpdateErrors instantiates a new WorkflowTransitionRulesUpdateErrors object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkflowTransitionRulesUpdateErrorsWithDefaults

`func NewWorkflowTransitionRulesUpdateErrorsWithDefaults() *WorkflowTransitionRulesUpdateErrors`

NewWorkflowTransitionRulesUpdateErrorsWithDefaults instantiates a new WorkflowTransitionRulesUpdateErrors object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUpdateResults

`func (o *WorkflowTransitionRulesUpdateErrors) GetUpdateResults() []WorkflowTransitionRulesUpdateErrorDetails`

GetUpdateResults returns the UpdateResults field if non-nil, zero value otherwise.

### GetUpdateResultsOk

`func (o *WorkflowTransitionRulesUpdateErrors) GetUpdateResultsOk() (*[]WorkflowTransitionRulesUpdateErrorDetails, bool)`

GetUpdateResultsOk returns a tuple with the UpdateResults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdateResults

`func (o *WorkflowTransitionRulesUpdateErrors) SetUpdateResults(v []WorkflowTransitionRulesUpdateErrorDetails)`

SetUpdateResults sets UpdateResults field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



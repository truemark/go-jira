# \WorkflowsApi

All URIs are relative to *https://your-domain.atlassian.net*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateWorkflow**](WorkflowsApi.md#CreateWorkflow) | **Post** /rest/api/3/workflow | Create workflow
[**DeleteInactiveWorkflow**](WorkflowsApi.md#DeleteInactiveWorkflow) | **Delete** /rest/api/3/workflow/{entityId} | Delete inactive workflow
[**GetAllWorkflows**](WorkflowsApi.md#GetAllWorkflows) | **Get** /rest/api/3/workflow | Get all workflows
[**GetWorkflowsPaginated**](WorkflowsApi.md#GetWorkflowsPaginated) | **Get** /rest/api/3/workflow/search | Get workflows paginated



## CreateWorkflow

> WorkflowIDs CreateWorkflow(ctx).CreateWorkflowDetails(createWorkflowDetails).Execute()

Create workflow



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    createWorkflowDetails := *openapiclient.NewCreateWorkflowDetails("Name_example", []openapiclient.CreateWorkflowTransitionDetails{*openapiclient.NewCreateWorkflowTransitionDetails("Name_example", "To_example", "Type_example")}, []openapiclient.CrateWorkflowStatusDetails{*openapiclient.NewCrateWorkflowStatusDetails("Id_example")}) // CreateWorkflowDetails | The workflow details.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowsApi.CreateWorkflow(context.Background()).CreateWorkflowDetails(createWorkflowDetails).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowsApi.CreateWorkflow``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateWorkflow`: WorkflowIDs
    fmt.Fprintf(os.Stdout, "Response from `WorkflowsApi.CreateWorkflow`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateWorkflowRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createWorkflowDetails** | [**CreateWorkflowDetails**](CreateWorkflowDetails.md) | The workflow details. | 

### Return type

[**WorkflowIDs**](WorkflowIDs.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteInactiveWorkflow

> DeleteInactiveWorkflow(ctx, entityId).Execute()

Delete inactive workflow



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    entityId := "entityId_example" // string | The entity ID of the workflow.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowsApi.DeleteInactiveWorkflow(context.Background(), entityId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowsApi.DeleteInactiveWorkflow``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**entityId** | **string** | The entity ID of the workflow. | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteInactiveWorkflowRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllWorkflows

> []DeprecatedWorkflow GetAllWorkflows(ctx).WorkflowName(workflowName).Execute()

Get all workflows



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    workflowName := "workflowName_example" // string | The name of the workflow to be returned. Only one workflow can be specified. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowsApi.GetAllWorkflows(context.Background()).WorkflowName(workflowName).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowsApi.GetAllWorkflows``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAllWorkflows`: []DeprecatedWorkflow
    fmt.Fprintf(os.Stdout, "Response from `WorkflowsApi.GetAllWorkflows`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetAllWorkflowsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowName** | **string** | The name of the workflow to be returned. Only one workflow can be specified. | 

### Return type

[**[]DeprecatedWorkflow**](DeprecatedWorkflow.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWorkflowsPaginated

> PageBeanWorkflow GetWorkflowsPaginated(ctx).StartAt(startAt).MaxResults(maxResults).WorkflowName(workflowName).Expand(expand).Execute()

Get workflows paginated



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    startAt := int64(789) // int64 | The index of the first item to return in a page of results (page offset). (optional) (default to 0)
    maxResults := int32(56) // int32 | The maximum number of items to return per page. (optional) (default to 50)
    workflowName := []string{"Inner_example"} // []string | The name of a workflow to return. (optional)
    expand := "expand_example" // string | Use [expand](#expansion) to include additional information in the response. This parameter accepts a comma-separated list. Expand options include:   *  `transitions` For each workflow, returns information about the transitions inside the workflow.  *  `transitions.rules` For each workflow transition, returns information about its rules. Transitions are included automatically if this expand is requested.  *  `statuses` For each workflow, returns information about the statuses inside the workflow.  *  `statuses.properties` For each workflow status, returns information about its properties. Statuses are included automatically if this expand is requested.  *  `default` For each workflow, returns information about whether this is the default workflow. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WorkflowsApi.GetWorkflowsPaginated(context.Background()).StartAt(startAt).MaxResults(maxResults).WorkflowName(workflowName).Expand(expand).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WorkflowsApi.GetWorkflowsPaginated``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetWorkflowsPaginated`: PageBeanWorkflow
    fmt.Fprintf(os.Stdout, "Response from `WorkflowsApi.GetWorkflowsPaginated`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetWorkflowsPaginatedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startAt** | **int64** | The index of the first item to return in a page of results (page offset). | [default to 0]
 **maxResults** | **int32** | The maximum number of items to return per page. | [default to 50]
 **workflowName** | **[]string** | The name of a workflow to return. | 
 **expand** | **string** | Use [expand](#expansion) to include additional information in the response. This parameter accepts a comma-separated list. Expand options include:   *  &#x60;transitions&#x60; For each workflow, returns information about the transitions inside the workflow.  *  &#x60;transitions.rules&#x60; For each workflow transition, returns information about its rules. Transitions are included automatically if this expand is requested.  *  &#x60;statuses&#x60; For each workflow, returns information about the statuses inside the workflow.  *  &#x60;statuses.properties&#x60; For each workflow status, returns information about its properties. Statuses are included automatically if this expand is requested.  *  &#x60;default&#x60; For each workflow, returns information about whether this is the default workflow. | 

### Return type

[**PageBeanWorkflow**](PageBeanWorkflow.md)

### Authorization

[OAuth2](../README.md#OAuth2), [basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


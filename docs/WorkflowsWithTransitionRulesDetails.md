# WorkflowsWithTransitionRulesDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Workflows** | [**[]WorkflowTransitionRulesDetails**](WorkflowTransitionRulesDetails.md) | The list of workflows with transition rules to delete. | 

## Methods

### NewWorkflowsWithTransitionRulesDetails

`func NewWorkflowsWithTransitionRulesDetails(workflows []WorkflowTransitionRulesDetails, ) *WorkflowsWithTransitionRulesDetails`

NewWorkflowsWithTransitionRulesDetails instantiates a new WorkflowsWithTransitionRulesDetails object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkflowsWithTransitionRulesDetailsWithDefaults

`func NewWorkflowsWithTransitionRulesDetailsWithDefaults() *WorkflowsWithTransitionRulesDetails`

NewWorkflowsWithTransitionRulesDetailsWithDefaults instantiates a new WorkflowsWithTransitionRulesDetails object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWorkflows

`func (o *WorkflowsWithTransitionRulesDetails) GetWorkflows() []WorkflowTransitionRulesDetails`

GetWorkflows returns the Workflows field if non-nil, zero value otherwise.

### GetWorkflowsOk

`func (o *WorkflowsWithTransitionRulesDetails) GetWorkflowsOk() (*[]WorkflowTransitionRulesDetails, bool)`

GetWorkflowsOk returns a tuple with the Workflows field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWorkflows

`func (o *WorkflowsWithTransitionRulesDetails) SetWorkflows(v []WorkflowTransitionRulesDetails)`

SetWorkflows sets Workflows field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// AutoCompleteSuggestion A field auto-complete suggestion.
type AutoCompleteSuggestion struct {
	// The value of a suggested item.
	Value *string `json:"value,omitempty"`
	// The display name of a suggested item. If `fieldValue` or `predicateValue` are provided, the matching text is highlighted with the HTML bold tag.
	DisplayName *string `json:"displayName,omitempty"`
}

// NewAutoCompleteSuggestion instantiates a new AutoCompleteSuggestion object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAutoCompleteSuggestion() *AutoCompleteSuggestion {
	this := AutoCompleteSuggestion{}
	return &this
}

// NewAutoCompleteSuggestionWithDefaults instantiates a new AutoCompleteSuggestion object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAutoCompleteSuggestionWithDefaults() *AutoCompleteSuggestion {
	this := AutoCompleteSuggestion{}
	return &this
}

// GetValue returns the Value field value if set, zero value otherwise.
func (o *AutoCompleteSuggestion) GetValue() string {
	if o == nil || o.Value == nil {
		var ret string
		return ret
	}
	return *o.Value
}

// GetValueOk returns a tuple with the Value field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AutoCompleteSuggestion) GetValueOk() (*string, bool) {
	if o == nil || o.Value == nil {
		return nil, false
	}
	return o.Value, true
}

// HasValue returns a boolean if a field has been set.
func (o *AutoCompleteSuggestion) HasValue() bool {
	if o != nil && o.Value != nil {
		return true
	}

	return false
}

// SetValue gets a reference to the given string and assigns it to the Value field.
func (o *AutoCompleteSuggestion) SetValue(v string) {
	o.Value = &v
}

// GetDisplayName returns the DisplayName field value if set, zero value otherwise.
func (o *AutoCompleteSuggestion) GetDisplayName() string {
	if o == nil || o.DisplayName == nil {
		var ret string
		return ret
	}
	return *o.DisplayName
}

// GetDisplayNameOk returns a tuple with the DisplayName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AutoCompleteSuggestion) GetDisplayNameOk() (*string, bool) {
	if o == nil || o.DisplayName == nil {
		return nil, false
	}
	return o.DisplayName, true
}

// HasDisplayName returns a boolean if a field has been set.
func (o *AutoCompleteSuggestion) HasDisplayName() bool {
	if o != nil && o.DisplayName != nil {
		return true
	}

	return false
}

// SetDisplayName gets a reference to the given string and assigns it to the DisplayName field.
func (o *AutoCompleteSuggestion) SetDisplayName(v string) {
	o.DisplayName = &v
}

func (o AutoCompleteSuggestion) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Value != nil {
		toSerialize["value"] = o.Value
	}
	if o.DisplayName != nil {
		toSerialize["displayName"] = o.DisplayName
	}
	return json.Marshal(toSerialize)
}

type NullableAutoCompleteSuggestion struct {
	value *AutoCompleteSuggestion
	isSet bool
}

func (v NullableAutoCompleteSuggestion) Get() *AutoCompleteSuggestion {
	return v.value
}

func (v *NullableAutoCompleteSuggestion) Set(val *AutoCompleteSuggestion) {
	v.value = val
	v.isSet = true
}

func (v NullableAutoCompleteSuggestion) IsSet() bool {
	return v.isSet
}

func (v *NullableAutoCompleteSuggestion) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAutoCompleteSuggestion(val *AutoCompleteSuggestion) *NullableAutoCompleteSuggestion {
	return &NullableAutoCompleteSuggestion{value: val, isSet: true}
}

func (v NullableAutoCompleteSuggestion) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAutoCompleteSuggestion) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



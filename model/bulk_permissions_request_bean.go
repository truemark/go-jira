/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// BulkPermissionsRequestBean Details of global permissions to look up and project permissions with associated projects and issues to look up.
type BulkPermissionsRequestBean struct {
	// Project permissions with associated projects and issues to look up.
	ProjectPermissions *[]BulkProjectPermissions `json:"projectPermissions,omitempty"`
	// Global permissions to look up.
	GlobalPermissions *[]string `json:"globalPermissions,omitempty"`
	// The account ID of a user.
	AccountId *string `json:"accountId,omitempty"`
}

// NewBulkPermissionsRequestBean instantiates a new BulkPermissionsRequestBean object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewBulkPermissionsRequestBean() *BulkPermissionsRequestBean {
	this := BulkPermissionsRequestBean{}
	return &this
}

// NewBulkPermissionsRequestBeanWithDefaults instantiates a new BulkPermissionsRequestBean object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewBulkPermissionsRequestBeanWithDefaults() *BulkPermissionsRequestBean {
	this := BulkPermissionsRequestBean{}
	return &this
}

// GetProjectPermissions returns the ProjectPermissions field value if set, zero value otherwise.
func (o *BulkPermissionsRequestBean) GetProjectPermissions() []BulkProjectPermissions {
	if o == nil || o.ProjectPermissions == nil {
		var ret []BulkProjectPermissions
		return ret
	}
	return *o.ProjectPermissions
}

// GetProjectPermissionsOk returns a tuple with the ProjectPermissions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *BulkPermissionsRequestBean) GetProjectPermissionsOk() (*[]BulkProjectPermissions, bool) {
	if o == nil || o.ProjectPermissions == nil {
		return nil, false
	}
	return o.ProjectPermissions, true
}

// HasProjectPermissions returns a boolean if a field has been set.
func (o *BulkPermissionsRequestBean) HasProjectPermissions() bool {
	if o != nil && o.ProjectPermissions != nil {
		return true
	}

	return false
}

// SetProjectPermissions gets a reference to the given []BulkProjectPermissions and assigns it to the ProjectPermissions field.
func (o *BulkPermissionsRequestBean) SetProjectPermissions(v []BulkProjectPermissions) {
	o.ProjectPermissions = &v
}

// GetGlobalPermissions returns the GlobalPermissions field value if set, zero value otherwise.
func (o *BulkPermissionsRequestBean) GetGlobalPermissions() []string {
	if o == nil || o.GlobalPermissions == nil {
		var ret []string
		return ret
	}
	return *o.GlobalPermissions
}

// GetGlobalPermissionsOk returns a tuple with the GlobalPermissions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *BulkPermissionsRequestBean) GetGlobalPermissionsOk() (*[]string, bool) {
	if o == nil || o.GlobalPermissions == nil {
		return nil, false
	}
	return o.GlobalPermissions, true
}

// HasGlobalPermissions returns a boolean if a field has been set.
func (o *BulkPermissionsRequestBean) HasGlobalPermissions() bool {
	if o != nil && o.GlobalPermissions != nil {
		return true
	}

	return false
}

// SetGlobalPermissions gets a reference to the given []string and assigns it to the GlobalPermissions field.
func (o *BulkPermissionsRequestBean) SetGlobalPermissions(v []string) {
	o.GlobalPermissions = &v
}

// GetAccountId returns the AccountId field value if set, zero value otherwise.
func (o *BulkPermissionsRequestBean) GetAccountId() string {
	if o == nil || o.AccountId == nil {
		var ret string
		return ret
	}
	return *o.AccountId
}

// GetAccountIdOk returns a tuple with the AccountId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *BulkPermissionsRequestBean) GetAccountIdOk() (*string, bool) {
	if o == nil || o.AccountId == nil {
		return nil, false
	}
	return o.AccountId, true
}

// HasAccountId returns a boolean if a field has been set.
func (o *BulkPermissionsRequestBean) HasAccountId() bool {
	if o != nil && o.AccountId != nil {
		return true
	}

	return false
}

// SetAccountId gets a reference to the given string and assigns it to the AccountId field.
func (o *BulkPermissionsRequestBean) SetAccountId(v string) {
	o.AccountId = &v
}

func (o BulkPermissionsRequestBean) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.ProjectPermissions != nil {
		toSerialize["projectPermissions"] = o.ProjectPermissions
	}
	if o.GlobalPermissions != nil {
		toSerialize["globalPermissions"] = o.GlobalPermissions
	}
	if o.AccountId != nil {
		toSerialize["accountId"] = o.AccountId
	}
	return json.Marshal(toSerialize)
}

type NullableBulkPermissionsRequestBean struct {
	value *BulkPermissionsRequestBean
	isSet bool
}

func (v NullableBulkPermissionsRequestBean) Get() *BulkPermissionsRequestBean {
	return v.value
}

func (v *NullableBulkPermissionsRequestBean) Set(val *BulkPermissionsRequestBean) {
	v.value = val
	v.isSet = true
}

func (v NullableBulkPermissionsRequestBean) IsSet() bool {
	return v.isSet
}

func (v *NullableBulkPermissionsRequestBean) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableBulkPermissionsRequestBean(val *BulkPermissionsRequestBean) *NullableBulkPermissionsRequestBean {
	return &NullableBulkPermissionsRequestBean{value: val, isSet: true}
}

func (v NullableBulkPermissionsRequestBean) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableBulkPermissionsRequestBean) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



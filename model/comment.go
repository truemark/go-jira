/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
	"time"
)

// Comment A comment.
type Comment struct {
	// The URL of the comment.
	Self *string `json:"self,omitempty"`
	// The ID of the comment.
	Id *string `json:"id,omitempty"`
	// The ID of the user who created the comment.
	Author *UserDetails `json:"author,omitempty"`
	// The comment text in [Atlassian Document Format](https://developer.atlassian.com/cloud/jira/platform/apis/document/structure/).
	Body interface{} `json:"body,omitempty"`
	// The rendered version of the comment.
	RenderedBody *string `json:"renderedBody,omitempty"`
	// The ID of the user who updated the comment last.
	UpdateAuthor *UserDetails `json:"updateAuthor,omitempty"`
	// The date and time at which the comment was created.
	Created *time.Time `json:"created,omitempty"`
	// The date and time at which the comment was updated last.
	Updated *time.Time `json:"updated,omitempty"`
	// The group or role to which this comment is visible. Optional on create and update.
	Visibility *Visibility `json:"visibility,omitempty"`
	// Whether the comment is visible in Jira Service Desk. Defaults to true when comments are created in the Jira Cloud Platform. This includes when the site doesn't use Jira Service Desk or the project isn't a Jira Service Desk project and, therefore, there is no Jira Service Desk for the issue to be visible on. To create a comment with its visibility in Jira Service Desk set to false, use the Jira Service Desk REST API [Create request comment](https://developer.atlassian.com/cloud/jira/service-desk/rest/#api-rest-servicedeskapi-request-issueIdOrKey-comment-post) operation.
	JsdPublic *bool `json:"jsdPublic,omitempty"`
	// A list of comment properties. Optional on create and update.
	Properties *[]EntityProperty `json:"properties,omitempty"`
	AdditionalProperties map[string]interface{}
}

type _Comment Comment

// NewComment instantiates a new Comment object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewComment() *Comment {
	this := Comment{}
	return &this
}

// NewCommentWithDefaults instantiates a new Comment object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCommentWithDefaults() *Comment {
	this := Comment{}
	return &this
}

// GetSelf returns the Self field value if set, zero value otherwise.
func (o *Comment) GetSelf() string {
	if o == nil || o.Self == nil {
		var ret string
		return ret
	}
	return *o.Self
}

// GetSelfOk returns a tuple with the Self field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetSelfOk() (*string, bool) {
	if o == nil || o.Self == nil {
		return nil, false
	}
	return o.Self, true
}

// HasSelf returns a boolean if a field has been set.
func (o *Comment) HasSelf() bool {
	if o != nil && o.Self != nil {
		return true
	}

	return false
}

// SetSelf gets a reference to the given string and assigns it to the Self field.
func (o *Comment) SetSelf(v string) {
	o.Self = &v
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *Comment) GetId() string {
	if o == nil || o.Id == nil {
		var ret string
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetIdOk() (*string, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *Comment) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given string and assigns it to the Id field.
func (o *Comment) SetId(v string) {
	o.Id = &v
}

// GetAuthor returns the Author field value if set, zero value otherwise.
func (o *Comment) GetAuthor() UserDetails {
	if o == nil || o.Author == nil {
		var ret UserDetails
		return ret
	}
	return *o.Author
}

// GetAuthorOk returns a tuple with the Author field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetAuthorOk() (*UserDetails, bool) {
	if o == nil || o.Author == nil {
		return nil, false
	}
	return o.Author, true
}

// HasAuthor returns a boolean if a field has been set.
func (o *Comment) HasAuthor() bool {
	if o != nil && o.Author != nil {
		return true
	}

	return false
}

// SetAuthor gets a reference to the given UserDetails and assigns it to the Author field.
func (o *Comment) SetAuthor(v UserDetails) {
	o.Author = &v
}

// GetBody returns the Body field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *Comment) GetBody() interface{} {
	if o == nil  {
		var ret interface{}
		return ret
	}
	return o.Body
}

// GetBodyOk returns a tuple with the Body field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *Comment) GetBodyOk() (*interface{}, bool) {
	if o == nil || o.Body == nil {
		return nil, false
	}
	return &o.Body, true
}

// HasBody returns a boolean if a field has been set.
func (o *Comment) HasBody() bool {
	if o != nil && o.Body != nil {
		return true
	}

	return false
}

// SetBody gets a reference to the given interface{} and assigns it to the Body field.
func (o *Comment) SetBody(v interface{}) {
	o.Body = v
}

// GetRenderedBody returns the RenderedBody field value if set, zero value otherwise.
func (o *Comment) GetRenderedBody() string {
	if o == nil || o.RenderedBody == nil {
		var ret string
		return ret
	}
	return *o.RenderedBody
}

// GetRenderedBodyOk returns a tuple with the RenderedBody field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetRenderedBodyOk() (*string, bool) {
	if o == nil || o.RenderedBody == nil {
		return nil, false
	}
	return o.RenderedBody, true
}

// HasRenderedBody returns a boolean if a field has been set.
func (o *Comment) HasRenderedBody() bool {
	if o != nil && o.RenderedBody != nil {
		return true
	}

	return false
}

// SetRenderedBody gets a reference to the given string and assigns it to the RenderedBody field.
func (o *Comment) SetRenderedBody(v string) {
	o.RenderedBody = &v
}

// GetUpdateAuthor returns the UpdateAuthor field value if set, zero value otherwise.
func (o *Comment) GetUpdateAuthor() UserDetails {
	if o == nil || o.UpdateAuthor == nil {
		var ret UserDetails
		return ret
	}
	return *o.UpdateAuthor
}

// GetUpdateAuthorOk returns a tuple with the UpdateAuthor field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetUpdateAuthorOk() (*UserDetails, bool) {
	if o == nil || o.UpdateAuthor == nil {
		return nil, false
	}
	return o.UpdateAuthor, true
}

// HasUpdateAuthor returns a boolean if a field has been set.
func (o *Comment) HasUpdateAuthor() bool {
	if o != nil && o.UpdateAuthor != nil {
		return true
	}

	return false
}

// SetUpdateAuthor gets a reference to the given UserDetails and assigns it to the UpdateAuthor field.
func (o *Comment) SetUpdateAuthor(v UserDetails) {
	o.UpdateAuthor = &v
}

// GetCreated returns the Created field value if set, zero value otherwise.
func (o *Comment) GetCreated() time.Time {
	if o == nil || o.Created == nil {
		var ret time.Time
		return ret
	}
	return *o.Created
}

// GetCreatedOk returns a tuple with the Created field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetCreatedOk() (*time.Time, bool) {
	if o == nil || o.Created == nil {
		return nil, false
	}
	return o.Created, true
}

// HasCreated returns a boolean if a field has been set.
func (o *Comment) HasCreated() bool {
	if o != nil && o.Created != nil {
		return true
	}

	return false
}

// SetCreated gets a reference to the given time.Time and assigns it to the Created field.
func (o *Comment) SetCreated(v time.Time) {
	o.Created = &v
}

// GetUpdated returns the Updated field value if set, zero value otherwise.
func (o *Comment) GetUpdated() time.Time {
	if o == nil || o.Updated == nil {
		var ret time.Time
		return ret
	}
	return *o.Updated
}

// GetUpdatedOk returns a tuple with the Updated field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetUpdatedOk() (*time.Time, bool) {
	if o == nil || o.Updated == nil {
		return nil, false
	}
	return o.Updated, true
}

// HasUpdated returns a boolean if a field has been set.
func (o *Comment) HasUpdated() bool {
	if o != nil && o.Updated != nil {
		return true
	}

	return false
}

// SetUpdated gets a reference to the given time.Time and assigns it to the Updated field.
func (o *Comment) SetUpdated(v time.Time) {
	o.Updated = &v
}

// GetVisibility returns the Visibility field value if set, zero value otherwise.
func (o *Comment) GetVisibility() Visibility {
	if o == nil || o.Visibility == nil {
		var ret Visibility
		return ret
	}
	return *o.Visibility
}

// GetVisibilityOk returns a tuple with the Visibility field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetVisibilityOk() (*Visibility, bool) {
	if o == nil || o.Visibility == nil {
		return nil, false
	}
	return o.Visibility, true
}

// HasVisibility returns a boolean if a field has been set.
func (o *Comment) HasVisibility() bool {
	if o != nil && o.Visibility != nil {
		return true
	}

	return false
}

// SetVisibility gets a reference to the given Visibility and assigns it to the Visibility field.
func (o *Comment) SetVisibility(v Visibility) {
	o.Visibility = &v
}

// GetJsdPublic returns the JsdPublic field value if set, zero value otherwise.
func (o *Comment) GetJsdPublic() bool {
	if o == nil || o.JsdPublic == nil {
		var ret bool
		return ret
	}
	return *o.JsdPublic
}

// GetJsdPublicOk returns a tuple with the JsdPublic field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetJsdPublicOk() (*bool, bool) {
	if o == nil || o.JsdPublic == nil {
		return nil, false
	}
	return o.JsdPublic, true
}

// HasJsdPublic returns a boolean if a field has been set.
func (o *Comment) HasJsdPublic() bool {
	if o != nil && o.JsdPublic != nil {
		return true
	}

	return false
}

// SetJsdPublic gets a reference to the given bool and assigns it to the JsdPublic field.
func (o *Comment) SetJsdPublic(v bool) {
	o.JsdPublic = &v
}

// GetProperties returns the Properties field value if set, zero value otherwise.
func (o *Comment) GetProperties() []EntityProperty {
	if o == nil || o.Properties == nil {
		var ret []EntityProperty
		return ret
	}
	return *o.Properties
}

// GetPropertiesOk returns a tuple with the Properties field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Comment) GetPropertiesOk() (*[]EntityProperty, bool) {
	if o == nil || o.Properties == nil {
		return nil, false
	}
	return o.Properties, true
}

// HasProperties returns a boolean if a field has been set.
func (o *Comment) HasProperties() bool {
	if o != nil && o.Properties != nil {
		return true
	}

	return false
}

// SetProperties gets a reference to the given []EntityProperty and assigns it to the Properties field.
func (o *Comment) SetProperties(v []EntityProperty) {
	o.Properties = &v
}

func (o Comment) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Self != nil {
		toSerialize["self"] = o.Self
	}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.Author != nil {
		toSerialize["author"] = o.Author
	}
	if o.Body != nil {
		toSerialize["body"] = o.Body
	}
	if o.RenderedBody != nil {
		toSerialize["renderedBody"] = o.RenderedBody
	}
	if o.UpdateAuthor != nil {
		toSerialize["updateAuthor"] = o.UpdateAuthor
	}
	if o.Created != nil {
		toSerialize["created"] = o.Created
	}
	if o.Updated != nil {
		toSerialize["updated"] = o.Updated
	}
	if o.Visibility != nil {
		toSerialize["visibility"] = o.Visibility
	}
	if o.JsdPublic != nil {
		toSerialize["jsdPublic"] = o.JsdPublic
	}
	if o.Properties != nil {
		toSerialize["properties"] = o.Properties
	}

	for key, value := range o.AdditionalProperties {
		toSerialize[key] = value
	}

	return json.Marshal(toSerialize)
}

func (o *Comment) UnmarshalJSON(bytes []byte) (err error) {
	varComment := _Comment{}

	if err = json.Unmarshal(bytes, &varComment); err == nil {
		*o = Comment(varComment)
	}

	additionalProperties := make(map[string]interface{})

	if err = json.Unmarshal(bytes, &additionalProperties); err == nil {
		delete(additionalProperties, "self")
		delete(additionalProperties, "id")
		delete(additionalProperties, "author")
		delete(additionalProperties, "body")
		delete(additionalProperties, "renderedBody")
		delete(additionalProperties, "updateAuthor")
		delete(additionalProperties, "created")
		delete(additionalProperties, "updated")
		delete(additionalProperties, "visibility")
		delete(additionalProperties, "jsdPublic")
		delete(additionalProperties, "properties")
		o.AdditionalProperties = additionalProperties
	}

	return err
}

type NullableComment struct {
	value *Comment
	isSet bool
}

func (v NullableComment) Get() *Comment {
	return v.value
}

func (v *NullableComment) Set(val *Comment) {
	v.value = val
	v.isSet = true
}

func (v NullableComment) IsSet() bool {
	return v.isSet
}

func (v *NullableComment) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableComment(val *Comment) *NullableComment {
	return &NullableComment{value: val, isSet: true}
}

func (v NullableComment) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableComment) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



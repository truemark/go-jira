/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ContainerForRegisteredWebhooks Container for a list of registered webhooks. Webhook details are returned in the same order as the request.
type ContainerForRegisteredWebhooks struct {
	// A list of registered webhooks.
	WebhookRegistrationResult *[]RegisteredWebhook `json:"webhookRegistrationResult,omitempty"`
}

// NewContainerForRegisteredWebhooks instantiates a new ContainerForRegisteredWebhooks object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewContainerForRegisteredWebhooks() *ContainerForRegisteredWebhooks {
	this := ContainerForRegisteredWebhooks{}
	return &this
}

// NewContainerForRegisteredWebhooksWithDefaults instantiates a new ContainerForRegisteredWebhooks object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewContainerForRegisteredWebhooksWithDefaults() *ContainerForRegisteredWebhooks {
	this := ContainerForRegisteredWebhooks{}
	return &this
}

// GetWebhookRegistrationResult returns the WebhookRegistrationResult field value if set, zero value otherwise.
func (o *ContainerForRegisteredWebhooks) GetWebhookRegistrationResult() []RegisteredWebhook {
	if o == nil || o.WebhookRegistrationResult == nil {
		var ret []RegisteredWebhook
		return ret
	}
	return *o.WebhookRegistrationResult
}

// GetWebhookRegistrationResultOk returns a tuple with the WebhookRegistrationResult field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContainerForRegisteredWebhooks) GetWebhookRegistrationResultOk() (*[]RegisteredWebhook, bool) {
	if o == nil || o.WebhookRegistrationResult == nil {
		return nil, false
	}
	return o.WebhookRegistrationResult, true
}

// HasWebhookRegistrationResult returns a boolean if a field has been set.
func (o *ContainerForRegisteredWebhooks) HasWebhookRegistrationResult() bool {
	if o != nil && o.WebhookRegistrationResult != nil {
		return true
	}

	return false
}

// SetWebhookRegistrationResult gets a reference to the given []RegisteredWebhook and assigns it to the WebhookRegistrationResult field.
func (o *ContainerForRegisteredWebhooks) SetWebhookRegistrationResult(v []RegisteredWebhook) {
	o.WebhookRegistrationResult = &v
}

func (o ContainerForRegisteredWebhooks) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.WebhookRegistrationResult != nil {
		toSerialize["webhookRegistrationResult"] = o.WebhookRegistrationResult
	}
	return json.Marshal(toSerialize)
}

type NullableContainerForRegisteredWebhooks struct {
	value *ContainerForRegisteredWebhooks
	isSet bool
}

func (v NullableContainerForRegisteredWebhooks) Get() *ContainerForRegisteredWebhooks {
	return v.value
}

func (v *NullableContainerForRegisteredWebhooks) Set(val *ContainerForRegisteredWebhooks) {
	v.value = val
	v.isSet = true
}

func (v NullableContainerForRegisteredWebhooks) IsSet() bool {
	return v.isSet
}

func (v *NullableContainerForRegisteredWebhooks) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableContainerForRegisteredWebhooks(val *ContainerForRegisteredWebhooks) *NullableContainerForRegisteredWebhooks {
	return &NullableContainerForRegisteredWebhooks{value: val, isSet: true}
}

func (v NullableContainerForRegisteredWebhooks) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableContainerForRegisteredWebhooks) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



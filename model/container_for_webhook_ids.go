/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ContainerForWebhookIDs Container for a list of webhook IDs.
type ContainerForWebhookIDs struct {
	// A list of webhook IDs.
	WebhookIds []int64 `json:"webhookIds"`
}

// NewContainerForWebhookIDs instantiates a new ContainerForWebhookIDs object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewContainerForWebhookIDs(webhookIds []int64) *ContainerForWebhookIDs {
	this := ContainerForWebhookIDs{}
	this.WebhookIds = webhookIds
	return &this
}

// NewContainerForWebhookIDsWithDefaults instantiates a new ContainerForWebhookIDs object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewContainerForWebhookIDsWithDefaults() *ContainerForWebhookIDs {
	this := ContainerForWebhookIDs{}
	return &this
}

// GetWebhookIds returns the WebhookIds field value
func (o *ContainerForWebhookIDs) GetWebhookIds() []int64 {
	if o == nil {
		var ret []int64
		return ret
	}

	return o.WebhookIds
}

// GetWebhookIdsOk returns a tuple with the WebhookIds field value
// and a boolean to check if the value has been set.
func (o *ContainerForWebhookIDs) GetWebhookIdsOk() (*[]int64, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.WebhookIds, true
}

// SetWebhookIds sets field value
func (o *ContainerForWebhookIDs) SetWebhookIds(v []int64) {
	o.WebhookIds = v
}

func (o ContainerForWebhookIDs) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["webhookIds"] = o.WebhookIds
	}
	return json.Marshal(toSerialize)
}

type NullableContainerForWebhookIDs struct {
	value *ContainerForWebhookIDs
	isSet bool
}

func (v NullableContainerForWebhookIDs) Get() *ContainerForWebhookIDs {
	return v.value
}

func (v *NullableContainerForWebhookIDs) Set(val *ContainerForWebhookIDs) {
	v.value = val
	v.isSet = true
}

func (v NullableContainerForWebhookIDs) IsSet() bool {
	return v.isSet
}

func (v *NullableContainerForWebhookIDs) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableContainerForWebhookIDs(val *ContainerForWebhookIDs) *NullableContainerForWebhookIDs {
	return &NullableContainerForWebhookIDs{value: val, isSet: true}
}

func (v NullableContainerForWebhookIDs) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableContainerForWebhookIDs) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



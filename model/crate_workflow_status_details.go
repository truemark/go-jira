/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// CrateWorkflowStatusDetails The details of a transition status.
type CrateWorkflowStatusDetails struct {
	// The ID of the status.
	Id string `json:"id"`
}

// NewCrateWorkflowStatusDetails instantiates a new CrateWorkflowStatusDetails object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCrateWorkflowStatusDetails(id string) *CrateWorkflowStatusDetails {
	this := CrateWorkflowStatusDetails{}
	this.Id = id
	return &this
}

// NewCrateWorkflowStatusDetailsWithDefaults instantiates a new CrateWorkflowStatusDetails object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCrateWorkflowStatusDetailsWithDefaults() *CrateWorkflowStatusDetails {
	this := CrateWorkflowStatusDetails{}
	return &this
}

// GetId returns the Id field value
func (o *CrateWorkflowStatusDetails) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *CrateWorkflowStatusDetails) GetIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *CrateWorkflowStatusDetails) SetId(v string) {
	o.Id = v
}

func (o CrateWorkflowStatusDetails) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	return json.Marshal(toSerialize)
}

type NullableCrateWorkflowStatusDetails struct {
	value *CrateWorkflowStatusDetails
	isSet bool
}

func (v NullableCrateWorkflowStatusDetails) Get() *CrateWorkflowStatusDetails {
	return v.value
}

func (v *NullableCrateWorkflowStatusDetails) Set(val *CrateWorkflowStatusDetails) {
	v.value = val
	v.isSet = true
}

func (v NullableCrateWorkflowStatusDetails) IsSet() bool {
	return v.isSet
}

func (v *NullableCrateWorkflowStatusDetails) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCrateWorkflowStatusDetails(val *CrateWorkflowStatusDetails) *NullableCrateWorkflowStatusDetails {
	return &NullableCrateWorkflowStatusDetails{value: val, isSet: true}
}

func (v NullableCrateWorkflowStatusDetails) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCrateWorkflowStatusDetails) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



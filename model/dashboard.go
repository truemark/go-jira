/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// Dashboard Details of a dashboard.
type Dashboard struct {
	Description *string `json:"description,omitempty"`
	// The ID of the dashboard.
	Id *string `json:"id,omitempty"`
	// Whether the dashboard is selected as a favorite by the user.
	IsFavourite *bool `json:"isFavourite,omitempty"`
	// The name of the dashboard.
	Name *string `json:"name,omitempty"`
	// The owner of the dashboard.
	Owner *UserBean `json:"owner,omitempty"`
	// The number of users who have this dashboard as a favorite.
	Popularity *int64 `json:"popularity,omitempty"`
	// The rank of this dashboard.
	Rank *int32 `json:"rank,omitempty"`
	// The URL of these dashboard details.
	Self *string `json:"self,omitempty"`
	// The details of any view share permissions for the dashboard.
	SharePermissions *[]SharePermission `json:"sharePermissions,omitempty"`
	// The details of any edit share permissions for the dashboard.
	EditPermissions *[]SharePermission `json:"editPermissions,omitempty"`
	// The URL of the dashboard.
	View *string `json:"view,omitempty"`
}

// NewDashboard instantiates a new Dashboard object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDashboard() *Dashboard {
	this := Dashboard{}
	return &this
}

// NewDashboardWithDefaults instantiates a new Dashboard object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDashboardWithDefaults() *Dashboard {
	this := Dashboard{}
	return &this
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *Dashboard) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *Dashboard) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *Dashboard) SetDescription(v string) {
	o.Description = &v
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *Dashboard) GetId() string {
	if o == nil || o.Id == nil {
		var ret string
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetIdOk() (*string, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *Dashboard) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given string and assigns it to the Id field.
func (o *Dashboard) SetId(v string) {
	o.Id = &v
}

// GetIsFavourite returns the IsFavourite field value if set, zero value otherwise.
func (o *Dashboard) GetIsFavourite() bool {
	if o == nil || o.IsFavourite == nil {
		var ret bool
		return ret
	}
	return *o.IsFavourite
}

// GetIsFavouriteOk returns a tuple with the IsFavourite field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetIsFavouriteOk() (*bool, bool) {
	if o == nil || o.IsFavourite == nil {
		return nil, false
	}
	return o.IsFavourite, true
}

// HasIsFavourite returns a boolean if a field has been set.
func (o *Dashboard) HasIsFavourite() bool {
	if o != nil && o.IsFavourite != nil {
		return true
	}

	return false
}

// SetIsFavourite gets a reference to the given bool and assigns it to the IsFavourite field.
func (o *Dashboard) SetIsFavourite(v bool) {
	o.IsFavourite = &v
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *Dashboard) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *Dashboard) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *Dashboard) SetName(v string) {
	o.Name = &v
}

// GetOwner returns the Owner field value if set, zero value otherwise.
func (o *Dashboard) GetOwner() UserBean {
	if o == nil || o.Owner == nil {
		var ret UserBean
		return ret
	}
	return *o.Owner
}

// GetOwnerOk returns a tuple with the Owner field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetOwnerOk() (*UserBean, bool) {
	if o == nil || o.Owner == nil {
		return nil, false
	}
	return o.Owner, true
}

// HasOwner returns a boolean if a field has been set.
func (o *Dashboard) HasOwner() bool {
	if o != nil && o.Owner != nil {
		return true
	}

	return false
}

// SetOwner gets a reference to the given UserBean and assigns it to the Owner field.
func (o *Dashboard) SetOwner(v UserBean) {
	o.Owner = &v
}

// GetPopularity returns the Popularity field value if set, zero value otherwise.
func (o *Dashboard) GetPopularity() int64 {
	if o == nil || o.Popularity == nil {
		var ret int64
		return ret
	}
	return *o.Popularity
}

// GetPopularityOk returns a tuple with the Popularity field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetPopularityOk() (*int64, bool) {
	if o == nil || o.Popularity == nil {
		return nil, false
	}
	return o.Popularity, true
}

// HasPopularity returns a boolean if a field has been set.
func (o *Dashboard) HasPopularity() bool {
	if o != nil && o.Popularity != nil {
		return true
	}

	return false
}

// SetPopularity gets a reference to the given int64 and assigns it to the Popularity field.
func (o *Dashboard) SetPopularity(v int64) {
	o.Popularity = &v
}

// GetRank returns the Rank field value if set, zero value otherwise.
func (o *Dashboard) GetRank() int32 {
	if o == nil || o.Rank == nil {
		var ret int32
		return ret
	}
	return *o.Rank
}

// GetRankOk returns a tuple with the Rank field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetRankOk() (*int32, bool) {
	if o == nil || o.Rank == nil {
		return nil, false
	}
	return o.Rank, true
}

// HasRank returns a boolean if a field has been set.
func (o *Dashboard) HasRank() bool {
	if o != nil && o.Rank != nil {
		return true
	}

	return false
}

// SetRank gets a reference to the given int32 and assigns it to the Rank field.
func (o *Dashboard) SetRank(v int32) {
	o.Rank = &v
}

// GetSelf returns the Self field value if set, zero value otherwise.
func (o *Dashboard) GetSelf() string {
	if o == nil || o.Self == nil {
		var ret string
		return ret
	}
	return *o.Self
}

// GetSelfOk returns a tuple with the Self field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetSelfOk() (*string, bool) {
	if o == nil || o.Self == nil {
		return nil, false
	}
	return o.Self, true
}

// HasSelf returns a boolean if a field has been set.
func (o *Dashboard) HasSelf() bool {
	if o != nil && o.Self != nil {
		return true
	}

	return false
}

// SetSelf gets a reference to the given string and assigns it to the Self field.
func (o *Dashboard) SetSelf(v string) {
	o.Self = &v
}

// GetSharePermissions returns the SharePermissions field value if set, zero value otherwise.
func (o *Dashboard) GetSharePermissions() []SharePermission {
	if o == nil || o.SharePermissions == nil {
		var ret []SharePermission
		return ret
	}
	return *o.SharePermissions
}

// GetSharePermissionsOk returns a tuple with the SharePermissions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetSharePermissionsOk() (*[]SharePermission, bool) {
	if o == nil || o.SharePermissions == nil {
		return nil, false
	}
	return o.SharePermissions, true
}

// HasSharePermissions returns a boolean if a field has been set.
func (o *Dashboard) HasSharePermissions() bool {
	if o != nil && o.SharePermissions != nil {
		return true
	}

	return false
}

// SetSharePermissions gets a reference to the given []SharePermission and assigns it to the SharePermissions field.
func (o *Dashboard) SetSharePermissions(v []SharePermission) {
	o.SharePermissions = &v
}

// GetEditPermissions returns the EditPermissions field value if set, zero value otherwise.
func (o *Dashboard) GetEditPermissions() []SharePermission {
	if o == nil || o.EditPermissions == nil {
		var ret []SharePermission
		return ret
	}
	return *o.EditPermissions
}

// GetEditPermissionsOk returns a tuple with the EditPermissions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetEditPermissionsOk() (*[]SharePermission, bool) {
	if o == nil || o.EditPermissions == nil {
		return nil, false
	}
	return o.EditPermissions, true
}

// HasEditPermissions returns a boolean if a field has been set.
func (o *Dashboard) HasEditPermissions() bool {
	if o != nil && o.EditPermissions != nil {
		return true
	}

	return false
}

// SetEditPermissions gets a reference to the given []SharePermission and assigns it to the EditPermissions field.
func (o *Dashboard) SetEditPermissions(v []SharePermission) {
	o.EditPermissions = &v
}

// GetView returns the View field value if set, zero value otherwise.
func (o *Dashboard) GetView() string {
	if o == nil || o.View == nil {
		var ret string
		return ret
	}
	return *o.View
}

// GetViewOk returns a tuple with the View field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Dashboard) GetViewOk() (*string, bool) {
	if o == nil || o.View == nil {
		return nil, false
	}
	return o.View, true
}

// HasView returns a boolean if a field has been set.
func (o *Dashboard) HasView() bool {
	if o != nil && o.View != nil {
		return true
	}

	return false
}

// SetView gets a reference to the given string and assigns it to the View field.
func (o *Dashboard) SetView(v string) {
	o.View = &v
}

func (o Dashboard) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.IsFavourite != nil {
		toSerialize["isFavourite"] = o.IsFavourite
	}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.Owner != nil {
		toSerialize["owner"] = o.Owner
	}
	if o.Popularity != nil {
		toSerialize["popularity"] = o.Popularity
	}
	if o.Rank != nil {
		toSerialize["rank"] = o.Rank
	}
	if o.Self != nil {
		toSerialize["self"] = o.Self
	}
	if o.SharePermissions != nil {
		toSerialize["sharePermissions"] = o.SharePermissions
	}
	if o.EditPermissions != nil {
		toSerialize["editPermissions"] = o.EditPermissions
	}
	if o.View != nil {
		toSerialize["view"] = o.View
	}
	return json.Marshal(toSerialize)
}

type NullableDashboard struct {
	value *Dashboard
	isSet bool
}

func (v NullableDashboard) Get() *Dashboard {
	return v.value
}

func (v *NullableDashboard) Set(val *Dashboard) {
	v.value = val
	v.isSet = true
}

func (v NullableDashboard) IsSet() bool {
	return v.isSet
}

func (v *NullableDashboard) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDashboard(val *Dashboard) *NullableDashboard {
	return &NullableDashboard{value: val, isSet: true}
}

func (v NullableDashboard) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDashboard) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



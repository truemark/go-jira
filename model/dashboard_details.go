/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// DashboardDetails Details of a dashboard.
type DashboardDetails struct {
	// The name of the dashboard.
	Name string `json:"name"`
	// The description of the dashboard.
	Description *string `json:"description,omitempty"`
	// The details of any share permissions for the dashboard.
	SharePermissions []SharePermission `json:"sharePermissions"`
}

// NewDashboardDetails instantiates a new DashboardDetails object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDashboardDetails(name string, sharePermissions []SharePermission) *DashboardDetails {
	this := DashboardDetails{}
	this.Name = name
	this.SharePermissions = sharePermissions
	return &this
}

// NewDashboardDetailsWithDefaults instantiates a new DashboardDetails object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDashboardDetailsWithDefaults() *DashboardDetails {
	this := DashboardDetails{}
	return &this
}

// GetName returns the Name field value
func (o *DashboardDetails) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *DashboardDetails) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *DashboardDetails) SetName(v string) {
	o.Name = v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *DashboardDetails) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DashboardDetails) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *DashboardDetails) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *DashboardDetails) SetDescription(v string) {
	o.Description = &v
}

// GetSharePermissions returns the SharePermissions field value
func (o *DashboardDetails) GetSharePermissions() []SharePermission {
	if o == nil {
		var ret []SharePermission
		return ret
	}

	return o.SharePermissions
}

// GetSharePermissionsOk returns a tuple with the SharePermissions field value
// and a boolean to check if the value has been set.
func (o *DashboardDetails) GetSharePermissionsOk() (*[]SharePermission, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.SharePermissions, true
}

// SetSharePermissions sets field value
func (o *DashboardDetails) SetSharePermissions(v []SharePermission) {
	o.SharePermissions = v
}

func (o DashboardDetails) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["name"] = o.Name
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if true {
		toSerialize["sharePermissions"] = o.SharePermissions
	}
	return json.Marshal(toSerialize)
}

type NullableDashboardDetails struct {
	value *DashboardDetails
	isSet bool
}

func (v NullableDashboardDetails) Get() *DashboardDetails {
	return v.value
}

func (v *NullableDashboardDetails) Set(val *DashboardDetails) {
	v.value = val
	v.isSet = true
}

func (v NullableDashboardDetails) IsSet() bool {
	return v.isSet
}

func (v *NullableDashboardDetails) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDashboardDetails(val *DashboardDetails) *NullableDashboardDetails {
	return &NullableDashboardDetails{value: val, isSet: true}
}

func (v NullableDashboardDetails) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDashboardDetails) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



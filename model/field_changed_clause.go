/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// FieldChangedClause A clause that asserts whether a field was changed. For example, `status CHANGED AFTER startOfMonth(-1M)`.See [CHANGED](https://confluence.atlassian.com/x/dgiiLQ#Advancedsearching-operatorsreference-CHANGEDCHANGED) for more information about the CHANGED operator.
type FieldChangedClause struct {
	Field JqlQueryField `json:"field"`
	// The operator applied to the field.
	Operator string `json:"operator"`
	// The list of time predicates.
	Predicates []JqlQueryClauseTimePredicate `json:"predicates"`
	AdditionalProperties map[string]interface{}
}

type _FieldChangedClause FieldChangedClause

// NewFieldChangedClause instantiates a new FieldChangedClause object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFieldChangedClause(field JqlQueryField, operator string, predicates []JqlQueryClauseTimePredicate) *FieldChangedClause {
	this := FieldChangedClause{}
	this.Field = field
	this.Operator = operator
	this.Predicates = predicates
	return &this
}

// NewFieldChangedClauseWithDefaults instantiates a new FieldChangedClause object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFieldChangedClauseWithDefaults() *FieldChangedClause {
	this := FieldChangedClause{}
	return &this
}

// GetField returns the Field field value
func (o *FieldChangedClause) GetField() JqlQueryField {
	if o == nil {
		var ret JqlQueryField
		return ret
	}

	return o.Field
}

// GetFieldOk returns a tuple with the Field field value
// and a boolean to check if the value has been set.
func (o *FieldChangedClause) GetFieldOk() (*JqlQueryField, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Field, true
}

// SetField sets field value
func (o *FieldChangedClause) SetField(v JqlQueryField) {
	o.Field = v
}

// GetOperator returns the Operator field value
func (o *FieldChangedClause) GetOperator() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Operator
}

// GetOperatorOk returns a tuple with the Operator field value
// and a boolean to check if the value has been set.
func (o *FieldChangedClause) GetOperatorOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Operator, true
}

// SetOperator sets field value
func (o *FieldChangedClause) SetOperator(v string) {
	o.Operator = v
}

// GetPredicates returns the Predicates field value
func (o *FieldChangedClause) GetPredicates() []JqlQueryClauseTimePredicate {
	if o == nil {
		var ret []JqlQueryClauseTimePredicate
		return ret
	}

	return o.Predicates
}

// GetPredicatesOk returns a tuple with the Predicates field value
// and a boolean to check if the value has been set.
func (o *FieldChangedClause) GetPredicatesOk() (*[]JqlQueryClauseTimePredicate, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Predicates, true
}

// SetPredicates sets field value
func (o *FieldChangedClause) SetPredicates(v []JqlQueryClauseTimePredicate) {
	o.Predicates = v
}

func (o FieldChangedClause) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["field"] = o.Field
	}
	if true {
		toSerialize["operator"] = o.Operator
	}
	if true {
		toSerialize["predicates"] = o.Predicates
	}

	for key, value := range o.AdditionalProperties {
		toSerialize[key] = value
	}

	return json.Marshal(toSerialize)
}

func (o *FieldChangedClause) UnmarshalJSON(bytes []byte) (err error) {
	varFieldChangedClause := _FieldChangedClause{}

	if err = json.Unmarshal(bytes, &varFieldChangedClause); err == nil {
		*o = FieldChangedClause(varFieldChangedClause)
	}

	additionalProperties := make(map[string]interface{})

	if err = json.Unmarshal(bytes, &additionalProperties); err == nil {
		delete(additionalProperties, "field")
		delete(additionalProperties, "operator")
		delete(additionalProperties, "predicates")
		o.AdditionalProperties = additionalProperties
	}

	return err
}

type NullableFieldChangedClause struct {
	value *FieldChangedClause
	isSet bool
}

func (v NullableFieldChangedClause) Get() *FieldChangedClause {
	return v.value
}

func (v *NullableFieldChangedClause) Set(val *FieldChangedClause) {
	v.value = val
	v.isSet = true
}

func (v NullableFieldChangedClause) IsSet() bool {
	return v.isSet
}

func (v *NullableFieldChangedClause) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFieldChangedClause(val *FieldChangedClause) *NullableFieldChangedClause {
	return &NullableFieldChangedClause{value: val, isSet: true}
}

func (v NullableFieldChangedClause) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFieldChangedClause) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



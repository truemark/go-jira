/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// FieldDetails Details about a field.
type FieldDetails struct {
	// The ID of the field.
	Id *string `json:"id,omitempty"`
	// The key of the field.
	Key *string `json:"key,omitempty"`
	// The name of the field.
	Name *string `json:"name,omitempty"`
	// Whether the field is a custom field.
	Custom *bool `json:"custom,omitempty"`
	// Whether the content of the field can be used to order lists.
	Orderable *bool `json:"orderable,omitempty"`
	// Whether the field can be used as a column on the issue navigator.
	Navigable *bool `json:"navigable,omitempty"`
	// Whether the content of the field can be searched.
	Searchable *bool `json:"searchable,omitempty"`
	// The names that can be used to reference the field in an advanced search. For more information, see [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ).
	ClauseNames *[]string `json:"clauseNames,omitempty"`
	// The scope of the field.
	Scope *Scope `json:"scope,omitempty"`
	// The data schema for the field.
	Schema *JsonTypeBean `json:"schema,omitempty"`
}

// NewFieldDetails instantiates a new FieldDetails object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFieldDetails() *FieldDetails {
	this := FieldDetails{}
	return &this
}

// NewFieldDetailsWithDefaults instantiates a new FieldDetails object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFieldDetailsWithDefaults() *FieldDetails {
	this := FieldDetails{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *FieldDetails) GetId() string {
	if o == nil || o.Id == nil {
		var ret string
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetIdOk() (*string, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *FieldDetails) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given string and assigns it to the Id field.
func (o *FieldDetails) SetId(v string) {
	o.Id = &v
}

// GetKey returns the Key field value if set, zero value otherwise.
func (o *FieldDetails) GetKey() string {
	if o == nil || o.Key == nil {
		var ret string
		return ret
	}
	return *o.Key
}

// GetKeyOk returns a tuple with the Key field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetKeyOk() (*string, bool) {
	if o == nil || o.Key == nil {
		return nil, false
	}
	return o.Key, true
}

// HasKey returns a boolean if a field has been set.
func (o *FieldDetails) HasKey() bool {
	if o != nil && o.Key != nil {
		return true
	}

	return false
}

// SetKey gets a reference to the given string and assigns it to the Key field.
func (o *FieldDetails) SetKey(v string) {
	o.Key = &v
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *FieldDetails) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *FieldDetails) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *FieldDetails) SetName(v string) {
	o.Name = &v
}

// GetCustom returns the Custom field value if set, zero value otherwise.
func (o *FieldDetails) GetCustom() bool {
	if o == nil || o.Custom == nil {
		var ret bool
		return ret
	}
	return *o.Custom
}

// GetCustomOk returns a tuple with the Custom field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetCustomOk() (*bool, bool) {
	if o == nil || o.Custom == nil {
		return nil, false
	}
	return o.Custom, true
}

// HasCustom returns a boolean if a field has been set.
func (o *FieldDetails) HasCustom() bool {
	if o != nil && o.Custom != nil {
		return true
	}

	return false
}

// SetCustom gets a reference to the given bool and assigns it to the Custom field.
func (o *FieldDetails) SetCustom(v bool) {
	o.Custom = &v
}

// GetOrderable returns the Orderable field value if set, zero value otherwise.
func (o *FieldDetails) GetOrderable() bool {
	if o == nil || o.Orderable == nil {
		var ret bool
		return ret
	}
	return *o.Orderable
}

// GetOrderableOk returns a tuple with the Orderable field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetOrderableOk() (*bool, bool) {
	if o == nil || o.Orderable == nil {
		return nil, false
	}
	return o.Orderable, true
}

// HasOrderable returns a boolean if a field has been set.
func (o *FieldDetails) HasOrderable() bool {
	if o != nil && o.Orderable != nil {
		return true
	}

	return false
}

// SetOrderable gets a reference to the given bool and assigns it to the Orderable field.
func (o *FieldDetails) SetOrderable(v bool) {
	o.Orderable = &v
}

// GetNavigable returns the Navigable field value if set, zero value otherwise.
func (o *FieldDetails) GetNavigable() bool {
	if o == nil || o.Navigable == nil {
		var ret bool
		return ret
	}
	return *o.Navigable
}

// GetNavigableOk returns a tuple with the Navigable field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetNavigableOk() (*bool, bool) {
	if o == nil || o.Navigable == nil {
		return nil, false
	}
	return o.Navigable, true
}

// HasNavigable returns a boolean if a field has been set.
func (o *FieldDetails) HasNavigable() bool {
	if o != nil && o.Navigable != nil {
		return true
	}

	return false
}

// SetNavigable gets a reference to the given bool and assigns it to the Navigable field.
func (o *FieldDetails) SetNavigable(v bool) {
	o.Navigable = &v
}

// GetSearchable returns the Searchable field value if set, zero value otherwise.
func (o *FieldDetails) GetSearchable() bool {
	if o == nil || o.Searchable == nil {
		var ret bool
		return ret
	}
	return *o.Searchable
}

// GetSearchableOk returns a tuple with the Searchable field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetSearchableOk() (*bool, bool) {
	if o == nil || o.Searchable == nil {
		return nil, false
	}
	return o.Searchable, true
}

// HasSearchable returns a boolean if a field has been set.
func (o *FieldDetails) HasSearchable() bool {
	if o != nil && o.Searchable != nil {
		return true
	}

	return false
}

// SetSearchable gets a reference to the given bool and assigns it to the Searchable field.
func (o *FieldDetails) SetSearchable(v bool) {
	o.Searchable = &v
}

// GetClauseNames returns the ClauseNames field value if set, zero value otherwise.
func (o *FieldDetails) GetClauseNames() []string {
	if o == nil || o.ClauseNames == nil {
		var ret []string
		return ret
	}
	return *o.ClauseNames
}

// GetClauseNamesOk returns a tuple with the ClauseNames field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetClauseNamesOk() (*[]string, bool) {
	if o == nil || o.ClauseNames == nil {
		return nil, false
	}
	return o.ClauseNames, true
}

// HasClauseNames returns a boolean if a field has been set.
func (o *FieldDetails) HasClauseNames() bool {
	if o != nil && o.ClauseNames != nil {
		return true
	}

	return false
}

// SetClauseNames gets a reference to the given []string and assigns it to the ClauseNames field.
func (o *FieldDetails) SetClauseNames(v []string) {
	o.ClauseNames = &v
}

// GetScope returns the Scope field value if set, zero value otherwise.
func (o *FieldDetails) GetScope() Scope {
	if o == nil || o.Scope == nil {
		var ret Scope
		return ret
	}
	return *o.Scope
}

// GetScopeOk returns a tuple with the Scope field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetScopeOk() (*Scope, bool) {
	if o == nil || o.Scope == nil {
		return nil, false
	}
	return o.Scope, true
}

// HasScope returns a boolean if a field has been set.
func (o *FieldDetails) HasScope() bool {
	if o != nil && o.Scope != nil {
		return true
	}

	return false
}

// SetScope gets a reference to the given Scope and assigns it to the Scope field.
func (o *FieldDetails) SetScope(v Scope) {
	o.Scope = &v
}

// GetSchema returns the Schema field value if set, zero value otherwise.
func (o *FieldDetails) GetSchema() JsonTypeBean {
	if o == nil || o.Schema == nil {
		var ret JsonTypeBean
		return ret
	}
	return *o.Schema
}

// GetSchemaOk returns a tuple with the Schema field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *FieldDetails) GetSchemaOk() (*JsonTypeBean, bool) {
	if o == nil || o.Schema == nil {
		return nil, false
	}
	return o.Schema, true
}

// HasSchema returns a boolean if a field has been set.
func (o *FieldDetails) HasSchema() bool {
	if o != nil && o.Schema != nil {
		return true
	}

	return false
}

// SetSchema gets a reference to the given JsonTypeBean and assigns it to the Schema field.
func (o *FieldDetails) SetSchema(v JsonTypeBean) {
	o.Schema = &v
}

func (o FieldDetails) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.Key != nil {
		toSerialize["key"] = o.Key
	}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.Custom != nil {
		toSerialize["custom"] = o.Custom
	}
	if o.Orderable != nil {
		toSerialize["orderable"] = o.Orderable
	}
	if o.Navigable != nil {
		toSerialize["navigable"] = o.Navigable
	}
	if o.Searchable != nil {
		toSerialize["searchable"] = o.Searchable
	}
	if o.ClauseNames != nil {
		toSerialize["clauseNames"] = o.ClauseNames
	}
	if o.Scope != nil {
		toSerialize["scope"] = o.Scope
	}
	if o.Schema != nil {
		toSerialize["schema"] = o.Schema
	}
	return json.Marshal(toSerialize)
}

type NullableFieldDetails struct {
	value *FieldDetails
	isSet bool
}

func (v NullableFieldDetails) Get() *FieldDetails {
	return v.value
}

func (v *NullableFieldDetails) Set(val *FieldDetails) {
	v.value = val
	v.isSet = true
}

func (v NullableFieldDetails) IsSet() bool {
	return v.isSet
}

func (v *NullableFieldDetails) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFieldDetails(val *FieldDetails) *NullableFieldDetails {
	return &NullableFieldDetails{value: val, isSet: true}
}

func (v NullableFieldDetails) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFieldDetails) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// Fields Key fields from the linked issue.
type Fields struct {
	// The summary description of the linked issue.
	Summary *string `json:"summary,omitempty"`
	// The status of the linked issue.
	Status *StatusDetails `json:"status,omitempty"`
	// The priority of the linked issue.
	Priority *Priority `json:"priority,omitempty"`
	// The assignee of the linked issue.
	Assignee *UserDetails `json:"assignee,omitempty"`
	// The time tracking of the linked issue.
	Timetracking *TimeTrackingDetails `json:"timetracking,omitempty"`
	Issuetype *IssueTypeDetails `json:"issuetype,omitempty"`
	// The type of the linked issue.
	IssueType *IssueTypeDetails `json:"issueType,omitempty"`
}

// NewFields instantiates a new Fields object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFields() *Fields {
	this := Fields{}
	return &this
}

// NewFieldsWithDefaults instantiates a new Fields object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFieldsWithDefaults() *Fields {
	this := Fields{}
	return &this
}

// GetSummary returns the Summary field value if set, zero value otherwise.
func (o *Fields) GetSummary() string {
	if o == nil || o.Summary == nil {
		var ret string
		return ret
	}
	return *o.Summary
}

// GetSummaryOk returns a tuple with the Summary field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Fields) GetSummaryOk() (*string, bool) {
	if o == nil || o.Summary == nil {
		return nil, false
	}
	return o.Summary, true
}

// HasSummary returns a boolean if a field has been set.
func (o *Fields) HasSummary() bool {
	if o != nil && o.Summary != nil {
		return true
	}

	return false
}

// SetSummary gets a reference to the given string and assigns it to the Summary field.
func (o *Fields) SetSummary(v string) {
	o.Summary = &v
}

// GetStatus returns the Status field value if set, zero value otherwise.
func (o *Fields) GetStatus() StatusDetails {
	if o == nil || o.Status == nil {
		var ret StatusDetails
		return ret
	}
	return *o.Status
}

// GetStatusOk returns a tuple with the Status field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Fields) GetStatusOk() (*StatusDetails, bool) {
	if o == nil || o.Status == nil {
		return nil, false
	}
	return o.Status, true
}

// HasStatus returns a boolean if a field has been set.
func (o *Fields) HasStatus() bool {
	if o != nil && o.Status != nil {
		return true
	}

	return false
}

// SetStatus gets a reference to the given StatusDetails and assigns it to the Status field.
func (o *Fields) SetStatus(v StatusDetails) {
	o.Status = &v
}

// GetPriority returns the Priority field value if set, zero value otherwise.
func (o *Fields) GetPriority() Priority {
	if o == nil || o.Priority == nil {
		var ret Priority
		return ret
	}
	return *o.Priority
}

// GetPriorityOk returns a tuple with the Priority field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Fields) GetPriorityOk() (*Priority, bool) {
	if o == nil || o.Priority == nil {
		return nil, false
	}
	return o.Priority, true
}

// HasPriority returns a boolean if a field has been set.
func (o *Fields) HasPriority() bool {
	if o != nil && o.Priority != nil {
		return true
	}

	return false
}

// SetPriority gets a reference to the given Priority and assigns it to the Priority field.
func (o *Fields) SetPriority(v Priority) {
	o.Priority = &v
}

// GetAssignee returns the Assignee field value if set, zero value otherwise.
func (o *Fields) GetAssignee() UserDetails {
	if o == nil || o.Assignee == nil {
		var ret UserDetails
		return ret
	}
	return *o.Assignee
}

// GetAssigneeOk returns a tuple with the Assignee field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Fields) GetAssigneeOk() (*UserDetails, bool) {
	if o == nil || o.Assignee == nil {
		return nil, false
	}
	return o.Assignee, true
}

// HasAssignee returns a boolean if a field has been set.
func (o *Fields) HasAssignee() bool {
	if o != nil && o.Assignee != nil {
		return true
	}

	return false
}

// SetAssignee gets a reference to the given UserDetails and assigns it to the Assignee field.
func (o *Fields) SetAssignee(v UserDetails) {
	o.Assignee = &v
}

// GetTimetracking returns the Timetracking field value if set, zero value otherwise.
func (o *Fields) GetTimetracking() TimeTrackingDetails {
	if o == nil || o.Timetracking == nil {
		var ret TimeTrackingDetails
		return ret
	}
	return *o.Timetracking
}

// GetTimetrackingOk returns a tuple with the Timetracking field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Fields) GetTimetrackingOk() (*TimeTrackingDetails, bool) {
	if o == nil || o.Timetracking == nil {
		return nil, false
	}
	return o.Timetracking, true
}

// HasTimetracking returns a boolean if a field has been set.
func (o *Fields) HasTimetracking() bool {
	if o != nil && o.Timetracking != nil {
		return true
	}

	return false
}

// SetTimetracking gets a reference to the given TimeTrackingDetails and assigns it to the Timetracking field.
func (o *Fields) SetTimetracking(v TimeTrackingDetails) {
	o.Timetracking = &v
}

// GetIssuetype returns the Issuetype field value if set, zero value otherwise.
func (o *Fields) GetIssuetype() IssueTypeDetails {
	if o == nil || o.Issuetype == nil {
		var ret IssueTypeDetails
		return ret
	}
	return *o.Issuetype
}

// GetIssuetypeOk returns a tuple with the Issuetype field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Fields) GetIssuetypeOk() (*IssueTypeDetails, bool) {
	if o == nil || o.Issuetype == nil {
		return nil, false
	}
	return o.Issuetype, true
}

// HasIssuetype returns a boolean if a field has been set.
func (o *Fields) HasIssuetype() bool {
	if o != nil && o.Issuetype != nil {
		return true
	}

	return false
}

// SetIssuetype gets a reference to the given IssueTypeDetails and assigns it to the Issuetype field.
func (o *Fields) SetIssuetype(v IssueTypeDetails) {
	o.Issuetype = &v
}

// GetIssueType returns the IssueType field value if set, zero value otherwise.
func (o *Fields) GetIssueType() IssueTypeDetails {
	if o == nil || o.IssueType == nil {
		var ret IssueTypeDetails
		return ret
	}
	return *o.IssueType
}

// GetIssueTypeOk returns a tuple with the IssueType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Fields) GetIssueTypeOk() (*IssueTypeDetails, bool) {
	if o == nil || o.IssueType == nil {
		return nil, false
	}
	return o.IssueType, true
}

// HasIssueType returns a boolean if a field has been set.
func (o *Fields) HasIssueType() bool {
	if o != nil && o.IssueType != nil {
		return true
	}

	return false
}

// SetIssueType gets a reference to the given IssueTypeDetails and assigns it to the IssueType field.
func (o *Fields) SetIssueType(v IssueTypeDetails) {
	o.IssueType = &v
}

func (o Fields) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Summary != nil {
		toSerialize["summary"] = o.Summary
	}
	if o.Status != nil {
		toSerialize["status"] = o.Status
	}
	if o.Priority != nil {
		toSerialize["priority"] = o.Priority
	}
	if o.Assignee != nil {
		toSerialize["assignee"] = o.Assignee
	}
	if o.Timetracking != nil {
		toSerialize["timetracking"] = o.Timetracking
	}
	if o.Issuetype != nil {
		toSerialize["issuetype"] = o.Issuetype
	}
	if o.IssueType != nil {
		toSerialize["issueType"] = o.IssueType
	}
	return json.Marshal(toSerialize)
}

type NullableFields struct {
	value *Fields
	isSet bool
}

func (v NullableFields) Get() *Fields {
	return v.value
}

func (v *NullableFields) Set(val *Fields) {
	v.value = val
	v.isSet = true
}

func (v NullableFields) IsSet() bool {
	return v.isSet
}

func (v *NullableFields) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFields(val *Fields) *NullableFields {
	return &NullableFields{value: val, isSet: true}
}

func (v NullableFields) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFields) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// GlobalScopeBean struct for GlobalScopeBean
type GlobalScopeBean struct {
	// Defines the behavior of the option in the global context.If notSelectable is set, the option cannot be set as the field's value. This is useful for archiving an option that has previously been selected but shouldn't be used anymore.If defaultValue is set, the option is selected by default.
	Attributes *[]string `json:"attributes,omitempty"`
}

// NewGlobalScopeBean instantiates a new GlobalScopeBean object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGlobalScopeBean() *GlobalScopeBean {
	this := GlobalScopeBean{}
	return &this
}

// NewGlobalScopeBeanWithDefaults instantiates a new GlobalScopeBean object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGlobalScopeBeanWithDefaults() *GlobalScopeBean {
	this := GlobalScopeBean{}
	return &this
}

// GetAttributes returns the Attributes field value if set, zero value otherwise.
func (o *GlobalScopeBean) GetAttributes() []string {
	if o == nil || o.Attributes == nil {
		var ret []string
		return ret
	}
	return *o.Attributes
}

// GetAttributesOk returns a tuple with the Attributes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GlobalScopeBean) GetAttributesOk() (*[]string, bool) {
	if o == nil || o.Attributes == nil {
		return nil, false
	}
	return o.Attributes, true
}

// HasAttributes returns a boolean if a field has been set.
func (o *GlobalScopeBean) HasAttributes() bool {
	if o != nil && o.Attributes != nil {
		return true
	}

	return false
}

// SetAttributes gets a reference to the given []string and assigns it to the Attributes field.
func (o *GlobalScopeBean) SetAttributes(v []string) {
	o.Attributes = &v
}

func (o GlobalScopeBean) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Attributes != nil {
		toSerialize["attributes"] = o.Attributes
	}
	return json.Marshal(toSerialize)
}

type NullableGlobalScopeBean struct {
	value *GlobalScopeBean
	isSet bool
}

func (v NullableGlobalScopeBean) Get() *GlobalScopeBean {
	return v.value
}

func (v *NullableGlobalScopeBean) Set(val *GlobalScopeBean) {
	v.value = val
	v.isSet = true
}

func (v NullableGlobalScopeBean) IsSet() bool {
	return v.isSet
}

func (v *NullableGlobalScopeBean) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGlobalScopeBean(val *GlobalScopeBean) *NullableGlobalScopeBean {
	return &NullableGlobalScopeBean{value: val, isSet: true}
}

func (v NullableGlobalScopeBean) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGlobalScopeBean) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



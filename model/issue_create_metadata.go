/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueCreateMetadata The wrapper for the issue creation metadata for a list of projects.
type IssueCreateMetadata struct {
	// Expand options that include additional project details in the response.
	Expand *string `json:"expand,omitempty"`
	// List of projects and their issue creation metadata.
	Projects *[]ProjectIssueCreateMetadata `json:"projects,omitempty"`
}

// NewIssueCreateMetadata instantiates a new IssueCreateMetadata object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueCreateMetadata() *IssueCreateMetadata {
	this := IssueCreateMetadata{}
	return &this
}

// NewIssueCreateMetadataWithDefaults instantiates a new IssueCreateMetadata object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueCreateMetadataWithDefaults() *IssueCreateMetadata {
	this := IssueCreateMetadata{}
	return &this
}

// GetExpand returns the Expand field value if set, zero value otherwise.
func (o *IssueCreateMetadata) GetExpand() string {
	if o == nil || o.Expand == nil {
		var ret string
		return ret
	}
	return *o.Expand
}

// GetExpandOk returns a tuple with the Expand field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueCreateMetadata) GetExpandOk() (*string, bool) {
	if o == nil || o.Expand == nil {
		return nil, false
	}
	return o.Expand, true
}

// HasExpand returns a boolean if a field has been set.
func (o *IssueCreateMetadata) HasExpand() bool {
	if o != nil && o.Expand != nil {
		return true
	}

	return false
}

// SetExpand gets a reference to the given string and assigns it to the Expand field.
func (o *IssueCreateMetadata) SetExpand(v string) {
	o.Expand = &v
}

// GetProjects returns the Projects field value if set, zero value otherwise.
func (o *IssueCreateMetadata) GetProjects() []ProjectIssueCreateMetadata {
	if o == nil || o.Projects == nil {
		var ret []ProjectIssueCreateMetadata
		return ret
	}
	return *o.Projects
}

// GetProjectsOk returns a tuple with the Projects field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueCreateMetadata) GetProjectsOk() (*[]ProjectIssueCreateMetadata, bool) {
	if o == nil || o.Projects == nil {
		return nil, false
	}
	return o.Projects, true
}

// HasProjects returns a boolean if a field has been set.
func (o *IssueCreateMetadata) HasProjects() bool {
	if o != nil && o.Projects != nil {
		return true
	}

	return false
}

// SetProjects gets a reference to the given []ProjectIssueCreateMetadata and assigns it to the Projects field.
func (o *IssueCreateMetadata) SetProjects(v []ProjectIssueCreateMetadata) {
	o.Projects = &v
}

func (o IssueCreateMetadata) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Expand != nil {
		toSerialize["expand"] = o.Expand
	}
	if o.Projects != nil {
		toSerialize["projects"] = o.Projects
	}
	return json.Marshal(toSerialize)
}

type NullableIssueCreateMetadata struct {
	value *IssueCreateMetadata
	isSet bool
}

func (v NullableIssueCreateMetadata) Get() *IssueCreateMetadata {
	return v.value
}

func (v *NullableIssueCreateMetadata) Set(val *IssueCreateMetadata) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueCreateMetadata) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueCreateMetadata) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueCreateMetadata(val *IssueCreateMetadata) *NullableIssueCreateMetadata {
	return &NullableIssueCreateMetadata{value: val, isSet: true}
}

func (v NullableIssueCreateMetadata) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueCreateMetadata) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



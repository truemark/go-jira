/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssuePickerSuggestions A list of issues suggested for use in auto-completion.
type IssuePickerSuggestions struct {
	// A list of issues for an issue type suggested for use in auto-completion.
	Sections *[]IssuePickerSuggestionsIssueType `json:"sections,omitempty"`
}

// NewIssuePickerSuggestions instantiates a new IssuePickerSuggestions object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssuePickerSuggestions() *IssuePickerSuggestions {
	this := IssuePickerSuggestions{}
	return &this
}

// NewIssuePickerSuggestionsWithDefaults instantiates a new IssuePickerSuggestions object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssuePickerSuggestionsWithDefaults() *IssuePickerSuggestions {
	this := IssuePickerSuggestions{}
	return &this
}

// GetSections returns the Sections field value if set, zero value otherwise.
func (o *IssuePickerSuggestions) GetSections() []IssuePickerSuggestionsIssueType {
	if o == nil || o.Sections == nil {
		var ret []IssuePickerSuggestionsIssueType
		return ret
	}
	return *o.Sections
}

// GetSectionsOk returns a tuple with the Sections field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssuePickerSuggestions) GetSectionsOk() (*[]IssuePickerSuggestionsIssueType, bool) {
	if o == nil || o.Sections == nil {
		return nil, false
	}
	return o.Sections, true
}

// HasSections returns a boolean if a field has been set.
func (o *IssuePickerSuggestions) HasSections() bool {
	if o != nil && o.Sections != nil {
		return true
	}

	return false
}

// SetSections gets a reference to the given []IssuePickerSuggestionsIssueType and assigns it to the Sections field.
func (o *IssuePickerSuggestions) SetSections(v []IssuePickerSuggestionsIssueType) {
	o.Sections = &v
}

func (o IssuePickerSuggestions) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Sections != nil {
		toSerialize["sections"] = o.Sections
	}
	return json.Marshal(toSerialize)
}

type NullableIssuePickerSuggestions struct {
	value *IssuePickerSuggestions
	isSet bool
}

func (v NullableIssuePickerSuggestions) Get() *IssuePickerSuggestions {
	return v.value
}

func (v *NullableIssuePickerSuggestions) Set(val *IssuePickerSuggestions) {
	v.value = val
	v.isSet = true
}

func (v NullableIssuePickerSuggestions) IsSet() bool {
	return v.isSet
}

func (v *NullableIssuePickerSuggestions) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssuePickerSuggestions(val *IssuePickerSuggestions) *NullableIssuePickerSuggestions {
	return &NullableIssuePickerSuggestions{value: val, isSet: true}
}

func (v NullableIssuePickerSuggestions) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssuePickerSuggestions) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueTypeCreateBean struct for IssueTypeCreateBean
type IssueTypeCreateBean struct {
	// The unique name for the issue type. The maximum length is 60 characters.
	Name string `json:"name"`
	// The description of the issue type.
	Description *string `json:"description,omitempty"`
	// Whether the issue type is `subtype` or `standard`. Defaults to `standard`.
	Type *string `json:"type,omitempty"`
}

// NewIssueTypeCreateBean instantiates a new IssueTypeCreateBean object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueTypeCreateBean(name string) *IssueTypeCreateBean {
	this := IssueTypeCreateBean{}
	this.Name = name
	return &this
}

// NewIssueTypeCreateBeanWithDefaults instantiates a new IssueTypeCreateBean object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueTypeCreateBeanWithDefaults() *IssueTypeCreateBean {
	this := IssueTypeCreateBean{}
	return &this
}

// GetName returns the Name field value
func (o *IssueTypeCreateBean) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *IssueTypeCreateBean) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *IssueTypeCreateBean) SetName(v string) {
	o.Name = v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *IssueTypeCreateBean) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueTypeCreateBean) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *IssueTypeCreateBean) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *IssueTypeCreateBean) SetDescription(v string) {
	o.Description = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *IssueTypeCreateBean) GetType() string {
	if o == nil || o.Type == nil {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueTypeCreateBean) GetTypeOk() (*string, bool) {
	if o == nil || o.Type == nil {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *IssueTypeCreateBean) HasType() bool {
	if o != nil && o.Type != nil {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *IssueTypeCreateBean) SetType(v string) {
	o.Type = &v
}

func (o IssueTypeCreateBean) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["name"] = o.Name
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.Type != nil {
		toSerialize["type"] = o.Type
	}
	return json.Marshal(toSerialize)
}

type NullableIssueTypeCreateBean struct {
	value *IssueTypeCreateBean
	isSet bool
}

func (v NullableIssueTypeCreateBean) Get() *IssueTypeCreateBean {
	return v.value
}

func (v *NullableIssueTypeCreateBean) Set(val *IssueTypeCreateBean) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueTypeCreateBean) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueTypeCreateBean) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueTypeCreateBean(val *IssueTypeCreateBean) *NullableIssueTypeCreateBean {
	return &NullableIssueTypeCreateBean{value: val, isSet: true}
}

func (v NullableIssueTypeCreateBean) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueTypeCreateBean) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueTypeInfo Details of an issue type.
type IssueTypeInfo struct {
	// The ID of the issue type.
	Id *int64 `json:"id,omitempty"`
	// The name of the issue type.
	Name *string `json:"name,omitempty"`
	// The avatar of the issue type.
	AvatarId *int64 `json:"avatarId,omitempty"`
}

// NewIssueTypeInfo instantiates a new IssueTypeInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueTypeInfo() *IssueTypeInfo {
	this := IssueTypeInfo{}
	return &this
}

// NewIssueTypeInfoWithDefaults instantiates a new IssueTypeInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueTypeInfoWithDefaults() *IssueTypeInfo {
	this := IssueTypeInfo{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *IssueTypeInfo) GetId() int64 {
	if o == nil || o.Id == nil {
		var ret int64
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueTypeInfo) GetIdOk() (*int64, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *IssueTypeInfo) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given int64 and assigns it to the Id field.
func (o *IssueTypeInfo) SetId(v int64) {
	o.Id = &v
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *IssueTypeInfo) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueTypeInfo) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *IssueTypeInfo) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *IssueTypeInfo) SetName(v string) {
	o.Name = &v
}

// GetAvatarId returns the AvatarId field value if set, zero value otherwise.
func (o *IssueTypeInfo) GetAvatarId() int64 {
	if o == nil || o.AvatarId == nil {
		var ret int64
		return ret
	}
	return *o.AvatarId
}

// GetAvatarIdOk returns a tuple with the AvatarId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueTypeInfo) GetAvatarIdOk() (*int64, bool) {
	if o == nil || o.AvatarId == nil {
		return nil, false
	}
	return o.AvatarId, true
}

// HasAvatarId returns a boolean if a field has been set.
func (o *IssueTypeInfo) HasAvatarId() bool {
	if o != nil && o.AvatarId != nil {
		return true
	}

	return false
}

// SetAvatarId gets a reference to the given int64 and assigns it to the AvatarId field.
func (o *IssueTypeInfo) SetAvatarId(v int64) {
	o.AvatarId = &v
}

func (o IssueTypeInfo) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.AvatarId != nil {
		toSerialize["avatarId"] = o.AvatarId
	}
	return json.Marshal(toSerialize)
}

type NullableIssueTypeInfo struct {
	value *IssueTypeInfo
	isSet bool
}

func (v NullableIssueTypeInfo) Get() *IssueTypeInfo {
	return v.value
}

func (v *NullableIssueTypeInfo) Set(val *IssueTypeInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueTypeInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueTypeInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueTypeInfo(val *IssueTypeInfo) *NullableIssueTypeInfo {
	return &NullableIssueTypeInfo{value: val, isSet: true}
}

func (v NullableIssueTypeInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueTypeInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



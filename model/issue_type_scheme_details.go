/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueTypeSchemeDetails Details of an issue type scheme and its associated issue types.
type IssueTypeSchemeDetails struct {
	// The name of the issue type scheme. The name must be unique. The maximum length is 255 characters.
	Name string `json:"name"`
	// The description of the issue type scheme. The maximum length is 4000 characters.
	Description *string `json:"description,omitempty"`
	// The ID of the default issue type of the issue type scheme. This ID must be included in `issueTypeIds`.
	DefaultIssueTypeId *string `json:"defaultIssueTypeId,omitempty"`
	// The list of issue types IDs of the issue type scheme. At least one standard issue type ID is required.
	IssueTypeIds []string `json:"issueTypeIds"`
}

// NewIssueTypeSchemeDetails instantiates a new IssueTypeSchemeDetails object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueTypeSchemeDetails(name string, issueTypeIds []string) *IssueTypeSchemeDetails {
	this := IssueTypeSchemeDetails{}
	this.Name = name
	this.IssueTypeIds = issueTypeIds
	return &this
}

// NewIssueTypeSchemeDetailsWithDefaults instantiates a new IssueTypeSchemeDetails object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueTypeSchemeDetailsWithDefaults() *IssueTypeSchemeDetails {
	this := IssueTypeSchemeDetails{}
	return &this
}

// GetName returns the Name field value
func (o *IssueTypeSchemeDetails) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *IssueTypeSchemeDetails) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *IssueTypeSchemeDetails) SetName(v string) {
	o.Name = v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *IssueTypeSchemeDetails) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueTypeSchemeDetails) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *IssueTypeSchemeDetails) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *IssueTypeSchemeDetails) SetDescription(v string) {
	o.Description = &v
}

// GetDefaultIssueTypeId returns the DefaultIssueTypeId field value if set, zero value otherwise.
func (o *IssueTypeSchemeDetails) GetDefaultIssueTypeId() string {
	if o == nil || o.DefaultIssueTypeId == nil {
		var ret string
		return ret
	}
	return *o.DefaultIssueTypeId
}

// GetDefaultIssueTypeIdOk returns a tuple with the DefaultIssueTypeId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueTypeSchemeDetails) GetDefaultIssueTypeIdOk() (*string, bool) {
	if o == nil || o.DefaultIssueTypeId == nil {
		return nil, false
	}
	return o.DefaultIssueTypeId, true
}

// HasDefaultIssueTypeId returns a boolean if a field has been set.
func (o *IssueTypeSchemeDetails) HasDefaultIssueTypeId() bool {
	if o != nil && o.DefaultIssueTypeId != nil {
		return true
	}

	return false
}

// SetDefaultIssueTypeId gets a reference to the given string and assigns it to the DefaultIssueTypeId field.
func (o *IssueTypeSchemeDetails) SetDefaultIssueTypeId(v string) {
	o.DefaultIssueTypeId = &v
}

// GetIssueTypeIds returns the IssueTypeIds field value
func (o *IssueTypeSchemeDetails) GetIssueTypeIds() []string {
	if o == nil {
		var ret []string
		return ret
	}

	return o.IssueTypeIds
}

// GetIssueTypeIdsOk returns a tuple with the IssueTypeIds field value
// and a boolean to check if the value has been set.
func (o *IssueTypeSchemeDetails) GetIssueTypeIdsOk() (*[]string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.IssueTypeIds, true
}

// SetIssueTypeIds sets field value
func (o *IssueTypeSchemeDetails) SetIssueTypeIds(v []string) {
	o.IssueTypeIds = v
}

func (o IssueTypeSchemeDetails) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["name"] = o.Name
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.DefaultIssueTypeId != nil {
		toSerialize["defaultIssueTypeId"] = o.DefaultIssueTypeId
	}
	if true {
		toSerialize["issueTypeIds"] = o.IssueTypeIds
	}
	return json.Marshal(toSerialize)
}

type NullableIssueTypeSchemeDetails struct {
	value *IssueTypeSchemeDetails
	isSet bool
}

func (v NullableIssueTypeSchemeDetails) Get() *IssueTypeSchemeDetails {
	return v.value
}

func (v *NullableIssueTypeSchemeDetails) Set(val *IssueTypeSchemeDetails) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueTypeSchemeDetails) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueTypeSchemeDetails) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueTypeSchemeDetails(val *IssueTypeSchemeDetails) *NullableIssueTypeSchemeDetails {
	return &NullableIssueTypeSchemeDetails{value: val, isSet: true}
}

func (v NullableIssueTypeSchemeDetails) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueTypeSchemeDetails) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



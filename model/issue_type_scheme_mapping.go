/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueTypeSchemeMapping Issue type scheme item.
type IssueTypeSchemeMapping struct {
	// The ID of the issue type scheme.
	IssueTypeSchemeId string `json:"issueTypeSchemeId"`
	// The ID of the issue type.
	IssueTypeId string `json:"issueTypeId"`
}

// NewIssueTypeSchemeMapping instantiates a new IssueTypeSchemeMapping object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueTypeSchemeMapping(issueTypeSchemeId string, issueTypeId string) *IssueTypeSchemeMapping {
	this := IssueTypeSchemeMapping{}
	this.IssueTypeSchemeId = issueTypeSchemeId
	this.IssueTypeId = issueTypeId
	return &this
}

// NewIssueTypeSchemeMappingWithDefaults instantiates a new IssueTypeSchemeMapping object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueTypeSchemeMappingWithDefaults() *IssueTypeSchemeMapping {
	this := IssueTypeSchemeMapping{}
	return &this
}

// GetIssueTypeSchemeId returns the IssueTypeSchemeId field value
func (o *IssueTypeSchemeMapping) GetIssueTypeSchemeId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.IssueTypeSchemeId
}

// GetIssueTypeSchemeIdOk returns a tuple with the IssueTypeSchemeId field value
// and a boolean to check if the value has been set.
func (o *IssueTypeSchemeMapping) GetIssueTypeSchemeIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.IssueTypeSchemeId, true
}

// SetIssueTypeSchemeId sets field value
func (o *IssueTypeSchemeMapping) SetIssueTypeSchemeId(v string) {
	o.IssueTypeSchemeId = v
}

// GetIssueTypeId returns the IssueTypeId field value
func (o *IssueTypeSchemeMapping) GetIssueTypeId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.IssueTypeId
}

// GetIssueTypeIdOk returns a tuple with the IssueTypeId field value
// and a boolean to check if the value has been set.
func (o *IssueTypeSchemeMapping) GetIssueTypeIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.IssueTypeId, true
}

// SetIssueTypeId sets field value
func (o *IssueTypeSchemeMapping) SetIssueTypeId(v string) {
	o.IssueTypeId = v
}

func (o IssueTypeSchemeMapping) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["issueTypeSchemeId"] = o.IssueTypeSchemeId
	}
	if true {
		toSerialize["issueTypeId"] = o.IssueTypeId
	}
	return json.Marshal(toSerialize)
}

type NullableIssueTypeSchemeMapping struct {
	value *IssueTypeSchemeMapping
	isSet bool
}

func (v NullableIssueTypeSchemeMapping) Get() *IssueTypeSchemeMapping {
	return v.value
}

func (v *NullableIssueTypeSchemeMapping) Set(val *IssueTypeSchemeMapping) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueTypeSchemeMapping) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueTypeSchemeMapping) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueTypeSchemeMapping(val *IssueTypeSchemeMapping) *NullableIssueTypeSchemeMapping {
	return &NullableIssueTypeSchemeMapping{value: val, isSet: true}
}

func (v NullableIssueTypeSchemeMapping) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueTypeSchemeMapping) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueTypeScreenSchemeId The ID of an issue type screen scheme.
type IssueTypeScreenSchemeId struct {
	// The ID of the issue type screen scheme.
	Id string `json:"id"`
}

// NewIssueTypeScreenSchemeId instantiates a new IssueTypeScreenSchemeId object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueTypeScreenSchemeId(id string) *IssueTypeScreenSchemeId {
	this := IssueTypeScreenSchemeId{}
	this.Id = id
	return &this
}

// NewIssueTypeScreenSchemeIdWithDefaults instantiates a new IssueTypeScreenSchemeId object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueTypeScreenSchemeIdWithDefaults() *IssueTypeScreenSchemeId {
	this := IssueTypeScreenSchemeId{}
	return &this
}

// GetId returns the Id field value
func (o *IssueTypeScreenSchemeId) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *IssueTypeScreenSchemeId) GetIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *IssueTypeScreenSchemeId) SetId(v string) {
	o.Id = v
}

func (o IssueTypeScreenSchemeId) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	return json.Marshal(toSerialize)
}

type NullableIssueTypeScreenSchemeId struct {
	value *IssueTypeScreenSchemeId
	isSet bool
}

func (v NullableIssueTypeScreenSchemeId) Get() *IssueTypeScreenSchemeId {
	return v.value
}

func (v *NullableIssueTypeScreenSchemeId) Set(val *IssueTypeScreenSchemeId) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueTypeScreenSchemeId) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueTypeScreenSchemeId) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueTypeScreenSchemeId(val *IssueTypeScreenSchemeId) *NullableIssueTypeScreenSchemeId {
	return &NullableIssueTypeScreenSchemeId{value: val, isSet: true}
}

func (v NullableIssueTypeScreenSchemeId) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueTypeScreenSchemeId) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



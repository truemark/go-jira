/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueTypeWithStatus Status details for an issue type.
type IssueTypeWithStatus struct {
	// The URL of the issue type's status details.
	Self string `json:"self"`
	// The ID of the issue type.
	Id string `json:"id"`
	// The name of the issue type.
	Name string `json:"name"`
	// Whether this issue type represents subtasks.
	Subtask bool `json:"subtask"`
	// List of status details for the issue type.
	Statuses []StatusDetails `json:"statuses"`
}

// NewIssueTypeWithStatus instantiates a new IssueTypeWithStatus object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueTypeWithStatus(self string, id string, name string, subtask bool, statuses []StatusDetails) *IssueTypeWithStatus {
	this := IssueTypeWithStatus{}
	this.Self = self
	this.Id = id
	this.Name = name
	this.Subtask = subtask
	this.Statuses = statuses
	return &this
}

// NewIssueTypeWithStatusWithDefaults instantiates a new IssueTypeWithStatus object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueTypeWithStatusWithDefaults() *IssueTypeWithStatus {
	this := IssueTypeWithStatus{}
	return &this
}

// GetSelf returns the Self field value
func (o *IssueTypeWithStatus) GetSelf() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Self
}

// GetSelfOk returns a tuple with the Self field value
// and a boolean to check if the value has been set.
func (o *IssueTypeWithStatus) GetSelfOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Self, true
}

// SetSelf sets field value
func (o *IssueTypeWithStatus) SetSelf(v string) {
	o.Self = v
}

// GetId returns the Id field value
func (o *IssueTypeWithStatus) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *IssueTypeWithStatus) GetIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *IssueTypeWithStatus) SetId(v string) {
	o.Id = v
}

// GetName returns the Name field value
func (o *IssueTypeWithStatus) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *IssueTypeWithStatus) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *IssueTypeWithStatus) SetName(v string) {
	o.Name = v
}

// GetSubtask returns the Subtask field value
func (o *IssueTypeWithStatus) GetSubtask() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.Subtask
}

// GetSubtaskOk returns a tuple with the Subtask field value
// and a boolean to check if the value has been set.
func (o *IssueTypeWithStatus) GetSubtaskOk() (*bool, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Subtask, true
}

// SetSubtask sets field value
func (o *IssueTypeWithStatus) SetSubtask(v bool) {
	o.Subtask = v
}

// GetStatuses returns the Statuses field value
func (o *IssueTypeWithStatus) GetStatuses() []StatusDetails {
	if o == nil {
		var ret []StatusDetails
		return ret
	}

	return o.Statuses
}

// GetStatusesOk returns a tuple with the Statuses field value
// and a boolean to check if the value has been set.
func (o *IssueTypeWithStatus) GetStatusesOk() (*[]StatusDetails, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Statuses, true
}

// SetStatuses sets field value
func (o *IssueTypeWithStatus) SetStatuses(v []StatusDetails) {
	o.Statuses = v
}

func (o IssueTypeWithStatus) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["self"] = o.Self
	}
	if true {
		toSerialize["id"] = o.Id
	}
	if true {
		toSerialize["name"] = o.Name
	}
	if true {
		toSerialize["subtask"] = o.Subtask
	}
	if true {
		toSerialize["statuses"] = o.Statuses
	}
	return json.Marshal(toSerialize)
}

type NullableIssueTypeWithStatus struct {
	value *IssueTypeWithStatus
	isSet bool
}

func (v NullableIssueTypeWithStatus) Get() *IssueTypeWithStatus {
	return v.value
}

func (v *NullableIssueTypeWithStatus) Set(val *IssueTypeWithStatus) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueTypeWithStatus) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueTypeWithStatus) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueTypeWithStatus(val *IssueTypeWithStatus) *NullableIssueTypeWithStatus {
	return &NullableIssueTypeWithStatus{value: val, isSet: true}
}

func (v NullableIssueTypeWithStatus) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueTypeWithStatus) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



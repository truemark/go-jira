/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// IssueUpdateDetails Details of an issue update request.
type IssueUpdateDetails struct {
	// Details of a transition. Required when performing a transition, optional when creating or editing an issue.
	Transition *IssueTransition `json:"transition,omitempty"`
	// List of issue screen fields to update, specifying the sub-field to update and its value for each field. This field provides a straightforward option when setting a sub-field. When multiple sub-fields or other operations are required, use `update`. Fields included in here cannot be included in `update`.
	Fields *map[string]interface{} `json:"fields,omitempty"`
	// List of operations to perform on issue screen fields. Note that fields included in here cannot be included in `fields`.
	Update *map[string][]FieldUpdateOperation `json:"update,omitempty"`
	// Additional issue history details.
	HistoryMetadata *HistoryMetadata `json:"historyMetadata,omitempty"`
	// Details of issue properties to be add or update.
	Properties *[]EntityProperty `json:"properties,omitempty"`
	AdditionalProperties map[string]interface{}
}

type _IssueUpdateDetails IssueUpdateDetails

// NewIssueUpdateDetails instantiates a new IssueUpdateDetails object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewIssueUpdateDetails() *IssueUpdateDetails {
	this := IssueUpdateDetails{}
	return &this
}

// NewIssueUpdateDetailsWithDefaults instantiates a new IssueUpdateDetails object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewIssueUpdateDetailsWithDefaults() *IssueUpdateDetails {
	this := IssueUpdateDetails{}
	return &this
}

// GetTransition returns the Transition field value if set, zero value otherwise.
func (o *IssueUpdateDetails) GetTransition() IssueTransition {
	if o == nil || o.Transition == nil {
		var ret IssueTransition
		return ret
	}
	return *o.Transition
}

// GetTransitionOk returns a tuple with the Transition field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueUpdateDetails) GetTransitionOk() (*IssueTransition, bool) {
	if o == nil || o.Transition == nil {
		return nil, false
	}
	return o.Transition, true
}

// HasTransition returns a boolean if a field has been set.
func (o *IssueUpdateDetails) HasTransition() bool {
	if o != nil && o.Transition != nil {
		return true
	}

	return false
}

// SetTransition gets a reference to the given IssueTransition and assigns it to the Transition field.
func (o *IssueUpdateDetails) SetTransition(v IssueTransition) {
	o.Transition = &v
}

// GetFields returns the Fields field value if set, zero value otherwise.
func (o *IssueUpdateDetails) GetFields() map[string]interface{} {
	if o == nil || o.Fields == nil {
		var ret map[string]interface{}
		return ret
	}
	return *o.Fields
}

// GetFieldsOk returns a tuple with the Fields field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueUpdateDetails) GetFieldsOk() (*map[string]interface{}, bool) {
	if o == nil || o.Fields == nil {
		return nil, false
	}
	return o.Fields, true
}

// HasFields returns a boolean if a field has been set.
func (o *IssueUpdateDetails) HasFields() bool {
	if o != nil && o.Fields != nil {
		return true
	}

	return false
}

// SetFields gets a reference to the given map[string]interface{} and assigns it to the Fields field.
func (o *IssueUpdateDetails) SetFields(v map[string]interface{}) {
	o.Fields = &v
}

// GetUpdate returns the Update field value if set, zero value otherwise.
func (o *IssueUpdateDetails) GetUpdate() map[string][]FieldUpdateOperation {
	if o == nil || o.Update == nil {
		var ret map[string][]FieldUpdateOperation
		return ret
	}
	return *o.Update
}

// GetUpdateOk returns a tuple with the Update field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueUpdateDetails) GetUpdateOk() (*map[string][]FieldUpdateOperation, bool) {
	if o == nil || o.Update == nil {
		return nil, false
	}
	return o.Update, true
}

// HasUpdate returns a boolean if a field has been set.
func (o *IssueUpdateDetails) HasUpdate() bool {
	if o != nil && o.Update != nil {
		return true
	}

	return false
}

// SetUpdate gets a reference to the given map[string][]FieldUpdateOperation and assigns it to the Update field.
func (o *IssueUpdateDetails) SetUpdate(v map[string][]FieldUpdateOperation) {
	o.Update = &v
}

// GetHistoryMetadata returns the HistoryMetadata field value if set, zero value otherwise.
func (o *IssueUpdateDetails) GetHistoryMetadata() HistoryMetadata {
	if o == nil || o.HistoryMetadata == nil {
		var ret HistoryMetadata
		return ret
	}
	return *o.HistoryMetadata
}

// GetHistoryMetadataOk returns a tuple with the HistoryMetadata field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueUpdateDetails) GetHistoryMetadataOk() (*HistoryMetadata, bool) {
	if o == nil || o.HistoryMetadata == nil {
		return nil, false
	}
	return o.HistoryMetadata, true
}

// HasHistoryMetadata returns a boolean if a field has been set.
func (o *IssueUpdateDetails) HasHistoryMetadata() bool {
	if o != nil && o.HistoryMetadata != nil {
		return true
	}

	return false
}

// SetHistoryMetadata gets a reference to the given HistoryMetadata and assigns it to the HistoryMetadata field.
func (o *IssueUpdateDetails) SetHistoryMetadata(v HistoryMetadata) {
	o.HistoryMetadata = &v
}

// GetProperties returns the Properties field value if set, zero value otherwise.
func (o *IssueUpdateDetails) GetProperties() []EntityProperty {
	if o == nil || o.Properties == nil {
		var ret []EntityProperty
		return ret
	}
	return *o.Properties
}

// GetPropertiesOk returns a tuple with the Properties field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *IssueUpdateDetails) GetPropertiesOk() (*[]EntityProperty, bool) {
	if o == nil || o.Properties == nil {
		return nil, false
	}
	return o.Properties, true
}

// HasProperties returns a boolean if a field has been set.
func (o *IssueUpdateDetails) HasProperties() bool {
	if o != nil && o.Properties != nil {
		return true
	}

	return false
}

// SetProperties gets a reference to the given []EntityProperty and assigns it to the Properties field.
func (o *IssueUpdateDetails) SetProperties(v []EntityProperty) {
	o.Properties = &v
}

func (o IssueUpdateDetails) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Transition != nil {
		toSerialize["transition"] = o.Transition
	}
	if o.Fields != nil {
		toSerialize["fields"] = o.Fields
	}
	if o.Update != nil {
		toSerialize["update"] = o.Update
	}
	if o.HistoryMetadata != nil {
		toSerialize["historyMetadata"] = o.HistoryMetadata
	}
	if o.Properties != nil {
		toSerialize["properties"] = o.Properties
	}

	for key, value := range o.AdditionalProperties {
		toSerialize[key] = value
	}

	return json.Marshal(toSerialize)
}

func (o *IssueUpdateDetails) UnmarshalJSON(bytes []byte) (err error) {
	varIssueUpdateDetails := _IssueUpdateDetails{}

	if err = json.Unmarshal(bytes, &varIssueUpdateDetails); err == nil {
		*o = IssueUpdateDetails(varIssueUpdateDetails)
	}

	additionalProperties := make(map[string]interface{})

	if err = json.Unmarshal(bytes, &additionalProperties); err == nil {
		delete(additionalProperties, "transition")
		delete(additionalProperties, "fields")
		delete(additionalProperties, "update")
		delete(additionalProperties, "historyMetadata")
		delete(additionalProperties, "properties")
		o.AdditionalProperties = additionalProperties
	}

	return err
}

type NullableIssueUpdateDetails struct {
	value *IssueUpdateDetails
	isSet bool
}

func (v NullableIssueUpdateDetails) Get() *IssueUpdateDetails {
	return v.value
}

func (v *NullableIssueUpdateDetails) Set(val *IssueUpdateDetails) {
	v.value = val
	v.isSet = true
}

func (v NullableIssueUpdateDetails) IsSet() bool {
	return v.isSet
}

func (v *NullableIssueUpdateDetails) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableIssueUpdateDetails(val *IssueUpdateDetails) *NullableIssueUpdateDetails {
	return &NullableIssueUpdateDetails{value: val, isSet: true}
}

func (v NullableIssueUpdateDetails) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableIssueUpdateDetails) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// JiraExpressionAnalysis Details about the analysed Jira expression.
type JiraExpressionAnalysis struct {
	// The analysed expression.
	Expression string `json:"expression"`
	// A list of validation errors. Not included if the expression is valid.
	Errors *[]JiraExpressionValidationError `json:"errors,omitempty"`
	// Whether the expression is valid and the interpreter will evaluate it. Note that the expression may fail at runtime (for example, if it executes too many expensive operations).
	Valid bool `json:"valid"`
	// EXPERIMENTAL. The inferred type of the expression.
	Type *string `json:"type,omitempty"`
	Complexity *JiraExpressionComplexity `json:"complexity,omitempty"`
}

// NewJiraExpressionAnalysis instantiates a new JiraExpressionAnalysis object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewJiraExpressionAnalysis(expression string, valid bool) *JiraExpressionAnalysis {
	this := JiraExpressionAnalysis{}
	this.Expression = expression
	this.Valid = valid
	return &this
}

// NewJiraExpressionAnalysisWithDefaults instantiates a new JiraExpressionAnalysis object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewJiraExpressionAnalysisWithDefaults() *JiraExpressionAnalysis {
	this := JiraExpressionAnalysis{}
	return &this
}

// GetExpression returns the Expression field value
func (o *JiraExpressionAnalysis) GetExpression() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Expression
}

// GetExpressionOk returns a tuple with the Expression field value
// and a boolean to check if the value has been set.
func (o *JiraExpressionAnalysis) GetExpressionOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Expression, true
}

// SetExpression sets field value
func (o *JiraExpressionAnalysis) SetExpression(v string) {
	o.Expression = v
}

// GetErrors returns the Errors field value if set, zero value otherwise.
func (o *JiraExpressionAnalysis) GetErrors() []JiraExpressionValidationError {
	if o == nil || o.Errors == nil {
		var ret []JiraExpressionValidationError
		return ret
	}
	return *o.Errors
}

// GetErrorsOk returns a tuple with the Errors field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *JiraExpressionAnalysis) GetErrorsOk() (*[]JiraExpressionValidationError, bool) {
	if o == nil || o.Errors == nil {
		return nil, false
	}
	return o.Errors, true
}

// HasErrors returns a boolean if a field has been set.
func (o *JiraExpressionAnalysis) HasErrors() bool {
	if o != nil && o.Errors != nil {
		return true
	}

	return false
}

// SetErrors gets a reference to the given []JiraExpressionValidationError and assigns it to the Errors field.
func (o *JiraExpressionAnalysis) SetErrors(v []JiraExpressionValidationError) {
	o.Errors = &v
}

// GetValid returns the Valid field value
func (o *JiraExpressionAnalysis) GetValid() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.Valid
}

// GetValidOk returns a tuple with the Valid field value
// and a boolean to check if the value has been set.
func (o *JiraExpressionAnalysis) GetValidOk() (*bool, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Valid, true
}

// SetValid sets field value
func (o *JiraExpressionAnalysis) SetValid(v bool) {
	o.Valid = v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *JiraExpressionAnalysis) GetType() string {
	if o == nil || o.Type == nil {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *JiraExpressionAnalysis) GetTypeOk() (*string, bool) {
	if o == nil || o.Type == nil {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *JiraExpressionAnalysis) HasType() bool {
	if o != nil && o.Type != nil {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *JiraExpressionAnalysis) SetType(v string) {
	o.Type = &v
}

// GetComplexity returns the Complexity field value if set, zero value otherwise.
func (o *JiraExpressionAnalysis) GetComplexity() JiraExpressionComplexity {
	if o == nil || o.Complexity == nil {
		var ret JiraExpressionComplexity
		return ret
	}
	return *o.Complexity
}

// GetComplexityOk returns a tuple with the Complexity field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *JiraExpressionAnalysis) GetComplexityOk() (*JiraExpressionComplexity, bool) {
	if o == nil || o.Complexity == nil {
		return nil, false
	}
	return o.Complexity, true
}

// HasComplexity returns a boolean if a field has been set.
func (o *JiraExpressionAnalysis) HasComplexity() bool {
	if o != nil && o.Complexity != nil {
		return true
	}

	return false
}

// SetComplexity gets a reference to the given JiraExpressionComplexity and assigns it to the Complexity field.
func (o *JiraExpressionAnalysis) SetComplexity(v JiraExpressionComplexity) {
	o.Complexity = &v
}

func (o JiraExpressionAnalysis) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["expression"] = o.Expression
	}
	if o.Errors != nil {
		toSerialize["errors"] = o.Errors
	}
	if true {
		toSerialize["valid"] = o.Valid
	}
	if o.Type != nil {
		toSerialize["type"] = o.Type
	}
	if o.Complexity != nil {
		toSerialize["complexity"] = o.Complexity
	}
	return json.Marshal(toSerialize)
}

type NullableJiraExpressionAnalysis struct {
	value *JiraExpressionAnalysis
	isSet bool
}

func (v NullableJiraExpressionAnalysis) Get() *JiraExpressionAnalysis {
	return v.value
}

func (v *NullableJiraExpressionAnalysis) Set(val *JiraExpressionAnalysis) {
	v.value = val
	v.isSet = true
}

func (v NullableJiraExpressionAnalysis) IsSet() bool {
	return v.isSet
}

func (v *NullableJiraExpressionAnalysis) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableJiraExpressionAnalysis(val *JiraExpressionAnalysis) *NullableJiraExpressionAnalysis {
	return &NullableJiraExpressionAnalysis{value: val, isSet: true}
}

func (v NullableJiraExpressionAnalysis) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableJiraExpressionAnalysis) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
	"fmt"
)

// JqlQueryClauseOperand Details of an operand in a JQL clause.
type JqlQueryClauseOperand struct {
	FunctionOperand *FunctionOperand
	KeywordOperand *KeywordOperand
	ListOperand *ListOperand
	ValueOperand *ValueOperand
}

// Unmarshal JSON data into any of the pointers in the struct
func (dst *JqlQueryClauseOperand) UnmarshalJSON(data []byte) error {
	var err error
	// try to unmarshal JSON data into FunctionOperand
	err = json.Unmarshal(data, &dst.FunctionOperand);
	if err == nil {
		jsonFunctionOperand, _ := json.Marshal(dst.FunctionOperand)
		if string(jsonFunctionOperand) == "{}" { // empty struct
			dst.FunctionOperand = nil
		} else {
			return nil // data stored in dst.FunctionOperand, return on the first match
		}
	} else {
		dst.FunctionOperand = nil
	}

	// try to unmarshal JSON data into KeywordOperand
	err = json.Unmarshal(data, &dst.KeywordOperand);
	if err == nil {
		jsonKeywordOperand, _ := json.Marshal(dst.KeywordOperand)
		if string(jsonKeywordOperand) == "{}" { // empty struct
			dst.KeywordOperand = nil
		} else {
			return nil // data stored in dst.KeywordOperand, return on the first match
		}
	} else {
		dst.KeywordOperand = nil
	}

	// try to unmarshal JSON data into ListOperand
	err = json.Unmarshal(data, &dst.ListOperand);
	if err == nil {
		jsonListOperand, _ := json.Marshal(dst.ListOperand)
		if string(jsonListOperand) == "{}" { // empty struct
			dst.ListOperand = nil
		} else {
			return nil // data stored in dst.ListOperand, return on the first match
		}
	} else {
		dst.ListOperand = nil
	}

	// try to unmarshal JSON data into ValueOperand
	err = json.Unmarshal(data, &dst.ValueOperand);
	if err == nil {
		jsonValueOperand, _ := json.Marshal(dst.ValueOperand)
		if string(jsonValueOperand) == "{}" { // empty struct
			dst.ValueOperand = nil
		} else {
			return nil // data stored in dst.ValueOperand, return on the first match
		}
	} else {
		dst.ValueOperand = nil
	}

	return fmt.Errorf("Data failed to match schemas in anyOf(JqlQueryClauseOperand)")
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src *JqlQueryClauseOperand) MarshalJSON() ([]byte, error) {
	if src.FunctionOperand != nil {
		return json.Marshal(&src.FunctionOperand)
	}

	if src.KeywordOperand != nil {
		return json.Marshal(&src.KeywordOperand)
	}

	if src.ListOperand != nil {
		return json.Marshal(&src.ListOperand)
	}

	if src.ValueOperand != nil {
		return json.Marshal(&src.ValueOperand)
	}

	return nil, nil // no data in anyOf schemas
}

type NullableJqlQueryClauseOperand struct {
	value *JqlQueryClauseOperand
	isSet bool
}

func (v NullableJqlQueryClauseOperand) Get() *JqlQueryClauseOperand {
	return v.value
}

func (v *NullableJqlQueryClauseOperand) Set(val *JqlQueryClauseOperand) {
	v.value = val
	v.isSet = true
}

func (v NullableJqlQueryClauseOperand) IsSet() bool {
	return v.isSet
}

func (v *NullableJqlQueryClauseOperand) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableJqlQueryClauseOperand(val *JqlQueryClauseOperand) *NullableJqlQueryClauseOperand {
	return &NullableJqlQueryClauseOperand{value: val, isSet: true}
}

func (v NullableJqlQueryClauseOperand) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableJqlQueryClauseOperand) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



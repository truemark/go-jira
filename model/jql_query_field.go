/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// JqlQueryField A field used in a JQL query. See [Advanced searching - fields reference](https://confluence.atlassian.com/x/dAiiLQ) for more information about fields in JQL queries.
type JqlQueryField struct {
	// The name of the field.
	Name string `json:"name"`
	// When the field refers to a value in an entity property, details of the entity property value.
	Property *[]JqlQueryFieldEntityProperty `json:"property,omitempty"`
}

// NewJqlQueryField instantiates a new JqlQueryField object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewJqlQueryField(name string) *JqlQueryField {
	this := JqlQueryField{}
	this.Name = name
	return &this
}

// NewJqlQueryFieldWithDefaults instantiates a new JqlQueryField object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewJqlQueryFieldWithDefaults() *JqlQueryField {
	this := JqlQueryField{}
	return &this
}

// GetName returns the Name field value
func (o *JqlQueryField) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *JqlQueryField) GetNameOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *JqlQueryField) SetName(v string) {
	o.Name = v
}

// GetProperty returns the Property field value if set, zero value otherwise.
func (o *JqlQueryField) GetProperty() []JqlQueryFieldEntityProperty {
	if o == nil || o.Property == nil {
		var ret []JqlQueryFieldEntityProperty
		return ret
	}
	return *o.Property
}

// GetPropertyOk returns a tuple with the Property field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *JqlQueryField) GetPropertyOk() (*[]JqlQueryFieldEntityProperty, bool) {
	if o == nil || o.Property == nil {
		return nil, false
	}
	return o.Property, true
}

// HasProperty returns a boolean if a field has been set.
func (o *JqlQueryField) HasProperty() bool {
	if o != nil && o.Property != nil {
		return true
	}

	return false
}

// SetProperty gets a reference to the given []JqlQueryFieldEntityProperty and assigns it to the Property field.
func (o *JqlQueryField) SetProperty(v []JqlQueryFieldEntityProperty) {
	o.Property = &v
}

func (o JqlQueryField) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["name"] = o.Name
	}
	if o.Property != nil {
		toSerialize["property"] = o.Property
	}
	return json.Marshal(toSerialize)
}

type NullableJqlQueryField struct {
	value *JqlQueryField
	isSet bool
}

func (v NullableJqlQueryField) Get() *JqlQueryField {
	return v.value
}

func (v *NullableJqlQueryField) Set(val *JqlQueryField) {
	v.value = val
	v.isSet = true
}

func (v NullableJqlQueryField) IsSet() bool {
	return v.isSet
}

func (v *NullableJqlQueryField) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableJqlQueryField(val *JqlQueryField) *NullableJqlQueryField {
	return &NullableJqlQueryField{value: val, isSet: true}
}

func (v NullableJqlQueryField) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableJqlQueryField) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



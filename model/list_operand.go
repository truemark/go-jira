/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ListOperand An operand that is a list of values.
type ListOperand struct {
	// The list of operand values.
	Values []JqlQueryUnitaryOperand `json:"values"`
	AdditionalProperties map[string]interface{}
}

type _ListOperand ListOperand

// NewListOperand instantiates a new ListOperand object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewListOperand(values []JqlQueryUnitaryOperand) *ListOperand {
	this := ListOperand{}
	this.Values = values
	return &this
}

// NewListOperandWithDefaults instantiates a new ListOperand object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewListOperandWithDefaults() *ListOperand {
	this := ListOperand{}
	return &this
}

// GetValues returns the Values field value
func (o *ListOperand) GetValues() []JqlQueryUnitaryOperand {
	if o == nil {
		var ret []JqlQueryUnitaryOperand
		return ret
	}

	return o.Values
}

// GetValuesOk returns a tuple with the Values field value
// and a boolean to check if the value has been set.
func (o *ListOperand) GetValuesOk() (*[]JqlQueryUnitaryOperand, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Values, true
}

// SetValues sets field value
func (o *ListOperand) SetValues(v []JqlQueryUnitaryOperand) {
	o.Values = v
}

func (o ListOperand) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["values"] = o.Values
	}

	for key, value := range o.AdditionalProperties {
		toSerialize[key] = value
	}

	return json.Marshal(toSerialize)
}

func (o *ListOperand) UnmarshalJSON(bytes []byte) (err error) {
	varListOperand := _ListOperand{}

	if err = json.Unmarshal(bytes, &varListOperand); err == nil {
		*o = ListOperand(varListOperand)
	}

	additionalProperties := make(map[string]interface{})

	if err = json.Unmarshal(bytes, &additionalProperties); err == nil {
		delete(additionalProperties, "values")
		o.AdditionalProperties = additionalProperties
	}

	return err
}

type NullableListOperand struct {
	value *ListOperand
	isSet bool
}

func (v NullableListOperand) Get() *ListOperand {
	return v.value
}

func (v *NullableListOperand) Set(val *ListOperand) {
	v.value = val
	v.isSet = true
}

func (v NullableListOperand) IsSet() bool {
	return v.isSet
}

func (v *NullableListOperand) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableListOperand(val *ListOperand) *NullableListOperand {
	return &NullableListOperand{value: val, isSet: true}
}

func (v NullableListOperand) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableListOperand) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// Operations Details of the operations that can be performed on the issue.
type Operations struct {
	// Details of the link groups defining issue operations.
	LinkGroups *[]LinkGroup `json:"linkGroups,omitempty"`
	AdditionalProperties map[string]interface{}
}

type _Operations Operations

// NewOperations instantiates a new Operations object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewOperations() *Operations {
	this := Operations{}
	return &this
}

// NewOperationsWithDefaults instantiates a new Operations object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewOperationsWithDefaults() *Operations {
	this := Operations{}
	return &this
}

// GetLinkGroups returns the LinkGroups field value if set, zero value otherwise.
func (o *Operations) GetLinkGroups() []LinkGroup {
	if o == nil || o.LinkGroups == nil {
		var ret []LinkGroup
		return ret
	}
	return *o.LinkGroups
}

// GetLinkGroupsOk returns a tuple with the LinkGroups field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Operations) GetLinkGroupsOk() (*[]LinkGroup, bool) {
	if o == nil || o.LinkGroups == nil {
		return nil, false
	}
	return o.LinkGroups, true
}

// HasLinkGroups returns a boolean if a field has been set.
func (o *Operations) HasLinkGroups() bool {
	if o != nil && o.LinkGroups != nil {
		return true
	}

	return false
}

// SetLinkGroups gets a reference to the given []LinkGroup and assigns it to the LinkGroups field.
func (o *Operations) SetLinkGroups(v []LinkGroup) {
	o.LinkGroups = &v
}

func (o Operations) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.LinkGroups != nil {
		toSerialize["linkGroups"] = o.LinkGroups
	}

	for key, value := range o.AdditionalProperties {
		toSerialize[key] = value
	}

	return json.Marshal(toSerialize)
}

func (o *Operations) UnmarshalJSON(bytes []byte) (err error) {
	varOperations := _Operations{}

	if err = json.Unmarshal(bytes, &varOperations); err == nil {
		*o = Operations(varOperations)
	}

	additionalProperties := make(map[string]interface{})

	if err = json.Unmarshal(bytes, &additionalProperties); err == nil {
		delete(additionalProperties, "linkGroups")
		o.AdditionalProperties = additionalProperties
	}

	return err
}

type NullableOperations struct {
	value *Operations
	isSet bool
}

func (v NullableOperations) Get() *Operations {
	return v.value
}

func (v *NullableOperations) Set(val *Operations) {
	v.value = val
	v.isSet = true
}

func (v NullableOperations) IsSet() bool {
	return v.isSet
}

func (v *NullableOperations) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableOperations(val *Operations) *NullableOperations {
	return &NullableOperations{value: val, isSet: true}
}

func (v NullableOperations) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableOperations) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



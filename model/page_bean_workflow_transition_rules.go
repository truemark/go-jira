/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// PageBeanWorkflowTransitionRules A page of items.
type PageBeanWorkflowTransitionRules struct {
	// The URL of the page.
	Self *string `json:"self,omitempty"`
	// If there is another page of results, the URL of the next page.
	NextPage *string `json:"nextPage,omitempty"`
	// The maximum number of items that could be returned.
	MaxResults *int32 `json:"maxResults,omitempty"`
	// The index of the first item returned.
	StartAt *int64 `json:"startAt,omitempty"`
	// The number of items returned.
	Total *int64 `json:"total,omitempty"`
	// Whether this is the last page.
	IsLast *bool `json:"isLast,omitempty"`
	// The list of items.
	Values *[]WorkflowTransitionRules `json:"values,omitempty"`
}

// NewPageBeanWorkflowTransitionRules instantiates a new PageBeanWorkflowTransitionRules object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPageBeanWorkflowTransitionRules() *PageBeanWorkflowTransitionRules {
	this := PageBeanWorkflowTransitionRules{}
	return &this
}

// NewPageBeanWorkflowTransitionRulesWithDefaults instantiates a new PageBeanWorkflowTransitionRules object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPageBeanWorkflowTransitionRulesWithDefaults() *PageBeanWorkflowTransitionRules {
	this := PageBeanWorkflowTransitionRules{}
	return &this
}

// GetSelf returns the Self field value if set, zero value otherwise.
func (o *PageBeanWorkflowTransitionRules) GetSelf() string {
	if o == nil || o.Self == nil {
		var ret string
		return ret
	}
	return *o.Self
}

// GetSelfOk returns a tuple with the Self field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PageBeanWorkflowTransitionRules) GetSelfOk() (*string, bool) {
	if o == nil || o.Self == nil {
		return nil, false
	}
	return o.Self, true
}

// HasSelf returns a boolean if a field has been set.
func (o *PageBeanWorkflowTransitionRules) HasSelf() bool {
	if o != nil && o.Self != nil {
		return true
	}

	return false
}

// SetSelf gets a reference to the given string and assigns it to the Self field.
func (o *PageBeanWorkflowTransitionRules) SetSelf(v string) {
	o.Self = &v
}

// GetNextPage returns the NextPage field value if set, zero value otherwise.
func (o *PageBeanWorkflowTransitionRules) GetNextPage() string {
	if o == nil || o.NextPage == nil {
		var ret string
		return ret
	}
	return *o.NextPage
}

// GetNextPageOk returns a tuple with the NextPage field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PageBeanWorkflowTransitionRules) GetNextPageOk() (*string, bool) {
	if o == nil || o.NextPage == nil {
		return nil, false
	}
	return o.NextPage, true
}

// HasNextPage returns a boolean if a field has been set.
func (o *PageBeanWorkflowTransitionRules) HasNextPage() bool {
	if o != nil && o.NextPage != nil {
		return true
	}

	return false
}

// SetNextPage gets a reference to the given string and assigns it to the NextPage field.
func (o *PageBeanWorkflowTransitionRules) SetNextPage(v string) {
	o.NextPage = &v
}

// GetMaxResults returns the MaxResults field value if set, zero value otherwise.
func (o *PageBeanWorkflowTransitionRules) GetMaxResults() int32 {
	if o == nil || o.MaxResults == nil {
		var ret int32
		return ret
	}
	return *o.MaxResults
}

// GetMaxResultsOk returns a tuple with the MaxResults field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PageBeanWorkflowTransitionRules) GetMaxResultsOk() (*int32, bool) {
	if o == nil || o.MaxResults == nil {
		return nil, false
	}
	return o.MaxResults, true
}

// HasMaxResults returns a boolean if a field has been set.
func (o *PageBeanWorkflowTransitionRules) HasMaxResults() bool {
	if o != nil && o.MaxResults != nil {
		return true
	}

	return false
}

// SetMaxResults gets a reference to the given int32 and assigns it to the MaxResults field.
func (o *PageBeanWorkflowTransitionRules) SetMaxResults(v int32) {
	o.MaxResults = &v
}

// GetStartAt returns the StartAt field value if set, zero value otherwise.
func (o *PageBeanWorkflowTransitionRules) GetStartAt() int64 {
	if o == nil || o.StartAt == nil {
		var ret int64
		return ret
	}
	return *o.StartAt
}

// GetStartAtOk returns a tuple with the StartAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PageBeanWorkflowTransitionRules) GetStartAtOk() (*int64, bool) {
	if o == nil || o.StartAt == nil {
		return nil, false
	}
	return o.StartAt, true
}

// HasStartAt returns a boolean if a field has been set.
func (o *PageBeanWorkflowTransitionRules) HasStartAt() bool {
	if o != nil && o.StartAt != nil {
		return true
	}

	return false
}

// SetStartAt gets a reference to the given int64 and assigns it to the StartAt field.
func (o *PageBeanWorkflowTransitionRules) SetStartAt(v int64) {
	o.StartAt = &v
}

// GetTotal returns the Total field value if set, zero value otherwise.
func (o *PageBeanWorkflowTransitionRules) GetTotal() int64 {
	if o == nil || o.Total == nil {
		var ret int64
		return ret
	}
	return *o.Total
}

// GetTotalOk returns a tuple with the Total field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PageBeanWorkflowTransitionRules) GetTotalOk() (*int64, bool) {
	if o == nil || o.Total == nil {
		return nil, false
	}
	return o.Total, true
}

// HasTotal returns a boolean if a field has been set.
func (o *PageBeanWorkflowTransitionRules) HasTotal() bool {
	if o != nil && o.Total != nil {
		return true
	}

	return false
}

// SetTotal gets a reference to the given int64 and assigns it to the Total field.
func (o *PageBeanWorkflowTransitionRules) SetTotal(v int64) {
	o.Total = &v
}

// GetIsLast returns the IsLast field value if set, zero value otherwise.
func (o *PageBeanWorkflowTransitionRules) GetIsLast() bool {
	if o == nil || o.IsLast == nil {
		var ret bool
		return ret
	}
	return *o.IsLast
}

// GetIsLastOk returns a tuple with the IsLast field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PageBeanWorkflowTransitionRules) GetIsLastOk() (*bool, bool) {
	if o == nil || o.IsLast == nil {
		return nil, false
	}
	return o.IsLast, true
}

// HasIsLast returns a boolean if a field has been set.
func (o *PageBeanWorkflowTransitionRules) HasIsLast() bool {
	if o != nil && o.IsLast != nil {
		return true
	}

	return false
}

// SetIsLast gets a reference to the given bool and assigns it to the IsLast field.
func (o *PageBeanWorkflowTransitionRules) SetIsLast(v bool) {
	o.IsLast = &v
}

// GetValues returns the Values field value if set, zero value otherwise.
func (o *PageBeanWorkflowTransitionRules) GetValues() []WorkflowTransitionRules {
	if o == nil || o.Values == nil {
		var ret []WorkflowTransitionRules
		return ret
	}
	return *o.Values
}

// GetValuesOk returns a tuple with the Values field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PageBeanWorkflowTransitionRules) GetValuesOk() (*[]WorkflowTransitionRules, bool) {
	if o == nil || o.Values == nil {
		return nil, false
	}
	return o.Values, true
}

// HasValues returns a boolean if a field has been set.
func (o *PageBeanWorkflowTransitionRules) HasValues() bool {
	if o != nil && o.Values != nil {
		return true
	}

	return false
}

// SetValues gets a reference to the given []WorkflowTransitionRules and assigns it to the Values field.
func (o *PageBeanWorkflowTransitionRules) SetValues(v []WorkflowTransitionRules) {
	o.Values = &v
}

func (o PageBeanWorkflowTransitionRules) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Self != nil {
		toSerialize["self"] = o.Self
	}
	if o.NextPage != nil {
		toSerialize["nextPage"] = o.NextPage
	}
	if o.MaxResults != nil {
		toSerialize["maxResults"] = o.MaxResults
	}
	if o.StartAt != nil {
		toSerialize["startAt"] = o.StartAt
	}
	if o.Total != nil {
		toSerialize["total"] = o.Total
	}
	if o.IsLast != nil {
		toSerialize["isLast"] = o.IsLast
	}
	if o.Values != nil {
		toSerialize["values"] = o.Values
	}
	return json.Marshal(toSerialize)
}

type NullablePageBeanWorkflowTransitionRules struct {
	value *PageBeanWorkflowTransitionRules
	isSet bool
}

func (v NullablePageBeanWorkflowTransitionRules) Get() *PageBeanWorkflowTransitionRules {
	return v.value
}

func (v *NullablePageBeanWorkflowTransitionRules) Set(val *PageBeanWorkflowTransitionRules) {
	v.value = val
	v.isSet = true
}

func (v NullablePageBeanWorkflowTransitionRules) IsSet() bool {
	return v.isSet
}

func (v *NullablePageBeanWorkflowTransitionRules) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePageBeanWorkflowTransitionRules(val *PageBeanWorkflowTransitionRules) *NullablePageBeanWorkflowTransitionRules {
	return &NullablePageBeanWorkflowTransitionRules{value: val, isSet: true}
}

func (v NullablePageBeanWorkflowTransitionRules) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePageBeanWorkflowTransitionRules) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ParsedJqlQuery Details of a parsed JQL query.
type ParsedJqlQuery struct {
	// The JQL query that was parsed and validated.
	Query string `json:"query"`
	// The syntax tree of the query. Empty if the query was invalid.
	Structure *JqlQuery `json:"structure,omitempty"`
	// The list of syntax or validation errors.
	Errors *[]string `json:"errors,omitempty"`
}

// NewParsedJqlQuery instantiates a new ParsedJqlQuery object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewParsedJqlQuery(query string) *ParsedJqlQuery {
	this := ParsedJqlQuery{}
	this.Query = query
	return &this
}

// NewParsedJqlQueryWithDefaults instantiates a new ParsedJqlQuery object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewParsedJqlQueryWithDefaults() *ParsedJqlQuery {
	this := ParsedJqlQuery{}
	return &this
}

// GetQuery returns the Query field value
func (o *ParsedJqlQuery) GetQuery() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Query
}

// GetQueryOk returns a tuple with the Query field value
// and a boolean to check if the value has been set.
func (o *ParsedJqlQuery) GetQueryOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Query, true
}

// SetQuery sets field value
func (o *ParsedJqlQuery) SetQuery(v string) {
	o.Query = v
}

// GetStructure returns the Structure field value if set, zero value otherwise.
func (o *ParsedJqlQuery) GetStructure() JqlQuery {
	if o == nil || o.Structure == nil {
		var ret JqlQuery
		return ret
	}
	return *o.Structure
}

// GetStructureOk returns a tuple with the Structure field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ParsedJqlQuery) GetStructureOk() (*JqlQuery, bool) {
	if o == nil || o.Structure == nil {
		return nil, false
	}
	return o.Structure, true
}

// HasStructure returns a boolean if a field has been set.
func (o *ParsedJqlQuery) HasStructure() bool {
	if o != nil && o.Structure != nil {
		return true
	}

	return false
}

// SetStructure gets a reference to the given JqlQuery and assigns it to the Structure field.
func (o *ParsedJqlQuery) SetStructure(v JqlQuery) {
	o.Structure = &v
}

// GetErrors returns the Errors field value if set, zero value otherwise.
func (o *ParsedJqlQuery) GetErrors() []string {
	if o == nil || o.Errors == nil {
		var ret []string
		return ret
	}
	return *o.Errors
}

// GetErrorsOk returns a tuple with the Errors field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ParsedJqlQuery) GetErrorsOk() (*[]string, bool) {
	if o == nil || o.Errors == nil {
		return nil, false
	}
	return o.Errors, true
}

// HasErrors returns a boolean if a field has been set.
func (o *ParsedJqlQuery) HasErrors() bool {
	if o != nil && o.Errors != nil {
		return true
	}

	return false
}

// SetErrors gets a reference to the given []string and assigns it to the Errors field.
func (o *ParsedJqlQuery) SetErrors(v []string) {
	o.Errors = &v
}

func (o ParsedJqlQuery) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["query"] = o.Query
	}
	if o.Structure != nil {
		toSerialize["structure"] = o.Structure
	}
	if o.Errors != nil {
		toSerialize["errors"] = o.Errors
	}
	return json.Marshal(toSerialize)
}

type NullableParsedJqlQuery struct {
	value *ParsedJqlQuery
	isSet bool
}

func (v NullableParsedJqlQuery) Get() *ParsedJqlQuery {
	return v.value
}

func (v *NullableParsedJqlQuery) Set(val *ParsedJqlQuery) {
	v.value = val
	v.isSet = true
}

func (v NullableParsedJqlQuery) IsSet() bool {
	return v.isSet
}

func (v *NullableParsedJqlQuery) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableParsedJqlQuery(val *ParsedJqlQuery) *NullableParsedJqlQuery {
	return &NullableParsedJqlQuery{value: val, isSet: true}
}

func (v NullableParsedJqlQuery) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableParsedJqlQuery) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



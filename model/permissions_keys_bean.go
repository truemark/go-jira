/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// PermissionsKeysBean struct for PermissionsKeysBean
type PermissionsKeysBean struct {
	// A list of permission keys.
	Permissions []string `json:"permissions"`
}

// NewPermissionsKeysBean instantiates a new PermissionsKeysBean object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPermissionsKeysBean(permissions []string) *PermissionsKeysBean {
	this := PermissionsKeysBean{}
	this.Permissions = permissions
	return &this
}

// NewPermissionsKeysBeanWithDefaults instantiates a new PermissionsKeysBean object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPermissionsKeysBeanWithDefaults() *PermissionsKeysBean {
	this := PermissionsKeysBean{}
	return &this
}

// GetPermissions returns the Permissions field value
func (o *PermissionsKeysBean) GetPermissions() []string {
	if o == nil {
		var ret []string
		return ret
	}

	return o.Permissions
}

// GetPermissionsOk returns a tuple with the Permissions field value
// and a boolean to check if the value has been set.
func (o *PermissionsKeysBean) GetPermissionsOk() (*[]string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Permissions, true
}

// SetPermissions sets field value
func (o *PermissionsKeysBean) SetPermissions(v []string) {
	o.Permissions = v
}

func (o PermissionsKeysBean) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["permissions"] = o.Permissions
	}
	return json.Marshal(toSerialize)
}

type NullablePermissionsKeysBean struct {
	value *PermissionsKeysBean
	isSet bool
}

func (v NullablePermissionsKeysBean) Get() *PermissionsKeysBean {
	return v.value
}

func (v *NullablePermissionsKeysBean) Set(val *PermissionsKeysBean) {
	v.value = val
	v.isSet = true
}

func (v NullablePermissionsKeysBean) IsSet() bool {
	return v.isSet
}

func (v *NullablePermissionsKeysBean) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePermissionsKeysBean(val *PermissionsKeysBean) *NullablePermissionsKeysBean {
	return &NullablePermissionsKeysBean{value: val, isSet: true}
}

func (v NullablePermissionsKeysBean) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePermissionsKeysBean) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



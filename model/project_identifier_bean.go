/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ProjectIdentifierBean The identifiers for a project.
type ProjectIdentifierBean struct {
	// The ID of the project.
	Id *int64 `json:"id,omitempty"`
	// The key of the project.
	Key *string `json:"key,omitempty"`
}

// NewProjectIdentifierBean instantiates a new ProjectIdentifierBean object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewProjectIdentifierBean() *ProjectIdentifierBean {
	this := ProjectIdentifierBean{}
	return &this
}

// NewProjectIdentifierBeanWithDefaults instantiates a new ProjectIdentifierBean object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewProjectIdentifierBeanWithDefaults() *ProjectIdentifierBean {
	this := ProjectIdentifierBean{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *ProjectIdentifierBean) GetId() int64 {
	if o == nil || o.Id == nil {
		var ret int64
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectIdentifierBean) GetIdOk() (*int64, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *ProjectIdentifierBean) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given int64 and assigns it to the Id field.
func (o *ProjectIdentifierBean) SetId(v int64) {
	o.Id = &v
}

// GetKey returns the Key field value if set, zero value otherwise.
func (o *ProjectIdentifierBean) GetKey() string {
	if o == nil || o.Key == nil {
		var ret string
		return ret
	}
	return *o.Key
}

// GetKeyOk returns a tuple with the Key field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectIdentifierBean) GetKeyOk() (*string, bool) {
	if o == nil || o.Key == nil {
		return nil, false
	}
	return o.Key, true
}

// HasKey returns a boolean if a field has been set.
func (o *ProjectIdentifierBean) HasKey() bool {
	if o != nil && o.Key != nil {
		return true
	}

	return false
}

// SetKey gets a reference to the given string and assigns it to the Key field.
func (o *ProjectIdentifierBean) SetKey(v string) {
	o.Key = &v
}

func (o ProjectIdentifierBean) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.Key != nil {
		toSerialize["key"] = o.Key
	}
	return json.Marshal(toSerialize)
}

type NullableProjectIdentifierBean struct {
	value *ProjectIdentifierBean
	isSet bool
}

func (v NullableProjectIdentifierBean) Get() *ProjectIdentifierBean {
	return v.value
}

func (v *NullableProjectIdentifierBean) Set(val *ProjectIdentifierBean) {
	v.value = val
	v.isSet = true
}

func (v NullableProjectIdentifierBean) IsSet() bool {
	return v.isSet
}

func (v *NullableProjectIdentifierBean) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableProjectIdentifierBean(val *ProjectIdentifierBean) *NullableProjectIdentifierBean {
	return &NullableProjectIdentifierBean{value: val, isSet: true}
}

func (v NullableProjectIdentifierBean) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableProjectIdentifierBean) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



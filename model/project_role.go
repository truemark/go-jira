/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ProjectRole Details about the roles in a project.
type ProjectRole struct {
	// The URL the project role details.
	Self *string `json:"self,omitempty"`
	// The name of the project role.
	Name *string `json:"name,omitempty"`
	// The ID of the project role.
	Id *int64 `json:"id,omitempty"`
	// The description of the project role.
	Description *string `json:"description,omitempty"`
	// The list of users who act in this role.
	Actors *[]RoleActor `json:"actors,omitempty"`
	// The scope of the role. Indicated for roles associated with [next-gen projects](https://confluence.atlassian.com/x/loMyO).
	Scope *Scope `json:"scope,omitempty"`
	// The translated name of the project role.
	TranslatedName *string `json:"translatedName,omitempty"`
	// Whether the calling user is part of this role.
	CurrentUserRole *bool `json:"currentUserRole,omitempty"`
	// Whether this role is the admin role for the project.
	Admin *bool `json:"admin,omitempty"`
	// Whether the roles are configurable for this project.
	RoleConfigurable *bool `json:"roleConfigurable,omitempty"`
	// Whether this role is the default role for the project
	Default *bool `json:"default,omitempty"`
}

// NewProjectRole instantiates a new ProjectRole object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewProjectRole() *ProjectRole {
	this := ProjectRole{}
	return &this
}

// NewProjectRoleWithDefaults instantiates a new ProjectRole object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewProjectRoleWithDefaults() *ProjectRole {
	this := ProjectRole{}
	return &this
}

// GetSelf returns the Self field value if set, zero value otherwise.
func (o *ProjectRole) GetSelf() string {
	if o == nil || o.Self == nil {
		var ret string
		return ret
	}
	return *o.Self
}

// GetSelfOk returns a tuple with the Self field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetSelfOk() (*string, bool) {
	if o == nil || o.Self == nil {
		return nil, false
	}
	return o.Self, true
}

// HasSelf returns a boolean if a field has been set.
func (o *ProjectRole) HasSelf() bool {
	if o != nil && o.Self != nil {
		return true
	}

	return false
}

// SetSelf gets a reference to the given string and assigns it to the Self field.
func (o *ProjectRole) SetSelf(v string) {
	o.Self = &v
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *ProjectRole) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *ProjectRole) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *ProjectRole) SetName(v string) {
	o.Name = &v
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *ProjectRole) GetId() int64 {
	if o == nil || o.Id == nil {
		var ret int64
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetIdOk() (*int64, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *ProjectRole) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given int64 and assigns it to the Id field.
func (o *ProjectRole) SetId(v int64) {
	o.Id = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *ProjectRole) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *ProjectRole) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *ProjectRole) SetDescription(v string) {
	o.Description = &v
}

// GetActors returns the Actors field value if set, zero value otherwise.
func (o *ProjectRole) GetActors() []RoleActor {
	if o == nil || o.Actors == nil {
		var ret []RoleActor
		return ret
	}
	return *o.Actors
}

// GetActorsOk returns a tuple with the Actors field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetActorsOk() (*[]RoleActor, bool) {
	if o == nil || o.Actors == nil {
		return nil, false
	}
	return o.Actors, true
}

// HasActors returns a boolean if a field has been set.
func (o *ProjectRole) HasActors() bool {
	if o != nil && o.Actors != nil {
		return true
	}

	return false
}

// SetActors gets a reference to the given []RoleActor and assigns it to the Actors field.
func (o *ProjectRole) SetActors(v []RoleActor) {
	o.Actors = &v
}

// GetScope returns the Scope field value if set, zero value otherwise.
func (o *ProjectRole) GetScope() Scope {
	if o == nil || o.Scope == nil {
		var ret Scope
		return ret
	}
	return *o.Scope
}

// GetScopeOk returns a tuple with the Scope field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetScopeOk() (*Scope, bool) {
	if o == nil || o.Scope == nil {
		return nil, false
	}
	return o.Scope, true
}

// HasScope returns a boolean if a field has been set.
func (o *ProjectRole) HasScope() bool {
	if o != nil && o.Scope != nil {
		return true
	}

	return false
}

// SetScope gets a reference to the given Scope and assigns it to the Scope field.
func (o *ProjectRole) SetScope(v Scope) {
	o.Scope = &v
}

// GetTranslatedName returns the TranslatedName field value if set, zero value otherwise.
func (o *ProjectRole) GetTranslatedName() string {
	if o == nil || o.TranslatedName == nil {
		var ret string
		return ret
	}
	return *o.TranslatedName
}

// GetTranslatedNameOk returns a tuple with the TranslatedName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetTranslatedNameOk() (*string, bool) {
	if o == nil || o.TranslatedName == nil {
		return nil, false
	}
	return o.TranslatedName, true
}

// HasTranslatedName returns a boolean if a field has been set.
func (o *ProjectRole) HasTranslatedName() bool {
	if o != nil && o.TranslatedName != nil {
		return true
	}

	return false
}

// SetTranslatedName gets a reference to the given string and assigns it to the TranslatedName field.
func (o *ProjectRole) SetTranslatedName(v string) {
	o.TranslatedName = &v
}

// GetCurrentUserRole returns the CurrentUserRole field value if set, zero value otherwise.
func (o *ProjectRole) GetCurrentUserRole() bool {
	if o == nil || o.CurrentUserRole == nil {
		var ret bool
		return ret
	}
	return *o.CurrentUserRole
}

// GetCurrentUserRoleOk returns a tuple with the CurrentUserRole field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetCurrentUserRoleOk() (*bool, bool) {
	if o == nil || o.CurrentUserRole == nil {
		return nil, false
	}
	return o.CurrentUserRole, true
}

// HasCurrentUserRole returns a boolean if a field has been set.
func (o *ProjectRole) HasCurrentUserRole() bool {
	if o != nil && o.CurrentUserRole != nil {
		return true
	}

	return false
}

// SetCurrentUserRole gets a reference to the given bool and assigns it to the CurrentUserRole field.
func (o *ProjectRole) SetCurrentUserRole(v bool) {
	o.CurrentUserRole = &v
}

// GetAdmin returns the Admin field value if set, zero value otherwise.
func (o *ProjectRole) GetAdmin() bool {
	if o == nil || o.Admin == nil {
		var ret bool
		return ret
	}
	return *o.Admin
}

// GetAdminOk returns a tuple with the Admin field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetAdminOk() (*bool, bool) {
	if o == nil || o.Admin == nil {
		return nil, false
	}
	return o.Admin, true
}

// HasAdmin returns a boolean if a field has been set.
func (o *ProjectRole) HasAdmin() bool {
	if o != nil && o.Admin != nil {
		return true
	}

	return false
}

// SetAdmin gets a reference to the given bool and assigns it to the Admin field.
func (o *ProjectRole) SetAdmin(v bool) {
	o.Admin = &v
}

// GetRoleConfigurable returns the RoleConfigurable field value if set, zero value otherwise.
func (o *ProjectRole) GetRoleConfigurable() bool {
	if o == nil || o.RoleConfigurable == nil {
		var ret bool
		return ret
	}
	return *o.RoleConfigurable
}

// GetRoleConfigurableOk returns a tuple with the RoleConfigurable field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetRoleConfigurableOk() (*bool, bool) {
	if o == nil || o.RoleConfigurable == nil {
		return nil, false
	}
	return o.RoleConfigurable, true
}

// HasRoleConfigurable returns a boolean if a field has been set.
func (o *ProjectRole) HasRoleConfigurable() bool {
	if o != nil && o.RoleConfigurable != nil {
		return true
	}

	return false
}

// SetRoleConfigurable gets a reference to the given bool and assigns it to the RoleConfigurable field.
func (o *ProjectRole) SetRoleConfigurable(v bool) {
	o.RoleConfigurable = &v
}

// GetDefault returns the Default field value if set, zero value otherwise.
func (o *ProjectRole) GetDefault() bool {
	if o == nil || o.Default == nil {
		var ret bool
		return ret
	}
	return *o.Default
}

// GetDefaultOk returns a tuple with the Default field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRole) GetDefaultOk() (*bool, bool) {
	if o == nil || o.Default == nil {
		return nil, false
	}
	return o.Default, true
}

// HasDefault returns a boolean if a field has been set.
func (o *ProjectRole) HasDefault() bool {
	if o != nil && o.Default != nil {
		return true
	}

	return false
}

// SetDefault gets a reference to the given bool and assigns it to the Default field.
func (o *ProjectRole) SetDefault(v bool) {
	o.Default = &v
}

func (o ProjectRole) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Self != nil {
		toSerialize["self"] = o.Self
	}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.Actors != nil {
		toSerialize["actors"] = o.Actors
	}
	if o.Scope != nil {
		toSerialize["scope"] = o.Scope
	}
	if o.TranslatedName != nil {
		toSerialize["translatedName"] = o.TranslatedName
	}
	if o.CurrentUserRole != nil {
		toSerialize["currentUserRole"] = o.CurrentUserRole
	}
	if o.Admin != nil {
		toSerialize["admin"] = o.Admin
	}
	if o.RoleConfigurable != nil {
		toSerialize["roleConfigurable"] = o.RoleConfigurable
	}
	if o.Default != nil {
		toSerialize["default"] = o.Default
	}
	return json.Marshal(toSerialize)
}

type NullableProjectRole struct {
	value *ProjectRole
	isSet bool
}

func (v NullableProjectRole) Get() *ProjectRole {
	return v.value
}

func (v *NullableProjectRole) Set(val *ProjectRole) {
	v.value = val
	v.isSet = true
}

func (v NullableProjectRole) IsSet() bool {
	return v.isSet
}

func (v *NullableProjectRole) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableProjectRole(val *ProjectRole) *NullableProjectRole {
	return &NullableProjectRole{value: val, isSet: true}
}

func (v NullableProjectRole) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableProjectRole) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



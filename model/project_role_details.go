/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ProjectRoleDetails Details about a project role.
type ProjectRoleDetails struct {
	// The URL the project role details.
	Self *string `json:"self,omitempty"`
	// The name of the project role.
	Name *string `json:"name,omitempty"`
	// The ID of the project role.
	Id *int64 `json:"id,omitempty"`
	// The description of the project role.
	Description *string `json:"description,omitempty"`
	// Whether this role is the admin role for the project.
	Admin *bool `json:"admin,omitempty"`
	// The scope of the role. Indicated for roles associated with [next-gen projects](https://confluence.atlassian.com/x/loMyO).
	Scope *Scope `json:"scope,omitempty"`
	// Whether the roles are configurable for this project.
	RoleConfigurable *bool `json:"roleConfigurable,omitempty"`
	// The translated name of the project role.
	TranslatedName *string `json:"translatedName,omitempty"`
	// Whether this role is the default role for the project.
	Default *bool `json:"default,omitempty"`
}

// NewProjectRoleDetails instantiates a new ProjectRoleDetails object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewProjectRoleDetails() *ProjectRoleDetails {
	this := ProjectRoleDetails{}
	return &this
}

// NewProjectRoleDetailsWithDefaults instantiates a new ProjectRoleDetails object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewProjectRoleDetailsWithDefaults() *ProjectRoleDetails {
	this := ProjectRoleDetails{}
	return &this
}

// GetSelf returns the Self field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetSelf() string {
	if o == nil || o.Self == nil {
		var ret string
		return ret
	}
	return *o.Self
}

// GetSelfOk returns a tuple with the Self field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetSelfOk() (*string, bool) {
	if o == nil || o.Self == nil {
		return nil, false
	}
	return o.Self, true
}

// HasSelf returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasSelf() bool {
	if o != nil && o.Self != nil {
		return true
	}

	return false
}

// SetSelf gets a reference to the given string and assigns it to the Self field.
func (o *ProjectRoleDetails) SetSelf(v string) {
	o.Self = &v
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *ProjectRoleDetails) SetName(v string) {
	o.Name = &v
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetId() int64 {
	if o == nil || o.Id == nil {
		var ret int64
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetIdOk() (*int64, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given int64 and assigns it to the Id field.
func (o *ProjectRoleDetails) SetId(v int64) {
	o.Id = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *ProjectRoleDetails) SetDescription(v string) {
	o.Description = &v
}

// GetAdmin returns the Admin field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetAdmin() bool {
	if o == nil || o.Admin == nil {
		var ret bool
		return ret
	}
	return *o.Admin
}

// GetAdminOk returns a tuple with the Admin field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetAdminOk() (*bool, bool) {
	if o == nil || o.Admin == nil {
		return nil, false
	}
	return o.Admin, true
}

// HasAdmin returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasAdmin() bool {
	if o != nil && o.Admin != nil {
		return true
	}

	return false
}

// SetAdmin gets a reference to the given bool and assigns it to the Admin field.
func (o *ProjectRoleDetails) SetAdmin(v bool) {
	o.Admin = &v
}

// GetScope returns the Scope field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetScope() Scope {
	if o == nil || o.Scope == nil {
		var ret Scope
		return ret
	}
	return *o.Scope
}

// GetScopeOk returns a tuple with the Scope field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetScopeOk() (*Scope, bool) {
	if o == nil || o.Scope == nil {
		return nil, false
	}
	return o.Scope, true
}

// HasScope returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasScope() bool {
	if o != nil && o.Scope != nil {
		return true
	}

	return false
}

// SetScope gets a reference to the given Scope and assigns it to the Scope field.
func (o *ProjectRoleDetails) SetScope(v Scope) {
	o.Scope = &v
}

// GetRoleConfigurable returns the RoleConfigurable field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetRoleConfigurable() bool {
	if o == nil || o.RoleConfigurable == nil {
		var ret bool
		return ret
	}
	return *o.RoleConfigurable
}

// GetRoleConfigurableOk returns a tuple with the RoleConfigurable field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetRoleConfigurableOk() (*bool, bool) {
	if o == nil || o.RoleConfigurable == nil {
		return nil, false
	}
	return o.RoleConfigurable, true
}

// HasRoleConfigurable returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasRoleConfigurable() bool {
	if o != nil && o.RoleConfigurable != nil {
		return true
	}

	return false
}

// SetRoleConfigurable gets a reference to the given bool and assigns it to the RoleConfigurable field.
func (o *ProjectRoleDetails) SetRoleConfigurable(v bool) {
	o.RoleConfigurable = &v
}

// GetTranslatedName returns the TranslatedName field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetTranslatedName() string {
	if o == nil || o.TranslatedName == nil {
		var ret string
		return ret
	}
	return *o.TranslatedName
}

// GetTranslatedNameOk returns a tuple with the TranslatedName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetTranslatedNameOk() (*string, bool) {
	if o == nil || o.TranslatedName == nil {
		return nil, false
	}
	return o.TranslatedName, true
}

// HasTranslatedName returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasTranslatedName() bool {
	if o != nil && o.TranslatedName != nil {
		return true
	}

	return false
}

// SetTranslatedName gets a reference to the given string and assigns it to the TranslatedName field.
func (o *ProjectRoleDetails) SetTranslatedName(v string) {
	o.TranslatedName = &v
}

// GetDefault returns the Default field value if set, zero value otherwise.
func (o *ProjectRoleDetails) GetDefault() bool {
	if o == nil || o.Default == nil {
		var ret bool
		return ret
	}
	return *o.Default
}

// GetDefaultOk returns a tuple with the Default field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ProjectRoleDetails) GetDefaultOk() (*bool, bool) {
	if o == nil || o.Default == nil {
		return nil, false
	}
	return o.Default, true
}

// HasDefault returns a boolean if a field has been set.
func (o *ProjectRoleDetails) HasDefault() bool {
	if o != nil && o.Default != nil {
		return true
	}

	return false
}

// SetDefault gets a reference to the given bool and assigns it to the Default field.
func (o *ProjectRoleDetails) SetDefault(v bool) {
	o.Default = &v
}

func (o ProjectRoleDetails) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Self != nil {
		toSerialize["self"] = o.Self
	}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.Admin != nil {
		toSerialize["admin"] = o.Admin
	}
	if o.Scope != nil {
		toSerialize["scope"] = o.Scope
	}
	if o.RoleConfigurable != nil {
		toSerialize["roleConfigurable"] = o.RoleConfigurable
	}
	if o.TranslatedName != nil {
		toSerialize["translatedName"] = o.TranslatedName
	}
	if o.Default != nil {
		toSerialize["default"] = o.Default
	}
	return json.Marshal(toSerialize)
}

type NullableProjectRoleDetails struct {
	value *ProjectRoleDetails
	isSet bool
}

func (v NullableProjectRoleDetails) Get() *ProjectRoleDetails {
	return v.value
}

func (v *NullableProjectRoleDetails) Set(val *ProjectRoleDetails) {
	v.value = val
	v.isSet = true
}

func (v NullableProjectRoleDetails) IsSet() bool {
	return v.isSet
}

func (v *NullableProjectRoleDetails) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableProjectRoleDetails(val *ProjectRoleDetails) *NullableProjectRoleDetails {
	return &NullableProjectRoleDetails{value: val, isSet: true}
}

func (v NullableProjectRoleDetails) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableProjectRoleDetails) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



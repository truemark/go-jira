/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// PublishDraftWorkflowScheme Details about the status mappings for publishing a draft workflow scheme.
type PublishDraftWorkflowScheme struct {
	// Mappings of statuses to new statuses for issue types.
	StatusMappings *[]StatusMapping `json:"statusMappings,omitempty"`
}

// NewPublishDraftWorkflowScheme instantiates a new PublishDraftWorkflowScheme object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPublishDraftWorkflowScheme() *PublishDraftWorkflowScheme {
	this := PublishDraftWorkflowScheme{}
	return &this
}

// NewPublishDraftWorkflowSchemeWithDefaults instantiates a new PublishDraftWorkflowScheme object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPublishDraftWorkflowSchemeWithDefaults() *PublishDraftWorkflowScheme {
	this := PublishDraftWorkflowScheme{}
	return &this
}

// GetStatusMappings returns the StatusMappings field value if set, zero value otherwise.
func (o *PublishDraftWorkflowScheme) GetStatusMappings() []StatusMapping {
	if o == nil || o.StatusMappings == nil {
		var ret []StatusMapping
		return ret
	}
	return *o.StatusMappings
}

// GetStatusMappingsOk returns a tuple with the StatusMappings field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PublishDraftWorkflowScheme) GetStatusMappingsOk() (*[]StatusMapping, bool) {
	if o == nil || o.StatusMappings == nil {
		return nil, false
	}
	return o.StatusMappings, true
}

// HasStatusMappings returns a boolean if a field has been set.
func (o *PublishDraftWorkflowScheme) HasStatusMappings() bool {
	if o != nil && o.StatusMappings != nil {
		return true
	}

	return false
}

// SetStatusMappings gets a reference to the given []StatusMapping and assigns it to the StatusMappings field.
func (o *PublishDraftWorkflowScheme) SetStatusMappings(v []StatusMapping) {
	o.StatusMappings = &v
}

func (o PublishDraftWorkflowScheme) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.StatusMappings != nil {
		toSerialize["statusMappings"] = o.StatusMappings
	}
	return json.Marshal(toSerialize)
}

type NullablePublishDraftWorkflowScheme struct {
	value *PublishDraftWorkflowScheme
	isSet bool
}

func (v NullablePublishDraftWorkflowScheme) Get() *PublishDraftWorkflowScheme {
	return v.value
}

func (v *NullablePublishDraftWorkflowScheme) Set(val *PublishDraftWorkflowScheme) {
	v.value = val
	v.isSet = true
}

func (v NullablePublishDraftWorkflowScheme) IsSet() bool {
	return v.isSet
}

func (v *NullablePublishDraftWorkflowScheme) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePublishDraftWorkflowScheme(val *PublishDraftWorkflowScheme) *NullablePublishDraftWorkflowScheme {
	return &NullablePublishDraftWorkflowScheme{value: val, isSet: true}
}

func (v NullablePublishDraftWorkflowScheme) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePublishDraftWorkflowScheme) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



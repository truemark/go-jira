/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// RegisteredWebhook ID of a registered webhook or error messages explaining why a webhook wasn't registered.
type RegisteredWebhook struct {
	// The ID of the webhook. Returned if the webhook is created.
	CreatedWebhookId *int64 `json:"createdWebhookId,omitempty"`
	// Error messages specifying why the webhook creation failed.
	Errors *[]string `json:"errors,omitempty"`
}

// NewRegisteredWebhook instantiates a new RegisteredWebhook object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewRegisteredWebhook() *RegisteredWebhook {
	this := RegisteredWebhook{}
	return &this
}

// NewRegisteredWebhookWithDefaults instantiates a new RegisteredWebhook object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewRegisteredWebhookWithDefaults() *RegisteredWebhook {
	this := RegisteredWebhook{}
	return &this
}

// GetCreatedWebhookId returns the CreatedWebhookId field value if set, zero value otherwise.
func (o *RegisteredWebhook) GetCreatedWebhookId() int64 {
	if o == nil || o.CreatedWebhookId == nil {
		var ret int64
		return ret
	}
	return *o.CreatedWebhookId
}

// GetCreatedWebhookIdOk returns a tuple with the CreatedWebhookId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RegisteredWebhook) GetCreatedWebhookIdOk() (*int64, bool) {
	if o == nil || o.CreatedWebhookId == nil {
		return nil, false
	}
	return o.CreatedWebhookId, true
}

// HasCreatedWebhookId returns a boolean if a field has been set.
func (o *RegisteredWebhook) HasCreatedWebhookId() bool {
	if o != nil && o.CreatedWebhookId != nil {
		return true
	}

	return false
}

// SetCreatedWebhookId gets a reference to the given int64 and assigns it to the CreatedWebhookId field.
func (o *RegisteredWebhook) SetCreatedWebhookId(v int64) {
	o.CreatedWebhookId = &v
}

// GetErrors returns the Errors field value if set, zero value otherwise.
func (o *RegisteredWebhook) GetErrors() []string {
	if o == nil || o.Errors == nil {
		var ret []string
		return ret
	}
	return *o.Errors
}

// GetErrorsOk returns a tuple with the Errors field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RegisteredWebhook) GetErrorsOk() (*[]string, bool) {
	if o == nil || o.Errors == nil {
		return nil, false
	}
	return o.Errors, true
}

// HasErrors returns a boolean if a field has been set.
func (o *RegisteredWebhook) HasErrors() bool {
	if o != nil && o.Errors != nil {
		return true
	}

	return false
}

// SetErrors gets a reference to the given []string and assigns it to the Errors field.
func (o *RegisteredWebhook) SetErrors(v []string) {
	o.Errors = &v
}

func (o RegisteredWebhook) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.CreatedWebhookId != nil {
		toSerialize["createdWebhookId"] = o.CreatedWebhookId
	}
	if o.Errors != nil {
		toSerialize["errors"] = o.Errors
	}
	return json.Marshal(toSerialize)
}

type NullableRegisteredWebhook struct {
	value *RegisteredWebhook
	isSet bool
}

func (v NullableRegisteredWebhook) Get() *RegisteredWebhook {
	return v.value
}

func (v *NullableRegisteredWebhook) Set(val *RegisteredWebhook) {
	v.value = val
	v.isSet = true
}

func (v NullableRegisteredWebhook) IsSet() bool {
	return v.isSet
}

func (v *NullableRegisteredWebhook) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableRegisteredWebhook(val *RegisteredWebhook) *NullableRegisteredWebhook {
	return &NullableRegisteredWebhook{value: val, isSet: true}
}

func (v NullableRegisteredWebhook) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableRegisteredWebhook) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



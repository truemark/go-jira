/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// ScreenSchemeId The ID of a screen scheme.
type ScreenSchemeId struct {
	// The ID of the screen scheme.
	Id int64 `json:"id"`
}

// NewScreenSchemeId instantiates a new ScreenSchemeId object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewScreenSchemeId(id int64) *ScreenSchemeId {
	this := ScreenSchemeId{}
	this.Id = id
	return &this
}

// NewScreenSchemeIdWithDefaults instantiates a new ScreenSchemeId object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewScreenSchemeIdWithDefaults() *ScreenSchemeId {
	this := ScreenSchemeId{}
	return &this
}

// GetId returns the Id field value
func (o *ScreenSchemeId) GetId() int64 {
	if o == nil {
		var ret int64
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *ScreenSchemeId) GetIdOk() (*int64, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *ScreenSchemeId) SetId(v int64) {
	o.Id = v
}

func (o ScreenSchemeId) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	return json.Marshal(toSerialize)
}

type NullableScreenSchemeId struct {
	value *ScreenSchemeId
	isSet bool
}

func (v NullableScreenSchemeId) Get() *ScreenSchemeId {
	return v.value
}

func (v *NullableScreenSchemeId) Set(val *ScreenSchemeId) {
	v.value = val
	v.isSet = true
}

func (v NullableScreenSchemeId) IsSet() bool {
	return v.isSet
}

func (v *NullableScreenSchemeId) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableScreenSchemeId(val *ScreenSchemeId) *NullableScreenSchemeId {
	return &NullableScreenSchemeId{value: val, isSet: true}
}

func (v NullableScreenSchemeId) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableScreenSchemeId) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



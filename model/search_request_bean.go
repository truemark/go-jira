/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// SearchRequestBean struct for SearchRequestBean
type SearchRequestBean struct {
	// A [JQL](https://confluence.atlassian.com/x/egORLQ) expression.
	Jql *string `json:"jql,omitempty"`
	// The index of the first item to return in the page of results (page offset). The base index is `0`.
	StartAt *int32 `json:"startAt,omitempty"`
	// The maximum number of items to return per page.
	MaxResults *int32 `json:"maxResults,omitempty"`
	// A list of fields to return for each issue, use it to retrieve a subset of fields. This parameter accepts a comma-separated list. Expand options include:   *  `*all` Returns all fields.  *  `*navigable` Returns navigable fields.  *  Any issue field, prefixed with a minus to exclude.  The default is `*navigable`.  Examples:   *  `summary,comment` Returns the summary and comments fields only.  *  `-description` Returns all navigable (default) fields except description.  *  `*all,-comment` Returns all fields except comments.  Multiple `fields` parameters can be included in a request.  Note: All navigable fields are returned by default. This differs from [GET issue](#api-rest-api-3-issue-issueIdOrKey-get) where the default is all fields.
	Fields *[]string `json:"fields,omitempty"`
	// Determines how to validate the JQL query and treat the validation results. Supported values:   *  `strict` Returns a 400 response code if any errors are found, along with a list of all errors (and warnings).  *  `warn` Returns all errors as warnings.  *  `none` No validation is performed.  *  `true` *Deprecated* A legacy synonym for `strict`.  *  `false` *Deprecated* A legacy synonym for `warn`.  The default is `strict`.  Note: If the JQL is not correctly formed a 400 response code is returned, regardless of the `validateQuery` value.
	ValidateQuery *string `json:"validateQuery,omitempty"`
	// Use [expand](em>#expansion) to include additional information about issues in the response. Note that, unlike the majority of instances where `expand` is specified, `expand` is defined as a list of values. The expand options are:   *  `renderedFields` Returns field values rendered in HTML format.  *  `names` Returns the display name of each field.  *  `schema` Returns the schema describing a field type.  *  `transitions` Returns all possible transitions for the issue.  *  `operations` Returns all possible operations for the issue.  *  `editmeta` Returns information about how each field can be edited.  *  `changelog` Returns a list of recent updates to an issue, sorted by date, starting from the most recent.  *  `versionedRepresentations` Instead of `fields`, returns `versionedRepresentations` a JSON array containing each version of a field's value, with the highest numbered item representing the most recent version.
	Expand *[]string `json:"expand,omitempty"`
	// A list of up to 5 issue properties to include in the results. This parameter accepts a comma-separated list.
	Properties *[]string `json:"properties,omitempty"`
	// Reference fields by their key (rather than ID). The default is `false`.
	FieldsByKeys *bool `json:"fieldsByKeys,omitempty"`
}

// NewSearchRequestBean instantiates a new SearchRequestBean object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSearchRequestBean() *SearchRequestBean {
	this := SearchRequestBean{}
	var maxResults int32 = 50
	this.MaxResults = &maxResults
	return &this
}

// NewSearchRequestBeanWithDefaults instantiates a new SearchRequestBean object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSearchRequestBeanWithDefaults() *SearchRequestBean {
	this := SearchRequestBean{}
	var maxResults int32 = 50
	this.MaxResults = &maxResults
	return &this
}

// GetJql returns the Jql field value if set, zero value otherwise.
func (o *SearchRequestBean) GetJql() string {
	if o == nil || o.Jql == nil {
		var ret string
		return ret
	}
	return *o.Jql
}

// GetJqlOk returns a tuple with the Jql field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetJqlOk() (*string, bool) {
	if o == nil || o.Jql == nil {
		return nil, false
	}
	return o.Jql, true
}

// HasJql returns a boolean if a field has been set.
func (o *SearchRequestBean) HasJql() bool {
	if o != nil && o.Jql != nil {
		return true
	}

	return false
}

// SetJql gets a reference to the given string and assigns it to the Jql field.
func (o *SearchRequestBean) SetJql(v string) {
	o.Jql = &v
}

// GetStartAt returns the StartAt field value if set, zero value otherwise.
func (o *SearchRequestBean) GetStartAt() int32 {
	if o == nil || o.StartAt == nil {
		var ret int32
		return ret
	}
	return *o.StartAt
}

// GetStartAtOk returns a tuple with the StartAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetStartAtOk() (*int32, bool) {
	if o == nil || o.StartAt == nil {
		return nil, false
	}
	return o.StartAt, true
}

// HasStartAt returns a boolean if a field has been set.
func (o *SearchRequestBean) HasStartAt() bool {
	if o != nil && o.StartAt != nil {
		return true
	}

	return false
}

// SetStartAt gets a reference to the given int32 and assigns it to the StartAt field.
func (o *SearchRequestBean) SetStartAt(v int32) {
	o.StartAt = &v
}

// GetMaxResults returns the MaxResults field value if set, zero value otherwise.
func (o *SearchRequestBean) GetMaxResults() int32 {
	if o == nil || o.MaxResults == nil {
		var ret int32
		return ret
	}
	return *o.MaxResults
}

// GetMaxResultsOk returns a tuple with the MaxResults field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetMaxResultsOk() (*int32, bool) {
	if o == nil || o.MaxResults == nil {
		return nil, false
	}
	return o.MaxResults, true
}

// HasMaxResults returns a boolean if a field has been set.
func (o *SearchRequestBean) HasMaxResults() bool {
	if o != nil && o.MaxResults != nil {
		return true
	}

	return false
}

// SetMaxResults gets a reference to the given int32 and assigns it to the MaxResults field.
func (o *SearchRequestBean) SetMaxResults(v int32) {
	o.MaxResults = &v
}

// GetFields returns the Fields field value if set, zero value otherwise.
func (o *SearchRequestBean) GetFields() []string {
	if o == nil || o.Fields == nil {
		var ret []string
		return ret
	}
	return *o.Fields
}

// GetFieldsOk returns a tuple with the Fields field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetFieldsOk() (*[]string, bool) {
	if o == nil || o.Fields == nil {
		return nil, false
	}
	return o.Fields, true
}

// HasFields returns a boolean if a field has been set.
func (o *SearchRequestBean) HasFields() bool {
	if o != nil && o.Fields != nil {
		return true
	}

	return false
}

// SetFields gets a reference to the given []string and assigns it to the Fields field.
func (o *SearchRequestBean) SetFields(v []string) {
	o.Fields = &v
}

// GetValidateQuery returns the ValidateQuery field value if set, zero value otherwise.
func (o *SearchRequestBean) GetValidateQuery() string {
	if o == nil || o.ValidateQuery == nil {
		var ret string
		return ret
	}
	return *o.ValidateQuery
}

// GetValidateQueryOk returns a tuple with the ValidateQuery field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetValidateQueryOk() (*string, bool) {
	if o == nil || o.ValidateQuery == nil {
		return nil, false
	}
	return o.ValidateQuery, true
}

// HasValidateQuery returns a boolean if a field has been set.
func (o *SearchRequestBean) HasValidateQuery() bool {
	if o != nil && o.ValidateQuery != nil {
		return true
	}

	return false
}

// SetValidateQuery gets a reference to the given string and assigns it to the ValidateQuery field.
func (o *SearchRequestBean) SetValidateQuery(v string) {
	o.ValidateQuery = &v
}

// GetExpand returns the Expand field value if set, zero value otherwise.
func (o *SearchRequestBean) GetExpand() []string {
	if o == nil || o.Expand == nil {
		var ret []string
		return ret
	}
	return *o.Expand
}

// GetExpandOk returns a tuple with the Expand field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetExpandOk() (*[]string, bool) {
	if o == nil || o.Expand == nil {
		return nil, false
	}
	return o.Expand, true
}

// HasExpand returns a boolean if a field has been set.
func (o *SearchRequestBean) HasExpand() bool {
	if o != nil && o.Expand != nil {
		return true
	}

	return false
}

// SetExpand gets a reference to the given []string and assigns it to the Expand field.
func (o *SearchRequestBean) SetExpand(v []string) {
	o.Expand = &v
}

// GetProperties returns the Properties field value if set, zero value otherwise.
func (o *SearchRequestBean) GetProperties() []string {
	if o == nil || o.Properties == nil {
		var ret []string
		return ret
	}
	return *o.Properties
}

// GetPropertiesOk returns a tuple with the Properties field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetPropertiesOk() (*[]string, bool) {
	if o == nil || o.Properties == nil {
		return nil, false
	}
	return o.Properties, true
}

// HasProperties returns a boolean if a field has been set.
func (o *SearchRequestBean) HasProperties() bool {
	if o != nil && o.Properties != nil {
		return true
	}

	return false
}

// SetProperties gets a reference to the given []string and assigns it to the Properties field.
func (o *SearchRequestBean) SetProperties(v []string) {
	o.Properties = &v
}

// GetFieldsByKeys returns the FieldsByKeys field value if set, zero value otherwise.
func (o *SearchRequestBean) GetFieldsByKeys() bool {
	if o == nil || o.FieldsByKeys == nil {
		var ret bool
		return ret
	}
	return *o.FieldsByKeys
}

// GetFieldsByKeysOk returns a tuple with the FieldsByKeys field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SearchRequestBean) GetFieldsByKeysOk() (*bool, bool) {
	if o == nil || o.FieldsByKeys == nil {
		return nil, false
	}
	return o.FieldsByKeys, true
}

// HasFieldsByKeys returns a boolean if a field has been set.
func (o *SearchRequestBean) HasFieldsByKeys() bool {
	if o != nil && o.FieldsByKeys != nil {
		return true
	}

	return false
}

// SetFieldsByKeys gets a reference to the given bool and assigns it to the FieldsByKeys field.
func (o *SearchRequestBean) SetFieldsByKeys(v bool) {
	o.FieldsByKeys = &v
}

func (o SearchRequestBean) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Jql != nil {
		toSerialize["jql"] = o.Jql
	}
	if o.StartAt != nil {
		toSerialize["startAt"] = o.StartAt
	}
	if o.MaxResults != nil {
		toSerialize["maxResults"] = o.MaxResults
	}
	if o.Fields != nil {
		toSerialize["fields"] = o.Fields
	}
	if o.ValidateQuery != nil {
		toSerialize["validateQuery"] = o.ValidateQuery
	}
	if o.Expand != nil {
		toSerialize["expand"] = o.Expand
	}
	if o.Properties != nil {
		toSerialize["properties"] = o.Properties
	}
	if o.FieldsByKeys != nil {
		toSerialize["fieldsByKeys"] = o.FieldsByKeys
	}
	return json.Marshal(toSerialize)
}

type NullableSearchRequestBean struct {
	value *SearchRequestBean
	isSet bool
}

func (v NullableSearchRequestBean) Get() *SearchRequestBean {
	return v.value
}

func (v *NullableSearchRequestBean) Set(val *SearchRequestBean) {
	v.value = val
	v.isSet = true
}

func (v NullableSearchRequestBean) IsSet() bool {
	return v.isSet
}

func (v *NullableSearchRequestBean) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSearchRequestBean(val *SearchRequestBean) *NullableSearchRequestBean {
	return &NullableSearchRequestBean{value: val, isSet: true}
}

func (v NullableSearchRequestBean) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSearchRequestBean) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
	"time"
)

// ServerInformation Details about the Jira instance.
type ServerInformation struct {
	// The base URL of the Jira instance.
	BaseUrl *string `json:"baseUrl,omitempty"`
	// The version of Jira.
	Version *string `json:"version,omitempty"`
	// The major, minor, and revision version numbers of the Jira version.
	VersionNumbers *[]int32 `json:"versionNumbers,omitempty"`
	// The type of server deployment. This is always returned as *Cloud*.
	DeploymentType *string `json:"deploymentType,omitempty"`
	// The build number of the Jira version.
	BuildNumber *int32 `json:"buildNumber,omitempty"`
	// The timestamp when the Jira version was built.
	BuildDate *time.Time `json:"buildDate,omitempty"`
	// The time in Jira when this request was responded to.
	ServerTime *time.Time `json:"serverTime,omitempty"`
	// The unique identifier of the Jira version.
	ScmInfo *string `json:"scmInfo,omitempty"`
	// The name of the Jira instance.
	ServerTitle *string `json:"serverTitle,omitempty"`
	// Jira instance health check results. Deprecated and no longer returned.
	HealthChecks *[]HealthCheckResult `json:"healthChecks,omitempty"`
}

// NewServerInformation instantiates a new ServerInformation object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewServerInformation() *ServerInformation {
	this := ServerInformation{}
	return &this
}

// NewServerInformationWithDefaults instantiates a new ServerInformation object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewServerInformationWithDefaults() *ServerInformation {
	this := ServerInformation{}
	return &this
}

// GetBaseUrl returns the BaseUrl field value if set, zero value otherwise.
func (o *ServerInformation) GetBaseUrl() string {
	if o == nil || o.BaseUrl == nil {
		var ret string
		return ret
	}
	return *o.BaseUrl
}

// GetBaseUrlOk returns a tuple with the BaseUrl field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetBaseUrlOk() (*string, bool) {
	if o == nil || o.BaseUrl == nil {
		return nil, false
	}
	return o.BaseUrl, true
}

// HasBaseUrl returns a boolean if a field has been set.
func (o *ServerInformation) HasBaseUrl() bool {
	if o != nil && o.BaseUrl != nil {
		return true
	}

	return false
}

// SetBaseUrl gets a reference to the given string and assigns it to the BaseUrl field.
func (o *ServerInformation) SetBaseUrl(v string) {
	o.BaseUrl = &v
}

// GetVersion returns the Version field value if set, zero value otherwise.
func (o *ServerInformation) GetVersion() string {
	if o == nil || o.Version == nil {
		var ret string
		return ret
	}
	return *o.Version
}

// GetVersionOk returns a tuple with the Version field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetVersionOk() (*string, bool) {
	if o == nil || o.Version == nil {
		return nil, false
	}
	return o.Version, true
}

// HasVersion returns a boolean if a field has been set.
func (o *ServerInformation) HasVersion() bool {
	if o != nil && o.Version != nil {
		return true
	}

	return false
}

// SetVersion gets a reference to the given string and assigns it to the Version field.
func (o *ServerInformation) SetVersion(v string) {
	o.Version = &v
}

// GetVersionNumbers returns the VersionNumbers field value if set, zero value otherwise.
func (o *ServerInformation) GetVersionNumbers() []int32 {
	if o == nil || o.VersionNumbers == nil {
		var ret []int32
		return ret
	}
	return *o.VersionNumbers
}

// GetVersionNumbersOk returns a tuple with the VersionNumbers field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetVersionNumbersOk() (*[]int32, bool) {
	if o == nil || o.VersionNumbers == nil {
		return nil, false
	}
	return o.VersionNumbers, true
}

// HasVersionNumbers returns a boolean if a field has been set.
func (o *ServerInformation) HasVersionNumbers() bool {
	if o != nil && o.VersionNumbers != nil {
		return true
	}

	return false
}

// SetVersionNumbers gets a reference to the given []int32 and assigns it to the VersionNumbers field.
func (o *ServerInformation) SetVersionNumbers(v []int32) {
	o.VersionNumbers = &v
}

// GetDeploymentType returns the DeploymentType field value if set, zero value otherwise.
func (o *ServerInformation) GetDeploymentType() string {
	if o == nil || o.DeploymentType == nil {
		var ret string
		return ret
	}
	return *o.DeploymentType
}

// GetDeploymentTypeOk returns a tuple with the DeploymentType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetDeploymentTypeOk() (*string, bool) {
	if o == nil || o.DeploymentType == nil {
		return nil, false
	}
	return o.DeploymentType, true
}

// HasDeploymentType returns a boolean if a field has been set.
func (o *ServerInformation) HasDeploymentType() bool {
	if o != nil && o.DeploymentType != nil {
		return true
	}

	return false
}

// SetDeploymentType gets a reference to the given string and assigns it to the DeploymentType field.
func (o *ServerInformation) SetDeploymentType(v string) {
	o.DeploymentType = &v
}

// GetBuildNumber returns the BuildNumber field value if set, zero value otherwise.
func (o *ServerInformation) GetBuildNumber() int32 {
	if o == nil || o.BuildNumber == nil {
		var ret int32
		return ret
	}
	return *o.BuildNumber
}

// GetBuildNumberOk returns a tuple with the BuildNumber field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetBuildNumberOk() (*int32, bool) {
	if o == nil || o.BuildNumber == nil {
		return nil, false
	}
	return o.BuildNumber, true
}

// HasBuildNumber returns a boolean if a field has been set.
func (o *ServerInformation) HasBuildNumber() bool {
	if o != nil && o.BuildNumber != nil {
		return true
	}

	return false
}

// SetBuildNumber gets a reference to the given int32 and assigns it to the BuildNumber field.
func (o *ServerInformation) SetBuildNumber(v int32) {
	o.BuildNumber = &v
}

// GetBuildDate returns the BuildDate field value if set, zero value otherwise.
func (o *ServerInformation) GetBuildDate() time.Time {
	if o == nil || o.BuildDate == nil {
		var ret time.Time
		return ret
	}
	return *o.BuildDate
}

// GetBuildDateOk returns a tuple with the BuildDate field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetBuildDateOk() (*time.Time, bool) {
	if o == nil || o.BuildDate == nil {
		return nil, false
	}
	return o.BuildDate, true
}

// HasBuildDate returns a boolean if a field has been set.
func (o *ServerInformation) HasBuildDate() bool {
	if o != nil && o.BuildDate != nil {
		return true
	}

	return false
}

// SetBuildDate gets a reference to the given time.Time and assigns it to the BuildDate field.
func (o *ServerInformation) SetBuildDate(v time.Time) {
	o.BuildDate = &v
}

// GetServerTime returns the ServerTime field value if set, zero value otherwise.
func (o *ServerInformation) GetServerTime() time.Time {
	if o == nil || o.ServerTime == nil {
		var ret time.Time
		return ret
	}
	return *o.ServerTime
}

// GetServerTimeOk returns a tuple with the ServerTime field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetServerTimeOk() (*time.Time, bool) {
	if o == nil || o.ServerTime == nil {
		return nil, false
	}
	return o.ServerTime, true
}

// HasServerTime returns a boolean if a field has been set.
func (o *ServerInformation) HasServerTime() bool {
	if o != nil && o.ServerTime != nil {
		return true
	}

	return false
}

// SetServerTime gets a reference to the given time.Time and assigns it to the ServerTime field.
func (o *ServerInformation) SetServerTime(v time.Time) {
	o.ServerTime = &v
}

// GetScmInfo returns the ScmInfo field value if set, zero value otherwise.
func (o *ServerInformation) GetScmInfo() string {
	if o == nil || o.ScmInfo == nil {
		var ret string
		return ret
	}
	return *o.ScmInfo
}

// GetScmInfoOk returns a tuple with the ScmInfo field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetScmInfoOk() (*string, bool) {
	if o == nil || o.ScmInfo == nil {
		return nil, false
	}
	return o.ScmInfo, true
}

// HasScmInfo returns a boolean if a field has been set.
func (o *ServerInformation) HasScmInfo() bool {
	if o != nil && o.ScmInfo != nil {
		return true
	}

	return false
}

// SetScmInfo gets a reference to the given string and assigns it to the ScmInfo field.
func (o *ServerInformation) SetScmInfo(v string) {
	o.ScmInfo = &v
}

// GetServerTitle returns the ServerTitle field value if set, zero value otherwise.
func (o *ServerInformation) GetServerTitle() string {
	if o == nil || o.ServerTitle == nil {
		var ret string
		return ret
	}
	return *o.ServerTitle
}

// GetServerTitleOk returns a tuple with the ServerTitle field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetServerTitleOk() (*string, bool) {
	if o == nil || o.ServerTitle == nil {
		return nil, false
	}
	return o.ServerTitle, true
}

// HasServerTitle returns a boolean if a field has been set.
func (o *ServerInformation) HasServerTitle() bool {
	if o != nil && o.ServerTitle != nil {
		return true
	}

	return false
}

// SetServerTitle gets a reference to the given string and assigns it to the ServerTitle field.
func (o *ServerInformation) SetServerTitle(v string) {
	o.ServerTitle = &v
}

// GetHealthChecks returns the HealthChecks field value if set, zero value otherwise.
func (o *ServerInformation) GetHealthChecks() []HealthCheckResult {
	if o == nil || o.HealthChecks == nil {
		var ret []HealthCheckResult
		return ret
	}
	return *o.HealthChecks
}

// GetHealthChecksOk returns a tuple with the HealthChecks field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServerInformation) GetHealthChecksOk() (*[]HealthCheckResult, bool) {
	if o == nil || o.HealthChecks == nil {
		return nil, false
	}
	return o.HealthChecks, true
}

// HasHealthChecks returns a boolean if a field has been set.
func (o *ServerInformation) HasHealthChecks() bool {
	if o != nil && o.HealthChecks != nil {
		return true
	}

	return false
}

// SetHealthChecks gets a reference to the given []HealthCheckResult and assigns it to the HealthChecks field.
func (o *ServerInformation) SetHealthChecks(v []HealthCheckResult) {
	o.HealthChecks = &v
}

func (o ServerInformation) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.BaseUrl != nil {
		toSerialize["baseUrl"] = o.BaseUrl
	}
	if o.Version != nil {
		toSerialize["version"] = o.Version
	}
	if o.VersionNumbers != nil {
		toSerialize["versionNumbers"] = o.VersionNumbers
	}
	if o.DeploymentType != nil {
		toSerialize["deploymentType"] = o.DeploymentType
	}
	if o.BuildNumber != nil {
		toSerialize["buildNumber"] = o.BuildNumber
	}
	if o.BuildDate != nil {
		toSerialize["buildDate"] = o.BuildDate
	}
	if o.ServerTime != nil {
		toSerialize["serverTime"] = o.ServerTime
	}
	if o.ScmInfo != nil {
		toSerialize["scmInfo"] = o.ScmInfo
	}
	if o.ServerTitle != nil {
		toSerialize["serverTitle"] = o.ServerTitle
	}
	if o.HealthChecks != nil {
		toSerialize["healthChecks"] = o.HealthChecks
	}
	return json.Marshal(toSerialize)
}

type NullableServerInformation struct {
	value *ServerInformation
	isSet bool
}

func (v NullableServerInformation) Get() *ServerInformation {
	return v.value
}

func (v *NullableServerInformation) Set(val *ServerInformation) {
	v.value = val
	v.isSet = true
}

func (v NullableServerInformation) IsSet() bool {
	return v.isSet
}

func (v *NullableServerInformation) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableServerInformation(val *ServerInformation) *NullableServerInformation {
	return &NullableServerInformation{value: val, isSet: true}
}

func (v NullableServerInformation) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableServerInformation) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



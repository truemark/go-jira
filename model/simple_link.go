/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// SimpleLink Details about the operations available in this version.
type SimpleLink struct {
	Id *string `json:"id,omitempty"`
	StyleClass *string `json:"styleClass,omitempty"`
	IconClass *string `json:"iconClass,omitempty"`
	Label *string `json:"label,omitempty"`
	Title *string `json:"title,omitempty"`
	Href *string `json:"href,omitempty"`
	Weight *int32 `json:"weight,omitempty"`
}

// NewSimpleLink instantiates a new SimpleLink object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSimpleLink() *SimpleLink {
	this := SimpleLink{}
	return &this
}

// NewSimpleLinkWithDefaults instantiates a new SimpleLink object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSimpleLinkWithDefaults() *SimpleLink {
	this := SimpleLink{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *SimpleLink) GetId() string {
	if o == nil || o.Id == nil {
		var ret string
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SimpleLink) GetIdOk() (*string, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *SimpleLink) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given string and assigns it to the Id field.
func (o *SimpleLink) SetId(v string) {
	o.Id = &v
}

// GetStyleClass returns the StyleClass field value if set, zero value otherwise.
func (o *SimpleLink) GetStyleClass() string {
	if o == nil || o.StyleClass == nil {
		var ret string
		return ret
	}
	return *o.StyleClass
}

// GetStyleClassOk returns a tuple with the StyleClass field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SimpleLink) GetStyleClassOk() (*string, bool) {
	if o == nil || o.StyleClass == nil {
		return nil, false
	}
	return o.StyleClass, true
}

// HasStyleClass returns a boolean if a field has been set.
func (o *SimpleLink) HasStyleClass() bool {
	if o != nil && o.StyleClass != nil {
		return true
	}

	return false
}

// SetStyleClass gets a reference to the given string and assigns it to the StyleClass field.
func (o *SimpleLink) SetStyleClass(v string) {
	o.StyleClass = &v
}

// GetIconClass returns the IconClass field value if set, zero value otherwise.
func (o *SimpleLink) GetIconClass() string {
	if o == nil || o.IconClass == nil {
		var ret string
		return ret
	}
	return *o.IconClass
}

// GetIconClassOk returns a tuple with the IconClass field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SimpleLink) GetIconClassOk() (*string, bool) {
	if o == nil || o.IconClass == nil {
		return nil, false
	}
	return o.IconClass, true
}

// HasIconClass returns a boolean if a field has been set.
func (o *SimpleLink) HasIconClass() bool {
	if o != nil && o.IconClass != nil {
		return true
	}

	return false
}

// SetIconClass gets a reference to the given string and assigns it to the IconClass field.
func (o *SimpleLink) SetIconClass(v string) {
	o.IconClass = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *SimpleLink) GetLabel() string {
	if o == nil || o.Label == nil {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SimpleLink) GetLabelOk() (*string, bool) {
	if o == nil || o.Label == nil {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *SimpleLink) HasLabel() bool {
	if o != nil && o.Label != nil {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *SimpleLink) SetLabel(v string) {
	o.Label = &v
}

// GetTitle returns the Title field value if set, zero value otherwise.
func (o *SimpleLink) GetTitle() string {
	if o == nil || o.Title == nil {
		var ret string
		return ret
	}
	return *o.Title
}

// GetTitleOk returns a tuple with the Title field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SimpleLink) GetTitleOk() (*string, bool) {
	if o == nil || o.Title == nil {
		return nil, false
	}
	return o.Title, true
}

// HasTitle returns a boolean if a field has been set.
func (o *SimpleLink) HasTitle() bool {
	if o != nil && o.Title != nil {
		return true
	}

	return false
}

// SetTitle gets a reference to the given string and assigns it to the Title field.
func (o *SimpleLink) SetTitle(v string) {
	o.Title = &v
}

// GetHref returns the Href field value if set, zero value otherwise.
func (o *SimpleLink) GetHref() string {
	if o == nil || o.Href == nil {
		var ret string
		return ret
	}
	return *o.Href
}

// GetHrefOk returns a tuple with the Href field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SimpleLink) GetHrefOk() (*string, bool) {
	if o == nil || o.Href == nil {
		return nil, false
	}
	return o.Href, true
}

// HasHref returns a boolean if a field has been set.
func (o *SimpleLink) HasHref() bool {
	if o != nil && o.Href != nil {
		return true
	}

	return false
}

// SetHref gets a reference to the given string and assigns it to the Href field.
func (o *SimpleLink) SetHref(v string) {
	o.Href = &v
}

// GetWeight returns the Weight field value if set, zero value otherwise.
func (o *SimpleLink) GetWeight() int32 {
	if o == nil || o.Weight == nil {
		var ret int32
		return ret
	}
	return *o.Weight
}

// GetWeightOk returns a tuple with the Weight field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SimpleLink) GetWeightOk() (*int32, bool) {
	if o == nil || o.Weight == nil {
		return nil, false
	}
	return o.Weight, true
}

// HasWeight returns a boolean if a field has been set.
func (o *SimpleLink) HasWeight() bool {
	if o != nil && o.Weight != nil {
		return true
	}

	return false
}

// SetWeight gets a reference to the given int32 and assigns it to the Weight field.
func (o *SimpleLink) SetWeight(v int32) {
	o.Weight = &v
}

func (o SimpleLink) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	if o.StyleClass != nil {
		toSerialize["styleClass"] = o.StyleClass
	}
	if o.IconClass != nil {
		toSerialize["iconClass"] = o.IconClass
	}
	if o.Label != nil {
		toSerialize["label"] = o.Label
	}
	if o.Title != nil {
		toSerialize["title"] = o.Title
	}
	if o.Href != nil {
		toSerialize["href"] = o.Href
	}
	if o.Weight != nil {
		toSerialize["weight"] = o.Weight
	}
	return json.Marshal(toSerialize)
}

type NullableSimpleLink struct {
	value *SimpleLink
	isSet bool
}

func (v NullableSimpleLink) Get() *SimpleLink {
	return v.value
}

func (v *NullableSimpleLink) Set(val *SimpleLink) {
	v.value = val
	v.isSet = true
}

func (v NullableSimpleLink) IsSet() bool {
	return v.isSet
}

func (v *NullableSimpleLink) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSimpleLink(val *SimpleLink) *NullableSimpleLink {
	return &NullableSimpleLink{value: val, isSet: true}
}

func (v NullableSimpleLink) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSimpleLink) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// StatusMapping Details about the mapping from a status to a new status for an issue type.
type StatusMapping struct {
	// The ID of the issue type.
	IssueTypeId string `json:"issueTypeId"`
	// The ID of the status.
	StatusId string `json:"statusId"`
	// The ID of the new status.
	NewStatusId string `json:"newStatusId"`
}

// NewStatusMapping instantiates a new StatusMapping object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewStatusMapping(issueTypeId string, statusId string, newStatusId string) *StatusMapping {
	this := StatusMapping{}
	this.IssueTypeId = issueTypeId
	this.StatusId = statusId
	this.NewStatusId = newStatusId
	return &this
}

// NewStatusMappingWithDefaults instantiates a new StatusMapping object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewStatusMappingWithDefaults() *StatusMapping {
	this := StatusMapping{}
	return &this
}

// GetIssueTypeId returns the IssueTypeId field value
func (o *StatusMapping) GetIssueTypeId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.IssueTypeId
}

// GetIssueTypeIdOk returns a tuple with the IssueTypeId field value
// and a boolean to check if the value has been set.
func (o *StatusMapping) GetIssueTypeIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.IssueTypeId, true
}

// SetIssueTypeId sets field value
func (o *StatusMapping) SetIssueTypeId(v string) {
	o.IssueTypeId = v
}

// GetStatusId returns the StatusId field value
func (o *StatusMapping) GetStatusId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.StatusId
}

// GetStatusIdOk returns a tuple with the StatusId field value
// and a boolean to check if the value has been set.
func (o *StatusMapping) GetStatusIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.StatusId, true
}

// SetStatusId sets field value
func (o *StatusMapping) SetStatusId(v string) {
	o.StatusId = v
}

// GetNewStatusId returns the NewStatusId field value
func (o *StatusMapping) GetNewStatusId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NewStatusId
}

// GetNewStatusIdOk returns a tuple with the NewStatusId field value
// and a boolean to check if the value has been set.
func (o *StatusMapping) GetNewStatusIdOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.NewStatusId, true
}

// SetNewStatusId sets field value
func (o *StatusMapping) SetNewStatusId(v string) {
	o.NewStatusId = v
}

func (o StatusMapping) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["issueTypeId"] = o.IssueTypeId
	}
	if true {
		toSerialize["statusId"] = o.StatusId
	}
	if true {
		toSerialize["newStatusId"] = o.NewStatusId
	}
	return json.Marshal(toSerialize)
}

type NullableStatusMapping struct {
	value *StatusMapping
	isSet bool
}

func (v NullableStatusMapping) Get() *StatusMapping {
	return v.value
}

func (v *NullableStatusMapping) Set(val *StatusMapping) {
	v.value = val
	v.isSet = true
}

func (v NullableStatusMapping) IsSet() bool {
	return v.isSet
}

func (v *NullableStatusMapping) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableStatusMapping(val *StatusMapping) *NullableStatusMapping {
	return &NullableStatusMapping{value: val, isSet: true}
}

func (v NullableStatusMapping) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableStatusMapping) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



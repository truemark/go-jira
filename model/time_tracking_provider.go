/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// TimeTrackingProvider Details about the time tracking provider.
type TimeTrackingProvider struct {
	// The key for the time tracking provider. For example, *JIRA*.
	Key string `json:"key"`
	// The name of the time tracking provider. For example, *JIRA provided time tracking*.
	Name *string `json:"name,omitempty"`
	// The URL of the configuration page for the time tracking provider app. For example, *_/example/config/url*. This property is only returned if the `adminPageKey` property is set in the module descriptor of the time tracking provider app.
	Url *string `json:"url,omitempty"`
}

// NewTimeTrackingProvider instantiates a new TimeTrackingProvider object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewTimeTrackingProvider(key string) *TimeTrackingProvider {
	this := TimeTrackingProvider{}
	this.Key = key
	return &this
}

// NewTimeTrackingProviderWithDefaults instantiates a new TimeTrackingProvider object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewTimeTrackingProviderWithDefaults() *TimeTrackingProvider {
	this := TimeTrackingProvider{}
	return &this
}

// GetKey returns the Key field value
func (o *TimeTrackingProvider) GetKey() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Key
}

// GetKeyOk returns a tuple with the Key field value
// and a boolean to check if the value has been set.
func (o *TimeTrackingProvider) GetKeyOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Key, true
}

// SetKey sets field value
func (o *TimeTrackingProvider) SetKey(v string) {
	o.Key = v
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *TimeTrackingProvider) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TimeTrackingProvider) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *TimeTrackingProvider) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *TimeTrackingProvider) SetName(v string) {
	o.Name = &v
}

// GetUrl returns the Url field value if set, zero value otherwise.
func (o *TimeTrackingProvider) GetUrl() string {
	if o == nil || o.Url == nil {
		var ret string
		return ret
	}
	return *o.Url
}

// GetUrlOk returns a tuple with the Url field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TimeTrackingProvider) GetUrlOk() (*string, bool) {
	if o == nil || o.Url == nil {
		return nil, false
	}
	return o.Url, true
}

// HasUrl returns a boolean if a field has been set.
func (o *TimeTrackingProvider) HasUrl() bool {
	if o != nil && o.Url != nil {
		return true
	}

	return false
}

// SetUrl gets a reference to the given string and assigns it to the Url field.
func (o *TimeTrackingProvider) SetUrl(v string) {
	o.Url = &v
}

func (o TimeTrackingProvider) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["key"] = o.Key
	}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.Url != nil {
		toSerialize["url"] = o.Url
	}
	return json.Marshal(toSerialize)
}

type NullableTimeTrackingProvider struct {
	value *TimeTrackingProvider
	isSet bool
}

func (v NullableTimeTrackingProvider) Get() *TimeTrackingProvider {
	return v.value
}

func (v *NullableTimeTrackingProvider) Set(val *TimeTrackingProvider) {
	v.value = val
	v.isSet = true
}

func (v NullableTimeTrackingProvider) IsSet() bool {
	return v.isSet
}

func (v *NullableTimeTrackingProvider) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableTimeTrackingProvider(val *TimeTrackingProvider) *NullableTimeTrackingProvider {
	return &NullableTimeTrackingProvider{value: val, isSet: true}
}

func (v NullableTimeTrackingProvider) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableTimeTrackingProvider) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



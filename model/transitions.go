/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// Transitions List of issue transitions.
type Transitions struct {
	// Expand options that include additional transitions details in the response.
	Expand *string `json:"expand,omitempty"`
	// List of issue transitions.
	Transitions *[]IssueTransition `json:"transitions,omitempty"`
}

// NewTransitions instantiates a new Transitions object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewTransitions() *Transitions {
	this := Transitions{}
	return &this
}

// NewTransitionsWithDefaults instantiates a new Transitions object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewTransitionsWithDefaults() *Transitions {
	this := Transitions{}
	return &this
}

// GetExpand returns the Expand field value if set, zero value otherwise.
func (o *Transitions) GetExpand() string {
	if o == nil || o.Expand == nil {
		var ret string
		return ret
	}
	return *o.Expand
}

// GetExpandOk returns a tuple with the Expand field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Transitions) GetExpandOk() (*string, bool) {
	if o == nil || o.Expand == nil {
		return nil, false
	}
	return o.Expand, true
}

// HasExpand returns a boolean if a field has been set.
func (o *Transitions) HasExpand() bool {
	if o != nil && o.Expand != nil {
		return true
	}

	return false
}

// SetExpand gets a reference to the given string and assigns it to the Expand field.
func (o *Transitions) SetExpand(v string) {
	o.Expand = &v
}

// GetTransitions returns the Transitions field value if set, zero value otherwise.
func (o *Transitions) GetTransitions() []IssueTransition {
	if o == nil || o.Transitions == nil {
		var ret []IssueTransition
		return ret
	}
	return *o.Transitions
}

// GetTransitionsOk returns a tuple with the Transitions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Transitions) GetTransitionsOk() (*[]IssueTransition, bool) {
	if o == nil || o.Transitions == nil {
		return nil, false
	}
	return o.Transitions, true
}

// HasTransitions returns a boolean if a field has been set.
func (o *Transitions) HasTransitions() bool {
	if o != nil && o.Transitions != nil {
		return true
	}

	return false
}

// SetTransitions gets a reference to the given []IssueTransition and assigns it to the Transitions field.
func (o *Transitions) SetTransitions(v []IssueTransition) {
	o.Transitions = &v
}

func (o Transitions) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Expand != nil {
		toSerialize["expand"] = o.Expand
	}
	if o.Transitions != nil {
		toSerialize["transitions"] = o.Transitions
	}
	return json.Marshal(toSerialize)
}

type NullableTransitions struct {
	value *Transitions
	isSet bool
}

func (v NullableTransitions) Get() *Transitions {
	return v.value
}

func (v *NullableTransitions) Set(val *Transitions) {
	v.value = val
	v.isSet = true
}

func (v NullableTransitions) IsSet() bool {
	return v.isSet
}

func (v *NullableTransitions) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableTransitions(val *Transitions) *NullableTransitions {
	return &NullableTransitions{value: val, isSet: true}
}

func (v NullableTransitions) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableTransitions) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



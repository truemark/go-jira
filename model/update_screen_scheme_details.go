/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// UpdateScreenSchemeDetails Details of a screen scheme.
type UpdateScreenSchemeDetails struct {
	// The name of the screen scheme. The name must be unique. The maximum length is 255 characters.
	Name *string `json:"name,omitempty"`
	// The description of the screen scheme. The maximum length is 255 characters.
	Description *string `json:"description,omitempty"`
	// The IDs of the screens for the screen types of the screen scheme. Only screens used in classic projects are accepted.
	Screens *UpdateScreenTypes `json:"screens,omitempty"`
}

// NewUpdateScreenSchemeDetails instantiates a new UpdateScreenSchemeDetails object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUpdateScreenSchemeDetails() *UpdateScreenSchemeDetails {
	this := UpdateScreenSchemeDetails{}
	return &this
}

// NewUpdateScreenSchemeDetailsWithDefaults instantiates a new UpdateScreenSchemeDetails object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUpdateScreenSchemeDetailsWithDefaults() *UpdateScreenSchemeDetails {
	this := UpdateScreenSchemeDetails{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *UpdateScreenSchemeDetails) GetName() string {
	if o == nil || o.Name == nil {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateScreenSchemeDetails) GetNameOk() (*string, bool) {
	if o == nil || o.Name == nil {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *UpdateScreenSchemeDetails) HasName() bool {
	if o != nil && o.Name != nil {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *UpdateScreenSchemeDetails) SetName(v string) {
	o.Name = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *UpdateScreenSchemeDetails) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateScreenSchemeDetails) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *UpdateScreenSchemeDetails) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *UpdateScreenSchemeDetails) SetDescription(v string) {
	o.Description = &v
}

// GetScreens returns the Screens field value if set, zero value otherwise.
func (o *UpdateScreenSchemeDetails) GetScreens() UpdateScreenTypes {
	if o == nil || o.Screens == nil {
		var ret UpdateScreenTypes
		return ret
	}
	return *o.Screens
}

// GetScreensOk returns a tuple with the Screens field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateScreenSchemeDetails) GetScreensOk() (*UpdateScreenTypes, bool) {
	if o == nil || o.Screens == nil {
		return nil, false
	}
	return o.Screens, true
}

// HasScreens returns a boolean if a field has been set.
func (o *UpdateScreenSchemeDetails) HasScreens() bool {
	if o != nil && o.Screens != nil {
		return true
	}

	return false
}

// SetScreens gets a reference to the given UpdateScreenTypes and assigns it to the Screens field.
func (o *UpdateScreenSchemeDetails) SetScreens(v UpdateScreenTypes) {
	o.Screens = &v
}

func (o UpdateScreenSchemeDetails) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Name != nil {
		toSerialize["name"] = o.Name
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.Screens != nil {
		toSerialize["screens"] = o.Screens
	}
	return json.Marshal(toSerialize)
}

type NullableUpdateScreenSchemeDetails struct {
	value *UpdateScreenSchemeDetails
	isSet bool
}

func (v NullableUpdateScreenSchemeDetails) Get() *UpdateScreenSchemeDetails {
	return v.value
}

func (v *NullableUpdateScreenSchemeDetails) Set(val *UpdateScreenSchemeDetails) {
	v.value = val
	v.isSet = true
}

func (v NullableUpdateScreenSchemeDetails) IsSet() bool {
	return v.isSet
}

func (v *NullableUpdateScreenSchemeDetails) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUpdateScreenSchemeDetails(val *UpdateScreenSchemeDetails) *NullableUpdateScreenSchemeDetails {
	return &NullableUpdateScreenSchemeDetails{value: val, isSet: true}
}

func (v NullableUpdateScreenSchemeDetails) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUpdateScreenSchemeDetails) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}



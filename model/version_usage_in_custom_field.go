/*
 * The Jira Cloud platform REST API
 *
 * Jira Cloud platform REST API documentation
 *
 * API version: 1001.0.0-SNAPSHOT
 * Contact: ecosystem@atlassian.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package go_jira

import (
	"encoding/json"
)

// VersionUsageInCustomField List of custom fields using the version.
type VersionUsageInCustomField struct {
	// The name of the custom field.
	FieldName *string `json:"fieldName,omitempty"`
	// The ID of the custom field.
	CustomFieldId *int64 `json:"customFieldId,omitempty"`
	// Count of the issues where the custom field contains the version.
	IssueCountWithVersionInCustomField *int64 `json:"issueCountWithVersionInCustomField,omitempty"`
}

// NewVersionUsageInCustomField instantiates a new VersionUsageInCustomField object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewVersionUsageInCustomField() *VersionUsageInCustomField {
	this := VersionUsageInCustomField{}
	return &this
}

// NewVersionUsageInCustomFieldWithDefaults instantiates a new VersionUsageInCustomField object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewVersionUsageInCustomFieldWithDefaults() *VersionUsageInCustomField {
	this := VersionUsageInCustomField{}
	return &this
}

// GetFieldName returns the FieldName field value if set, zero value otherwise.
func (o *VersionUsageInCustomField) GetFieldName() string {
	if o == nil || o.FieldName == nil {
		var ret string
		return ret
	}
	return *o.FieldName
}

// GetFieldNameOk returns a tuple with the FieldName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VersionUsageInCustomField) GetFieldNameOk() (*string, bool) {
	if o == nil || o.FieldName == nil {
		return nil, false
	}
	return o.FieldName, true
}

// HasFieldName returns a boolean if a field has been set.
func (o *VersionUsageInCustomField) HasFieldName() bool {
	if o != nil && o.FieldName != nil {
		return true
	}

	return false
}

// SetFieldName gets a reference to the given string and assigns it to the FieldName field.
func (o *VersionUsageInCustomField) SetFieldName(v string) {
	o.FieldName = &v
}

// GetCustomFieldId returns the CustomFieldId field value if set, zero value otherwise.
func (o *VersionUsageInCustomField) GetCustomFieldId() int64 {
	if o == nil || o.CustomFieldId == nil {
		var ret int64
		return ret
	}
	return *o.CustomFieldId
}

// GetCustomFieldIdOk returns a tuple with the CustomFieldId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VersionUsageInCustomField) GetCustomFieldIdOk() (*int64, bool) {
	if o == nil || o.CustomFieldId == nil {
		return nil, false
	}
	return o.CustomFieldId, true
}

// HasCustomFieldId returns a boolean if a field has been set.
func (o *VersionUsageInCustomField) HasCustomFieldId() bool {
	if o != nil && o.CustomFieldId != nil {
		return true
	}

	return false
}

// SetCustomFieldId gets a reference to the given int64 and assigns it to the CustomFieldId field.
func (o *VersionUsageInCustomField) SetCustomFieldId(v int64) {
	o.CustomFieldId = &v
}

// GetIssueCountWithVersionInCustomField returns the IssueCountWithVersionInCustomField field value if set, zero value otherwise.
func (o *VersionUsageInCustomField) GetIssueCountWithVersionInCustomField() int64 {
	if o == nil || o.IssueCountWithVersionInCustomField == nil {
		var ret int64
		return ret
	}
	return *o.IssueCountWithVersionInCustomField
}

// GetIssueCountWithVersionInCustomFieldOk returns a tuple with the IssueCountWithVersionInCustomField field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *VersionUsageInCustomField) GetIssueCountWithVersionInCustomFieldOk() (*int64, bool) {
	if o == nil || o.IssueCountWithVersionInCustomField == nil {
		return nil, false
	}
	return o.IssueCountWithVersionInCustomField, true
}

// HasIssueCountWithVersionInCustomField returns a boolean if a field has been set.
func (o *VersionUsageInCustomField) HasIssueCountWithVersionInCustomField() bool {
	if o != nil && o.IssueCountWithVersionInCustomField != nil {
		return true
	}

	return false
}

// SetIssueCountWithVersionInCustomField gets a reference to the given int64 and assigns it to the IssueCountWithVersionInCustomField field.
func (o *VersionUsageInCustomField) SetIssueCountWithVersionInCustomField(v int64) {
	o.IssueCountWithVersionInCustomField = &v
}

func (o VersionUsageInCustomField) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.FieldName != nil {
		toSerialize["fieldName"] = o.FieldName
	}
	if o.CustomFieldId != nil {
		toSerialize["customFieldId"] = o.CustomFieldId
	}
	if o.IssueCountWithVersionInCustomField != nil {
		toSerialize["issueCountWithVersionInCustomField"] = o.IssueCountWithVersionInCustomField
	}
	return json.Marshal(toSerialize)
}

type NullableVersionUsageInCustomField struct {
	value *VersionUsageInCustomField
	isSet bool
}

func (v NullableVersionUsageInCustomField) Get() *VersionUsageInCustomField {
	return v.value
}

func (v *NullableVersionUsageInCustomField) Set(val *VersionUsageInCustomField) {
	v.value = val
	v.isSet = true
}

func (v NullableVersionUsageInCustomField) IsSet() bool {
	return v.isSet
}

func (v *NullableVersionUsageInCustomField) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableVersionUsageInCustomField(val *VersionUsageInCustomField) *NullableVersionUsageInCustomField {
	return &NullableVersionUsageInCustomField{value: val, isSet: true}
}

func (v NullableVersionUsageInCustomField) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableVersionUsageInCustomField) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


